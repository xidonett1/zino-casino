<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPersonalData extends Model
{
    protected $table = 'users_personal_data';

    use HasFactory;
}
