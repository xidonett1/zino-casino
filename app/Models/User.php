<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'email',
        'password',
        'role',
        'login',
        'avatar',
        'registration_password'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id');
    }

    public function betsDuring24Hours()
    {
        return $this->hasMany(Bet::class, 'user_id')->whereDate('created_at', Carbon::today());
    }

    public function realBets()
    {
        return $this->hasMany(Bet::class, 'user_id')->where('is_real', 1);
    }

    public function isPlayer()
    {
        return $this->where('role', 1);
    }

    public function manager()
    {
        return $this->hasOne(ManagerClients::class, 'user_id', 'id');
    }

    public function unfortunateWithdrawals()
    {
        return $this->hasMany(UnfortunateWithdrawal::class, 'user_id');
    }
}
