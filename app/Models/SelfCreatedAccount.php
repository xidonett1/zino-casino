<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SelfCreatedAccount extends Model
{
    use HasFactory;

    protected $table = 'self_created_account';

    protected $fillable = [
      'user_id'
    ];
}
