<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Error;

class Tournament extends Model
{
    use HasFactory;

    private $games = [];

    protected $fillable = [
        'title',
        'games',
        'prizes',
        'description',
        'thumbnail',
        'slug',
        'is_available',
        'from',
        'until'
    ];

    public function getTournamentGames(string $tournament_games) : array
    {
        $tournament_games_arr = json_decode($tournament_games);

        if($tournament_games_arr) {
            foreach ($tournament_games_arr as $game) {
                $game_slug = str_replace(']', '', explode('[', $game)[1]);
                $game = Game::whereSlug($game_slug)->first();

                if ($game) {
                    $this->games[] = $game;
                }
            }
            return $this->games;
        }

        throw new \Exception('No games in tournament array');
    }

}
