<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Ref extends Model
{
    use HasFactory;

    private const REF_CODE_COOKIE_MINUTES = 1800;

    protected $fillable = [
      'user_id',
      'ref'
    ];

    public function setRefCookie(Request $request, $news, $lang = 'en')
    {
        if ( $request->get('ref') && !$request->cookie('ref') ) {
            $minutes = $this::REF_CODE_COOKIE_MINUTES;
            Cookie::queue('ref', $request->get('ref'), $minutes);

            return redirect()->route('index', compact('lang', 'news'));
        }
    }
}
