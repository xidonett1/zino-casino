<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
      'is_answered',
      'for',
      'user_id',
      'is_admin',
      'msg'
    ];

    public function sender()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
