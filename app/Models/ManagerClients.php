<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManagerClients extends Model
{
    use HasFactory;

    public $fillable = [
      'user_id',
      'manager_id'
    ];
}
