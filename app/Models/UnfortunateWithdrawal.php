<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnfortunateWithdrawal extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'time_from_registration'
    ];
}
