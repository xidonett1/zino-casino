<?php

namespace App\Http\Controllers;

use App\Models\Tournament;
use App\Models\TournamentParticipant;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\AdminController;

class TournamentController extends Controller
{
    public function deleteThumbnail($thumbnail)
    {
        unlink($_SERVER['DOCUMENT_ROOT'] . '/img/tournaments/' . $thumbnail);
    }

    public static function uploadThumbnail(Request $request)
    {
         $image = $request->file('thumbnail');
         $imageName = time().'.webp';

         $filePath = public_path('/img/tournaments');

         $img = Image::make($image->path())->encode('webp', 90);
         $img->save($filePath.'/'.$imageName);

         return $imageName;
    }

    public static function createTournament(Request $request)
    {
        $prizes = [
            'ir' => $request->post('ir_prizes'),
            'en' => $request->post('en_prizes'),
        ];
        $description = [
            'ir' => $request->post('ir_description'),
            'en' => $request->post('en_description'),
        ];
        $title = [
            'ir' => $request->post('ir_title'),
            'en' => $request->post('en_title'),
        ];
        $slug = $request->post('slug');
        $games = $request->post('games');
        $is_available = $request->post('is_available');

        if ($request->post('mode') === 'edit') {
            $tournament = Tournament::findOrFail($request->post('id'));
            if($request->file('thumbnail')) {
                self::deleteThumbnail($tournament->thumbnail);
                $thumbnail = self::uploadThumbnail($request);
            } else {
                $thumbnail = $tournament->thumbnail;
            }

            $tournament->update([
                'title' => json_encode($title),
                'prizes' => json_encode($prizes),
                'description' => json_encode($description),
                'thumbnail' => $thumbnail,
                'slug' => $slug,
                'games' => $games,
                'is_available' => $is_available
            ]);
            return redirect()->route('admin.tournaments')->with('status', __('Tournament updated.'));

        } elseif($request->post('mode') === 'create') {
            $thumbnail = self::uploadThumbnail($request);
        }

        Tournament::create([
            'title' => json_encode($title),
            'prizes' => json_encode($prizes),
            'description' => json_encode($description),
            'thumbnail' => $thumbnail,
            'slug' => $request->post('slug'),
            'games' => $request->post('games'),
            'is_available' => $request->post('is_available')
        ]);

        return redirect()->route('admin.tournaments')->with('status', __('Tournament saved.'));
    }

    public static function deleteTournament($id)
    {
        $tournament = Tournament::findOrFail($id);

        if($tournament->thumbnail) {
            self::deleteThumbnail($tournament->thumbnail);
        }

        $tournament->delete();

        return redirect()->back()->with('status', __('Tournament successfully deleted.'));
    }

    public function getActiveTournaments(array $participating_tournaments)
    {
        return Tournament::whereNotIn('id', array_values($participating_tournaments))
            ->where(function ($query){
                $query->where('is_available', '=', '1');
            })
            ->latest()
            ->get();
    }

    public function getParticipatingTournaments(array $participating_tournaments_ids)
    {
        return Tournament::whereIn('id', array_values($participating_tournaments_ids))
            ->where(function($query){
                $query->where('is_available', '1');
            })
            ->latest()
            ->get();
    }
}
