<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\TicketCategory;
use App\Models\TicketTemplate;
use App\Models\User as User;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function getTicketsPage(Request $request, $slug = 'new', $id = 0)
    {
        $managers = User::where('role', 2)->get();
        $new_messages_count = Message::whereIsAdminAndIsAnswered(0, 0)->count('*');
        $user = 0;

        $manager_id = $selected_manager = $request->get('manager');

        if($selected_manager) {
            $selected_manager = User::find($selected_manager)->login;
        }

        switch ($slug) {
            case 'new':
                if($selected_manager) {
                    $messages = Message::with('sender')->whereIsAdminAndIsAnsweredAndFor(0, 0, $selected_manager)->latest('created_at')->paginate(1);
                } else {
                    $messages = Message::with('sender')->whereIsAdminAndIsAnswered(0, 0)->latest('created_at')->paginate(1);
                }
                break;
            case 'user':
                $user = User::whereId($id)->first(); $messages = Message::with('sender')->whereUserId($id)->latest('created_at')->paginate(1);
                break;
            case 'income':
                if($selected_manager) {
                    $messages = Message::with('sender')->whereIsAdminAndFor(0, $selected_manager)->latest('created_at')->paginate(5);
                } else {
                    $messages = Message::with('sender')->whereIsAdmin(0)->latest('created_at')->paginate(5);
                }
                break;
            case 'sent':
                if($selected_manager) {
                    $messages = Message::with('sender')->whereIsAdminAndUserId(1, $manager_id)->latest('created_at')->paginate(5);
                } else {
                    $messages = Message::with('sender')->whereIsAdmin(1)->latest('created_at')->paginate(5);
                }
                break;
            case 'all':
                if($selected_manager){
                    $messages = Message::with('sender')->whereUserId($manager_id)->latest('created_at')->paginate(5);
                } else {
                    $messages = Message::with('sender')->latest('created_at')->paginate(5);
                }
                break;
        }
        return view('admin.pages.support.tickets',
            [
                'managers' => $managers,
                'messages' => $messages,
                'new_messages_count' => $new_messages_count,
                'user' => $user,
                'slug' => $slug
            ]);
    }

    public static function checkTicketType( $type)
    {
        switch ($type) {
            case 'category': return 'App\\Models\\TicketCategory'; break;
            case 'template': return 'App\\Models\\TicketTemplate'; break;
            default: return back()->with('status', __('Undefined ticket type.')); break;
        }
    }

    public function delete($id, $type)
    {
        $model = self::checkTicketType($type);
        $model::find($id)->delete($id);

        return back()->with('status', __('Element deleted.'));
    }

    public function update(Request $request)
    {
        $model = self::checkTicketType($request->post('type'));
        $element = $model::find($request->post('id'));

        foreach ($request->except('_token', 'type', 'id') as $k => $v) {
            $element->$k = $v;
        }

        $element->save();

        return back()->with('status', __('Element changed.'));
    }
}
