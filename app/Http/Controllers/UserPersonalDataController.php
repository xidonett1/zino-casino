<?php

namespace App\Http\Controllers;

use App\Models\UserPersonalData;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class UserPersonalDataController extends Controller
{
    public function updateData(Request $request)
    {
        $data = $request->except('_token', 'user_id');
        $user_personal_data = UserPersonalData::all()->where('user_id', $request->user_id)->first();

        if ($user_personal_data) {
            foreach ($data as $k => $v) {
                $user_personal_data->$k = $v;
            }
            $user_personal_data->save();
            return redirect()->route('user.settings', app()->getLocale())->with('status', __('Personal information successfully updated.'));
        }

        $user_personal_data = new UserPersonalData();
        $user_personal_data->user_id = $request->user_id;
        foreach($data as $k => $v){
            $user_personal_data->$k = $v;
        }

        $user_personal_data->save();

        return redirect()->route('user.settings', app()->getLocale())->with('status', __('Personal information successfully updated.'));
    }

    public static function deleteData($user_id)
    {
        $personal_data = UserPersonalData::where('user_id', $user_id);

        if ($personal_data) {
            $personal_data->delete();
        }
    }
}
