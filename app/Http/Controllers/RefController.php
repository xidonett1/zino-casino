<?php

namespace App\Http\Controllers;

use App\Models\Ref;
use Illuminate\Http\Request;

class RefController extends Controller
{
    public function generateRef(Request $request)
    {
        $data = $request->except('_token');
        Ref::create($data);
        return back();
    }
}
