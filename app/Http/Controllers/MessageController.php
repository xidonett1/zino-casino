<?php

namespace App\Http\Controllers;

use App\Helpers\Uploads;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use phpDocumentor\Reflection\Types\Self_;

class MessageController extends Controller
{
    public function sendMessage(Request $request)
    {
        DB::beginTransaction();

        $data = $request->except(['_token', 'file', 'template_category', 'template', 'message_id', 'is_answered']);

        if ($request->post('message_id')) {
            $sender = Message::whereId($request->post('message_id'))->first();
            $sender->is_answered = 1;
            $sender->save();
        }

        if ($request->file('file')) {
            $request->validate([
                'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $image = $request->file('file');

            $filePath = '/img/tickets';
            $imageName = Uploads::uploadImage($filePath, $image);
            $data['file'] = $imageName;
        }

        $message = new Message();
        foreach ($data as $k => $v) {
            $message->$k = $v;
        }

        $message->save();

        DB::commit();
        return back()->with('status', __('Ваше сообщение успешно отправлено.'));
    }

    public static function deleteUserMessages($user_id)
    {
        Message::whereUserId($user_id)->delete();

        return 0;
    }

    public static function deleteAddressedMessages($user_login)
    {
        Message::whereFor($user_login)->delete();

        return 0;
    }

    public static function deleteUserAttachments($user_id)
    {
        $messages = Message::whereUserId($user_id)->get();
        $files = array();

        foreach ($messages as $message) {
            array_push($files, $message->file);
        }

        if (count($files) != 0) {
           foreach($files as $file) {
               unlink($_SERVER['DOCUMENT_ROOT'].'/img/tickets/'.$file);
           }
        }

        return 0;
    }

    public static function deleteAddressedAttachments($user_login)
    {
        $messages = Message::whereFor($user_login)->get();
        $files = array();

        foreach ($messages as $message) {
            array_push($files, $message->file);
        }

        if (count($files) != 0) {
            foreach($files as $file) {
                unlink($_SERVER['DOCUMENT_ROOT'].'/img/tickets/'.$file);
            }
        }

        return 0;
    }

    public static function deleteUserData($user_id, $user_login)
    {
        self::deleteUserAttachments($user_id);
        self::deleteAddressedAttachments($user_login);
        self::deleteAddressedMessages($user_login);
        self::deleteUserMessages($user_id);

        return 0;
    }

    public static function deleteMessage($id)
    {
        Message::whereId($id)->delete();
        return back()->with('status', __('Сообщение успешно удалено.'));
    }

    public static function deleteAllMessages($user_id, $user_login)
    {
        Message::whereUserId($user_id)->delete();
        Message::whereFor($user_login)->delete();

        return back()->with('status', __('Сообщения успешно удалены.'));
    }
}
