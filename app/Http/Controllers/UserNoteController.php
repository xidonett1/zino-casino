<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserNote;
use Illuminate\Http\Request;

class UserNoteController extends Controller
{
    public function updateNote(Request $request)
    {
        $note = UserNote::where('user_id', $request->post('user_id'))->first();

        if ($note) {
           $note->note = $request->post('note');
           $note->save();
        } else {
            UserNote::create([
               'user_id' => $request->post('user_id'),
               'note' => $request->post('note')
            ]);
        }

        return back()->with('status', __('Сохранено.'));
    }
}
