<?php

namespace App\Http\Controllers;

use App\Models\TicketCategory;
use Illuminate\Http\Request;

class TicketCategoryController extends Controller
{
    public function getPage(Request $request)
    {
        return view('admin.pages.support.ticket-categories', ['categories' => TicketCategory::orderBy('id', 'desc')->paginate(10)]);
    }

    public function create(Request $request)
    {
        $name = $request->post('name');

        TicketCategory::create([
            'name' => $name
        ]);

        return back()->with('status', __('Ticket category <b>').$name.__('</b> successfully created.'));
    }
}
