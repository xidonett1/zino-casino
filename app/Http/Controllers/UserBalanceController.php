<?php

namespace App\Http\Controllers;

use App\Models\User as User;
use App\Models\UserBalance as UserBalance;
use Illuminate\Http\Request;

class UserBalanceController extends Controller
{
    public static function deleteBalance($user_id)
    {
        $user_balance = UserBalance::whereUserId($user_id);
        if($user_balance) {
            $user_balance->delete();
        }
    }

    public function updateUserBalance(Request $request, $id)
    {
        $balance = UserBalance::whereUserId($id)->update($request->all());
		
        return response()->json($balance);
    }

    public function getUserBalance(Request $request, $id)
    {
        $user_balance = UserBalance::whereUserId($id)->first();
        
		return \response()->json($user_balance);
    }
}
