<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function getGamePage(Request $request, $lang = 'ru', $slug = 'european_roulette', $is_real = 'real')
    {
        $game = Game::whereSlug($slug)->get()->first();

        return view('user.game', ['lang' => $lang, 'slug' => $slug, 'is_real' => $is_real, 'game' => $game]);
    }
}
