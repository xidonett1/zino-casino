<?php

namespace App\Http\Controllers;

use App\Helpers\Uploads;
use App\Models\Bet;
use App\Models\Game;
use App\Models\ManagerClients;
use App\Models\Message;
use App\Models\Page;
use App\Models\Settings;
use App\Models\Tournament;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{

    const FAST_WITHDRAW_MESSAGE = "Dear player! <br><br>Casino thanks you for deposit. <br><br>With best wishes, <br>Your casino";

    public static $pm = [
        'Payee_Account' => 'U31487946',
    ];

    public static function checkIfRoleAllowed($allowed_roles = array(2, 3))
    {
        $user_role = auth()->user()->role;
        abort_if(!in_array($user_role, $allowed_roles),403);
    }

    public function getWithdrawalPage(Request $request)
    {
        self::checkIfRoleAllowed();



        $min_withdrawal_amount = Settings::where('key', 'min_withdrawal_amount')->first();
        $managers = User::where('role', 2)->get();

        if ($request->get('search')) {
            return view('admin.pages.withdrawal',
                [
                    'users' => User::where('login', 'like', '%'.$request->get('search').'%')->get(),
                    'min_withdrawal_amount' => $min_withdrawal_amount,
                    'managers' => $managers
                ]);
        } else if ($request->get('manager')) {
            if ($request->get('manager') == 'all') {
               return redirect()->route('admin.withdrawal');
            }
            $manager_clients = ManagerClients::where('manager_id', $request->get('manager'))->get();
            $clients = array();
            foreach ($manager_clients as $client) {
                array_push($clients, $client->user_id);
            }
            return view('admin.pages.withdrawal',
            [
                'users' => User::whereIn('id', $clients)->get(),
                'min_withdrawal_amount' => $min_withdrawal_amount,
                'managers' => $managers
            ]);
        }

        $params = $this->getViewForRole($request,
            function($manager_clients) use ($min_withdrawal_amount, $managers){
                return [
                    'users' => User::orderBy('id', 'asc')->whereIn('id', $manager_clients)->get(),
                    'min_withdrawal_amount' => $min_withdrawal_amount,
                    'managers' => $managers
                ];
            },
            function() use($request, $min_withdrawal_amount, $managers){
                return [
                    'users' => User::orderBy('id', 'asc')->get(),
                    'min_withdrawal_amount' => $min_withdrawal_amount,
                    'managers' => $managers
                ];
            }
        );

        return view('admin.pages.withdrawal', $params);
    }

    public function sendThankYouMessage(Request $request)
    {
        $for = User::whereId($request->post('user_id'))->first()->login;
        Message::create([
            'user_id' => auth()->id(),
            'is_admin' => '1',
            'msg' => self::FAST_WITHDRAW_MESSAGE,
            'for' => $for
        ]);
    }

    public function makeWithdraw(Request $request)
    {
        $data = $request->except('_token', 'checkbox-message');

        $transaction = new Transaction();
        foreach ($data as $k => $v) {
            $transaction->$k = $v;
        }
        $transaction->status = 1;
        $transaction->save();


        if ($request->post('checkbox-message')) {
            $this->sendThankYouMessage($request);
        }
        return back()->with('status', __('Transaction created.'));
    }

    public function paymentPerfectMoney(Request $request, $data, $type = 'deposit')
    {
        $transaction_status = 0;
        if($request->post('PAYMENT_BATCH_NUM') != 0) {
            $transaction_status = 2;
        }

        $transaction_credentials = isset($data['PAYER_ACCOUNT']) ? $data['PAYER_ACCOUNT'] : 'Unfortunate transaction.';

        $transaction = new Transaction();
        $transaction->amount = $data['PAYMENT_AMOUNT'];
        $transaction->credentials = $transaction_credentials;
        $transaction->payment_system = 'PerfectMoney';
        $transaction->user_id = $data['SUGGESTED_MEMO'];
        $transaction->status = $transaction_status;
        $transaction->type = $type;

        $transaction->save();

        return $transaction_status;
    }

    public function makeDeposit(Request $request)
    {

        $data = $request->except('_token', 'checkbox-message', 'transaction', 'is_user_request');

        if ($request->post('transaction')) {
            $transaction = new Transaction();
            foreach ($data as $k => $v) {
                $transaction->$k = $v;
            }
            $transaction->status = 2;

            $transaction->save();
        }

        $transaction_status = 2;
        $request->post('PAYMENT_AMOUNT') ? $transaction_status = $this->paymentPerfectMoney($request, $data) : $transaction_status = 2;

        $user_id = isset($data['user_id']) ? $data['user_id'] : $data['SUGGESTED_MEMO'];
        $amount = isset($data['amount']) ? $data['amount'] : $data['PAYMENT_AMOUNT'];

        if($transaction_status === 2) {
            $user_balance = UserBalance::where('user_id', $user_id)->first();
            $user_balance->real = $user_balance->real + floatval($amount);
            $user_balance->save();
        }

        if ($request->post('checkbox-message')) {
            $this->sendThankYouMessage($request);
        }

        if ($request->post('is_user_request')) {
            return redirect()->route('user.balance', app()->getLocale())->with(__('Thank you for deposit.'));
        } else if($request->post('PAYMENT_AMOUNT')){
            return redirect()->route('index', app()->getLocale())->with(__('Thank you for deposit.'));
        } else {
            return back()->with('status', __('Balance added.'));
        }
    }

    public function makeSwift(Request $request)
    {

        $is_canceled = $request->post('cancel');
        if ($is_canceled) {
            TransactionController::setLastPendingTransactionStatus(0);

            return back()->with('status', 'canceled');
        }

        $data = $request->except('_token', 'checkbox-message', 'transaction', 'is_user_request');

        if ($request->post('transaction')) {
            $transaction = new Transaction();
            foreach ($data as $k => $v) {
                $transaction->$k = $v;
            }
            $transaction->status = 2;

            $transaction->save();
        }

        $transaction_status = $this->paymentPerfectMoney($request, $data, 'swift');

        $user_id = isset($data['user_id']) ? $data['user_id'] : $data['SUGGESTED_MEMO'];
        $amount = isset($data['user_id']) ? $data['user_id'] : $data['PAYMENT_AMOUNT'];


//        if($transaction_status === 2) {
//            $user_balance = UserBalance::where('user_id', $user_id)->first();
//            $user_balance->real = $user_balance->real + floatval($amount);
//            $user_balance->save();
//        }

//        if ($request->post('checkbox-message')) {
//            $this->sendThankYouMessage($request);
//        }

        if ($request->post('is_user_request')) {
            return redirect()->route('user.balance', app()->getLocale())->with(__('Thank you for deposit.'));
        } else if($request->post('PAYMENT_AMOUNT')){
            return redirect()->route('index', app()->getLocale())->with(__('Thank you for deposit.'));
        } else {
            return back()->with('status', __('Balance added.'));
        }
    }

    public function makeTax(Request $request)
    {
        $is_canceled = $request->post('cancel');
        if ($is_canceled) {
            TransactionController::setLastPendingTransactionStatus(0);

            return back()->with('status', 'canceled');
        }

        $data = $request->except('_token', 'checkbox-message', 'transaction', 'is_user_request');

        if ($request->post('transaction')) {
            $transaction = new Transaction();
            foreach ($data as $k => $v) {
                $transaction->$k = $v;
            }
            $transaction->status = 2;

            $transaction->save();
        }

        $transaction_status = $this->paymentPerfectMoney($request, $data, 'tax');

        $user_id = isset($data['user_id']) ? $data['user_id'] : $data['SUGGESTED_MEMO'];
        $amount = isset($data['user_id']) ? $data['user_id'] : $data['PAYMENT_AMOUNT'];


//        if($transaction_status === 2) {
//            $user_balance = UserBalance::where('user_id', $user_id)->first();
//            $user_balance->real = $user_balance->real + floatval($amount);
//            $user_balance->save();
//        }

//        if ($request->post('checkbox-message')) {
//            $this->sendThankYouMessage($request);
//        }

        if ($request->post('is_user_request')) {
            return redirect()->route('user.balance', app()->getLocale())->with(__('Thank you for deposit.'));
        } else if($request->post('PAYMENT_AMOUNT')){
            return redirect()->route('index', app()->getLocale())->with(__('Thank you for deposit.'));
        } else {
            return back()->with('status', __('Balance added.'));
        }

    }

    public function getTransactionsPage(Request $request)
    {
        self::checkIfRoleAllowed();
        $managers = User::where('role', 2)->get();

        $transactions = $this->getViewForRole($request,
            function($manager_clients) use($managers){
                return Transaction::orderBy('created_at', 'desc')->whereIn('user_id', $manager_clients)->paginate(10);
            },
            function() use($request){
                $transactions = $this->sortByManagers($request,
                    function($manager_clients){
                        return Transaction::orderBy('created_at', 'desc')->whereIn('user_id', $manager_clients)->paginate(10);
                    },
                    function(){
                        return Transaction::orderBy('created_at', 'desc')->paginate(10);
                    }
                );

                return $transactions;
            }
        );

        return view('admin.pages.transactions', ['transactions' => $transactions, 'managers' => $managers]);
    }

    public function getWithdrawalsPage(Request $request)
    {
        self::checkIfRoleAllowed();
        $managers = User::where('role', 2)->get();


        $users = $this->getViewForRole($request,
            function($manager_clients) use($managers){
                return User::whereHas('transactions', function($query) use ($manager_clients){
                    $query->where('type', 'tax');
                    $query->whereIn('user_id', $manager_clients);
                })->paginate(10);
            },
            function() use($request){
                $users = $this->sortByManagers($request,
                    function($manager_clients){
                        return $users =  User::whereHas('transactions', function($query) use ($manager_clients){
                            $query->where('type', 'tax');
                            $query->whereIn('user_id', $manager_clients);
                        })->paginate(10);
                    },
                    function(){
                        return $users =  User::whereHas('transactions', function($query){
                            $query->where('type', 'tax');
                        })->paginate(10);
                    }
                );

                return $users;
            }
        );

        return view('admin.pages.withdrawals', ['users' => $users, 'managers' => $managers]);
    }

    public function getPaymentsPage(Request $request)
    {
        self::checkIfRoleAllowed();
        $managers = User::where('role', 2)->get();

        $payments = Transaction::whereDay('created_at', now()->day)->latest()->get();

        $payments = $this->sortByManagers($request,
            function($manager_clients){
                return Transaction::whereDay('created_at', now()->day)->whereIn('user_id', $manager_clients)->latest()->get();
            },
            function(){
                return Transaction::whereDay('created_at', now()->day)->latest()->get();
            }
        );

        return view('admin.pages.payments', ['payments' => $payments, 'managers' => $managers]);
    }

    public function sortByManagers($request, $manager_callback, $default_callback)
    {
        $selected_manager = $request->get('manager');

        if ($selected_manager && $selected_manager != 'all') {
            $manager_clients = ManagerClientsController::getManagerClientsBySelectedManager($selected_manager);
            return $manager_callback($manager_clients);
        } else{
            return $default_callback();
        }
    }

    public function getViewForRole(Request $request, $manager_callback, $admin_callback)
    {
        $role = auth()->user()->role;

        if ($role === 2) {
            $manager_clients = ManagerClientsController::getManagerClientsBySelectedManager(auth()->id());
            return $manager_callback($manager_clients);
        } elseif ($role === 3) {
            return $admin_callback();
        }
    }

    public function getUsersPage(Request $request)
    {
        self::checkIfRoleAllowed();
        $managers = User::where('role', 2)->get();
        if ($request->get('search')) {
            $users = User::where('login', 'like', '%'.$request->get('search').'%')->paginate(10);
            return view('admin.pages.users', ['users' => $users]);
        }

        $users = $this->getViewForRole($request,
            function($manager_clients){
                return User::orderBy('id', 'desc')->whereIn('id', $manager_clients)->paginate(10);
            },
            function() use($request){
                return $this->sortByManagers($request,
                    function($manager_clients){
                        return User::orderBy('id', 'desc')->whereIn('id', $manager_clients)->paginate(10);
                    },
                    function(){
                        return User::orderBy('id', 'desc')->where('role', '1')->paginate(10);
                    }
                );
            }
        );

        return view('admin.pages.users', ['users' => $users, 'managers' => $managers]);
    }

    public function getTodayReport(Request $request)
    {
        return $this->sortByManagers($request,
            function($manager_clients){
                return [
                    'users' => User::whereDate('created_at', Carbon::today())->whereIn('id', $manager_clients)->count(),
                    'swifts' => Transaction::whereDate('created_at', Carbon::today())->where('type', 'swift')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'taxes' => Transaction::whereDate('created_at', Carbon::today())->where('type', 'tax')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'deposits' => Transaction::whereDate('created_at', Carbon::today())->where('type', 'deposit')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'all' => Transaction::whereDate('created_at', Carbon::today())->whereIn('user_id', $manager_clients)->sum('amount')
                ];
            },
            function(){
                return [
                    'users' => User::whereDate('created_at', Carbon::today())->count(),
                    'swifts' => Transaction::whereDate('created_at', Carbon::today())->where('type', 'swift')->sum('amount'),
                    'taxes' => Transaction::whereDate('created_at', Carbon::today())->where('type', 'tax')->sum('amount'),
                    'deposits' => Transaction::whereDate('created_at', Carbon::today())->where('type', 'deposit')->sum('amount'),
                    'all' => Transaction::whereDate('created_at', Carbon::today())->sum('amount')
                ];
            }
        );
    }

    public function getAllReport(Request $request)
    {
        return $this->sortByManagers($request,
            function($manager_clients){
                return [
                    'users' => User::all()->whereIn('id', $manager_clients)->count(),
                    'swifts' => Transaction::all()->where('type', 'swift')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'taxes' => Transaction::all()->where('type', 'tax')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'deposits' => Transaction::all()->where('type', 'deposit')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'all' => Transaction::all()->whereIn('user_id', $manager_clients)->sum('amount')
                ];
            },
            function(){
                return [
                    'users' => User::all()->count(),
                    'swifts' => Transaction::all()->where('type', 'swift')->sum('amount'),
                    'taxes' => Transaction::all()->where('type', 'tax')->sum('amount'),
                    'deposits' => Transaction::all()->where('type', 'deposit')->sum('amount'),
                    'all' => Transaction::all()->sum('amount')
                ];
            }
        );
    }

    public function getWeekReport(Request $request)
    {
        $now = Carbon::now();

        $weekStartDate = $now->startOfWeek()->format('Y-m-d H:i:s');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d H:i:s');

        return $this->sortByManagers($request,
            function($manager_clients) use ($weekStartDate, $weekEndDate){
                return [
                    'users' => User::whereBetween('created_at', [$weekStartDate, $weekEndDate])->whereIn('id', $manager_clients)->count(),
                    'swifts' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('type', 'swift')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'taxes' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('type', 'tax')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'deposits' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('type', 'deposit')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'all' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->whereIn('user_id', $manager_clients)->sum('amount')
                ];
            },
            function() use ($weekStartDate, $weekEndDate){
                return [
                    'users' => User::whereBetween('created_at', [$weekStartDate, $weekEndDate])->count(),
                    'swifts' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('type', 'swift')->sum('amount'),
                    'taxes' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('type', 'tax')->sum('amount'),
                    'deposits' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->where('type', 'deposit')->sum('amount'),
                    'all' => Transaction::whereBetween('created_at', [$weekStartDate, $weekEndDate])->sum('amount')
                ];
            }
        );
    }

    public function getMonthReport(Request $request)
    {
        $now = Carbon::now();

        $monthStartDate = $now->startOfMonth()->format('Y-m-d H:i:s');

        $monthEndDate = $now->endOfMonth()->format('Y-m-d H:i:s');

        return $this->sortByManagers($request,
            function($manager_clients) use ($monthStartDate, $monthEndDate){
                return [
                    'users' => User::whereBetween('created_at', [$monthStartDate, $monthEndDate])->whereIn('id', $manager_clients)->count(),
                    'swifts' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->where('type', 'swift')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'taxes' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->where('type', 'tax')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'deposits' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->where('type', 'deposit')->whereIn('user_id', $manager_clients)->sum('amount'),
                    'all' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->whereIn('user_id', $manager_clients)->sum('amount')
                ];
            },
            function() use ($monthStartDate, $monthEndDate){
                return [
                    'users' => User::whereBetween('created_at', [$monthStartDate, $monthEndDate])->count(),
                    'swifts' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->where('type', 'swift')->sum('amount'),
                    'taxes' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->where('type', 'tax')->sum('amount'),
                    'deposits' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->where('type', 'deposit')->sum('amount'),
                    'all' => Transaction::whereBetween('created_at', [$monthStartDate, $monthEndDate])->sum('amount')
                ];
            }
        );

    }

    public function getDateRangeReport(Request $request)
    {
        $daterange = explode('-', urldecode(str_replace('+', '', urlencode($request->get('daterange')))));

        $start_date = Carbon::createFromFormat('d/m/Y', $daterange[0])->format('Y-m-d H:i:s');
        $end_date = Carbon::createFromFormat('d/m/Y', $daterange[1])->format('Y-m-d H:i:s');

        return [
            'users' => User::whereBetween('created_at', [$start_date, $end_date])->count(),
            'swifts' => Transaction::whereBetween('created_at', [$start_date, $end_date])->where('type', 'swift')->sum('amount'),
            'taxes' => Transaction::whereBetween('created_at', [$start_date, $end_date])->where('type', 'tax')->sum('amount'),
            'deposits' => Transaction::whereBetween('created_at', [$start_date, $end_date])->where('type', 'deposit')->sum('amount'),
            'all' => Transaction::whereBetween('created_at', [$start_date, $end_date])->sum('amount')
        ];
    }

    public function getReportPage(Request $request)
    {
        self::checkIfRoleAllowed();
        $managers = User::where('role', 2)->get();
        if($request->get('period')) {

            if ($request->get('period') == 'today' || !$request->get('period')) {
                $view_params = $this->getTodayReport($request);
            } elseif ($request->get('period') == 'week') {
                $view_params = $this->getWeekReport($request);
            } elseif ($request->get('period') == 'month') {
                $view_params = $this->getMonthReport($request);
            } elseif ($request->get('period') == 'all') {
                $view_params = $this->getAllReport($request);
            }

        } elseif($request->get('daterange')) {
            $view_params = $this->getDateRangeReport($request);
        } else {
            $view_params = $this->getTodayReport($request);
        }


        return view('admin.pages.report', [
            'managers' => $managers,
            'view_params' => $view_params
        ]);
    }

    public function getNoBetsPage(Request $request)
    {
        self::checkIfRoleAllowed();

        $data = $this->getViewForRole($request,
            function($manager_clients){
                $no_bets = User::doesnthave('betsDuring24Hours')->has('realBets')->whereIn('id', $manager_clients)->whereRole('1')->paginate(10);

                return ['no_bets' => $no_bets];
            },
            function() use ($request){
                $managers = User::where('role', 2)->get();

                $no_bets = $this->sortByManagers($request,
                    function($manager_clients){
                        return User::doesnthave('betsDuring24Hours')->has('realBets')->whereIn('id', $manager_clients)->whereRole('1')->paginate(10);
                    },
                    function(){
                        return User::doesnthave('betsDuring24Hours')->has('realBets')->where('role', 1)->paginate(10);
                    }
                );

                return ['managers' => $managers, 'no_bets' => $no_bets];
            }
        );



        return view('admin.pages.no-bets', $data);
    }

    public function getUnfortunateWithdrawalPage(Request $request)
    {
        self::checkIfRoleAllowed();

        $users = $this->getViewForRole($request,
            function($manager_clients){
                return User::orderBy('id', 'desc')->where('role', 1)->whereIn('id', $manager_clients)->get();
            },
            function(){
                return User::orderBy('id', 'desc')->where('role', 1)->get();
            }
        );

        return view('admin.pages.unfortunate-withdrawal', ['users' => $users]);

    }

    public function getPagesPage(Request $request)
    {
        return view('admin.pages.pages', ['pages' => Page::where('category', '!=', 'news')->latest()->paginate(10)]);
    }

    public function getPagePage($page_id, Request $request)
    {

        $page = Page::where(['id' => $page_id])->first();
        if ($page) {
            return view('admin.pages.pages.page', ['mode' => 'edit', 'page' => $page, 'request' => $request]);
        }
        return view('admin.pages.pages.page', ['request' => $request]);
    }

    public function deleteUser(Request $request)
    {
        DB::beginTransaction();

        $user = User::find($request->post('user_id'));

        UserController::deleteAvatar($user);
        UserBalanceController::deleteBalance($user->id);
        UserPersonalDataController::deleteData($user->id);

        MessageController::deleteUserData($user->id, $user->login);

        $user->delete();

        DB::commit();

        return back()->with('status', __('Client deleted.'));
    }

    public function deletePage(Request $request)
    {
        $page = Page::find($request->post('page_id'));

        $existing_thumbnail = $page->thumbnail;

        if ($existing_thumbnail) {
            self::deleteThumbnail($existing_thumbnail);
        }

        $page->delete();
        return back()->with('status', __('Page deleted.'));
    }

    public static function deleteThumbnail($thumbnail)
    {
        unlink($_SERVER['DOCUMENT_ROOT'] . '/img/news/' . $thumbnail);
    }

    public static function uploadNewsThumbnailIfExists(Request $request, $page_id)
    {
        if($request->file('thumbnail')){

            $image = $request->file('thumbnail');

            $path = '/img/news';

            $imageName = Uploads::uploadImage($path, $image);

            $page_obj = Page::find($page_id);

            $existing_thumbnail = $page_obj->thumbnail;

            if ($existing_thumbnail) {
                self::deleteThumbnail($existing_thumbnail);
            }

            $page_obj->thumbnail = $imageName;
            $page_obj->save();
        }
    }

    public function pageManipulation(Request $request)
    {
        $route = $request->post('category') !== 'news' ? 'admin.pages' : 'admin.news';



        if ($request->post('page_id')) {
            $page = Page::find($request->post('page_id'))->update(
                [
                    'title' => json_encode([
                        'ir' => $request->post('ir_title'),
                        'en' => $request->post('en_title')
                    ]),
                    'content' => json_encode([
                        'ir' => $request->post('ir_content'),
                        'en' => $request->post('en_content')
                    ]),
                    'category' => $request->post('category'),
                    'slug' => $request->post('slug')
                ]
            );

            AdminController::uploadNewsThumbnailIfExists($request, $request->post('page_id'));

                return redirect()
                    ->route($route)
                    ->with('status', __("<b>{$request->post('en_title')}</b> successfully updated."));

        } else {
            $page = Page::create([
                'title' => json_encode([
                   'ir' => $request->post('ir_title'),
                   'en' => $request->post('en_title')
                ]),
                'content' => json_encode([
                    'ir' => $request->post('ir_content'),
                    'en' => $request->post('en_content')
                ]),
                'category' => $request->post('category'),
                'slug' => $request->post('slug')
            ]);

            AdminController::uploadNewsThumbnailIfExists($request, $page->id);
        }
        return redirect()
            ->route($route)
            ->with('status', __("<b>{$request->post('en_title')}</b> created."));
    }

    public function getAdminsPage(Request $request)
    {
        self::checkIfRoleAllowed();
        if ($request->get('role') && $request->get('role') != 'all') {
            $users = User::where('role', $request->get('role'))
                ->paginate(10);
        } else if ($request->get('search')) {
            $users = User::where('login', 'like', '%'.$request->get('search').'%')->paginate(10);
        } else {
            $users = User::orderBy('id', 'desc')->paginate(10);
        }

        return view('admin.pages.admins', ['users' => $users, 'request' => $request]);
    }

    public function getSettingsPage(Request $request)
    {
        $data = array();

        $settings = Settings::all();
        foreach ($settings as $s) {
            $data[$s->key] = $s->value;
        }

        return view('admin.pages.settings', ['settings' => json_encode($data)]);
    }

    public function getGamesHistoryPage(Request $request, $id)
    {
        $user = User::find($id);
        $bets = Bet::whereUserIdAndIsReal($id, 1)->paginate(5);

        return view('admin.pages.games-history', ['user' => $user, 'bets' => $bets]);
    }

    public function getPaymentHistoryPage(Request $request, $id)
    {
        $user = User::find($id);

        $filter = $request->get('filter');
        $sort = $request->get('sort');

        $transactions = Transaction::query();

        if ( isset($filter) ) {

            switch( $filter ) {
                case 'all': $transactions = $transactions->where('user_id', $id); break;
                case 'income': $transactions = $transactions->where([
                    ['type', '=', 'deposit'],
                    ['user_id', '=', $id]
                ]); break;
                case 'outcome': $transactions = $transactions->where([
                    ['type','!=','deposit'],
                    ['user_id', '=', $id]
                ]); break;
            }

        }

        if( isset($sort) ) {

            switch( $sort ) {
                case 'latest': $transactions = $transactions->latest();
                case 'oldest': $transactions = $transactions->oldest()->get();
            }

        }

        if(!$sort && !$filter)
            $transactions = $transactions->where('user_id', $id)->latest()->get();

        $transactions_sum = $transactions->sum('amount');

        return view('admin.pages.payment-history', ['user' => $user, 'transactions' => $transactions, 'transactions_sum' => $transactions_sum]);
    }

    public function applyPaymentHistoryActions(Request $request)
    {
        $action = $request->post('action');
        $transaction = $request->post('transaction');

        foreach ($transaction as $t) {
            $t = Transaction::find($t);
            if ($action === 'reject') {
               $t->status = 0;
            } else {
                $t->status = 2;
            }
            $t->save();
        }

        return back()->with('status', __('Changes saved.'));
    }

    public function getTranslatePage(Request $request)
    {
        $en_translation = file_get_contents(resource_path().'/lang/ir.json');
        return view('admin.pages.translate', ['en' => $en_translation]);
    }

    public function getGamesPage(Request $request)
    {
        $games = Game::all();
        return view('admin.pages.games', ['games' => $games]);
    }

    public function saveTranslation(Request $request)
    {
        $translation = $request->post('translation');
        file_put_contents(resource_path().'/lang/en.json', $translation);

        return back()->with('status', __('Translation successfully saved.'));
    }

    public function getCreateGamePage()
    {
        return view('admin.pages.games.create');
    }

    public function createGame(Request $request)
    {
        $is_enabled = $request->post('is_enabled') ? 1 : 0;
        $is_popular = $request->post('is_popular') ? 1 : 0;
        $is_new = $request->post('is_new') ? 1 : 0;
        $is_slot = $request->post('is_slot') ? 1 : 0;
        $is_board = $request->post('is_board') ? 1 : 0;

        $categories = [
            'is_popular' => $is_popular,
            'is_new' => $is_new,
            'is_slot' => $is_slot,
            'is_board' => $is_board
        ];

        $title = $request->post('title');
        $slug = $request->post('slug');
        $integration = $request->post('integration');

        $image = $request->file('image');

        $filePath = '/img/games';

        $imageName = Uploads::uploadImage($filePath, $image);

        Game::create([
            'title' => $title,
            'slug' => $slug,
            'integration' => $integration,
            'categories' => json_encode($categories),
            'image' => $imageName,
            'is_enabled' => $is_enabled
        ]);

        return redirect()->route('admin.games')->with('status', __('Game added.'));

    }

    public function deleteGame($id)
    {
        Game::find($id)->delete();

        return redirect()->route('admin.games')->with('status', __('Game deleted.'));
    }

    /* News */

    public function getNewsPage()
    {
        return view('admin.pages.news.news', ['news' => Page::whereCategory('news')->latest()->get()]);
    }

    public function getNewsPagePage($page_id, Request $request)
    {

        $page = Page::where(['id' => $page_id])->first();
        if ($page) {
            return view('admin.pages.news.page', ['mode' => 'edit', 'page' => $page, 'request' => $request]);
        }
        return view('admin.pages.news.page', ['request' => $request]);
    }

    /* ./News */

    /* Tournament */

    public function getTournamentsPage(Request $request)
    {
        $tournaments = Tournament::all();

        return view('admin.pages.games.tournaments.tournaments', ['tournaments' => $tournaments]);
    }

    public function getTournamentPage($id, Request $request)
    {
        $games = Game::all();
        $tournament = Tournament::whereId($id)->first();
        if ($tournament) {
            return view('admin.pages.games.tournaments.tournament', ['mode' => 'edit', 'tournament' => $tournament, 'request' => $request, 'games' => $games]);
        }
        return view('admin.pages.games.tournaments.tournament', ['request' => $request, 'games' => $games]);
    }

    /* ./Tournament */
}
