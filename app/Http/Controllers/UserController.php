<?php

namespace App\Http\Controllers;

use App\Helpers\Uploads;
use App\Models\ManagerClients;
use App\Models\Message;
use App\Models\Ref;
use App\Models\SelfCreatedAccount;
use App\Models\Settings;
use App\Models\Transaction;
use App\Models\UnfortunateWithdrawal;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\UserPersonalData;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function getUser(Request $request)
    {
        $data = array();

        $user = User::find($request->post('id'));
        $ref = Ref::where('user_id', $request->post('id'))->first();

        if ($ref) {
            $data = [
              'user' => $user,
              'ref' => $ref
            ];
        } else {
            $data = [
                'user' => $user,
            ];
        }
        return response()->json($data);
    }

    public function getSettingsPage()
    {
        $personal_data = UserPersonalData::all()->where('user_id', auth()->user()->id)->first();
        return view('user.settings', ['personal_data' => $personal_data]);
    }

    public function getBalancePage()
    {
        $balance = UserBalance::all()->where('user_id', auth()->user()->id)->first();
        return view('user.balance', ['balance' => $balance]);
    }

    public function getTransactionsPage()
    {
        $transaction_id = Transaction::latest()->get()[0]->id+1;

        $client_withdrawal_cancellation = Settings::where('key', 'client_withdrawal_cancellation')->first();
        $client_withdrawal_cancellation = $client_withdrawal_cancellation->value;

        $transactions = Transaction::latest('id')->where('user_id', auth()->user()->id)->get();
        $last_pending_transaction = Transaction::where(['user_id' => auth()->user()->id, 'status' => 1])->first();

        $has_pending_transactions = $last_pending_transaction ? $last_pending_transaction : false;
        return view('user.transactions', ['transactions' => $transactions, 'transaction_id' => $transaction_id,'has_pending_transactions' => $has_pending_transactions, 'client_withdrawal_cancellation' => $client_withdrawal_cancellation]);
    }

    public function swiftTaxPayment(Request $request)
    {
        $is_canceled = $request->post('cancel');
        if ($is_canceled) {
            TransactionController::setLastPendingTransactionStatus(0);

            return back()->with('status', 'canceled');
        } else {

            TransactionController::setLastPendingTransactionStatus(2);

            $transaction = new Transaction();
            foreach ($request->except('_token', 'cancel') as $k => $v) {
                $transaction->$k = $v;
            }
            $transaction->save();

            return back()->with('status', 'completed');
        }
    }

    public function winnerTaxPayment(Request $request)
    {
        $transaction = new Transaction();
        foreach ($request->except('_token', 'cancel') as $k => $v) {
            $transaction->$k = $v;
        }
        $transaction->save();

        return back()->with('status', 'completed');
    }

    public function getDepositPage(Request $request)
    {
        $self_created = SelfCreatedAccount::where('user_id', auth()->user()->id) ? true : false;
        return view('user.deposit', ['self_created' => $self_created]);
    }

    public function getWithdrawalRequestPage(Request $request)
    {
        $min_withdrawal_amount = Settings::where('key', 'min_withdrawal_amount')->first();
        return view('user.withdrawal-request', ['min_withdrawal_amount' => $min_withdrawal_amount->value]);
    }

    public function getDepositRequestPage(Request $request)
    {
        $transaction_id = Transaction::latest()->get()[0]->id+1;
        return view('user.deposit-request', ['transaction_id' => $transaction_id]);
    }

    public function getMakeDepositPage()
    {
        $min_withdrawal_amount = Settings::where('key', 'min_withdrawal_amount')->first();
        return view('user.make-deposit', ['min_withdrawal_amount' => $min_withdrawal_amount->value]);
    }

    public function withdraw(Request $request)
    {
        $min_withdrawal_time = Settings::where('key', 'min_withdrawal_time')->first();
        $min_withdrawal_time = $min_withdrawal_time->value;
        $user_registration_date = Carbon::createFromFormat('Y-m-d H:i:s', auth()->user()->created_at);

        $min_register_date = Carbon::createFromFormat('Y-m-d H:i:s', $user_registration_date)->addHours(intval($min_withdrawal_time));
        $diff = Carbon::today()->diffInHours($min_register_date, false);

        if($diff <= 0) {
            $transaction = new Transaction();

            foreach ($request->except('_token') as $k => $v) {
                $transaction->$k = $v;
            }

            $transaction->save();
            return back()->with('status', 'success');
        } else {

            UnfortunateWithdrawal::create([
                'user_id' => auth()->id(),
                'time_from_registration' => $diff
            ]);

            $transaction = new Transaction();

            foreach ($request->except('_token') as $k => $v) {
                $transaction->$k = $v;
            }

            $transaction->status = 0;

            $transaction->save();
            return redirect()->route('user.transactions', app()->getLocale());
        }
    }

    public function updateLogin(Request $request)
    {
        $user = User::find($request->user_id);
        $user->login = $request->login;
        $user->save();

        return back()->with('status', __('Login successfully changed!'));
    }

    public function updateEmail(Request $request)
    {
        $user = User::find($request->user_id);
        $user->email = $request->email;
        $user->save();

        return back()->with('status', __('Email successfully changed!'));
    }

    public static function deleteAvatar($user){
        if ($user->avatar) {
            unlink($_SERVER['DOCUMENT_ROOT'].'/img/avatars/'.$user->avatar);
        }
    }

    public function updateAvatar(Request $request)
    {
        $request->validate([
            'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('img');
        $filePath = '/img/avatars';

        $imageName = Uploads::uploadImage($filePath, $image);

        $user = User::find($request->user_id);

        self::deleteAvatar($user);

        $user->avatar = $imageName;
        $user->save();

        return back()->with('status', __('Аватар успешно обновлён!'));
    }

    public function updateRole(Request $request)
    {

        User::find($request->post('user_id'))->update(
            ['role' => $request->post('role')]
        );

        $ref = Ref::where('user_id', $request->post('user_id'))->first();
        if (!$ref) {
            Ref::create([
                'user_id' => $request->post('user_id'),
                'ref' => $request->post('ref')
            ]);
        }
        return back()->with('status', __('Изменения сохранены.'));
    }

    public function addUserPlayer(Request $request)
    {

        $user = User::create([
           'login' => $request->post('login'),
           'email' => $request->post('email'),
           'password' => Hash::make($request->post('password')),
            'registration_password' => $request->post('password')
        ]);

        UserBalance::create([
            'user_id' => $user->id,
            'real' => floatval($request->post('balance')),
            'virtual' => 5000
        ]);

        return back()->with('status', __('Пользователь-игрок').' <b>'.$request->post('login').'</b> '.__('успешно создан'));
    }

    public function getWriteUsPage(Request $request)
    {

        $user_manager = ManagerClients::where('user_id', auth()->id())->first();
        if($user_manager) {
            $user_manager = User::find($user_manager->manager_id)->login;
        }

        $user_messages =  Message::whereUserId(auth()->id())->get();
        $admin_messages =  Message::whereIsAdminAndFor(1, auth()->user()->login)->get();

        $messages = collect($user_messages)->merge($admin_messages)->sortDesc();

        abort_if(!Settings::where('key', 'client_messages_allowed')->first()->value, 404);
        return view('user.write-us', ['messages' => $messages, 'user_manager' => $user_manager]);
    }

}
