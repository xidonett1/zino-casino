<?php

namespace App\Http\Controllers;

use App\Models\ManagerClients;
use App\Models\Ref;
use App\Models\SelfCreatedAccount;
use App\Models\UserBalance;
use App\Models\UserLocation;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Validator;
use MongoDB\Driver\Manager;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Stevebauman\Location\Facades\Location;

class CustomAuthController extends Controller
{

    protected $admin_cards = [
        'Unfortunate withdrawals' => 'admin.unfortunate-withdrawal',
        'No bets' => 'admin.no-bets',
        'Clients' => 'admin.users',
        'Roles' => 'admin.admins',
        'To withdraw' => 'admin.withdrawal',
        'Transactions' => 'admin.transactions',
        'Pages' => 'admin.pages',
        'Payments' => 'admin.payments',
        'Withdrawals' => 'admin.withdrawals',
        'Report' => 'admin.report',
        'Settings' => 'admin.settings',
    ];

    protected $manager_cards = [
        'Unfortunate withdrawals' => 'admin.unfortunate-withdrawal',
        'No bets' => 'admin.no-bets',
        'Clients' => 'admin.users',
        'To withdraw' => 'admin.withdrawal',
        'Transactions' => 'admin.transactions',
        'Payments' => 'admin.payments',
        'Withdrawals' => 'admin.withdrawals',
        'Report' => 'admin.report',
    ];

    public function index()
    {
        return view('auth.login');
    }


    public function customLogin(Request $request)
    {
        $request->validate([
            'login' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('login', 'password');
        if (Auth::attempt($credentials)) {
            return \auth()->user()->role == 1 ? redirect()->intended(app()->getLocale().'/my/balance') : redirect()->intended('/admin')
                ->withSuccess('Signed in');
        }

        return back()->with('status', __('Data is incorrect!'));
    }



    public function register()
    {
        return view('auth.register');
    }


    public function customRegistration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
           return back()->with('status_register', __('User with this login is already exists!'));
        }

        $data = $request->all();

        $check = $this->create($data);

        $credentials = $request->only('login', 'password');
        if (Auth::attempt($credentials)) {

            return \auth()->user()->role == 1 ? redirect()->intended(app()->getLocale().'/my/balance') : redirect()->intended('admin')
                ->withSuccess('Signed in');
        }

        return redirect('admin')->withSuccess('You have signed-in');
    }


    public function create(array $data)
    {
        \DB::transaction( function() use(&$data) {

            $new_user = User::create([
                'login' => $data['login'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'registration_password' => $data['password']
            ]);

            $ref_code = $data['ref'];

            if ($ref_code) {
                $manager_id = Ref::where('ref', $ref_code)->first();
                $manager_id = $manager_id->user_id;

                ManagerClients::create(
                    [
                        'user_id' => $new_user->id,
                        'manager_id' => $manager_id
                    ]
                );
            }
            UserBalance::create([
                'user_id' => $new_user->id,
                'real' => 0,
                'virtual' => 5000
            ]);

            UserLocationController::create(
                $new_user->id,
                $_SERVER['REMOTE_ADDR']
            );

            if ($data['self_created']) {
                SelfCreatedAccount::create(
                    ['user_id' => $new_user->id]
                );
            }

        }, 5);
    }


    public function getAdminDashboard()
    {
        if(Auth::check()) {
            switch (Auth::user()->role) {
                case 1: break;
                case 2: $cards = $this->manager_cards; break;
                case 3: $cards = $this->admin_cards; break;
            }
            return view('admin.dashboard', ['cards' => $cards]);
        }

        return redirect()->route('index', app()->getLocale())->withSuccess('You are not allowed to access');
    }

    public function changePassword(Request $request){
        $user = User::find($request->user_id);

        $old_pass = $request->old_pass;
        $new_pass = Hash::make($request->new_pass);
        if(Hash::check($old_pass, $user->password)){
            $user->password = $new_pass;
            $user->save();

            return redirect()->route('user.settings', app()->getLocale())->with('status', __('Password successfully changed!'));
        }
        return redirect()->route('user.settings', app()->getLocale())->with('status', __('Old password is incorrect!'));
    }

    public function signOut() {
        Session::flush();
        Auth::logout();

        return redirect()->route('index', app()->getLocale());
    }
}
