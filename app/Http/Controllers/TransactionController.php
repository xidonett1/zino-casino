<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function getTransaction(Request $request)
    {
        $id = $request->id;
        return response()->json(Transaction::find($id));
    }

    public function updateTransaction(Request $request)
    {
        $data = $request->except('_token', 'transaction_id');
        $transaction = Transaction::where('id', $request->transaction_id)->first();

        foreach ($data as $k => $v) {
            $transaction->$k = $v;
        }

        $transaction->save();

        return back()->with('status', __('Transaction successfully updated.'));
    }

    public static function setLastPendingTransactionStatus($status = 0)
    {
        $last_pending_transaction = Transaction::where(['user_id' => auth()->user()->id, 'status' => 1])->first();
        $last_pending_transaction->status = $status;
        $last_pending_transaction->save();
    }

    public static function getAllUserDepositTransactionsSum($user_id)
    {
        return Transaction::where(['user_id' => $user_id, 'type' => 'deposit'])->get()->sum('amount');
    }

    public static function getAllWithdrawalsSum($user_id)
    {
        return Transaction::where(['user_id' => $user_id, 'type' => 'withdrawal'])->get()->sum('amount');
    }

}
