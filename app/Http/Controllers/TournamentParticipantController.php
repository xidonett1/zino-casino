<?php

namespace App\Http\Controllers;

use App\Models\TournamentParticipant;
use Illuminate\Http\Request;

class TournamentParticipantController extends Controller
{
    public static function participate(Request $request)
    {
        TournamentParticipant::create($request->post());

        return redirect()->back();
    }
}
