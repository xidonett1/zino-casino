<?php

namespace App\Http\Controllers;

use App\Models\TicketTemplate;
use Illuminate\Http\Request;

class TicketTemplateController extends Controller
{

    public function getPage(Request $request)
    {
        return view('admin.pages.support.ticket-templates', ['templates' => TicketTemplate::orderBy('id', 'desc')->paginate(10)]);
    }

    public function create(Request $request)
    {
        $name = $request->post('name');

        TicketTemplate::create([
            'name' => $name
        ]);

        return back()->with('status', __('Ticket template <b>').$name.__('</b> successfully created.'));
    }
}
