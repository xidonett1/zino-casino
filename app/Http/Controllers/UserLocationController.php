<?php

namespace App\Http\Controllers;

use App\Models\UserLocation;
use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;

class UserLocationController extends Controller
{

    public static function create($user_id, $remote_addr)
    {
        $location = Location::get($remote_addr);

        $country = 'N/A';
        $city = 'N/A';

        if($location){
            $country = $location->countryCode ? $location->countryCode : 'N/A';
            $city = $location->cityName ? $location->cityName : 'N/A';
        }

        UserLocation::create([
            'user_id' => $user_id,
            'country' => $country,
            'city' => $city,
            'ip' => $remote_addr
        ]);
    }

}
