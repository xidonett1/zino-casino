<?php

namespace App\Http\Controllers;

use App\Models\Blocked;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BlockedController extends Controller
{
    public function change(Request $request, $user_id)
    {

        $user = User::whereId($user_id)->first();
        $blocked = Blocked::where('user_id', $user_id)->first();


        if ($blocked) {
            Blocked::find($blocked->id)->delete();
            return back()->with('status', __('Client unblocked.'));
        }

        $new_passowrd = uniqid().'_'.$user->login;
        $user->password = Hash::make($new_passowrd);
        $user->save();

        Blocked::create([
            'user_id' => $user_id,
            'new_password' => $new_passowrd
        ]);

        return back()->with('status', __('Client blocked.'));
    }
}
