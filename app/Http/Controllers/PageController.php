<?php

namespace App\Http\Controllers;

use App\Models\Bet;
use App\Models\Game;
use App\Models\Games;
use App\Models\Ref;
use App\Models\Tournament;
use App\Models\TournamentParticipant;
use http\Client\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redis;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function getPage($lang, $slug, Request $request)
    {
        $page = Page::where(['slug' => $slug])->first();

        $title = json_decode($page->title, true)[$lang];
        $content = json_decode($page->content, true)[$lang];

        return view('layouts.custom-page.page', compact('title', 'content', 'request'));
    }

    public function getIndexPage(Request $request, $lang = 'en')
    {
        $news = Page::whereCategory('news')->latest()->take(9)->get();
        $games = Game::all();

        // If request has ref code
        (new Ref())->setRefCookie($request, $news, $lang);

        return view('index', compact('lang', 'games', 'news'));
    }

    public function getGamesHistoryPage($lang = 'en')
    {
        $real = Bet::whereUserIdAndIsReal(auth()->id(), 1)->get();
        $virtual = Bet::whereUserIdAndIsReal(auth()->id(), 0)->get();

        return view('user.games-history', compact('lang', 'real', 'virtual'));
    }

    public function getCasinoPage()
    {
        $games = Games::all();

        return view('casino', compact('games'));
    }

    public function getNewsPage($lang)
    {
        $news = Page::whereCategory('news')->latest()->get();

        return view('news', compact('lang', 'news'));
    }

    public function getSingleNewsPage(Request $request, $lang, $slug)
    {
        $page = Page::whereSlugAndCategory($slug, 'news')->first();

        $title = json_decode($page->title, true)[$lang];
        $content = json_decode($page->content, true)[$lang];

        return view('layouts.custom-page.page', compact('page', 'title', 'content', 'request'));
    }

    public function getTournamentsPage(Request $request)
    {
        $participating_tournaments_ids = TournamentParticipant::whereUserId(auth()->id())
            ->get('tournament_id')
            ->pluck('tournament_id')
            ->toArray();

        $active_tournaments = (new TournamentController())->getActiveTournaments($participating_tournaments_ids);
        $inactive_tournaments = Tournament::whereIsAvailable('0')->latest()->get();

        $participating_tournaments = (new TournamentController())->getParticipatingTournaments($participating_tournaments_ids);

        return view('tournaments', compact('request', 'active_tournaments', 'inactive_tournaments', 'participating_tournaments'));
    }

    public function getLotteriesPage(Request $request)
    {
        return view('lotteries');
    }
}
