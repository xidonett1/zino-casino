<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function updateSettings(Request $request)
    {
        $data = $request->except('_token');

        foreach ($data as $k => $v) {
            Settings::where('key', $k)->update(
              [
                  'value' => $v
              ]
            );
        }

        return back()->with('status', __('Настройки успешно сохранены.'));
    }
}
