<?php

namespace App\Http\Controllers;

use App\Models\Bet;
use Illuminate\Http\Request;

class BetController extends Controller
{
    public function createBet(Request $request)
    {
        $bet = Bet::create($request->all());
        return response()->json($bet);
    }
}
