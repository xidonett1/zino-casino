<?php

namespace App\Http\Controllers;

use App\Models\ManagerClients;
use Illuminate\Http\Request;

class ManagerClientsController extends Controller
{
    public static function getManagerClientsBySelectedManager($selected_manager) : array
    {
        $manager_clients = ManagerClients::where('manager_id', $selected_manager)->get();
        return $manager_clients->pluck('user_id')->toArray();
    }
}
