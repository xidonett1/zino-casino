<?php

namespace App\Http\Middleware;

use App\Http\Controllers\BlockedController;
use App\Models\Blocked;
use App\Models\Settings;
use Closure;
use Illuminate\Http\Request;

class IsUnderConstruction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected static $allowedUris = [
      '/login',
      '/custom-login'
    ];

    public function handle(Request $request, Closure $next)
    {
//        if(@auth()->id()) {
//            abort_if(Blocked::where('user_id', @auth()->id())->first(), 403);
//        }
        $is_under_construction = Settings::where('key', 'under_construction_mode')->first();
        $uri = $request->getRequestUri();
        abort_if(
            $is_under_construction->value
            && !in_array($uri, IsUnderConstruction::$allowedUris)
            && @auth()->user()->role != 3
            && @auth()->user()->role != 2,503);

        return $next($request);
    }
}
