<?php

namespace App\Helpers;

use Intervention\Image\Facades\Image;

class Uploads {

    const IMAGE_ENCODING_QUALITY = 90;
    const IMAGE_FORMAT = 'webp';

    public static function uploadImage($path,  $image)
    {
        $imageName = time().'.'.self::IMAGE_FORMAT;

        $filePath = public_path($path);

        $img = Image::make($image->path())->encode(self::IMAGE_FORMAT, self::IMAGE_ENCODING_QUALITY);
        $img->save($filePath.'/'.$imageName);

        return $imageName;
    }
}
