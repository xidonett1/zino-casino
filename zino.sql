-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table docker.bets
CREATE TABLE IF NOT EXISTS `bets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `round_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `bet` double(8,2) NOT NULL DEFAULT '0.00',
  `result` double(8,2) NOT NULL DEFAULT '0.00',
  `game` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_real` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.bets: ~11 rows (approximately)
/*!40000 ALTER TABLE `bets` DISABLE KEYS */;
INSERT IGNORE INTO `bets` (`id`, `user_id`, `round_id`, `balance`, `bet`, `result`, `game`, `is_real`, `created_at`, `updated_at`) VALUES
	(1, 1, 'jfq9jfwq9ijf', 5000.00, 20.00, 0.00, 'european_roulette', 1, '2022-01-31 09:03:05', '2022-01-31 09:04:21'),
	(2, 1, 'jfq9jfwq9ijf', 5000.00, 20.00, 20.00, 'european-roulette', 1, '2022-01-31 09:03:06', '2022-01-31 09:04:22'),
	(3, 1, 'jfq9jfwq9ijf', 5000.00, 20.00, -20.00, 'european_roulette', 1, '2022-01-31 09:03:07', '2022-01-31 09:04:24'),
	(4, 1, 'jfq9jfwq9ijf', 7533.00, 512.00, 0.00, 'european_roulette', 0, '2022-01-31 09:04:16', '2022-01-31 09:04:25'),
	(5, 1, 'jfq9jfwq9ijf', 2334.00, 233.00, 124.00, 'european_roulette', 0, '2022-01-31 09:04:17', '2022-01-31 09:04:26'),
	(6, 1, 'jfq9jfwq9ijf', 5552.00, 502.00, -5532.00, 'european_roulette', 0, '2022-01-31 09:04:18', '2022-01-31 09:04:27'),
	(7, 3, 'jfq9jfwq9ijf', 5000.00, 24.00, 0.00, 'jfq9jfwq9ijf', 1, '2022-02-08 01:07:17', '2022-02-08 09:19:23'),
	(8, 34, 'jfq9jfwq9ijf', 10000.00, 5.00, 4.00, 'european_roulette', 1, '2022-02-07 01:10:10', '2022-02-08 09:19:23'),
	(9, 1, 'fksoakfaso', 7325.00, 100.00, 10000.00, 'european-roulette', 1, '2022-03-21 08:56:30', '2022-03-21 08:57:15'),
	(10, 1, 'fjkasoojfas', 42444.00, 222.00, -1000.00, 'other_region', 1, '2022-03-21 08:57:13', '2022-03-21 08:57:14'),
	(11, 42, 'fjkasoojfasr', 2332.00, 222.00, 32232.00, 'european-roulette', 1, '2022-03-21 13:50:15', '2022-03-21 14:10:00');
/*!40000 ALTER TABLE `bets` ENABLE KEYS */;

-- Dumping structure for table docker.blocked
CREATE TABLE IF NOT EXISTS `blocked` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `new_password` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.blocked: ~0 rows (approximately)
/*!40000 ALTER TABLE `blocked` DISABLE KEYS */;
/*!40000 ALTER TABLE `blocked` ENABLE KEYS */;

-- Dumping structure for table docker.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table docker.games
CREATE TABLE IF NOT EXISTS `games` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `integration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.games: ~2 rows (approximately)
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT IGNORE INTO `games` (`id`, `title`, `image`, `integration`, `slug`, `categories`, `is_enabled`, `created_at`, `updated_at`) VALUES
	(5, 'European Roulette', '1644859643.png', 'http://host1835222.hostland.pro/games/table/', 'european-roulette', '{"is_popular":1,"is_new":1,"is_slot":0,"is_board":0}', 1, '2022-02-14 17:27:24', '2022-02-14 17:27:24'),
	(6, 'Other Region', '1644861758.png', '[[[', 'other-region', '{"is_popular":1,"is_new":1,"is_slot":1,"is_board":1}', 0, '2022-02-14 18:02:38', '2022-02-14 18:02:38');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;

-- Dumping structure for table docker.greeting_templates
CREATE TABLE IF NOT EXISTS `greeting_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.greeting_templates: ~0 rows (approximately)
/*!40000 ALTER TABLE `greeting_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `greeting_templates` ENABLE KEYS */;

-- Dumping structure for table docker.manager_clients
CREATE TABLE IF NOT EXISTS `manager_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.manager_clients: ~5 rows (approximately)
/*!40000 ALTER TABLE `manager_clients` DISABLE KEYS */;
INSERT IGNORE INTO `manager_clients` (`id`, `user_id`, `manager_id`, `created_at`, `updated_at`) VALUES
	(2, 33, 29, '2022-01-17 20:26:31', '2022-01-17 20:26:31'),
	(3, 1, 3, '2022-02-07 21:21:07', '2022-02-07 21:21:10'),
	(4, 34, 3, '2022-02-07 23:09:10', '2022-02-07 23:09:10'),
	(5, 35, 3, '2022-02-08 14:43:14', '2022-02-08 14:43:14'),
	(6, 30, 1, '2022-02-09 15:57:55', NULL);
/*!40000 ALTER TABLE `manager_clients` ENABLE KEYS */;

-- Dumping structure for table docker.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `msg` text COLLATE utf8mb4_unicode_ci,
  `file` text COLLATE utf8mb4_unicode_ci,
  `for` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_answered` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.messages: ~11 rows (approximately)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT IGNORE INTO `messages` (`id`, `user_id`, `is_admin`, `msg`, `file`, `for`, `created_at`, `updated_at`, `is_answered`) VALUES
	(46, 1, 1, 'Здравствуйте, уважаемый игрок!\r\n\r\nbfgf\r\n\r\nС уважением, служба поддержки Casino.', NULL, 'fijijqwij', '2022-01-23 12:11:32', '2022-01-23 12:11:32', 0),
	(49, 1, 1, 'Здравствуйте, уважаемый игрок!\r\n\r\nНовое сообщение\r\n\r\nС уважением, служба поддержки Casino.', NULL, 'logintest', '2022-01-23 13:32:32', '2022-01-23 13:32:32', 0),
	(55, 30, 0, 'ffff', '1642945439.png', 'xidonett', '2022-01-23 13:43:59', '2022-01-23 13:44:24', 1),
	(56, 1, 1, 'Здравствуйте, уважаемый игрок!\r\n\r\nffff\r\n\r\nС уважением, служба поддержки Casino.', NULL, 'testtestov', '2022-01-23 13:44:24', '2022-01-23 13:44:24', 0),
	(59, 1, 1, 'Уважаемый игрок! <br>Казино благодарит вас за быстрый перевод средств. <br> С уважением, <br>Ваше казино', NULL, 'newrefff', '2022-02-02 17:33:54', '2022-02-02 17:33:54', 0),
	(60, 1, 1, 'Уважаемый игрок! <br><br>Казино благодарит вас за быстрый перевод средств. <br><br>С уважением, <br>Ваше казино', NULL, 'newrefff', '2022-02-02 17:34:58', '2022-02-02 17:34:58', 0),
	(61, 1, 1, 'Уважаемый игрок! <br><br>Казино благодарит вас за быстрый перевод средств. <br><br>С уважением, <br>Ваше казино', NULL, 'newrefff', '2022-02-02 17:41:31', '2022-02-02 17:41:31', 0),
	(62, 1, 0, 'Для админа менеджера', '1644261759.png', 'cool', '2022-02-07 19:22:39', '2022-02-07 20:14:02', 1),
	(63, 1, 1, 'Здравствуйте, уважаемый игрок!\r\n\r\n\r\n\r\nС уважением, служба поддержки Casino.', NULL, 'xidonett', '2022-02-07 20:14:02', '2022-02-07 20:14:02', 0),
	(64, 1, 1, 'Уважаемый игрок! <br><br>Казино благодарит вас за быстрый перевод средств. <br><br>С уважением, <br>Ваше казино', NULL, 'xidonett', '2022-02-11 20:42:13', '2022-02-11 20:42:13', 0),
	(65, 42, 0, 'پیام متنیپیام متنیپیام متنی', '1647774721.webp', 'xidonett', '2022-03-20 11:12:01', '2022-03-20 11:12:01', 0);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Dumping structure for table docker.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.migrations: ~26 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2021_11_28_035113_create_users_personal_data_table', 1),
	(6, '2021_11_28_184414_create_user_balance_table', 1),
	(10, '2021_12_08_055437_create_pages_table', 2),
	(11, '2021_12_10_030713_create_refs_table', 3),
	(12, '2021_12_10_070126_create_settings_table', 4),
	(13, '2021_12_11_021927_create_manager_clients_table', 5),
	(14, '2021_12_13_225010_create_ticket_templates_table', 6),
	(15, '2021_12_13_225152_create_ticket_categories_table', 7),
	(16, '2021_12_14_134807_create_self_created_account_table', 8),
	(18, '2021_12_01_021611_create_transactions_table', 9),
	(20, '2021_12_16_140056_create_unfortunate_withdrawals_table', 11),
	(21, '2021_12_20_204439_create_greeting_templates_table', 12),
	(22, '2021_12_22_044103_create_blocked_table', 12),
	(23, '2021_12_22_053100_create_user_notes_table', 13),
	(24, '2022_01_07_152148_create_messages_table', 14),
	(25, '2022_01_10_165524_create_user_locations_table', 15),
	(29, '2022_01_21_012531_alter_is_answered_messages', 16),
	(30, '2022_01_21_042855_alter_new_password_blocked', 17),
	(31, '2022_01_31_034658_create_bets_table', 18),
	(34, '2022_02_14_132515_create_games_table', 19),
	(36, '2022_03_16_032706_create_tournament_participants_table', 21),
	(37, '2022_03_16_031421_create_tournaments_table', 22);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table docker.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.pages: ~12 rows (approximately)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT IGNORE INTO `pages` (`id`, `title`, `slug`, `content`, `category`, `position`, `thumbnail`, `created_at`, `updated_at`) VALUES
	(1, '{"ir":"\\u0628\\u0627\\u0632\\u06cc \\u0645\\u0633\\u0626\\u0648\\u0644\\u0627\\u0646\\u0647","en":"Responsible game"}', 'responsible-game', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u0628\\u0627\\u0632\\u06cc \\u0645\\u0633\\u0626\\u0648\\u0644\\u0627\\u0646\\u0647<\\/span><br><\\/p>","en":"<p><span style=\\"font-size: 14.4px;\\">Responsible game<\\/span><br><\\/p>"}', 'information', 0, NULL, '2021-12-10 00:07:52', '2022-03-11 18:01:18'),
	(2, '{"ir":"\\u062d\\u0631\\u06cc\\u0645 \\u062e\\u0635\\u0648\\u0635\\u06cc","en":"Privacy"}', 'privacy', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u062d\\u0631\\u06cc\\u0645 \\u062e\\u0635\\u0648\\u0635\\u06cc<\\/span><br><\\/p>","en":"<p><span style=\\"font-size: 14.4px;\\">Privacy<\\/span><br><\\/p>"}', 'information', 0, NULL, '2021-12-10 00:08:19', '2022-03-11 18:01:04'),
	(3, '{"ir":"\\u0634\\u0631\\u0627\\u06cc\\u0637 \\u0627\\u0633\\u062a\\u0641\\u0627\\u062f\\u0647","en":"Terms of use"}', 'terms-of-use', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u0634\\u0631\\u0627\\u06cc\\u0637 \\u0627\\u0633\\u062a\\u0641\\u0627\\u062f\\u0647<\\/span><br><\\/p>","en":"<p><span style=\\"font-size: 14.4px;\\">Terms of use<\\/span><br><\\/p>"}', 'information', 0, NULL, '2021-12-10 00:08:54', '2022-03-11 18:00:40'),
	(4, '{"ir":"\\u0645\\u062c\\u0648\\u0632","en":"License"}', 'license', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u0645\\u062c\\u0648\\u0632<\\/span><br><\\/p>","en":"<p><span style=\\"font-size: 14.4px;\\">License<\\/span><br><\\/p>"}', 'information', 0, NULL, '2021-12-10 00:09:22', '2022-03-11 18:00:27'),
	(5, '{"ir":"\\u0628\\u0631\\u0631\\u0633\\u06cc \\u0627\\u062c\\u0645\\u0627\\u0644\\u06cc","en":"Overview"}', 'overview', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u0628\\u0631\\u0631\\u0633\\u06cc \\u0627\\u062c\\u0645\\u0627\\u0644\\u06cc<\\/span><br><\\/p>","en":"<p><span style=\\"font-size: 14.4px;\\">Overview<\\/span><br><\\/p>"}', 'safety', 0, NULL, '2021-12-10 00:20:48', '2022-03-11 18:00:06'),
	(6, '{"ir":"\\u0627\\u0645\\u0646\\u06cc\\u062a","en":"Safety"}', 'safety', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u0627\\u0645\\u0646\\u06cc\\u062a<\\/span><br><\\/p>","en":"<p><span style=\\"font-size: 14.4px;\\">Safety<\\/span><br><\\/p>"}', 'safety', 0, NULL, '2021-12-10 00:21:20', '2022-03-11 17:59:43'),
	(7, '{"ir":"\\u0642\\u0627\\u0628\\u0644\\u06cc\\u062a \\u0627\\u0637\\u0645\\u06cc\\u0646\\u0627\\u0646","en":"Reliability"}', 'reliability', '{"ir":"<font color=\\"#222222\\" face=\\"Source Sans Pro, sans-serif\\"><span style=\\"font-size: 16px;\\">\\u0642\\u0627\\u0628\\u0644\\u06cc\\u062a \\u0627\\u0637\\u0645\\u06cc\\u0646\\u0627\\u0646<\\/span><\\/font><br>","en":"<span style=\\"font-size: 14.4px;\\">Reliability<\\/span>"}', 'safety', 0, NULL, '2021-12-10 00:21:57', '2022-03-11 17:59:27'),
	(8, '{"ir":"\\u0630\\u062e\\u06cc\\u0631\\u0647 \\u0633\\u0627\\u0632\\u06cc \\u0627\\u0645\\u0646 \\u062f\\u0627\\u062f\\u0647 \\u0647\\u0627","en":"Safe data storage"}', 'safe-data-storage', '{"ir":"<p>\\u0630\\u062e\\u06cc\\u0631\\u0647 \\u0633\\u0627\\u0632\\u06cc \\u0627\\u0645\\u0646 \\u062f\\u0627\\u062f\\u0647 \\u0647\\u0627<\\/p>","en":"<p>fqwqfwfqw<\\/p>"}', 'safety', 0, NULL, '2021-12-10 00:22:21', '2022-03-11 17:59:03'),
	(9, '{"ir":"\\u0642\\u0637\\u0639\\u06cc \\u0647\\u0627\\u06cc \\u0628\\u0631\\u0646\\u0627\\u0645\\u0647 \\u0631\\u06cc\\u0632\\u06cc \\u0634\\u062f\\u0647","en":"Scheduled outages"}', 'scheduled-outages', '{"ir":"<span style=\\"font-size: 14.4px;\\">\\u0642\\u0637\\u0639\\u06cc \\u0647\\u0627\\u06cc \\u0628\\u0631\\u0646\\u0627\\u0645\\u0647 \\u0631\\u06cc\\u0632\\u06cc \\u0634\\u062f\\u0647<\\/span><br>","en":"<span style=\\"font-size: 14.4px;\\">Scheduled outages<\\/span>"}', 'support', 0, NULL, '2021-12-10 00:22:54', '2022-03-11 17:58:40'),
	(10, '{"ir":"\\u0645\\u062e\\u0627\\u0637\\u0628","en":"Contacts"}', 'contacts', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u0628\\u0627\\u0632\\u06cc \\u0645\\u0633\\u0626\\u0648\\u0644\\u0627\\u0646\\u0647<\\/span><br><\\/p>","en":"<p><span style=\\"font-size: 14.4px;\\">Responsible game<\\/span><br><\\/p>"}', 'support', 0, NULL, '2021-12-10 00:23:15', '2022-03-11 17:58:07'),
	(22, '{"ir":"\\u0630\\u062e\\u06cc\\u0631\\u0647 \\u0633\\u0627\\u0632\\u06cc \\u0627\\u0645\\u0646 \\u062f\\u0627\\u062f\\u0647 \\u0647\\u0627","en":"New post"}', 'new-post', '{"ir":"<p>\\u0630\\u062e\\u06cc\\u0631\\u0647 \\u0633\\u0627\\u0632\\u06cc \\u0627\\u0645\\u0646 \\u062f\\u0627\\u062f\\u0647 \\u0647\\u0627<\\/p>","en":"<p>News post<\\/p>"}', 'news', 0, '1647724944.png', '2022-03-19 21:11:44', '2022-03-20 00:40:05'),
	(23, '{"ir":"\\u0630\\u062e\\u06cc\\u0631\\u0647 \\u0633\\u0627\\u0632\\u06cc \\u0627\\u0645\\u0646 \\u062f\\u0627\\u062f\\u0647 \\u0647\\u0627","en":"New"}', 'new', '{"ir":"<p>\\u0630\\u062e\\u06cc\\u0631\\u0647 \\u0633\\u0627\\u0632\\u06cc \\u0627\\u0645\\u0646 \\u062f\\u0627\\u062f\\u0647 \\u0647\\u0627<\\/p>","en":"<p>fsafasfasfas<\\/p>"}', 'news', 0, '1647734436.webp', '2022-03-19 21:22:45', '2022-03-20 00:39:55');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table docker.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table docker.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.personal_access_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

-- Dumping structure for table docker.refs
CREATE TABLE IF NOT EXISTS `refs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `ref` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.refs: ~7 rows (approximately)
/*!40000 ALTER TABLE `refs` DISABLE KEYS */;
INSERT IGNORE INTO `refs` (`id`, `user_id`, `ref`, `created_at`, `updated_at`) VALUES
	(2, 2, '3o5jw71d', '2021-12-10 04:30:06', '2021-12-10 04:30:06'),
	(3, 3, '1gz4o3r4', '2021-12-10 04:57:22', '2021-12-10 04:57:22'),
	(4, 5, NULL, '2021-12-20 01:08:51', '2021-12-20 01:08:51'),
	(5, 20, 'zwwt2guc', '2022-01-10 19:33:01', '2022-01-10 19:33:01'),
	(6, 1, 'e2zhrmbz', '2022-01-12 10:02:16', '2022-01-12 10:02:16'),
	(7, 7, 'm7dwvwqs', '2022-01-12 10:03:17', '2022-01-12 10:03:17'),
	(8, 29, 'laifb1n5', '2022-01-17 19:52:37', '2022-01-17 19:52:37');
/*!40000 ALTER TABLE `refs` ENABLE KEYS */;

-- Dumping structure for table docker.self_created_account
CREATE TABLE IF NOT EXISTS `self_created_account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.self_created_account: ~21 rows (approximately)
/*!40000 ALTER TABLE `self_created_account` DISABLE KEYS */;
INSERT IGNORE INTO `self_created_account` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 5, '2021-12-14 14:55:11', '2021-12-14 14:55:11'),
	(2, 6, '2021-12-16 17:37:09', '2021-12-16 17:37:09'),
	(3, 7, '2021-12-20 02:06:20', '2021-12-20 02:06:20'),
	(4, 8, '2021-12-20 02:19:53', '2021-12-20 02:19:53'),
	(5, 9, '2021-12-20 02:25:31', '2021-12-20 02:25:31'),
	(6, 10, '2021-12-20 02:37:48', '2021-12-20 02:37:48'),
	(7, 16, '2021-12-23 00:49:46', '2021-12-23 00:49:46'),
	(8, 17, '2022-01-07 16:27:28', '2022-01-07 16:27:28'),
	(9, 18, '2022-01-10 17:02:46', '2022-01-10 17:02:46'),
	(10, 19, '2022-01-10 17:05:33', '2022-01-10 17:05:33'),
	(11, 20, '2022-01-10 17:06:50', '2022-01-10 17:06:50'),
	(12, 26, '2022-01-17 15:57:44', '2022-01-17 15:57:44'),
	(13, 27, '2022-01-17 16:05:33', '2022-01-17 16:05:33'),
	(14, 29, '2022-01-17 19:51:47', '2022-01-17 19:51:47'),
	(15, 30, '2022-01-17 20:22:15', '2022-01-17 20:22:15'),
	(16, 31, '2022-01-17 20:23:27', '2022-01-17 20:23:27'),
	(17, 33, '2022-01-17 20:26:31', '2022-01-17 20:26:31'),
	(18, 34, '2022-01-21 04:04:13', '2022-01-21 04:04:13'),
	(19, 34, '2022-02-07 23:09:11', '2022-02-07 23:09:11'),
	(20, 35, '2022-02-08 14:43:15', '2022-02-08 14:43:15'),
	(21, 41, '2022-03-12 18:48:06', '2022-03-12 18:48:06'),
	(22, 42, '2022-03-14 23:38:18', '2022-03-14 23:38:18');
/*!40000 ALTER TABLE `self_created_account` ENABLE KEYS */;

-- Dumping structure for table docker.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.settings: ~8 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT IGNORE INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'min_withdrawal_amount', '1500', '2021-12-10 09:06:05', '2022-02-14 11:56:04'),
	(2, 'client_withdrawal_cancellation', '1', NULL, '2022-02-14 11:56:04'),
	(3, 'min_withdrawal_time', '40', NULL, '2022-02-14 11:56:04'),
	(4, 'managers_ref_creation_permit', '1', NULL, '2022-02-14 11:56:04'),
	(5, 'under_construction_mode', '0', NULL, '2022-02-14 11:56:04'),
	(6, 'client_messages_allowed', '1', '2022-01-07 16:20:52', '2022-02-14 11:56:04'),
	(7, 'min_to_withdraw_tab_amount', '4000', NULL, '2022-02-14 11:56:04'),
	(8, 'support_email', 'support@casino.com', '2022-02-14 13:51:19', '2022-02-14 11:56:04');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table docker.ticket_categories
CREATE TABLE IF NOT EXISTS `ticket_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.ticket_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `ticket_categories` DISABLE KEYS */;
INSERT IGNORE INTO `ticket_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(4, 'Test category', '2021-12-14 04:40:51', '2022-03-11 18:29:21');
/*!40000 ALTER TABLE `ticket_categories` ENABLE KEYS */;

-- Dumping structure for table docker.ticket_templates
CREATE TABLE IF NOT EXISTS `ticket_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.ticket_templates: ~0 rows (approximately)
/*!40000 ALTER TABLE `ticket_templates` DISABLE KEYS */;
INSERT IGNORE INTO `ticket_templates` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(3, 'Test template', '2021-12-14 04:47:30', '2022-03-11 18:26:48');
/*!40000 ALTER TABLE `ticket_templates` ENABLE KEYS */;

-- Dumping structure for table docker.tournaments
CREATE TABLE IF NOT EXISTS `tournaments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `games` text COLLATE utf8mb4_unicode_ci,
  `prizes` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_available` int(11) NOT NULL DEFAULT '0',
  `from` timestamp NULL DEFAULT NULL,
  `until` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.tournaments: ~3 rows (approximately)
/*!40000 ALTER TABLE `tournaments` DISABLE KEYS */;
INSERT IGNORE INTO `tournaments` (`id`, `title`, `games`, `prizes`, `description`, `thumbnail`, `slug`, `is_available`, `from`, `until`, `created_at`, `updated_at`) VALUES
	(9, '{"ir":"\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646","en":"New tournament"}', '["European Roulette [european-roulette]","Other Region [other-region]"]', '{"ir":"[\\"10 BTC\\",\\"5 BTC\\"]","en":"[\\"New prize\\",\\"5 TRX\\"]"}', '{"ir":"<ul><li><span style=\\"font-size: 14.4px;\\">\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646<\\/span><br><\\/li><\\/ul>","en":"<p>Tournament short description test<\\/p>"}', '1647789990.webp', 'new-tournament', 1, NULL, NULL, '2022-03-18 16:10:35', '2022-03-21 11:26:27'),
	(10, '{"ir":"\\u062f\\u0648\\u0645\\u06cc\\u0646","en":"Test changed"}', '["European Roulette [european-roulette]","Other Region [other-region]"]', '{"ir":"[\\"\\u062f\\u0648\\u0645\\u06cc\\u0646\\",\\"\\u062f\\u0648\\u0645\\u06cc\\u06462\\",\\"\\u062f\\u0648\\u0645\\u06cc\\u06463\\"]","en":"[\\"Test\\",\\"Test2\\",\\"Test3\\"]"}', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646<\\/span><br><\\/p>","en":"<p>Tournament desc<\\/p>"}', '1647734654.webp', 'test-changed', 0, NULL, NULL, '2022-03-19 20:47:20', '2022-03-20 00:04:15'),
	(11, '{"ir":"\\u062f\\u0648\\u0645\\u06cc\\u0646","en":"Fair Play"}', '["European Roulette [european-roulette]"]', '{"ir":"[\\"\\u062f\\u0648\\u0645\\u06cc\\u0646\\",\\"\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\",\\"\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\"]","en":"[\\"10 BTC\\",\\"iPhone X\\",\\"Tesla Model X\\"]"}', '{"ir":"<p><span style=\\"font-size: 14.4px;\\">\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646\\u062f\\u0648\\u0645\\u06cc\\u0646<\\/span><br><\\/p>","en":"<p>Play and win a lot of prizes! Tesla Model X, iPhone X and 10 BTC for TOP-3 players!<\\/p>"}', '1647862291.webp', 'fair-play', 0, NULL, NULL, '2022-03-21 11:31:31', '2022-03-21 11:36:09');
/*!40000 ALTER TABLE `tournaments` ENABLE KEYS */;

-- Dumping structure for table docker.tournament_participants
CREATE TABLE IF NOT EXISTS `tournament_participants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.tournament_participants: ~3 rows (approximately)
/*!40000 ALTER TABLE `tournament_participants` DISABLE KEYS */;
INSERT IGNORE INTO `tournament_participants` (`id`, `tournament_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 9, 1, '2022-03-21 05:44:50', '2022-03-21 05:44:50'),
	(2, 11, 1, '2022-03-21 11:32:52', '2022-03-21 11:32:52'),
	(3, 9, 42, '2022-03-21 11:48:40', '2022-03-21 11:48:40');
/*!40000 ALTER TABLE `tournament_participants` ENABLE KEYS */;

-- Dumping structure for table docker.transactions
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `payment_system` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credentials` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.transactions: ~127 rows (approximately)
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT IGNORE INTO `transactions` (`id`, `user_id`, `amount`, `payment_system`, `credentials`, `type`, `status`, `created_at`, `updated_at`) VALUES
	(2, 5, 31412.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-15 23:36:44', '2021-12-16 00:42:25'),
	(8, 5, 1001.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-16 00:42:43', '2021-12-16 00:47:05'),
	(9, 0, 160.00, 'SWIFT', 'SWIFT', 'swift', 0, '2021-12-16 00:45:06', '2021-12-16 00:45:06'),
	(10, 5, 110.00, 'SWIFT', 'SWIFT', 'swift', 0, '2021-12-16 00:46:38', '2021-12-16 00:46:38'),
	(11, 5, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 00:47:01', '2021-12-16 00:47:01'),
	(12, 5, 13213.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-16 00:49:49', '2021-12-16 00:49:54'),
	(13, 5, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 00:49:54', '2021-12-16 00:49:54'),
	(14, 5, 4124.00, 'PerfectMoney', 'fwqfwqfqw', 'withdraw', 2, '2021-12-16 01:35:01', '2021-12-16 01:35:05'),
	(15, 5, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 01:35:05', '2021-12-16 01:35:05'),
	(16, 5, 536.12, 'TAX', 'TAX', 'tax', 2, '2021-12-16 01:41:51', '2021-12-16 01:41:51'),
	(17, 5, 536.12, 'TAX', 'TAX', 'tax', 2, '2021-12-16 01:42:03', '2021-12-16 01:42:03'),
	(18, 5, 536.12, 'TAX', 'TAX', 'tax', 2, '2021-12-16 01:42:05', '2021-12-16 01:42:05'),
	(19, 5, 536.12, 'TAX', 'TAX', 'tax', 2, '2021-12-16 01:42:07', '2021-12-16 01:42:07'),
	(20, 1, 1500.01, 'PerfectMoney', 'edited_new', 'withdraw', 2, '2021-12-16 03:19:19', '2022-02-03 02:42:58'),
	(21, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 03:47:16', '2021-12-16 03:47:16'),
	(22, 1, 195.00, 'TAX', 'TAX', 'tax', 2, '2021-12-16 03:47:19', '2021-12-16 03:47:19'),
	(23, 1, 2000.00, 'PerfectMoney', 'ffff3121244121', 'withdraw', 2, '2021-12-16 03:54:01', '2021-12-16 03:57:33'),
	(24, 1, 0.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 03:57:33', '2021-12-16 03:57:33'),
	(25, 1, 260.00, 'TAX', 'TAX', 'tax', 2, '2021-12-16 03:59:23', '2021-12-16 03:59:23'),
	(26, 1, 2000.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-16 03:59:36', '2021-12-16 03:59:41'),
	(27, 1, 0.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 03:59:41', '2021-12-16 03:59:41'),
	(28, 1, 260.00, 'TAX', 'TAX', 'tax', 2, '2021-12-16 04:00:17', '2021-12-16 04:00:17'),
	(29, 1, 10003.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-16 04:00:25', '2021-12-16 04:00:29'),
	(30, 1, 0.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 04:00:29', '2021-12-16 04:00:29'),
	(31, 1, 1300.39, 'TAX', 'TAX', 'tax', 2, '2021-12-16 04:00:32', '2021-12-16 04:00:32'),
	(32, 1, 12000.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-16 04:00:47', '2021-12-16 04:01:03'),
	(33, 1, 0.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 04:01:03', '2021-12-16 04:01:03'),
	(34, 1, 1560.00, 'TAX', 'TAX', 'tax', 2, '2021-12-16 04:01:07', '2021-12-16 04:01:07'),
	(35, 1, 2444.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-16 04:01:38', '2021-12-16 04:02:08'),
	(36, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 04:02:08', '2021-12-16 04:02:08'),
	(37, 1, 317.72, 'TAX', 'TAX', 'tax', 2, '2021-12-16 04:02:11', '2021-12-16 04:02:11'),
	(38, 1, 4333.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-16 17:32:16', '2021-12-16 17:32:33'),
	(39, 1, 12342.00, 'PerfectMoney', 'fwqfwqfqw', 'withdraw', 2, '2021-12-16 17:35:25', '2021-12-16 17:35:28'),
	(40, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 17:35:28', '2021-12-16 17:35:28'),
	(41, 1, 1604.46, 'TAX', 'TAX', 'tax', 2, '2021-12-16 17:35:32', '2021-12-16 17:35:32'),
	(42, 1, 12002.00, 'PerfectMoney', 'ffff3121244121', 'withdraw', 2, '2021-12-16 17:35:49', '2021-12-16 17:35:55'),
	(43, 1, 10002.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-16 17:36:39', '2021-12-16 17:36:41'),
	(44, 6, 1501.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-16 17:37:23', '2021-12-16 17:47:06'),
	(45, 6, 2133.00, 'PerfectMoney', 'ffff3121244121', 'withdraw', 0, '2021-12-16 17:46:25', '2021-12-16 17:46:25'),
	(46, 6, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-16 17:47:06', '2021-12-16 17:47:06'),
	(47, 6, 277.29, 'TAX', 'TAX', 'tax', 2, '2021-12-16 17:47:09', '2021-12-16 17:47:09'),
	(48, 6, 2222.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-16 17:47:16', '2021-12-16 17:47:16'),
	(49, 6, 12234.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-16 17:48:12', '2021-12-16 17:48:12'),
	(50, 5, 1600.00, 'Perfect Money', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 01:26:26', '2021-12-20 01:27:12'),
	(51, 1, 1501.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 02:08:06', '2021-12-20 02:08:06'),
	(52, 1, 2000.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 02:09:15', '2021-12-20 02:09:15'),
	(53, 1, 4325.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 02:09:49', '2021-12-20 02:09:49'),
	(54, 1, 3233.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 02:10:26', '2021-12-20 02:10:26'),
	(55, 1, 3233.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 02:11:01', '2021-12-20 02:11:01'),
	(56, 1, 3233.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 02:11:18', '2021-12-20 02:11:18'),
	(57, 9, 12442.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2021-12-20 02:36:56', '2021-12-20 02:37:12'),
	(58, 9, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2021-12-20 02:37:12', '2021-12-20 02:37:12'),
	(59, 9, 1617.46, 'TAX', 'TAX', 'tax', 2, '2021-12-20 02:37:15', '2021-12-20 02:37:15'),
	(60, 10, 2000.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 0, '2021-12-20 02:37:59', '2021-12-20 02:37:59'),
	(61, 15, 4000.00, 'Perfect Money', NULL, NULL, 1, '2021-12-22 07:35:58', '2021-12-22 07:35:58'),
	(62, 15, 7000.00, 'Perfect Money', NULL, 'withdrawal', 1, '2021-12-22 07:37:09', '2021-12-22 07:37:09'),
	(63, 15, 2500.00, 'Perfect Money', NULL, 'deposit', 2, '2021-12-22 08:28:24', '2021-12-22 08:28:24'),
	(64, 14, 5000.00, 'Perfect Money', NULL, 'deposit', 2, '2021-12-22 08:29:36', '2021-12-22 08:29:36'),
	(65, 15, 8000.00, 'Perfect Money', NULL, 'deposit', 2, '2021-12-22 08:29:48', '2021-12-22 08:29:48'),
	(66, 10, 1200.00, 'Perfect Money', NULL, 'deposit', 2, '2021-12-22 08:33:46', '2021-12-22 08:33:46'),
	(67, 16, 1200.00, 'Perfect Money', NULL, 'withdrawal', 1, '2021-12-23 02:23:01', '2021-12-23 02:23:01'),
	(68, 16, 10.00, 'Perfect Money', NULL, 'deposit', 2, '2021-12-23 03:06:43', '2021-12-23 03:06:43'),
	(69, 10, 4444.00, 'Perfect Money', NULL, 'deposit', 2, '2021-12-23 03:17:56', '2021-12-23 03:17:56'),
	(70, 11, 5555.00, 'Perfect Money', NULL, 'withdrawal', 1, '2021-12-23 03:18:03', '2021-12-23 03:18:03'),
	(71, 3, 1200.00, 'Perfect Money', NULL, 'withdrawal', 1, '2021-12-23 03:22:34', '2021-12-23 03:22:34'),
	(72, 1, 5555.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2022-01-10 20:52:01', '2022-01-10 20:52:07'),
	(73, 1, 110.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-01-10 20:52:07', '2022-01-10 20:52:07'),
	(74, 1, 722.15, 'TAX', 'TAX', 'tax', 2, '2022-01-10 20:52:11', '2022-01-10 20:52:11'),
	(75, 1, 5555.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2022-01-10 20:52:26', '2022-01-12 07:52:10'),
	(76, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-01-12 07:52:10', '2022-01-12 07:52:10'),
	(77, 1, 722.15, 'TAX', 'TAX', 'tax', 2, '2022-01-12 07:52:13', '2022-01-12 07:52:13'),
	(78, 1, 4425.00, 'PerfectMoney', 'fwqfwqfqw', 'withdraw', 0, '2022-01-23 10:30:05', '2022-01-23 10:30:18'),
	(79, 1, 1500.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2022-01-23 10:46:15', '2022-01-23 10:46:29'),
	(80, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-01-23 10:46:29', '2022-01-23 10:46:29'),
	(81, 1, 195.00, 'TAX', 'TAX', 'tax', 2, '2022-01-23 10:46:40', '2022-01-23 10:46:40'),
	(82, 1, 2002.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2022-01-23 10:51:45', '2022-01-23 10:52:27'),
	(83, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-01-23 10:52:27', '2022-01-23 10:52:27'),
	(84, 1, 260.26, 'TAX', 'TAX', 'tax', 2, '2022-01-23 10:52:30', '2022-02-06 14:41:02'),
	(85, 6, 1000.00, 'Perfect Money', NULL, 'withdrawal', 2, '2022-02-02 17:21:36', '2022-02-05 06:28:07'),
	(86, 1, 2000.00, 'Perfect Money', 'xyz112412-44--125195090512121dsafasvasff', 'withdrawal', 2, '2022-02-02 17:33:06', '2022-02-06 14:41:02'),
	(87, 33, 2555.00, 'Perfect Money', NULL, 'withdrawal', 1, '2022-02-02 17:33:54', '2022-02-02 17:33:54'),
	(88, 33, 2500.00, 'Perfect Money', NULL, 'withdrawal', 1, '2022-02-02 17:34:58', '2022-02-02 17:34:58'),
	(89, 33, 2500.00, 'Perfect Money', 'xyz112412-44--125195090512121dsafasvasff', 'deposit', 2, '2022-02-02 17:38:59', '2022-02-05 05:44:53'),
	(90, 33, 5000.00, 'Perfect Money', 'xyz112412-44--125195090512121dsafasvasff', 'deposit', 2, '2022-02-02 17:41:31', '2022-02-05 05:44:59'),
	(91, 6, 1500.00, 'PerfectMoney', 'fwqfwqfqw', 'withdraw', 2, '2022-02-05 06:27:54', '2022-02-05 06:28:11'),
	(92, 6, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-02-05 06:28:07', '2022-02-05 06:28:07'),
	(93, 6, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-02-05 06:28:11', '2022-02-05 06:28:11'),
	(94, 6, 195.00, 'TAX', 'TAX', 'tax', 2, '2022-02-05 06:28:19', '2022-02-05 06:28:19'),
	(95, 6, 195.00, 'TAX', 'TAX', 'tax', 2, '2022-02-05 06:28:22', '2022-02-05 06:28:22'),
	(96, 1, 2341.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'deposit', 2, '2022-02-05 06:37:14', '2022-02-05 06:37:14'),
	(97, 6, 1500.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'withdraw', 2, '2022-02-05 06:45:11', '2022-02-05 06:45:15'),
	(98, 6, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-02-05 06:45:15', '2022-02-05 06:45:15'),
	(99, 6, 195.00, 'TAX', 'TAX', 'tax', 2, '2022-02-05 06:45:17', '2022-02-05 06:45:17'),
	(100, 6, 5555.00, 'PerfectMoney', 'xyz112412-44--125195090512121dsafasvasff', 'deposit', 2, '2022-02-05 06:49:06', '2022-02-05 06:49:06'),
	(101, 1, 3333.00, 'PerfectMoney', 'u32412421', 'deposit', 2, '2022-02-07 14:12:11', '2022-02-07 14:12:11'),
	(102, 1, 3333.00, 'PerfectMoney', 'U773214', 'deposit', 2, '2022-02-07 14:25:59', '2022-02-07 14:25:59'),
	(103, 1, 2000.00, 'PerfectMoney', 'U3214421', 'deposit', 2, '2022-02-08 09:33:24', NULL),
	(104, 1, 0.10, 'PerfectMoney', 'U35414899', 'deposit', 2, '2022-02-09 17:46:44', '2022-02-09 17:46:44'),
	(105, 1, 0.10, 'PerfectMoney', 'U35414899', 'deposit', 2, '2022-02-09 17:47:29', '2022-02-09 17:47:29'),
	(106, 1, 0.10, 'PerfectMoney', 'U35414899', 'deposit', 2, '2022-02-09 17:50:41', '2022-02-09 17:50:41'),
	(107, 1, 0.10, 'PerfectMoney', 'U35414899', 'deposit', 2, '2022-02-09 17:51:01', '2022-02-09 17:51:01'),
	(108, 1, 0.10, 'PerfectMoney', 'U35414899', 'deposit', 2, '2022-02-09 17:52:29', '2022-02-09 17:52:29'),
	(109, 1, 0.01, 'PerfectMoney', 'U35414899', 'deposit', 2, '2022-02-09 18:03:50', '2022-02-09 18:03:50'),
	(110, 1, 2222.00, 'PerfectMoney', 'Транзакция не удалась.', 'deposit', 0, '2022-02-09 18:04:23', '2022-02-09 18:04:23'),
	(111, 1, 2222.00, 'PerfectMoney', 'Транзакция не удалась.', 'deposit', 0, '2022-02-09 18:06:36', '2022-02-09 18:06:36'),
	(112, 1, 5000.00, 'Perfect Money', NULL, 'withdrawal', 2, '2022-02-11 20:42:13', '2022-02-11 20:42:28'),
	(113, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-02-11 20:42:28', '2022-02-11 20:42:28'),
	(114, 1, 260.26, 'TAX', 'TAX', 'tax', 2, '2022-02-11 20:43:12', '2022-02-11 20:43:12'),
	(115, 1, 2005.00, 'PerfectMoney', 'U773214', 'withdraw', 2, '2022-02-11 20:46:38', '2022-02-11 20:46:43'),
	(116, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-02-11 20:46:43', '2022-02-11 20:46:43'),
	(117, 1, 260.65, 'TAX', 'TAX', 'tax', 2, '2022-02-11 20:46:55', '2022-02-11 20:46:55'),
	(118, 1, 2133.00, 'PerfectMoney', 'Транзакция не удалась.', 'deposit', 0, '2022-02-11 20:47:34', '2022-02-11 20:47:34'),
	(119, 1, 1500.00, 'PerfectMoney', 'U773214', 'withdraw', 2, '2022-02-14 18:26:16', '2022-02-14 18:26:38'),
	(120, 1, 160.00, 'SWIFT', 'SWIFT', 'swift', 2, '2022-02-14 18:26:38', '2022-02-14 18:26:38'),
	(121, 1, 195.00, 'TAX', 'TAX', 'tax', 2, '2022-02-14 18:26:43', '2022-02-14 18:26:43'),
	(122, 1, 2444.00, 'PerfectMoney', 'U773214', 'withdraw', 1, '2022-02-14 21:14:30', '2022-02-14 21:14:30'),
	(123, 1, 1500.00, 'PerfectMoney', 'U773214', 'withdraw', 1, '2022-02-14 22:15:07', '2022-02-14 22:15:07'),
	(124, 1, 1777.00, 'PerfectMoney', 'U773214', 'withdraw', 1, '2022-02-14 22:16:55', '2022-02-14 22:16:55'),
	(125, 1, 160.00, 'PerfectMoney', 'Транзакция не удалась.', 'swift', 0, '2022-02-14 22:17:12', '2022-02-14 22:17:12'),
	(126, 1, 160.00, 'PerfectMoney', 'Транзакция не удалась.', 'swift', 0, '2022-02-14 22:18:01', '2022-02-14 22:18:01'),
	(127, 1, 160.00, 'PerfectMoney', 'Транзакция не удалась.', 'swift', 0, '2022-02-14 22:18:34', '2022-02-14 22:18:34'),
	(128, 1, 160.00, 'PerfectMoney', 'Транзакция не удалась.', 'swift', 0, '2022-02-14 22:19:30', '2022-02-14 22:19:30'),
	(129, 1, 160.00, 'PerfectMoney', 'Транзакция не удалась.', 'swift', 0, '2022-02-14 22:20:18', '2022-02-14 22:20:18'),
	(130, 1, 160.00, 'PerfectMoney', 'Транзакция не удалась.', 'swift', 0, '2022-02-14 22:22:47', '2022-02-14 22:22:47'),
	(131, 1, 160.00, 'PerfectMoney', 'Транзакция не удалась.', 'swift', 0, '2022-02-14 22:23:50', '2022-02-14 22:23:50'),
	(132, 1, 231.01, 'PerfectMoney', 'Транзакция не удалась.', 'tax', 0, '2022-02-15 10:02:09', '2022-02-15 10:02:09'),
	(133, 1, 160.00, 'PerfectMoney', 'Unfortunate transaction.', 'swift', 0, '2022-03-20 12:37:36', '2022-03-20 12:37:36');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;

-- Dumping structure for table docker.unfortunate_withdrawals
CREATE TABLE IF NOT EXISTS `unfortunate_withdrawals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `time_from_registration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.unfortunate_withdrawals: ~11 rows (approximately)
/*!40000 ALTER TABLE `unfortunate_withdrawals` DISABLE KEYS */;
INSERT IGNORE INTO `unfortunate_withdrawals` (`id`, `user_id`, `time_from_registration`, `created_at`, `updated_at`) VALUES
	(1, 6, '38', '2021-12-16 17:46:25', '2021-12-16 17:46:25'),
	(2, 6, '38', '2021-12-16 17:47:16', '2021-12-16 17:47:16'),
	(3, 6, '38', '2021-12-16 17:48:12', '2021-12-16 17:48:12'),
	(4, 5, '170', '2021-12-20 01:26:26', '2021-12-20 01:26:26'),
	(5, 1, '476', '2021-12-20 02:08:06', '2021-12-20 02:08:06'),
	(6, 1, '476', '2021-12-20 02:09:15', '2021-12-20 02:09:15'),
	(7, 1, '476', '2021-12-20 02:09:49', '2021-12-20 02:09:49'),
	(8, 1, '476', '2021-12-20 02:10:26', '2021-12-20 02:10:26'),
	(9, 1, '476', '2021-12-20 02:11:01', '2021-12-20 02:11:01'),
	(10, 1, '476', '2021-12-20 02:11:18', '2021-12-20 02:11:18'),
	(11, 10, '41', '2021-12-20 02:37:59', '2021-12-20 02:37:59');
/*!40000 ALTER TABLE `unfortunate_withdrawals` ENABLE KEYS */;

-- Dumping structure for table docker.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `registration_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.users: ~27 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `login`, `avatar`, `email`, `email_verified_at`, `password`, `role`, `registration_password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'xidonett', '1647734782.webp', 'xidonett@gmail.com', NULL, '$2y$10$1tCTC49NPRR0cUtkYbGX7..TGHnuOGtYceTu4K.6OdMvPdIJOFGeC', 3, NULL, NULL, '2021-12-01 21:31:14', '2022-03-20 00:06:22'),
	(3, 'cool', '0', 'homedom.uz@gmail.com', NULL, '$2y$10$Juxh619mWp8ngpeCuOHb/eArl1gVzMZVYo4UnE0LxLN1Hliv38Gji', 2, NULL, NULL, '2021-12-10 04:56:51', '2021-12-10 05:02:11'),
	(5, 'selfcreated2', '0', 'superuser@gmail.com', NULL, '$2y$10$hy04fHJ8Yc/it533y9U8aOHkpIkSAQZ2BKksUhG5wQQeVOAutDq7i', 2, NULL, NULL, '2021-12-14 14:55:11', '2021-12-20 01:08:51'),
	(6, 'newaccount', '0', 'newaccount@gmail.com', NULL, '$2y$10$TaFSxRHBGpqp0XHMS2s6cO3tnaLOmTmmaoRwZK5SsF3uwVznAyd1K', 1, NULL, NULL, '2021-12-16 19:37:09', '2021-12-16 17:37:09'),
	(7, 'newuser', '0', 'homedom2.uz@gmail.com', NULL, '$2y$10$QR7kr5QOS7.ovPK/wEFiOOOE517yH15.CbbCPLsK5RmlOI.qzRDs2', 2, NULL, NULL, '2021-12-20 02:06:20', '2021-12-20 02:06:20'),
	(8, 'vladimir', '0', 'supermail@gmail.com', NULL, '$2y$10$5yY4HgFz2/W/pdenqRD06u2vidkrWv3p7.cGSnRMtDgDozbkn.DNa', 1, NULL, NULL, '2021-11-20 21:29:52', '2021-12-20 02:19:52'),
	(9, 'coolusernew', '0', 'deeee@gmail.com', NULL, '$2y$10$f55cTttNb99SdbWX5H4e0e0MW/6lmmxYkUWybu8N.zgJ6CqaODNbe', 1, NULL, NULL, '2021-12-10 02:25:31', '2022-03-11 19:12:44'),
	(11, 'created_user', '0', 'created2user@gmail.com', NULL, '$2y$10$J3jElGB6QT54hkuTDm5e8uV.hB70nN99hT2oHUbQuoZaFgocdmWN.', 1, NULL, NULL, '2021-12-21 11:49:38', '2021-12-21 11:49:38'),
	(15, 'jjjj', '0', 'mymail42@gmail.com', NULL, '$2y$10$PhSHVvdBWLDeL0jb40CNVOHr9n4DYkNatvYw3LzoyW2ik3sl0eyoG', 1, NULL, NULL, '2021-12-21 11:54:38', '2021-12-21 11:54:38'),
	(16, 'maxim33333', '0', 'max@gmail.com', NULL, '$2y$10$6WkqsZbCBm9/Di1Z6Td77u/WlEF8FcnUEFdv2I6xxyWT4knfT8re2', 1, NULL, NULL, '2021-12-23 00:49:46', '2021-12-23 00:49:46'),
	(17, 'newusernew', '0', 'newmail2333@gmail.com', NULL, '$2y$10$FchKc1v5BF4p1u9nUiITuOxbslsfPt.cRI87Ba7OY0Q.0QVOtxUeG', 1, NULL, NULL, '2022-01-07 16:27:28', '2022-01-07 16:27:28'),
	(18, 'fijijqwij', '0', 'gametvwqw457@gmail.com', NULL, '$2y$10$1TwhqfW09uSo./aKGp2gI.sY3PMOsv9YTF7z7wVmi4iUPyRq2Nw0K', 1, NULL, NULL, '2022-01-10 17:02:46', '2022-01-10 17:02:46'),
	(19, 'justlogin', '0', 'gametv542247@gmail.com', NULL, '$2y$10$jbvB4pYABluu3Uft/kz5wOZyd9tN.Fh0IROwxojmj3zMf8uvk.Uh.', 1, NULL, NULL, '2022-01-10 17:05:33', '2022-01-10 17:05:33'),
	(20, 'testjusrww', '0', 'sushi-test@test.ua', NULL, '$2y$10$bnW8541Ehsmz2THGWTjXFePu1gR2UA5oxH5BDb4Zjdy9kKZihuXJO', 2, NULL, NULL, '2022-01-10 17:06:50', '2022-01-10 17:06:50'),
	(21, 'fwqfwqqfw', '0', 'n/a', NULL, '$2y$10$Eh7qQpoPNbajWNo5h2xr6eleYvaVJ5pkthBziZLqAK61rRgtvTLze', 1, NULL, NULL, '2022-01-10 20:35:14', '2022-01-10 20:35:14'),
	(24, 'r21r211r2r1', '0', 'n/a_88', NULL, '$2y$10$QMJKP1PXl3brvCAR.AAo6euA/XLGSYzJTiiXkc9sVqP3fyAklj9Ne', 1, NULL, NULL, '2022-01-10 20:36:54', '2022-01-10 20:36:54'),
	(25, '321321d', '0', 'n/a_61dc9901623c6', NULL, '$2y$10$Rir6q/ZqOJAt4D4a5GLwCOeepXiY5CHMufy2y5nQNQz8LBY.B5aam', 1, NULL, NULL, '2022-01-10 20:37:27', '2022-01-10 20:37:27'),
	(26, 'logintest', '0', 'testmail@gmail.com', NULL, '$2y$10$4WUXhbjKN8k5wKfbBkLQSevAfgHIlDe62A0g16S5BZPtDbAWtzJCu', 1, NULL, NULL, '2022-01-17 15:57:44', '2022-01-17 15:57:44'),
	(27, 'molodec222', '0', 'newnewnew@gmail.com', NULL, '$2y$10$eZjasorNdYYlYclyEski9u1oR3UGBouhrCB3v9pDoZEKqhKWyP/2y', 1, NULL, NULL, '2022-01-17 16:05:32', '2022-01-17 16:05:32'),
	(29, 'manager', '0', 'manager@gmail.com', NULL, '$2y$10$eSI6A/1Nn3yIyAfEFxErjeet4JTDqaYe2.HrXGvfMiDPiU9wTyzN2', 2, NULL, NULL, '2022-01-17 19:51:46', '2022-01-17 19:51:46'),
	(30, 'testtestov', '0', 'xidonett7777@gmail.com', NULL, '$2y$10$WhVfWqK/CP7MPiitCFxARe2YhkvuqNleu0FbJyydQnBTmeLsB31Oy', 1, NULL, NULL, '2022-01-17 20:22:15', '2022-01-23 13:45:01'),
	(31, 'reeeef', '0', 'xidonett699@gmail.com', NULL, '$2y$10$YBBnISZWhEvydPyJYxSafuLgySDqLyGHw3/FNrX1fnjcgBHNkBLDW', 1, NULL, NULL, '2022-01-17 20:23:27', '2022-01-17 20:23:27'),
	(33, 'newrefff', '1642451336.png', 'xidonettref@gmail.com', NULL, '$2y$10$m1LOdpR6RCaE/GSFEBJ9F.hmeiaQ5pxdrUf.MjcR0dsiZtxMCmn6O', 1, NULL, NULL, '2022-01-17 20:26:31', '2022-01-23 10:04:00'),
	(34, 'newmail222', '0', 'ankfkasn@gmail.com', NULL, '$2y$10$J0BMn0LjkPLyGPe9qJ/DPu32.dxD.F9jH587fLps/O/GLQBK2jqiK', 1, NULL, NULL, '2022-02-07 23:09:10', '2022-02-07 23:09:10'),
	(35, 'fjasfska', '0', 'gagsasa@gmail.com', NULL, '$2y$10$NOjGJ9M.sWoBNwGH.hac6.0xHt02pEf.j3EuXv4WI54g2HYeFEub6', 1, NULL, NULL, '2022-02-08 14:43:14', '2022-02-08 14:43:14'),
	(41, 'casino777', '0', 'casino777@gmail.com', NULL, '$2y$10$J74BG2qkP/zJLIeEcoPuL.o.ZvzJf1I4FfQT0wIO1zLDq8n0k40iG', 1, 'superman2012', NULL, '2022-03-12 18:48:05', '2022-03-12 18:48:05'),
	(42, 'docker', '0', 'zinopay@gmail.com', NULL, '$2y$10$RyoVdH.BwyfpOdK9hUrJ3OiQgqh5LxaAQd7cJzN8EZWBN.VbmDzpW', 1, 'superman2012', NULL, '2022-03-14 23:38:17', '2022-03-14 23:38:17');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table docker.users_personal_data
CREATE TABLE IF NOT EXISTS `users_personal_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_day` int(11) NOT NULL DEFAULT '0',
  `birth_month` int(11) NOT NULL DEFAULT '0',
  `birth_year` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.users_personal_data: ~1 rows (approximately)
/*!40000 ALTER TABLE `users_personal_data` DISABLE KEYS */;
INSERT IGNORE INTO `users_personal_data` (`id`, `user_id`, `name`, `surname`, `birth_day`, `birth_month`, `birth_year`, `phone`, `created_at`, `updated_at`) VALUES
	(1, 42, 'Ivan', 'Ivanov', 14, 8, 1959, NULL, '2022-03-20 11:10:02', '2022-03-20 12:27:03');
/*!40000 ALTER TABLE `users_personal_data` ENABLE KEYS */;

-- Dumping structure for table docker.user_balance
CREATE TABLE IF NOT EXISTS `user_balance` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `real` double(8,2) NOT NULL DEFAULT '0.00',
  `virtual` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.user_balance: ~28 rows (approximately)
/*!40000 ALTER TABLE `user_balance` DISABLE KEYS */;
INSERT IGNORE INTO `user_balance` (`id`, `user_id`, `real`, `virtual`, `created_at`, `updated_at`) VALUES
	(1, 1, 20552.11, 75500.00, '2021-12-01 21:31:14', '2022-02-09 18:04:23'),
	(3, 3, 5000.00, 0.00, '2021-12-01 21:32:52', '2021-12-01 21:32:52'),
	(5, 3, 1000.00, 0.00, '2021-12-10 04:56:51', '2021-12-10 04:56:51'),
	(7, 5, 0.00, 0.00, '2021-12-14 14:55:11', '2021-12-14 14:55:11'),
	(8, 6, 7555.00, 0.00, '2021-12-16 17:37:09', '2022-02-05 06:49:06'),
	(9, 7, 0.00, 0.00, '2021-12-20 02:06:20', '2021-12-20 02:06:20'),
	(10, 8, 0.00, 0.00, '2021-12-20 02:19:53', '2021-12-20 02:19:53'),
	(11, 9, 0.00, 0.00, '2021-12-20 02:25:31', '2021-12-20 02:25:31'),
	(13, 11, 5000.00, 0.00, '2021-12-21 11:49:38', '2021-12-22 08:34:28'),
	(16, 15, 19000.00, 5000.00, '2021-12-21 11:54:38', '2021-12-22 08:33:22'),
	(17, 16, 10.00, 5000.00, '2021-12-23 00:49:46', '2021-12-23 03:06:43'),
	(18, 17, 0.00, 5000.00, '2022-01-07 16:27:28', '2022-01-07 16:27:28'),
	(19, 18, 0.00, 5000.00, '2022-01-10 17:02:46', '2022-01-10 17:02:46'),
	(20, 19, 0.00, 5000.00, '2022-01-10 17:05:33', '2022-01-10 17:05:33'),
	(21, 20, 0.00, 5000.00, '2022-01-10 17:06:50', '2022-01-10 17:06:50'),
	(22, 21, 5500.00, 5000.00, '2022-01-10 20:35:14', '2022-01-10 20:35:14'),
	(23, 24, 3123.00, 5000.00, '2022-01-10 20:36:54', '2022-01-10 20:36:54'),
	(24, 25, 4444.00, 5000.00, '2022-01-10 20:37:27', '2022-01-10 20:37:27'),
	(25, 26, 0.00, 5000.00, '2022-01-17 15:57:44', '2022-01-17 15:57:44'),
	(26, 27, 0.00, 5000.00, '2022-01-17 16:05:32', '2022-01-17 16:05:32'),
	(28, 29, 0.00, 5000.00, '2022-01-17 19:51:46', '2022-01-17 19:51:46'),
	(29, 30, 0.00, 5000.00, '2022-01-17 20:22:15', '2022-01-17 20:22:15'),
	(30, 31, 0.00, 5000.00, '2022-01-17 20:23:27', '2022-01-17 20:23:27'),
	(31, 33, 7500.00, 5000.00, '2022-01-17 20:26:31', '2022-02-02 17:41:31'),
	(32, 34, 0.00, 5000.00, '2022-02-07 23:09:10', '2022-02-07 23:09:10'),
	(33, 35, 0.00, 5000.00, '2022-02-08 14:43:14', '2022-02-08 14:43:14'),
	(39, 41, 0.00, 5000.00, '2022-03-12 18:48:05', '2022-03-12 18:48:05'),
	(40, 42, 0.00, 5000.00, '2022-03-14 23:38:17', '2022-03-14 23:38:17');
/*!40000 ALTER TABLE `user_balance` ENABLE KEYS */;

-- Dumping structure for table docker.user_locations
CREATE TABLE IF NOT EXISTS `user_locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.user_locations: ~10 rows (approximately)
/*!40000 ALTER TABLE `user_locations` DISABLE KEYS */;
INSERT IGNORE INTO `user_locations` (`id`, `user_id`, `city`, `country`, `ip`, `created_at`, `updated_at`) VALUES
	(1, 20, 'Sandomierz', 'PL', '127.0.0.1', '2022-01-10 17:06:51', '2022-01-10 17:06:51'),
	(2, 27, 'N/A', 'N/A', '127.0.0.1', '2022-01-17 16:05:33', '2022-01-17 16:05:33'),
	(3, 29, 'N/A', 'N/A', '127.0.0.1', '2022-01-17 19:51:47', '2022-01-17 19:51:47'),
	(4, 30, 'N/A', 'N/A', '127.0.0.1', '2022-01-17 20:22:15', '2022-01-17 20:22:15'),
	(5, 31, 'N/A', 'N/A', '127.0.0.1', '2022-01-17 20:23:27', '2022-01-17 20:23:27'),
	(6, 33, 'N/A', 'N/A', '127.0.0.1', '2022-01-17 20:26:31', '2022-01-17 20:26:31'),
	(7, 34, 'N/A', 'N/A', '127.0.0.1', '2022-01-21 04:04:13', '2022-01-21 04:04:13'),
	(8, 34, 'N/A', 'N/A', '127.0.0.1', '2022-02-07 23:09:11', '2022-02-07 23:09:11'),
	(9, 35, 'N/A', 'N/A', '127.0.0.1', '2022-02-08 14:43:15', '2022-02-08 14:43:15'),
	(10, 41, 'N/A', 'N/A', '127.0.0.1', '2022-03-12 18:48:06', '2022-03-12 18:48:06'),
	(11, 42, 'N/A', 'N/A', '127.0.0.1', '2022-03-14 23:38:18', '2022-03-14 23:38:18');
/*!40000 ALTER TABLE `user_locations` ENABLE KEYS */;

-- Dumping structure for table docker.user_notes
CREATE TABLE IF NOT EXISTS `user_notes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table docker.user_notes: ~8 rows (approximately)
/*!40000 ALTER TABLE `user_notes` DISABLE KEYS */;
INSERT IGNORE INTO `user_notes` (`id`, `user_id`, `note`, `created_at`, `updated_at`) VALUES
	(1, 0, NULL, '2021-12-22 06:53:37', '2021-12-22 06:53:37'),
	(2, 0, NULL, '2021-12-22 06:54:17', '2021-12-22 06:54:17'),
	(3, 0, NULL, '2021-12-22 06:55:07', '2021-12-22 06:55:07'),
	(4, 0, NULL, '2021-12-22 06:55:17', '2021-12-22 06:55:17'),
	(5, 0, NULL, '2021-12-22 06:55:19', '2021-12-22 06:55:19'),
	(6, 15, 'Моя заметка была переписана', '2021-12-22 06:59:41', '2021-12-22 07:00:02'),
	(7, 10, 'Просто заметка', '2021-12-23 03:14:37', '2021-12-23 03:14:37'),
	(8, 5, 'Заметка', '2022-01-20 21:37:58', '2022-01-20 21:37:58');
/*!40000 ALTER TABLE `user_notes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
