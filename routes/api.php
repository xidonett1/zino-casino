<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use http\Client\Response;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/update_user_balance/{id}', [\App\Http\Controllers\UserBalanceController::class, 'updateUserBalance']);
Route::get('/user_balance/{id}', [\App\Http\Controllers\UserBalanceController::class, 'getUserBalance']);

Route::post('/bets/', [\App\Http\Controllers\BetController::class, 'createBet']);
