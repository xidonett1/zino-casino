<?php

use App\Http\Controllers\CustomAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('admin/deposit/make-deposit', [\App\Http\Controllers\AdminController::class, 'makeDeposit'])->name('admin.post.make-deposit');
Route::post('admin/deposit/make-swift', [\App\Http\Controllers\AdminController::class, 'makeSwift'])->name('admin.post.make-swift');
Route::post('admin/deposit/make-tax', [\App\Http\Controllers\AdminController::class, 'makeTax'])->name('admin.post.make-tax');

Route::get('admin', [CustomAuthController::class, 'getAdminDashboard'])->name('admin');
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom');
Route::get('register', [CustomAuthController::class, 'register'])->name('register');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom');
Route::post('api/change-password', [CustomAuthController::class, 'changePassword'])->name('auth.change-password');
Route::post('api/update-personal-data', [\App\Http\Controllers\UserPersonalDataController::class, 'updateData'])->name('api.update-personal-data');
Route::post('api/get-user', [\App\Http\Controllers\UserController::class, 'getUser'])->name('api.get-user');
Route::post('api/update-avatar', [\App\Http\Controllers\UserController::class, 'updateAvatar'])->name('api.update-avatar');
Route::post('api/update-login', [\App\Http\Controllers\UserController::class, 'updateLogin'])->name('api.update-login');
Route::post('api/update-role', [\App\Http\Controllers\UserController::class, 'updateRole'])->name('api.update-role');
Route::post('api/update-email', [\App\Http\Controllers\UserController::class, 'updateEmail'])->name('api.update-email');
Route::post('api/get-transaction', [\App\Http\Controllers\TransactionController::class, 'getTransaction'])->name('api.get-transaction');
Route::post('api/update-transaction', [\App\Http\Controllers\TransactionController::class, 'updateTransaction'])->name('api.update-transaction');
Route::post('api/update-ticket-misc', [\App\Http\Controllers\SupportController::class, 'update'])->name('api.update-ticket-misc');
Route::post('api/withdraw', [\App\Http\Controllers\UserController::class, 'withdraw'])->name('api.withdraw');

Route::post('api/swift-tax-payment', [\App\Http\Controllers\UserController::class, 'swiftTaxPayment'])->name('api.swift-tax-payment');

Route::post('api/winner-tax-payment', [\App\Http\Controllers\UserController::class, 'winnerTaxPayment'])->name('api.winner-tax-payment');
Route::post('api/add-user-player', [\App\Http\Controllers\UserController::class, 'addUserPlayer'])->name('api.add-user-player');
Route::post('api/user-note-update', [\App\Http\Controllers\UserNoteController::class, 'updateNote'])->name('api.user-note-update');
Route::post('api/send-message', [\App\Http\Controllers\MessageController::class, 'sendMessage'])->name('api.send-message');
Route::get('api/delete-message/{id}', [\App\Http\Controllers\MessageController::class, 'deleteMessage'])->name('api.delete-message');
Route::get('api/delete-all-messages/{user_id}/{user_login}', [\App\Http\Controllers\MessageController::class, 'deleteAllMessages'])->name('api.delete-all-messages');
Route::post('api/generate-ref', [\App\Http\Controllers\RefController::class, 'generateRef'])->name('api.generate-ref');
Route::post('api/apply-payment-history-actions', [\App\Http\Controllers\AdminController::class, 'applyPaymentHistoryActions'])->name('api.apply-payment-history-actions');
Route::get('api/block-user/{user_id?}', [\App\Http\Controllers\BlockedController::class, 'change'])->name('api.block-user');
Route::get('logout', [CustomAuthController::class, 'signOut'])->name('logout');
Route::post('api/save-translation', [\App\Http\Controllers\AdminController::class, 'saveTranslation'])->name('admin.post.save-translation');
Route::post('api/tournament/participate', [\App\Http\Controllers\TournamentParticipantController::class, 'participate'])->name('api.tournament.participate');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('/users', [\App\Http\Controllers\AdminController::class, 'getUsersPage'])->name('admin.users');
    Route::get('/admins', [\App\Http\Controllers\AdminController::class, 'getAdminsPage'])->name('admin.admins');
    Route::post('/user/delete-user', [\App\Http\Controllers\AdminController::class, 'deleteUser'])->name('admin.post.delete-user');
    Route::post('/withdrawal/make-withdraw', [\App\Http\Controllers\AdminController::class, 'makeWithdraw'])->name('admin.post.make-withdraw');
    Route::get('/withdrawal', [\App\Http\Controllers\AdminController::class, 'getWithdrawalPage'])->name('admin.withdrawal');
    Route::get('/withdrawals', [\App\Http\Controllers\AdminController::class, 'getWithdrawalsPage'])->name('admin.withdrawals');
    Route::get('/transactions', [\App\Http\Controllers\AdminController::class, 'getTransactionsPage'])->name('admin.transactions');
    Route::get('/payments', [\App\Http\Controllers\AdminController::class, 'getPaymentsPage'])->name('admin.payments');
    Route::get('/no-bets', [\App\Http\Controllers\AdminController::class, 'getNoBetsPage'])->name('admin.no-bets');
    Route::get('/report', [\App\Http\Controllers\AdminController::class, 'getReportPage'])->name('admin.report');
    Route::get('/unfortunate-withdrawal', [\App\Http\Controllers\AdminController::class, 'getUnfortunateWithdrawalPage'])->name('admin.unfortunate-withdrawal');
    Route::get('/pages', [\App\Http\Controllers\AdminController::class, 'getPagesPage'])->name('admin.pages');
    Route::get('/pages/{page_id}', [\App\Http\Controllers\AdminController::class, 'getPagePage'])->name('admin.pages.page');
    Route::post('/page', [\App\Http\Controllers\AdminController::class, 'pageManipulation'])->name('admin.post.page');
    Route::post('/page-delete', [\App\Http\Controllers\AdminController::class, 'deletePage'])->name('admin.post.delete-page');
    Route::get('/games-history/{id?}', [\App\Http\Controllers\AdminController::class, 'getGamesHistoryPage'])->name('admin.games-history');
    Route::get('/payment-history/{id?}', [\App\Http\Controllers\AdminController::class, 'getPaymentHistoryPage'])->name('admin.payment-history');

    Route::prefix('/news')->group(function(){
        Route::get('/', [\App\Http\Controllers\AdminController::class, 'getNewsPage'])->name('admin.news');
        Route::get('/pages/{page_id}', [\App\Http\Controllers\AdminController::class, 'getNewsPagePage'])->name('admin.news.page');
    });

    // Support routes
    Route::prefix('/support')->group(function(){
        Route::get('/tickets/{slug?}/{id?}', [\App\Http\Controllers\SupportController::class, 'getTicketsPage'])->name('admin.support.tickets');
        Route::get('/ticket-templates', [\App\Http\Controllers\TicketTemplateController::class, 'getPage'])->name('admin.support.ticket-templates');
        Route::get('/ticket-categories', [\App\Http\Controllers\TicketCategoryController::class, 'getPage'])->name('admin.support.ticket-categories');

        Route::post('/create-ticket-template', [\App\Http\Controllers\TicketTemplateController::class, 'create'])->name('admin.post.create-ticket-template');
        Route::post('/create-ticket-category', [\App\Http\Controllers\TicketCategoryController::class, 'create'])->name('admin.post.create-ticket-category');
        Route::get('/delete-ticket-template-or-category/{id?}/{type?}', [\App\Http\Controllers\SupportController::class, 'delete'])->name('admin.post.delete-ticket-template-or-category');
    });

    Route::get('/translate', [\App\Http\Controllers\AdminController::class, 'getTranslatePage'])->name('admin.translate');

    Route::get('/settings', [\App\Http\Controllers\AdminController::class, 'getSettingsPage'])->name('admin.settings');
    Route::post('/update-settings', [\App\Http\Controllers\SettingsController::class, 'updateSettings'])->name('admin.post.update-settings');

    Route::prefix('/games')->group(function() {
        //Games
        Route::get('/', [\App\Http\Controllers\AdminController::class, 'getGamesPage'])->name('admin.games');
        Route::get('/create', [\App\Http\Controllers\AdminController::class, 'getCreateGamePage'])->name('admin.games.create');
        Route::post('/create', [\App\Http\Controllers\AdminController::class, 'createGame'])->name('admin.post.game.create');
        Route::get('/delete/{id?}', [\App\Http\Controllers\AdminController::class, 'deleteGame'])->name('admin.post.game.delete');

        //Tournaments
        Route::get('/tournaments', [\App\Http\Controllers\AdminController::class, 'getTournamentsPage'])->name('admin.tournaments');
        Route::get('/tournament/{id}', [\App\Http\Controllers\AdminController::class, 'getTournamentPage'])->name('admin.tournament');
        Route::post('/tournament', [\App\Http\Controllers\TournamentController::class, 'createTournament'])->name('admin.post.tournament');
        Route::get('/delete-tournament/{id}', [\App\Http\Controllers\TournamentController::class, 'deleteTournament'])->name('admin.delete-tournament');
    });
});

Route::group(['prefix' => '{lang?}', 'middleware' => ['language', 'is_under_construction']], function () {

    Route::get('/template-page', function(){
       return view('layouts.custom-page.page');
    })->name('template-page');

    Route::get('/', [\App\Http\Controllers\PageController::class, 'getIndexPage'])->name('index');

    Route::get('/casino', [\App\Http\Controllers\PageController::class, 'getCasinoPage'])->name('casino');

    Route::get('/lotteries', [\App\Http\Controllers\PageController::class, 'getLotteriesPage'])->name('lotteries');

    Route::get('/news', [\App\Http\Controllers\PageController::class, 'getNewsPage'])->name('news');
    Route::get('/news/{slug?}', [\App\Http\Controllers\PageController::class, 'getSingleNewsPage'])->name('news.single');

    Route::get('/tournaments', [\App\Http\Controllers\PageController::class, 'getTournamentsPage'])->name('tournaments');

    Route::get('/page/{slug?}', [\App\Http\Controllers\PageController::class, 'getPage'])->name('page');

    Route::get('/game/{slug?}/{is_real?}', [\App\Http\Controllers\GameController::class, 'getGamePage'])->name('user.game');

    Route::group(['prefix' => 'my', 'middleware' => 'auth'], function () {
        Route::get('/', [\App\Http\Controllers\UserController::class, 'getBalancePage'])->name('user.my');

        Route::get('/settings', [\App\Http\Controllers\UserController::class, 'getSettingsPage'])->name('user.settings');
        Route::get('/balance', [\App\Http\Controllers\UserController::class, 'getBalancePage'])->name('user.balance');
        Route::get('/transactions', [\App\Http\Controllers\UserController::class, 'getTransactionsPage'])->name('user.transactions');
        Route::get('/deposit', [\App\Http\Controllers\UserController::class, 'getDepositPage'])->name('user.deposit');

        Route::get('/bank-transfer', function () {
            return view('user.bank-transfer');
        })->name('user.bank-transfer');

        Route::get('/games-history', [\App\Http\Controllers\PageController::class, 'getGamesHistoryPage'])->name('user.games-history');

        Route::get('/tax', function () {
            return view('user.tax');
        })->name('user.tax');

        Route::get('/withdrawal', function () {
            return view('user.withdrawal');
        })->name('user.withdrawal');

        Route::get('/withdrawal-request', [\App\Http\Controllers\UserController::class, 'getWithdrawalRequestPage'])->name('user.withdrawal-request');
        Route::get('/deposit-request', [\App\Http\Controllers\UserController::class, 'getDepositRequestPage'])->name('user.deposit-request');

        Route::get('/write-us', [\App\Http\Controllers\UserController::class, 'getWriteUsPage'])->name('user.write-us');
    });
});
