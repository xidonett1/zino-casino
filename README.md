Casino template for the gambling sites.

<b>Stack:</b>
<ul>
    <li>Laravel Framework</li>
    <li>Bootstrap 4</li>
    <li>Axios.js</li>
</ul>

<b>Features:</b>
<ul>
    <li>Multi-lang support</li>
    <li>Admin Dashboard</li>
    <li>Payment systems integration</li>
</ul>
