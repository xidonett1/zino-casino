<html>

<head>

<title>European Roulette</title>
	<meta charset="utf-8">
	<meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

	 <link href='css/fonts.css' rel='stylesheet' type='text/css'>


	<script src="js/lib/createjs-2015.11.26.min.js" type="text/javascript"></script>
	<script src="js/lib/jquery-2.0.3.min.js" type="text/javascript"></script>

	<script src="js/classes/GameObj.js" type="text/javascript"></script>
	<script src="js/classes/GameBack.js" type="text/javascript"></script>
	<script src="js/classes/GameUI.js" type="text/javascript"></script>
	<script src="js/classes/GameWheel.js" type="text/javascript"></script>
	<script src="js/soundManager.js" type="text/javascript"></script>
	<script src="js/settings.js" type="text/javascript"></script>
	<script src="js/core.js" type="text/javascript"></script>

</head>


<body onload="InitGame();" style="margin:0px;background-color:black">

<canvas id="game" width="1024" height="768" class="mainScreen"></canvas>

</body>


</html>
