
var canvas;
var stage;
var gameBack;
var gameUI;
var gameNumFields;
var gameWheel;
var mainGame;
var gameState="";
var totalResCnt=0;
var currentResCnt=0;
var currentResCnt2=0;
var scaleMode="exactFit";
var scaleModeAlign="T";
var gamePreloader=new Object(null);

var testDrop=true;
var testDropCnt=new Array(0,0,0);
var betEvent=new Event("update_counters");




function GameResize(){

var gc=document.getElementById("game");

if(scaleMode=="showAll"){

gc.style.width=window.innerWidth+"px";

}else if(scaleMode=="exactFit"){

gc.style.width=window.innerWidth+"px";
gc.style.height=window.innerHeight+"px";

}


}


function InitGame(id, domain, is_real, game){


	window.onresize=function(ev){

GameResize();

}


	/*canvas setting*/
	canvas = document.getElementById("game");
	stage = new createjs.Stage(canvas);

	createjs.Touch.enable(stage);
	createjs.Ticker.setFPS(40);
	createjs.Ticker.addEventListener("tick", UpdateGame);

	stage.enableMouseOver(50);
	/*canvas setting*/





/*init game objects*/

mainGame=new createjs.Container();

gameUI=new GameUI();
gameBack=new GameBack();
gameWheel=new GameWheel();








/*resource loading*/

totalResCnt+=gameBack.GetResCount()+gameUI.GetResCount()+gameWheel.GetResCount();

gameUI.LoadResource();
gameBack.LoadResource();
gameWheel.LoadResource();




addEventListener("isResLoad",function(ev){LoadCount();});
addEventListener("isErrLoad",function(ev){LoadCount();});
addEventListener("ui_btn",function(ev){ButtonsController(ev.detail.bname);});




/*preloader*/
gamePreloader.cc=new createjs.Container();
gamePreloader.ptext= new createjs.Text("0%", "52px ArialBold", "#FFFFFF");
gamePreloader.ptext.set({x:55,y:10,textAlign:"center"});
gamePreloader.cc.set({x:920,y:495});
gamePreloader.cc.addChild(gamePreloader.ptext);

///////////////////////////

gamePreloader.circle_g=new Array();
gamePreloader.circle_g2=new Array();
gamePreloader.circle_g2cnt=0;
gamePreloader.circle_gcnt=0;

var rad=180;
var rad2=100;
var alph=0.10;


for(var jj=0; jj<24; jj++){

gamePreloader.circle_g[jj]=new createjs.Shape();

gamePreloader.circle_g[jj].graphics.beginLinearGradientFill(["#FFFFFF","#FFFFFF"], [0.1, 0], 0, 40, 0, 200);
gamePreloader.circle_g[jj].graphics.drawRect(0, 0,23, 18);

gamePreloader.circle_g[jj].set({alpha:alph,x:50,y:50,regX:15,regY:rad,rotation:jj*15});
gamePreloader.cc.addChild(gamePreloader.circle_g[jj]);
	alph+=0.04;
////////////////////

gamePreloader.circle_g2[jj]=new createjs.Shape();

gamePreloader.circle_g2[jj].graphics.beginLinearGradientFill(["#FFFFFF","#FFFFFF"], [0.1, 0], 0, 40, 0, 200);
gamePreloader.circle_g2[jj].graphics.drawRect(0, 0,12, 10);

gamePreloader.circle_g2[jj].set({x:50,y:50,visible:false,regX:15,regY:rad2,rotation:jj*15});
gamePreloader.cc.addChild(gamePreloader.circle_g2[jj]);


}

///////////////////////////////



stage.addChild(gamePreloader.cc);



/*set view*/
GameResize();


gameState="Load";

}


function FillWeatherBox(){

var bArr=new Array(2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35);
var nums=new Array();
for(var i=0; i<=36;i++){

nums[i]=i;

}

nums=shuffle(nums);

for(var i=0; i<=3;i++){


var rndNum=nums.shift();

if(rndNum==0){
gameUI.cObjects['weather_box_hot'][i].gotoAndStop(2);

}else if(bArr.indexOf(rndNum)!=-1){
gameUI.cObjects['weather_box_hot'][i].gotoAndStop(0);
}else{
gameUI.cObjects['weather_box_hot'][i].gotoAndStop(1);
}

gameUI.cObjects['weather_box_hot'][i].name=rndNum;

gameUI.cObjects['weather_box_hot'][i].addEventListener("click",function(ev){if(gameState=="Idle"){gameUI.AddBet(ev.currentTarget.name,gameInfo.currentBet);}});

gameUI.cObjects['weather_box_hot'][i+4].text=RandomInt(29,66);

gameUI.cObjects['weather_box_hot'][i+8].text=rndNum;


}



////////////////////////////


for(var i=0; i<=3;i++){


var rndNum=nums.shift();

if(rndNum==0){
gameUI.cObjects['weather_box_cold'][i].gotoAndStop(2);

}else if(bArr.indexOf(rndNum)!=-1){
gameUI.cObjects['weather_box_cold'][i].gotoAndStop(0);
}else{
gameUI.cObjects['weather_box_cold'][i].gotoAndStop(1);
}

gameUI.cObjects['weather_box_cold'][i].name=rndNum;

gameUI.cObjects['weather_box_cold'][i].addEventListener("click",function(ev){if(gameState=="Idle"){gameUI.AddBet(ev.currentTarget.name,gameInfo.currentBet);}});

gameUI.cObjects['weather_box_cold'][i+4].text=RandomInt(29,66);

gameUI.cObjects['weather_box_cold'][i+8].text=rndNum;


}



};

function StrToBet(strNum){

var strBetArr=specialBets[strNum-1].split("|");


for(var i=0; i<strBetArr.length; i++){

var betTmp=strBetArr[i].split(":");

if(betTmp[1]>0){

gameUI.AddBet(betTmp[0],gameInfo.currentBet);

}

}

return strBetArr.join("|");
}



function GetBetStr(){

var strBetArr=new Array("");
gameInfo.History=new Array();
gameInfo.betsHistory=new Array();
for(var i=0; i<=156; i++){

if(gameUI.cObjects['bet_places2chips'][i].curBet>0){
gameInfo.betsHistory[i]=gameUI.cObjects['bet_places2chips'][i].curBet;
strBetArr.push(i+":"+(gameUI.cObjects['bet_places2chips'][i].curBet/100));

}
}

return strBetArr.join("|");
}

function ButtonsController(bname){

if(bname=="b_exit"){
document.location.href="../../../";
}

if(bname.split("bet")[0]=="s"){

StrToBet(bname.split("bet")[1]);

}


if(bname=="btnSpin"){

    ServerSend(new Object(
        {
            action:"spin",
            domain: GET('domain'),
            id: GET('id'),
            game: GET('game'),
            is_real: GET('is_real'),
            round_id: GET('round_id'),
            bets:GetBetStr()
        }
    ));

gameUI.cObjects['bet_places2'][166].visible=false;
gameUI.cObjects['bet_places2'][167].visible=false;
gameUI.cObjects['bet_places2'][168].visible=false;

if(gameUI.cObjects['trackbet'].visible){
gameUI.cObjects['trackbet'].visible=false;
}

if(gameUI.cObjects['fbet'].y>=0){

createjs.Tween.get(gameUI.cObjects['fbet']).to({y:-400}, 500, createjs.Ease.getBackInOut(0.2));

}

gameUI.LockButtons();


}else if(bname=="btnTrackBet1"){
PlaySound("11_tmp.SoundClass");

if(gameInfo.trackbetCnt>0){
gameInfo.trackbetCnt--;
}
gameUI.cObjects['trackbet1'].text=gameInfo.trackbetCnt;

}else if(bname=="btnTrackBet2"){
PlaySound("11_tmp.SoundClass");

if(gameInfo.trackbetCnt<8){
gameInfo.trackbetCnt++;
}
gameUI.cObjects['trackbet1'].text=gameInfo.trackbetCnt;
}else if(bname=="btnInfo4"){
PlaySound("11_tmp.SoundClass");

if(gameUI.cObjects['fbet'].y>=0){

createjs.Tween.get(gameUI.cObjects['fbet']).to({y:-400}, 500, createjs.Ease.getBackInOut(0.2));

}else{

createjs.Tween.get(gameUI.cObjects['fbet']).to({y:0}, 500, createjs.Ease.getBackInOut(0.2));

}

}else if(bname=="btnInfo5"){
PlaySound("11_tmp.SoundClass");
gameUI.DoubleAllBets();

PlaySound("1_tmp.SoundClass");

}else if(bname=="btnInfo6"){
PlaySound("11_tmp.SoundClass");
gameUI.RemoveLastBet();

PlaySound("1_tmp.SoundClass");

}else if(bname=="btnInfo7"){
PlaySound("11_tmp.SoundClass");
gameUI.RemoveAllBets();

PlaySound("1_tmp.SoundClass");

}else if(bname=="btnInfo8"){
PlaySound("11_tmp.SoundClass");
gameUI.ReturnLastBets();

PlaySound("1_tmp.SoundClass");

}else if(bname=="btnInfo1"){
PlaySound("16_tmp.SoundClass");

if(gameUI.cObjects['help_wnd'].x<=-270){
createjs.Tween.get(gameUI.cObjects['help_wnd']).to({x:70}, 500, createjs.Ease.getBackInOut(1));
}else if(gameUI.cObjects['help_wnd'].x>=70){
createjs.Tween.get(gameUI.cObjects['help_wnd']).to({x:-270}, 500, createjs.Ease.getBackInOut(1));
}


}else if(bname=="btnSnd1" || bname=="btnSnd2"){
PlaySound("16_tmp.SoundClass");

if(gameUI.cObjects['snd_wnd'].y>610){
createjs.Tween.get(gameUI.cObjects['snd_wnd']).to({y:610}, 500, createjs.Ease.getBackInOut(1));
}else if(gameUI.cObjects['snd_wnd'].y<=800){
createjs.Tween.get(gameUI.cObjects['snd_wnd']).to({y:800}, 500, createjs.Ease.getBackInOut(1));
}


}else if(bname=="btnSettings" || bname=="btnSettingsClose"){
PlaySound("16_tmp.SoundClass");

if(gameUI.cObjects['set_wnd'].y>292){
createjs.Tween.get(gameUI.cObjects['set_wnd']).to({y:292}, 500, createjs.Ease.getBackInOut(1));
}else if(gameUI.cObjects['set_wnd'].y<=292){
createjs.Tween.get(gameUI.cObjects['set_wnd']).to({y:292*3}, 500, createjs.Ease.getBackInOut(1));
}


}else if(bname=="btnAuto" || bname=="btnAutoClose"){
PlaySound("16_tmp.SoundClass");

if(gameUI.cObjects['auto_wnd'].y>532){
createjs.Tween.get(gameUI.cObjects['auto_wnd']).to({y:532}, 500, createjs.Ease.getBackInOut(1));
}else if(gameUI.cObjects['auto_wnd'].y<=532){
createjs.Tween.get(gameUI.cObjects['auto_wnd']).to({y:900}, 500, createjs.Ease.getBackInOut(1));
}


}else if(bname=="btnInfo2"){
PlaySound("16_tmp.SoundClass");

if(!gameUI.cObjects['trackbet'].visible){
gameUI.cObjects['trackbet'].visible=true;
}else{
gameUI.cObjects['trackbet'].visible=false;
}

}else if(bname=="btnInfo3"){
PlaySound("16_tmp.SoundClass");
if(gameUI.cObjects['wheelstat'].name=="open"){
gameUI.StatShowWheel(false);
}else{
gameUI.StatShowWheel(true);
}

}else if(bname=="btnInfo4"){
PlaySound("16_tmp.SoundClass");

}else if(bname=="btnRight" || bname=="btnLeft"){

PlaySound("7_tmp.SoundClass");

gameUI.MoveBet(bname);

}else if(bname=="btnAuto1" || bname=="btnAuto2" || bname=="btnAuto3" || bname=="btnAuto4" || bname=="btnAuto5"){

bname=="btnAuto1";

}





}

function AddNumToStat(num){

var arr=new Array(2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35);


for(var i=10; i>=1; i--){

gameUI.cObjects['ws_text'][i].text=gameUI.cObjects['ws_text'][i-1].text;
gameUI.cObjects['ws_text'][i].textAlign=gameUI.cObjects['ws_text'][i-1].textAlign;
gameUI.cObjects['ws_text'][i].color=gameUI.cObjects['ws_text'][i-1].color;


}


if(arr.indexOf(num)!=-1){

gameUI.cObjects['ws_text'][i].text=num+"  ";
gameUI.cObjects['ws_text'][i].textAlign="right";
gameUI.cObjects['ws_text'][i].color="white";

}else if(num==0){

gameUI.cObjects['ws_text'][i].text=num;
gameUI.cObjects['ws_text'][i].textAlign="center";
gameUI.cObjects['ws_text'][i].color="green";

}else{

gameUI.cObjects['ws_text'][i].text="  "+num;
gameUI.cObjects['ws_text'][i].textAlign="left";
gameUI.cObjects['ws_text'][i].color="red";

}

/////////////////////////////////////





};


function CheckboxChange(bname,state){

gameSettings['betlight']=true;
gameSettings['quick']=false;
gameSettings['sndVoice']=true;
gameSettings['sndMusic']=true;
gameSettings['sndEffects']=true;



if(bname==1){

if(state==0){gameSettings['sndVoice']=true;}else{gameSettings['sndVoice']=false;}

}else if(bname==2){

if(state==0){gameSettings['sndMusic']=true;backMusic.play();}else{gameSettings['sndMusic']=false;backMusic.pause();}

}else if(bname==3){

if(state==0){gameSettings['sndEffects']=true;}else{gameSettings['sndEffects']=false;}

}else if(bname==4){

if(state==0){gameSettings['quick']=true;}else{gameSettings['quick']=false;}

}else if(bname==5){

if(state==0){gameSettings['betlight']=true;}else{gameSettings['betlight']=false;}

}



}

function SetWheelStat(){

var pnum=new Array(0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26);

var prc=gameStat[39]/37;
var oddCnt=0;
var evenCnt=0;

var blackCnt=0;
var redCnt=0;
var greenCnt=gameStat[0];

var clr=-1;

for(var i=0; i<=36; i++){

var cprc=gameStat[pnum[i]]/prc;

gameUI.cObjects['ws_pie'][pnum[i]].set({scaleX:cprc*0.009*37,scaleY:cprc*0.012*37});

if(clr==0){
redCnt+=gameStat[i];
}else if(clr==1){
blackCnt+=gameStat[i];
}
clr++;

var odd=""+(pnum[i]/2);

if(pnum[i]>0){

if( parseFloat(odd.split(".")[1])>0){
evenCnt+=gameStat[pnum[i]];
}else{
oddCnt+=gameStat[pnum[i]];
}

}

if(clr>1){clr=0;}


}

var bp=Math.round(blackCnt/(gameStat[39]/100));
var rp=Math.round(redCnt/(gameStat[39]/100));
var gp=Math.round(greenCnt/(gameStat[39]/100));

gameUI.cObjects['ws_black'].set({x:340,y:50,scaleX:0.02*bp});
gameUI.cObjects['ws_green'].set({x:340+(bp*2.04),y:50,scaleX:0.02*gp});
gameUI.cObjects['ws_red'].set({x:340+(bp*2.04)+(gp*2.04),y:50,scaleX:0.02*rp});

gameUI.cObjects['ws_black_text'].text=bp+"%";
gameUI.cObjects['ws_red_text'].text=rp+"%";



var bp=Math.round(oddCnt/(gameStat[39]/100));
var rp=Math.round(evenCnt/(gameStat[39]/100));
var gp=Math.round(greenCnt/(gameStat[39]/100));



gameUI.cObjects['ws_odd'].set({x:340,scaleX:0.02*bp});
gameUI.cObjects['ws_green2'].set({x:340+(bp*2.04),scaleX:0.02*gp});
gameUI.cObjects['ws_even'].set({x:340+(bp*2.04)+(gp*2.04),scaleX:0.02*rp});

gameUI.cObjects['ws_odd_text'].text=bp+"%";
gameUI.cObjects['ws_even_text'].text=rp+"%";


}

function LoadCount(){

currentResCnt++;




}



function ServerLoad(data){

var servInfo=data.split("&");

if(servInfo[0].split("=")[1]=="state"){
gameInfo.credit=parseFloat(servInfo[1].split("=")[1]);

gameUI.cObjects['ui_lbl_credit'].text="Cash: "+gameInfo.cashType+ NumFormat(gameInfo.credit)+"   Bet: "+gameInfo.cashType+ NumFormat(gameInfo.bet)+"   Win: "+gameInfo.cashType+ NumFormat(gameInfo.win);

gameSettings['sndMusicNum']=RandomInt(99,103);
PlayMusic(gameSettings['sndMusicNum']);


}

if(servInfo[0].split("=")[1]=="spin"){
gameInfo.wheelNumber=parseInt(servInfo[1].split("=")[1]);
gameInfo.credit=parseFloat(servInfo[2].split("=")[1]);


gameStat[gameInfo.wheelNumber]++;
gameUI.UpdateCounters();

gameInfo.credit=parseFloat(servInfo[3].split("=")[1]);
gameInfo.win=parseInt(servInfo[4].split("=")[1]);
/////////////////
gameWheel.wheelDirection=-gameWheel.wheelDirection;
gameInfo.rad = 180;
gameInfo.xoff = 160;
gameInfo.yoff = 125;
gameInfo.pi = Math.PI;
//inc = 0.06;
gameInfo.inc = 0.08;
gameInfo.t=0;
gameInfo.k=0.76*gameWheel.wheelDirection;
gameInfo.rollCnt=0.76;

gameWheel.sp=1.8*gameWheel.wheelDirection;

gameWheel.toBall=false;
gameWheel.bSpeed=0.01;
gameWheel.rollTime=0;
gameWheel.rollTime2=0;
gameWheel.rollTime3=0.005;
gameWheel.rollTimeLimit=RandomInt(50,100)+RandomInt(50,100);

PlaySound("39_tmp.SoundClass");
PlaySound("17_tmp.SoundClass");
gameState="rollBall";

///////////////////

}

}



function ShowWin(){


PlaySound(RandomInt(97,98)+"_tmp.SoundClass");
AddNumToStat(gameInfo.wheelNumber);
var numArr=gameUI.GetNumForBet(153);
var clr=numArr.indexOf(gameInfo.wheelNumber);

var numArr=gameUI.GetNumForBet(154);
var clr2=numArr.indexOf(gameInfo.wheelNumber);

if(clr!=-1){
gameUI.cObjects['winnum'].gotoAndStop(0);
}else if(clr2!=-1){
gameUI.cObjects['winnum'].gotoAndStop(1);
}else{
gameUI.cObjects['winnum'].gotoAndStop(2);
}


gameUI.cObjects['view'].addChild(gameUI.cObjects['winnum']);
gameUI.cObjects['view'].removeChild(gameUI.cObjects['weather']);
gameUI.cObjects['view'].removeChild(gameUI.cObjects['wheelstat']);

gameUI.cObjects['view'].addChild(gameUI.cObjects['winnum_text']);
gameUI.cObjects['view'].addChild(gameUI.cObjects['winnum_text2']);


gameUI.cObjects['winnum_text'].text=gameInfo.wheelNumber;
gameUI.cObjects['winnum_text2'].text=gameInfo.wheelNumber;

setTimeout(function(){if(!gameSettings['sndVoice']){return;}PlaySound(numId[gameInfo.wheelNumber])},1300);
setTimeout(function(){gameUI.ShowWinFrame(gameInfo.wheelNumber);},2000);


gameUI.UpdateCounters();

//PlaySound("58_tmp.SoundClass"); //winsound


}

function ServerSend(dataSend){

gameState="Spin";

if(dataSend.action=="spin" && gameUI.cObjects['wheelstat'].name=="open"){
gameUI.StatShowWheel(false);
}

$.post("ge_server.php",dataSend,ServerLoad,"text");

}

function GET(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function CreateGame(){

/*create game objects*/
gameBack.Create();
gameUI.Create();
gameWheel.Create();




//main container to stage
stage.addChild(mainGame);


//add game objects to stage
mainGame.addChild(gameBack.GetSprite('view'));
mainGame.addChild(gameWheel.GetSprite('view'));
mainGame.addChild(gameUI.GetSprite('view'));




FillWeatherBox();

ServerSend(new Object(
    {
        action:"state",
        domain: GET('domain'),
        id: GET('id'),
        game: GET('game'),
        is_real: GET('is_real'),
        round_id: GET('round_id')
    }
    ));

}



function TrackbetShow(bname){
var trackbetName=new Array(-1,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26,0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5);




for(var i=1; i<=41; i++){
gameUI.cObjects['trackbet_c'+i].alpha=0.01;

}

if(bname==38){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.LightBet2(5);
gameUI.LightBet2(8);
gameUI.LightBet2(11);

gameUI.LightBet2(10);
gameUI.LightBet2(13);
gameUI.LightBet2(16);

gameUI.LightBet2(23);
gameUI.LightBet2(24);

gameUI.LightBet2(27);
gameUI.LightBet2(30);
gameUI.LightBet2(33);
gameUI.LightBet2(36);
return;
}

if(bname==39){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.LightBet2(1);
gameUI.LightBet2(6);
gameUI.LightBet2(9);

gameUI.LightBet2(14);
gameUI.LightBet2(17);
gameUI.LightBet2(20);

gameUI.LightBet2(31);
gameUI.LightBet2(34);


return;
}

if(bname==40){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.LightBet2(0);
gameUI.LightBet2(2);
gameUI.LightBet2(3);

gameUI.LightBet2(4);
gameUI.LightBet2(7);
gameUI.LightBet2(12);

gameUI.LightBet2(15);
gameUI.LightBet2(18);

gameUI.LightBet2(19);
gameUI.LightBet2(21);
gameUI.LightBet2(22);
gameUI.LightBet2(25);
gameUI.LightBet2(26);
gameUI.LightBet2(28);

gameUI.LightBet2(29);
gameUI.LightBet2(32);
gameUI.LightBet2(35);
	return;
}

if(bname==41){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.LightBet2(0);
gameUI.LightBet2(3);
gameUI.LightBet2(12);

gameUI.LightBet2(15);
gameUI.LightBet2(26);
gameUI.LightBet2(32);

gameUI.LightBet2(35);


return;

}

if(gameInfo.trackbetCnt>0){

var pos=trackbetName.indexOf(bname);

gameUI.cObjects['trackbet_c'+pos].alpha=0.5;

for(var i=1; i<=gameInfo.trackbetCnt; i++){
var ps=(pos+i);

if(ps>37){
ps=0+ps-37;
}

gameUI.cObjects['trackbet_c'+ps].alpha=0.5;

gameUI.LightBet2(trackbetName[ps]);

}


for(var i=1; i<=gameInfo.trackbetCnt; i++){

var ps=(pos-i);

if(ps<1){
ps=37+ps;
}

gameUI.cObjects['trackbet_c'+ps].alpha=0.5;
gameUI.LightBet2(trackbetName[ps]);

}


}else{

var pos=trackbetName.indexOf(bname);

gameUI.cObjects['trackbet_c'+pos].alpha=0.5;
gameUI.LightBet2(bname);
}


};

function TrackbetHide(bname){



var trackbetName=new Array(0,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26,0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5);

for(var i=1; i<=37; i++){
gameUI.cObjects['trackbet_c'+i].alpha=0.01;
gameUI.HideLightBet(bname);
}


};
function TrackbetSelect(bname){

var trackbetName=new Array(-1,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26,0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5);

console.log("bname=",bname);

if(bname==38){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.AddBet(5,gameInfo.currentBet);
gameUI.AddBet(8,gameInfo.currentBet);
gameUI.AddBet(11,gameInfo.currentBet);

gameUI.AddBet(10,gameInfo.currentBet);
gameUI.AddBet(13,gameInfo.currentBet);
gameUI.AddBet(16,gameInfo.currentBet);

gameUI.AddBet(23,gameInfo.currentBet);
gameUI.AddBet(24,gameInfo.currentBet);

gameUI.AddBet(27,gameInfo.currentBet);
gameUI.AddBet(30,gameInfo.currentBet);
gameUI.AddBet(33,gameInfo.currentBet);
gameUI.AddBet(36,gameInfo.currentBet);
return;
}

if(bname==39){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.AddBet(1,gameInfo.currentBet);
gameUI.AddBet(6,gameInfo.currentBet);
gameUI.AddBet(9,gameInfo.currentBet);

gameUI.AddBet(14,gameInfo.currentBet);
gameUI.AddBet(17,gameInfo.currentBet);
gameUI.AddBet(20,gameInfo.currentBet);

gameUI.AddBet(31,gameInfo.currentBet);
gameUI.AddBet(34,gameInfo.currentBet);


return;
}

if(bname==40){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.AddBet(0,gameInfo.currentBet);
gameUI.AddBet(2,gameInfo.currentBet);
gameUI.AddBet(3,gameInfo.currentBet);

gameUI.AddBet(4,gameInfo.currentBet);
gameUI.AddBet(7,gameInfo.currentBet);
gameUI.AddBet(12,gameInfo.currentBet);

gameUI.AddBet(15,gameInfo.currentBet);
gameUI.AddBet(18,gameInfo.currentBet);

gameUI.AddBet(19,gameInfo.currentBet);
gameUI.AddBet(21,gameInfo.currentBet);
gameUI.AddBet(22,gameInfo.currentBet);
gameUI.AddBet(25,gameInfo.currentBet);
gameUI.AddBet(26,gameInfo.currentBet);
gameUI.AddBet(28,gameInfo.currentBet);

gameUI.AddBet(29,gameInfo.currentBet);
gameUI.AddBet(32,gameInfo.currentBet);
gameUI.AddBet(35,gameInfo.currentBet);
	return;
}

if(bname==41){

gameUI.cObjects['trackbet_c'+bname].alpha=0.5;

gameUI.AddBet(0,gameInfo.currentBet);
gameUI.AddBet(3,gameInfo.currentBet);
gameUI.AddBet(12,gameInfo.currentBet);

gameUI.AddBet(15,gameInfo.currentBet);
gameUI.AddBet(26,gameInfo.currentBet);
gameUI.AddBet(32,gameInfo.currentBet);

gameUI.AddBet(35,gameInfo.currentBet);


return;

}


if(gameInfo.trackbetCnt>0){

var pos=trackbetName.indexOf(bname);

gameUI.cObjects['trackbet_c'+pos].alpha=0.5;

for(var i=1; i<=gameInfo.trackbetCnt; i++){
var ps=(pos+i);

if(ps>37){
ps=0+ps-37;
}

gameUI.AddBet(trackbetName[ps],gameInfo.currentBet);
}


for(var i=1; i<=gameInfo.trackbetCnt; i++){

var ps=(pos-i);

if(ps<1){
ps=37+ps;
}

gameUI.AddBet(trackbetName[ps],gameInfo.currentBet);


}


}

var pos=trackbetName.indexOf(bname);

gameUI.AddBet(trackbetName[pos],gameInfo.currentBet);






};

function UpdateGame(){

if(gameState=="Load"){

if(currentResCnt>currentResCnt2){
currentResCnt2++;
}

var prc=Math.round(currentResCnt2/(totalResCnt/100));

gamePreloader.ptext.text=prc+"%";
var wp=gamePreloader.cc.getBounds();

gamePreloader.cc.set({x:490,y:320,scaleX:0.7,scaleY:0.7});

gamePreloader.circle_g[gamePreloader.circle_gcnt].alpha=1;

gamePreloader.circle_gcnt++;

if(gamePreloader.circle_gcnt>23){
gamePreloader.circle_gcnt=0;
}



for(var jj=0; jj<24; jj++){

if(jj<Math.round(prc/4)){
gamePreloader.circle_g2[jj].visible=true;
}

if(gamePreloader.circle_g[jj].alpha>0){
gamePreloader.circle_g[jj].alpha-=0.041;
}
////////////////////
}


if(prc>=100){
createjs.Ticker.setFPS(40);

stage.removeChild(gamePreloader.cc);
delete gamePreloader;
CreateGame();
gameState="Idle";
}

}

if(gameState=="Idle"){
gameWheel.RotateWheel();
gameWheel.BallToNumber();
}
if(gameState=="Idle2"){
gameWheel.RotateWheel();

gameWheel.BallJump();
}

if(gameState=="rollBall"){
gameWheel.RotateWheel();
gameWheel.BallMove();
}

if(gameState=="dropBall"){
gameWheel.RotateWheel();
gameWheel.BallDrop();
}

stage.update();

}
