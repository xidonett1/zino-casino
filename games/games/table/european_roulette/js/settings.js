


function RandomInt(min, max)
{

  return Math.floor(Math.random() * (max - min + 1)) + min;

};

function NumFormat(n){

    var a, s = Number(n).toFixed(2);

    while (a = s.match(/\d(\d{3}[^\d])/)) s = s.replace(a[1], "," + a[1]);

    return s;

}


function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;


  while (0 !== currentIndex) {


    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;


    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}




/////////////////////////////game setting




var blinkState=false;

var gameInfo=new Object(null);
    gameInfo.betline=1;
    gameInfo.cashType="$";
	gameInfo.bet=0;
	gameInfo.credit=0;
	gameInfo.win=0;
	gameInfo.currentBet="50";
	gameInfo.fieldsCnt=4;
	gameInfo.bJump=-10;
	gameInfo.autoStart=false;
gameInfo.betFrames=new Array();
gameInfo.wheelNumber=RandomInt(0,36);

gameInfo.bc=0;
////////////////////
gameInfo.betsHistory=new Array();
gameInfo.betsHistory2=new Array();
////////////////////
/*
gameInfo.betFrames['1']=0;
gameInfo.betFrames['5']=1;
gameInfo.betFrames['10']=2;
gameInfo.betFrames['20']=3;
gameInfo.betFrames['25']=4;
gameInfo.betFrames['50']=5;
gameInfo.betFrames['100']=6;
gameInfo.betFrames['200']=7;
gameInfo.betFrames['250']=8;
gameInfo.betFrames['500']=9;
gameInfo.betFrames['750']=10;
gameInfo.betFrames['1000']=11;
gameInfo.betFrames['2000']=12;
gameInfo.betFrames['2500']=13;
gameInfo.betFrames['5000']=14;
gameInfo.betFrames['10000']=15;
gameInfo.betFrames['20000']=16;
gameInfo.betFrames['25000']=17;
gameInfo.betFrames['50000']=18;
gameInfo.betFrames['75000']=19;
gameInfo.betFrames['100000']=20;
gameInfo.betFrames['150000']=21;
gameInfo.betFrames['200000']=22;
gameInfo.betFrames['500000']=23;
gameInfo.betFrames['1000000']=24;
gameInfo.betFrames['2000000']=25;
gameInfo.betFrames['5000000']=26;
*/




gameInfo.betFrames['50']=5;
gameInfo.betFrames['100']=6;
gameInfo.betFrames['200']=7;
gameInfo.betFrames['500']=9;
gameInfo.betFrames['1000']=11;
gameInfo.betFrames['2000']=12;
gameInfo.betFrames['2500']=13;
gameInfo.betFrames['5000']=14;
gameInfo.betFrames['10000']=15;
gameInfo.betFrames['20000']=16;
gameInfo.betFrames['25000']=17;
gameInfo.betFrames['50000']=18;

gameInfo.betFrames['100000']=20;

gameInfo.soundVolume=1;

/*paytable*/
////0-36
var paytable=new Array();
paytable['number']=36;
paytable['split']=18;
paytable['three']=12;
paytable['square']=9;
paytable['six']=6;
paytable['column']=3;
paytable['dozen']=3;
paytable['half']=2;

gameInfo.btIntr=0;
var betLimit=new Array();

for(var i=0; i<=36;i++){

betLimit[i]=new Array();
betLimit[i][0]=1;
betLimit[i][1]=10;


}

for(var i=37; i<=96;i++){

betLimit[i]=new Array();
betLimit[i][0]=1;
betLimit[i][1]=10;


}
for(var i=97; i<=110;i++){

betLimit[i]=new Array();
betLimit[i][0]=1;
betLimit[i][1]=40;


}
for(var i=111; i<=133;i++){

betLimit[i]=new Array();
betLimit[i][0]=1;
betLimit[i][1]=60;


}
for(var i=134; i<=156;i++){

betLimit[i]=new Array();
betLimit[i][0]=1;
betLimit[i][1]=100;


}

var gameSettings=new Array();
gameSettings['betlight']=true;
gameSettings['quick']=false;
gameSettings['sndVoice']=true;
gameSettings['sndMusic']=true;
gameSettings['sndEffects']=true;

gameSettings['sndMusicNum']=99;

var gameStat=new Array();
gameStat[39]=0;
for(var i=0; i<=36;i++){
var bcnt=RandomInt(0,5);
gameStat[i]=bcnt;
gameStat[39]+=bcnt;

}

var specialBets=new Array();

specialBets[0]="0:0|43:1|64:1|70:1";
specialBets[1]="0:0|41:1|47:1|52:1|55:1|58:1|76:1|82:1";
specialBets[2]="0:0|1:1|6:1|9:1|14:1|17:1|20:1|31:1|34:1";
specialBets[3]="0:0|0:1|10:1|20:1|30:1";
specialBets[4]="0:0|1:1|11:1|21:1|28:1";
specialBets[5]="0:0|2:1|12:1|22:1|32:1";
specialBets[6]="0:0|3:1|13:1|23:1|33:1";








