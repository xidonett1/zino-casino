
////////////////buttons class///////////////////

function ButtonUI(btnLabel,btnName,btnImg,blinkImg){

var btnEvent=new CustomEvent("ui_btn",{detail:{bname:btnName}});


var blink=new createjs.Bitmap(blinkImg);
this.cButton=new createjs.Container();	
this.spriteSheet = new createjs.SpriteSheet({ images: [btnImg],frames: {width:btnImg.width, height:(btnImg.height/4)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});
this.btnSprite = new createjs.Sprite(this.spriteSheet, "enabled");
//this.btnSprite = new createjs.Bitmap(btnImg);
this.btnSprite.name=btnName;


var blinkInt=0;
var blinkSpeed=0.05;

this.btnText=new createjs.Text(btnLabel, "25px VerdanaBold", "#000000");
this.btnText2=new createjs.Text(btnLabel, "25px VerdanaBold", "#FFFFFF");
this.btnText3=new createjs.Text(btnLabel, "25px VerdanaBold", "#FFFFFF");

var textAligned=this.btnText.getBounds();

this.btnText.set({textAlign:"center",x:(btnImg.width/2),y:(btnImg.height/4)/4});
this.btnText2.set({textAlign:"center",x:(btnImg.width/2),y:(btnImg.height/4)/4});
this.btnText3.set({textAlign:"center",x:(btnImg.width/2),y:(btnImg.height/4)/4});
this.btnText3.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);
this.btnText2.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);
this.btnText.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);



this.cButton.addChild(this.btnSprite);

this.cButton.addChild(blink);
this.cButton.addChild(this.btnText3);
this.cButton.addChild(this.btnText2);
this.cButton.addChild(this.btnText);



blink.alpha=0.0;
blink.set({x:7,y:5});

this.ShowBlink=function(){
	
this.blinkInt=setInterval(this.Blinked,50);	
blink.alpha=blinkSpeed;	
blinkSpeed=0.05;
};

this.HideBlink=function(){
	
this.blinkInt=setInterval(this.Blinked,50);	
	
};

this.Blinked=function(){

blink.alpha+=blinkSpeed;

if(blink.alpha>0.50){
blinkSpeed=-blinkSpeed;	
}	
if(blink.alpha<0.05){
blinkSpeed=-blinkSpeed;	
}
	
	

}

this.Disable=function(){

this.btnSprite.gotoAndStop("disabled");
this.btnText.color="#5B5B5B";
this.btnText.shadow = new createjs.Shadow("#E9E9E9", 0, 0, 3);	

this.btnText2.color="#5B5B5B";
this.btnText2.shadow = new createjs.Shadow("#E9E9E9", 0, 0, 3);	

this.btnText3.color="#5B5B5B";
this.btnText3.shadow = new createjs.Shadow("#E9E9E9", 0, 0, 3);	


};

this.Enable=function(){

this.btnSprite.gotoAndStop("enabled");

this.btnText.color="#000000";
this.btnText.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);	


this.btnText2.color="#FFFFFF";
this.btnText2.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);	


this.btnText3.color="#FFFFFF";
this.btnText3.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);	


};

this.ChangeText=function(newText){

this.btnText.text=newText;
	
};


this.btnSprite.addEventListener("mouseover",function(ev){
	
	if(ev.currentTarget.currentAnimation=="enabled"){
	ev.currentTarget.gotoAndStop("onover");
	}
	
	});
	
this.btnSprite.addEventListener("mouseout",function(ev){
	
	if(ev.currentTarget.currentAnimation=="onover"){
	ev.currentTarget.gotoAndStop("enabled");
	}
	
	});	
this.btnSprite.addEventListener("mousedown",function(ev){
	
	if(ev.currentTarget.currentAnimation!="disabled"){
	ev.currentTarget.gotoAndStop("pressed");
	}
	
	});
	
this.btnSprite.addEventListener("pressup",function(ev){
	
	if(ev.currentTarget.currentAnimation!="disabled"){
	ev.currentTarget.gotoAndStop("onover");
	
	dispatchEvent(btnEvent);
	
	}
	
	});	


	
}

/////////////////////////////////

 (function () {
        function GradientText(text, font, color) {
            this.Text_constructor(text, font, color);
        }

        var p = createjs.extend(GradientText, createjs.Text);

        p._drawTextLine = function (ctx, text, y) {
            if (this.gradient) {
                var height = this.getMeasuredLineHeight();
                var my_gradient = ctx.createLinearGradient(0, y, 0, y + height+2);
                my_gradient.addColorStop(0, "#FB5415");
                my_gradient.addColorStop(1, "#FBDE29");

                ctx.fillStyle = my_gradient;
                ctx.fillText(text, 0, y, this.maxWidth || 0xFFFF);
            } else {
                this.Text__drawTextLine(ctx, text, y);
            }
        };

        window.GradientText = createjs.promote(GradientText, "Text");
    }());
    





///////////////////////////////////


/////////////UI class////////////////////
function GameUI(){

GameObj.apply(this);	

this.cResources	=new Array("images/ui/fieldbackground.png");	


this.Create=function(){

////objects
this.cObjects['view']=new createjs.Container();
this.cObjects['ui_bg']=new createjs.Bitmap(this.cImages['fieldbackground']);
this.cObjects['view'].addChild(this.cObjects['ui_bg']);

this.cObjects['view'].set({x:400,y:890});

}

	
}