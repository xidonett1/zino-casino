


/////////////UI class////////////////////
function GameWheel(){

GameObj.apply(this);	

this.cResources	=new Array("images/wheel/tower.png","images/wheel/front_2.png","images/wheel/front.png","images/wheel/wheel.jpg","images/wheel/wheel_s.png","images/wheel/ball.png");	

this.sp=0.3;
this.isDrop=false;
this.bSpeed=0.01;
this.rollTime=0;
this.rollTimeLimit=0;
this.wheelDirection=1;
this.dl=0;
this.towerFrame=0;

this.RotateWheel=function(){
this.dl++;

if(this.dl>2-(this.sp*gameWheel.wheelDirection)){

if(gameWheel.wheelDirection<0){
this.towerFrame--;
}else{	
this.towerFrame++;
}
this.dl=0;
if(this.towerFrame>89){
this.towerFrame=0;	
}
if(this.towerFrame<0){
this.towerFrame=89;	
}
this.cObjects['tower'].gotoAndStop(this.towerFrame);
	
	
	
	
}


if(gameState=="Idle" && (this.sp>0.3 || this.sp<-0.3)){
this.sp-=0.015*gameWheel.wheelDirection;	
	
}


this.cObjects['wheel_b'].rotation+=this.sp;


}


this.BallToNumber=function(){





var pt =  this.cObjects['wheel_b'].localToGlobal( this.cObjects['wheel_points'][gameInfo.wheelNumber].x,  this.cObjects['wheel_points'][gameInfo.wheelNumber].y);

  
 var pt3 = this.cObjects['view'].globalToLocal(pt.x, pt.y);

this.cObjects['ball'].x=pt3.x;
this.cObjects['ball'].y=pt3.y;

}

this.BallJump=function(){

gameWheel.rollTime2+=gameWheel.rollTime3;
var pt =  this.cObjects['wheel_b'].localToGlobal( this.cObjects['wheel_points'][gameInfo.wheelNumber].x,  this.cObjects['wheel_points'][gameInfo.wheelNumber].y);

  
 var pt3 = this.cObjects['view'].globalToLocal(pt.x, pt.y);	
this.cObjects['ball'].x=pt3.x;
this.cObjects['ball'].y=pt3.y;





this.cObjects['ball'].y+=Math.cos(gameWheel.rollTime2);


/*if(gameWheel.rollTime2>0.4 || gameWheel.rollTime2<0.4){
gameWheel.rollTime3=gameWheel.rollTime3*-1;	
}*/






};

this.BallMove=function(){



this.cObjects['ball'].x =(gameInfo.rad*Math.sin(gameInfo.t)) + gameInfo.xoff;
this.cObjects['ball'].y =(gameInfo.rad*gameInfo.k*Math.cos(gameInfo.t)) + gameInfo.yoff;
gameInfo.t += gameInfo.inc; 
gameWheel.rollTime++;



if(gameInfo.inc>=0.03){
gameInfo.inc-=0.00005;


}



if((this.sp>0.3 || this.sp<-0.3)){
this.sp-=0.001*gameWheel.wheelDirection;
	
}



var pt =  this.cObjects['wheel_b'].localToGlobal( this.cObjects['wheel_points'][gameInfo.wheelNumber].x,  this.cObjects['wheel_points'][gameInfo.wheelNumber].y);

  
 var pt3 = this.cObjects['view'].globalToLocal(pt.x, pt.y);




var c = Math.sqrt(Math.pow(pt3.x - this.cObjects['ball'].x, 2) + Math.pow(pt3.y - this.cObjects['ball'].y, 2));	

 if(gameWheel.rollTime>gameWheel.rollTimeLimit && c<130 && gameInfo.bc>c){
StopSound("17_tmp.SoundClass");
PlaySound(RandomInt(18,30)+"_tmp.SoundClass");
gameInfo.inc2=0.1;
gameInfo.t2=4;
gameState="dropBall";
gameWheel.rollTime=0;
gameWheel.rollTimeLimit=30;
gameWheel.xx=this.cObjects['ball'].x+(159-this.cObjects['ball'].x)/2.2;	
gameWheel.yy=this.cObjects['ball'].y+(100-this.cObjects['ball'].y)/2.2;		

}	

gameInfo.bc=c;

};


this.BallDrop=function(){




if((this.sp>0.3 || this.sp<-0.3)){
this.sp-=0.001*gameWheel.wheelDirection;
	
}

if(gameInfo.inc2<0.5){

gameInfo.inc2+=0.02;	
	
}
 

gameInfo.t2 -= gameInfo.inc2; 	 

if(gameInfo.t2<-2){gameInfo.t2=RandomInt(3,4);PlaySound(RandomInt(18,30)+"_tmp.SoundClass");gameWheel.toBall=!gameWheel.toBall;}
	 


//////////////////////




var pt =  this.cObjects['wheel_b'].localToGlobal( this.cObjects['wheel_points'][gameInfo.wheelNumber].x,  this.cObjects['wheel_points'][gameInfo.wheelNumber].y);

  
 var pt3 = this.cObjects['view'].globalToLocal(pt.x, pt.y);




var c = Math.sqrt(Math.pow(pt3.x - this.cObjects['ball'].x, 2) + Math.pow(pt3.y - this.cObjects['ball'].y, 2));	

if(c<10){
gameState="Idle";
setTimeout(ShowWin,600);
PlaySound(RandomInt(31,35)+"_tmp.SoundClass");
}


this.cObjects['ball'].x+=(gameWheel.xx-this.cObjects['ball'].x)*0.03;
this.cObjects['ball'].y+=(gameWheel.yy-this.cObjects['ball'].y)*0.03;	

if(gameWheel.toBall){
this.cObjects['ball'].x+=(pt3.x-this.cObjects['ball'].x)*0.05;
this.cObjects['ball'].y+=(pt3.y-this.cObjects['ball'].y)*0.05;
}


if(c<20){
this.cObjects['ball'].x+=(pt3.x-this.cObjects['ball'].x)*0.5;
this.cObjects['ball'].y+=(pt3.y-this.cObjects['ball'].y)*0.5;
}else{

this.cObjects['ball'].y +=(Math.cos(gameInfo.t2))*4;


}


};

this.Create=function(){




////objects

var spriteSheet = new createjs.SpriteSheet({ images:[this.cImages['tower']],frames: {width:(this.cImages['tower'].width/10), height:(this.cImages['tower'].height/9)},animations: {anim:[0,89,"anim",1]}});
this.cObjects['tower'] = new createjs.Sprite(spriteSheet, "anim");	
/*var spriteSheet = new createjs.SpriteSheet({ images:[this.cImages['wheel']],frames: {width:(this.cImages['wheel'].width/10), height:(this.cImages['wheel'].height/9)},animations: {anim:[0,89,"anim",0.01]}});
this.cObjects['wheel'] = new createjs.Sprite(spriteSheet);	


*/
this.cObjects['tower'].gotoAndStop(0);

this.cObjects['wheel_c'] = new createjs.Container();	
this.cObjects['wheel_b'] = new createjs.Container();	
this.cObjects['wheel_bb'] = new createjs.Container();	

this.cObjects['wheel'] = new createjs.Bitmap(this.cImages['wheel_s']);	

this.cObjects['wheel_b'].addChild(this.cObjects['wheel']);

//this.cObjects['wheel'].set({regX:160,regY:160});
this.cObjects['wheel_b'].set({regX:160,regY:161});


this.cObjects['wheel_c'].set({scaleY:0.71,scaleX:0.98,x:160,y:117});
//this.cObjects['wheel_c'].set({x:159,y:418});



this.cObjects['wheel_points']=new Array();





this.cObjects['wheel_c'].addChild(this.cObjects['wheel_b']);


this.cObjects['view']=new createjs.Container();
this.cObjects['front']=new createjs.Bitmap(this.cImages['front']);
this.cObjects['front2']=new createjs.Bitmap(this.cImages['front_2']);
this.cObjects['view'].addChild(this.cObjects['wheel_c']);
this.cObjects['view'].addChild(this.cObjects['front']);



this.cObjects['front2'].set({x:-355,y:130});

this.cObjects['tower'].set({x:110,y:13});

this.cObjects['view'].set({x:355,y:47});

this.xy=0;

var self=this;

for(var i=0; i<=36;i++){

this.cObjects['wheel_points'][i] = new createjs.Shape();
this.cObjects['wheel_points'][i].graphics.beginFill("#ff0000").drawRect(0, 0, 5, 5);	
this.cObjects['wheel_points'][i].visible=false;
	this.cObjects['wheel_b'].addChild(this.cObjects['wheel_points'][i]);
	
	
}

self.cObjects['wheel_points'][0].set({x:157.16612377850163,y:43.279069767441854});
 self.cObjects['wheel_points'][1].set({x:74.61237785016289,y:247.0507399577167});
 self.cObjects['wheel_points'][2].set({x:257.23127035830623,y:91.98942917547572});
 self.cObjects['wheel_points'][3].set({x:120.47557003257327,y:50.58562367864698});
 self.cObjects['wheel_points'][4].set({x:228.8794788273616,y:64.38689217758986});
 self.cObjects['wheel_points'][5].set({x:150.49511400651465,y:281.9598308668077});
 self.cObjects['wheel_points'][6].set({x:278.9120521172639,y:168.3023255813954});
 self.cObjects['wheel_points'][7].set({x:57.93485342019545,y:94.42494714587741});
 self.cObjects['wheel_points'][8].set({x:211.3680781758958,y:266.5348837209302});
 self.cObjects['wheel_points'][9].set({x:37.92182410423453,y:172.3615221987315});
 self.cObjects['wheel_points'][10].set({x:170.50814332247558,y:279.52431289640595});
 self.cObjects['wheel_points'][11].set({x:247.22475570032577,y:242.17970401691332});
 self.cObjects['wheel_points'][12].set({x:85.45276872964172,y:67.63424947145882});
 self.cObjects['wheel_points'][13].set({x:267.2377850162867,y:206.45877378435523});
 self.cObjects['wheel_points'][14].set({x:52.93159609120522,y:212.14164904862582});
 self.cObjects['wheel_points'][15].set({x:193.85667752442998,y:48.15010570824529});
 self.cObjects['wheel_points'][16].set({x:110.46905537459281,y:269.7822410147992});
 self.cObjects['wheel_points'][17].set({x:273.90879478827367,y:128.52219873150108});
 self.cObjects['wheel_points'][18].set({x:44.592833876221505,y:133.39323467230446});
 self.cObjects['wheel_points'][19].set({x:212.20195439739416,y:54.644820295983095});
 self.cObjects['wheel_points'][20].set({x:59.602605863192196,y:228.3784355179704});
 self.cObjects['wheel_points'][21].set({x:243.0553745928339,y:78.18816067653279});
 self.cObjects['wheel_points'][22].set({x:39.589576547231275,y:150.4418604651163});
 self.cObjects['wheel_points'][23].set({x:190.5211726384365,y:275.4651162790698});
 self.cObjects['wheel_points'][24].set({x:127.98045602605862,y:276.276955602537});
 self.cObjects['wheel_points'][25].set({x:264.7361563517916,y:110.66173361522203});
 self.cObjects['wheel_points'][26].set({x:137.98697068403908,y:45.7145877378436});
 self.cObjects['wheel_points'][27].set({x:273.90879478827367,y:189.4101479915434});
 self.cObjects['wheel_points'][28].set({x:71.2768729641694,y:80.62367864693448});
 self.cObjects['wheel_points'][29].set({x:49.596091205211735,y:113.09725158562372});
 self.cObjects['wheel_points'][30].set({x:228.04560260586322,y:256.79281183932346});
 self.cObjects['wheel_points'][31].set({x:43.75895765472313,y:191.03382663847782});
 self.cObjects['wheel_points'][32].set({x:176.34527687296418,y:44.90274841437633});
 self.cObjects['wheel_points'][33].set({x:89.62214983713358,y:259.2283298097252});
 self.cObjects['wheel_points'][34].set({x:275.5765472312704,y:148.81818181818187});
 self.cObjects['wheel_points'][35].set({x:101.29641693811072,y:58.704016913319265});
 self.cObjects['wheel_points'][36].set({x:255.5635179153095,y:225.94291754756875});

/*
this.cObjects['wheel'].addEventListener("click",function(ev){
	self.cObjects['wheel_points'][self.xy].set({x:ev.localX,y:ev.localY});

console.log("self.cObjects['wheel_points']["+self.xy+"].set({x:"+ev.localX+",y:"+ev.localY+"});");self.xy++;

});

*//*
this.cObjects['view'].addEventListener("click",function(ev){console.log(ev);});
*/
this.cObjects['ball'] = new createjs.Bitmap(this.cImages['ball']);	
this.cObjects['ball'].set({regX:10.5,regY:10.5,scaleX:0.65,scaleY:0.65});
this.cObjects['view'].addChild(this.cObjects['ball']);

this.cObjects['view'].addChild(this.cObjects['front2']);

this.cObjects['view'].addChild(this.cObjects['tower']);


};





	
}