
////////////////buttons class///////////////////

function ButtonUI(btnLabel,btnName,btnImg,blinkImg){

var btnEvent=new CustomEvent("ui_btn",{detail:{bname:btnName}});
var btnEvent2=new CustomEvent("ui_btn_overout",{detail:{bname:btnName}});


var blink=new createjs.Bitmap(blinkImg);
this.cButton=new createjs.Container();
this.spriteSheet = new createjs.SpriteSheet({ images: [btnImg],frames: {width:btnImg.width, height:(btnImg.height/4)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});
this.btnSprite = new createjs.Sprite(this.spriteSheet, "enabled");
//this.btnSprite = new createjs.Bitmap(btnImg);
this.btnSprite.name=btnName;


var blinkInt=0;
var blinkSpeed=0.05;

this.btnText=new createjs.Text(btnLabel, "22px Arial", "#000000");
this.btnText2=new createjs.Text(btnLabel, "22px Arial", "#FFFFFF");
this.btnText3=new createjs.Text(btnLabel, "22px Arial", "#FFFFFF");

var textAligned=this.btnText.getBounds();

this.btnText.set({textAlign:"center",x:(btnImg.width/2),y:(btnImg.height/4)/4});
this.btnText2.set({textAlign:"center",x:(btnImg.width/2),y:(btnImg.height/4)/4});
this.btnText3.set({textAlign:"center",x:(btnImg.width/2),y:(btnImg.height/4)/4});
this.btnText3.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);
this.btnText2.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);
this.btnText.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);



this.cButton.addChild(this.btnSprite);

this.cButton.addChild(blink);
this.cButton.addChild(this.btnText3);
this.cButton.addChild(this.btnText2);
this.cButton.addChild(this.btnText);



blink.alpha=0.0;
blink.set({x:7,y:5});

this.ShowBlink=function(){

this.blinkInt=setInterval(this.Blinked,50);
blink.alpha=blinkSpeed;
blinkSpeed=0.05;
};

this.HideBlink=function(){

this.blinkInt=setInterval(this.Blinked,50);

};

this.Blinked=function(){

blink.alpha+=blinkSpeed;

if(blink.alpha>0.50){
blinkSpeed=-blinkSpeed;
}
if(blink.alpha<0.05){
blinkSpeed=-blinkSpeed;
}



}

this.Disable=function(){

this.btnSprite.gotoAndStop("disabled");
this.btnText.color="#5B5B5B";
this.btnText.shadow = new createjs.Shadow("#E9E9E9", 0, 0, 3);

this.btnText2.color="#5B5B5B";
this.btnText2.shadow = new createjs.Shadow("#E9E9E9", 0, 0, 3);

this.btnText3.color="#5B5B5B";
this.btnText3.shadow = new createjs.Shadow("#E9E9E9", 0, 0, 3);


};

this.Enable=function(){

this.btnSprite.gotoAndStop("enabled");

this.btnText.color="#000000";
this.btnText.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);


this.btnText2.color="#FFFFFF";
this.btnText2.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);


this.btnText3.color="#FFFFFF";
this.btnText3.shadow = new createjs.Shadow("#FFFFFF", 0, 0, 3);


};

this.ChangeText=function(newText){

this.btnText.text=newText;

};


this.btnSprite.addEventListener("mouseover",function(ev){

	if(ev.currentTarget.currentAnimation=="enabled"){
	ev.currentTarget.gotoAndStop("onover");

	}

	});

this.btnSprite.addEventListener("mouseout",function(ev){

	if(ev.currentTarget.currentAnimation=="onover"){
	ev.currentTarget.gotoAndStop("enabled");

	}

	});
this.btnSprite.addEventListener("mousedown",function(ev){

	if(ev.currentTarget.currentAnimation!="disabled"){
	ev.currentTarget.gotoAndStop("pressed");
	}

	});

this.btnSprite.addEventListener("pressup",function(ev){

	if(ev.currentTarget.currentAnimation!="disabled"){
	ev.currentTarget.gotoAndStop("onover");

	dispatchEvent(btnEvent);

	}

	});



}

/////////////////////////////////


function Button2UI(btnLabel,btnName,btnImg,blinkImg,chNum){

var btnEvent=new CustomEvent("ui_btn",{detail:{bname:btnName}});
var btnEvent2=new CustomEvent("ui_btn_overout",{detail:{bname:btnName}});


var blink=new createjs.Bitmap(blinkImg);
this.cButton=new createjs.Container();
this.spriteSheet = new createjs.SpriteSheet({ images: [btnImg],frames: {width:btnImg.width, height:(btnImg.height/4)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});
this.btnSprite = new createjs.Sprite(this.spriteSheet, "enabled");
//this.btnSprite = new createjs.Bitmap(btnImg);
this.btnSprite.name=btnName;


var blinkInt=0;
var blinkSpeed=0.05;

this.btnText=new createjs.Text(btnLabel, "14px Arial", "#A5EA00");
this.btnText2=new createjs.Text(chNum, "14px Arial", "#A5EA00");


var textAligned=this.btnText.getBounds();

this.btnText.set({textAlign:"left",x:20,y:(btnImg.height/4)/4});
this.btnText2.set({textAlign:"right",x:220,y:(btnImg.height/4)/4});





this.cButton.addChild(this.btnSprite);

this.cButton.addChild(blink);
this.cButton.addChild(this.btnText3);
this.cButton.addChild(this.btnText2);
this.cButton.addChild(this.btnText);



blink.alpha=0.0;
blink.set({x:7,y:5});

this.ShowBlink=function(){

this.blinkInt=setInterval(this.Blinked,50);
blink.alpha=blinkSpeed;
blinkSpeed=0.05;
};

this.HideBlink=function(){

this.blinkInt=setInterval(this.Blinked,50);

};

this.Blinked=function(){

blink.alpha+=blinkSpeed;

if(blink.alpha>0.50){
blinkSpeed=-blinkSpeed;
}
if(blink.alpha<0.05){
blinkSpeed=-blinkSpeed;
}



}

this.Disable=function(){

this.btnSprite.gotoAndStop("disabled");
this.btnText.color="#666666";


this.btnText2.color="#666666";





};

this.Enable=function(){

this.btnSprite.gotoAndStop("enabled");

this.btnText.color="#A5EA00";



this.btnText2.color="#A5EA00";







};
var self=this;
this.ChangeText=function(newText){

this.btnText.text=newText;

};


this.btnSprite.addEventListener("mouseover",function(ev){

	if(ev.currentTarget.currentAnimation=="enabled"){
	ev.currentTarget.gotoAndStop("onover");
	self.btnText.color="#FFFFFF";
	self.btnText2.color="#FFFFFF";
	}

	});

this.btnSprite.addEventListener("mouseout",function(ev){

	if(ev.currentTarget.currentAnimation=="onover"){
	ev.currentTarget.gotoAndStop("enabled");
	self.btnText.color="#A5EA00";
	self.btnText2.color="#A5EA00";
	}

	});
this.btnSprite.addEventListener("mousedown",function(ev){

	if(ev.currentTarget.currentAnimation!="disabled"){
	ev.currentTarget.gotoAndStop("pressed");
	}

	});

this.btnSprite.addEventListener("pressup",function(ev){

	if(ev.currentTarget.currentAnimation!="disabled"){
	ev.currentTarget.gotoAndStop("onover");

	dispatchEvent(btnEvent);

	}

	});



}

/////////////////////////////////////

 (function () {
        function GradientText(text, font, color) {
            this.Text_constructor(text, font, color);
        }

        var p = createjs.extend(GradientText, createjs.Text);

        p._drawTextLine = function (ctx, text, y) {
            if (this.gradient) {
                var height = this.getMeasuredLineHeight();
                var my_gradient = ctx.createLinearGradient(0, y, 0, y + height+2);
                my_gradient.addColorStop(0, "#FB5415");
                my_gradient.addColorStop(1, "#FBDE29");

                ctx.fillStyle = my_gradient;
                ctx.fillText(text, 0, y, this.maxWidth || 0xFFFF);
            } else {
                this.Text__drawTextLine(ctx, text, y);
            }
        };

        window.GradientText = createjs.promote(GradientText, "Text");
    }());







/////////////UI class////////////////////
function GameUI(){

GameObj.apply(this);

this.cResources	=new Array("images/ui/bcell2.png","images/ui/bcell3.png","images/ui/bcell1.png","images/ui/311.png","images/ui/color_stat.png","images/ui/ws_cover.png","images/ui/pie_btn.png","images/ui/pie.png","images/ui/auto_stop.png","images/ui/auto.png","images/ui/auto_cell.png","images/ui/wnd_close.png","images/ui/info.png","images/ui/wheelstat.png","images/ui/fieldbackground.png","images/ui/left.png","images/ui/right.png","images/ui/fieldbackground2.png","images/ui/b_settings.png","images/ui/b_snd.png","images/ui/b_snd2.png","images/ui/b_auto.png",
"images/ui/b_info1.png","images/ui/b_info2.png","images/ui/b_info3.png","images/ui/b_info4.png","images/ui/b_info5.png","images/ui/b_info6.png","images/ui/b_info7.png","images/ui/b_info8.png",
"images/ui/b_spin.png","images/ui/chips.png","images/ui/bt.png","images/ui/chips3d.png","images/ui/zero.png","images/ui/weather_box.png","images/ui/weather_num.png","images/ui/winnum.png",
"images/ui/settings.png","images/ui/bet_wnd.png","images/ui/sound_set.png","images/ui/sound_set1.png","images/ui/sound_set2.png","images/ui/trackbet_b2.png","images/ui/trackbet_b1.png","images/ui/track.png","images/ui/track_c1.png","images/ui/track_c2.png","images/ui/track_c3.png","images/ui/track_c4.png","images/ui/track_c5.png","images/ui/track_c6.png","images/ui/soundtrack.png","images/ui/checkbox.png");

////////////////////////////////////////

var self=this;

this.ShowWinFrame=function(bname){





this.cObjects['bet_places2'][166].visible=true;
this.cObjects['bet_places2'][167].visible=true;
this.cObjects['bet_places2'][168].visible=true;



this.cObjects['bet_places2'][166].x=this.cObjects['bet_places2'][bname].x;
this.cObjects['bet_places2'][166].y=this.cObjects['bet_places2'][bname].y;



this.cObjects['bet_places2'][167].x=this.cObjects['bet_places2'][bname].x;
this.cObjects['bet_places2'][167].y=this.cObjects['bet_places2'][bname].y;



this.cObjects['bet_places2'][168].x=this.cObjects['bet_places2'][bname].x;
this.cObjects['bet_places2'][168].y=this.cObjects['bet_places2'][bname].y;


this.cObjects['bet_places2'][168].set({scaleY:0.86,scaleX:1.1,alpha:1});
this.cObjects['bet_places2'][167].set({scaleY:0.86,scaleX:1.1,alpha:1});
this.cObjects['bet_places2'][166].set({scaleY:0.86,scaleX:1.1,alpha:1});


createjs.Tween.get(this.cObjects['bet_places2'][166]).to({scaleY:0.76,scaleX:1}, 200, createjs.Ease.linear).to({scaleY:0.80,scaleX:1.05}, 100, createjs.Ease.linear).to({scaleY:0.76,scaleX:1}, 200, createjs.Ease.linear).to({scaleY:0.56,scaleX:0.8}, 500, createjs.Ease.linear);
createjs.Tween.get(this.cObjects['bet_places2'][167]).to({scaleY:0.66,scaleX:0.8}, 300, createjs.Ease.linear).to({scaleY:0.86,scaleX:1.06}, 200, createjs.Ease.linear).to({scaleY:0.66,scaleX:0.8}, 300, createjs.Ease.linear).to({scaleY:0.36,scaleX:0.6,alpha:0}, 100, createjs.Ease.linear);
createjs.Tween.get(this.cObjects['bet_places2'][168]).to({scaleY:0.56,scaleX:0.8}, 500, createjs.Ease.linear).to({scaleY:0.86,scaleX:1.06}, 200, createjs.Ease.linear).to({scaleY:0.66,scaleX:0.8}, 300, createjs.Ease.linear).to({scaleY:0.36,scaleX:0.6,alpha:0}, 100, createjs.Ease.linear);

if(gameInfo.win>0){
PlaySound("58_tmp.SoundClass");
this.cObjects['win_text'].set({x:this.cObjects['bet_places2'][bname].x,y:this.cObjects['bet_places2'][bname].y,visible:true,alpha:0});
this.cObjects['ui_lbl_wintext1'].text=gameInfo.cashType+NumFormat(gameInfo.win);
this.cObjects['ui_lbl_wintext2'].text=gameInfo.cashType+NumFormat(gameInfo.win);
createjs.Tween.get(this.cObjects['win_text']).to({y:this.cObjects['bet_places2'][bname].y-80,alpha:1}, 500, createjs.Ease.getBackInOut(1)).wait(1000).to({y:this.cObjects['bet_places2'][bname].y,alpha:0,visible:false}, 500, createjs.Ease.getBackInOut(1));



}


if(gameInfo.win>0){
setTimeout(function(){self.ShowWinChips();PlaySound("11_tmp.SoundClass");},2500);
setTimeout(function(){self.AnimWinChips();},3200);
setTimeout(function(){self.ResetGame();},4200);
}else{
setTimeout(function(){self.ResetGame();},2500);
}

};


this.StatShowWheel=function(md){

if(md){
SetWheelStat();
createjs.Tween.get(this.cObjects['wheelstat']).to({x:340}, 500, createjs.Ease.getBackInOut(0.2));
this.cObjects['wheelstat'].name="open";
}else{

this.cObjects['wheelstat'].name="close";
createjs.Tween.get(this.cObjects['wheelstat']).to({x:841}, 500, createjs.Ease.getBackInOut(0.2));

}


}


this.ResetGame=function(){

this.RemoveAllBets();
PlaySound("5_tmp.SoundClass");
gameUI.cObjects['view'].addChild(gameUI.cObjects['wheelstat']);
this.cObjects['view'].removeChild(this.cObjects['winnum']);
this.cObjects['view'].addChild(this.cObjects['weather']);
this.cObjects['view'].removeChild(this.cObjects['winnum_text']);
this.cObjects['view'].removeChild(this.cObjects['winnum_text2']);

this.UnlockButtons();

};


this.ChipOver=function(ev){

if(ev.currentTarget.y>652 && ev.currentTarget.y<=662){
createjs.Tween.get(ev.currentTarget).to({y:652}, 200, createjs.Ease.linear);

}


}
this.ChipOut=function(ev){

if(ev.currentTarget.y>=652 && ev.currentTarget.y<=662){
createjs.Tween.get(ev.currentTarget).to({y:662}, 200, createjs.Ease.linear);

}

};

this.ChipPosition=function(ev){

for(var i=0; i<self.chipSprites.length;i++){

if(self.chipSprites[i].y==652){
createjs.Tween.get(self.chipSprites[i]).to({y:662}, 200, createjs.Ease.linear).call(self.ChipPosition);

}

}

};

this.ChipClick=function(ev){

for(var i=0; i<self.chipSprites.length;i++){

if(self.chipSprites[i].name!=ev.currentTarget.name && self.chipSprites[i].y<=662){
createjs.Tween.get(self.chipSprites[i]).to({y:662}, 200, createjs.Ease.linear).call(self.ChipPosition);

}

}


if(ev.currentTarget.y>=652 && ev.currentTarget.y<=662){

createjs.Tween.get(ev.currentTarget).to({y:650}, 50, createjs.Ease.getBackIn(1));
gameInfo.currentBet=ev.currentTarget.name+"";
}

};


this.MoveBet=function(dr){



var ii=-1;

if(dr=="btnRight"){
var ii=-1;
if(self.chipPos[self.chipSprites.length-1]==3){
return;
}
}else{
var ii=1;

if(self.chipPos[0]==0){
return;
}

}



for(var i=0;i<self.chipSprites.length; i++){

self.chipPos[i]+=ii;

createjs.Tween.get(self.chipSprites[i]).to({x:self.posCoordinates[self.chipPos[i]].x,y:self.posCoordinates[self.chipPos[i]].y}, 200, createjs.Ease.linear);

}


};

///////////////////////////////////////
this.CreateButtons=function(){

this.cObjects['ui_btnSettings']=new ButtonUI(" ","btnSettings",this.cImages['b_settings'],new Image());
this.cObjects['ui_btnSnd1']=new ButtonUI(" ","btnSnd1",this.cImages['b_snd'],new Image());
this.cObjects['ui_btnSnd2']=new ButtonUI(" ","btnSnd2",this.cImages['b_snd2'],new Image());
this.cObjects['ui_btnAuto']=new ButtonUI(" ","btnAuto",this.cImages['b_auto'],new Image());

this.cObjects['ui_btnSpin']=new ButtonUI(" ","btnSpin",this.cImages['b_spin'],new Image());

this.cObjects['ui_btnLeft']=new ButtonUI(" ","btnLeft",this.cImages['left'],new Image());
this.cObjects['ui_btnRight']=new ButtonUI(" ","btnRight",this.cImages['right'],new Image());





this.cObjects['ui_btnSettings'].cButton.set({x:5,y:739});
this.cObjects['ui_btnSnd1'].cButton.set({x:38,y:739});
this.cObjects['ui_btnSnd2'].cButton.set({x:38,y:739});
this.cObjects['ui_btnAuto'].cButton.set({x:68,y:739});
this.cObjects['ui_btnSpin'].cButton.set({x:908,y:645});

this.cObjects['ui_btnLeft'].cButton.set({x:330,y:672});
this.cObjects['ui_btnRight'].cButton.set({x:640,y:672});


/////add stage



this.cObjects['view'].addChild(this.cObjects['ui_btnSettings'].cButton);
this.cObjects['view'].addChild(this.cObjects['ui_btnSnd1'].cButton);
this.cObjects['view'].addChild(this.cObjects['ui_btnAuto'].cButton);
this.cObjects['view'].addChild(this.cObjects['ui_btnSpin'].cButton);

this.cObjects['view'].addChild(this.cObjects['ui_btnLeft'].cButton);
this.cObjects['view'].addChild(this.cObjects['ui_btnRight'].cButton);

/////add stage
var xo=0;
for(var i=1;i<=8;i++){

if(i>4){xo=350;}

this.cObjects['ui_btnInfo'+i]=new ButtonUI(" ","btnInfo"+i,this.cImages['b_info'+i],new Image());
this.cObjects['ui_btnInfo'+i].cButton.set({x:140+((i-1)*46)+xo,y:664});
this.cObjects['view'].addChild(this.cObjects['ui_btnInfo'+i].cButton);
}


};
////////////////////////////////

this.LockButtons=function(){

this.cObjects['ui_btnSettings'].Disable();
this.cObjects['ui_btnSnd2'].Disable();
this.cObjects['ui_btnAuto'].Disable();
this.cObjects['ui_btnSpin'].Disable();

this.cObjects['ui_btnLeft'].Disable();
this.cObjects['ui_btnRight'].Disable();

for(var i=1;i<=8;i++){



this.cObjects['ui_btnInfo'+i].Disable();


}

for(var i=0; i<=156;i++){




this.cObjects['bet_places2'][i].removeAllEventListeners("mouseover",function(ev){self.LightBet(ev.currentTarget.name);});
this.cObjects['bet_places2'][i].removeAllEventListeners("mouseout",function(ev){self.HideLightBet(ev.currentTarget.name);});
this.cObjects['bet_places2'][i].removeAllEventListeners("click",function(ev){self.AddBet(ev.currentTarget.name,gameInfo.currentBet)});

}

};
this.UnlockButtons=function(){

this.cObjects['ui_btnSettings'].Enable();
this.cObjects['ui_btnSnd2'].Enable();
this.cObjects['ui_btnAuto'].Enable();
this.cObjects['ui_btnSpin'].Enable();

this.cObjects['ui_btnLeft'].Enable();
this.cObjects['ui_btnRight'].Enable();

for(var i=1;i<=8;i++){



this.cObjects['ui_btnInfo'+i].Enable();


}


for(var i=0; i<=156;i++){




this.cObjects['bet_places2'][i].addEventListener("mouseover",function(ev){self.LightBet(ev.currentTarget.name);});
this.cObjects['bet_places2'][i].addEventListener("mouseout",function(ev){self.HideLightBet(ev.currentTarget.name);});
this.cObjects['bet_places2'][i].addEventListener("click",function(ev){self.AddBet(ev.currentTarget.name,gameInfo.currentBet)});

}

};


this.RemoveAllBets=function(){





for(var i=0; i<=156;i++){
var bp=self.cObjects['bet_places2chips'][i];
bp.curBet=0;
self.ReBet(i);
bp.sprtBet=new Array();
bp.betCnt=0;
gameInfo.bet=0;
self.UpdateCounters();
}



};

this.DoubleAllBets=function(){

if(gameInfo.credit<gameInfo.bet){
return;
}

gameInfo.bet=0;

for(var i=0; i<=156;i++){
var bp=self.cObjects['bet_places2chips'][i];



if(bp.curBet>0 && (betLimit[i][1] >=(bp.curBet*2)/100) && (gameInfo.credit>=(gameInfo.bet+parseFloat(bp.curBet*2))/100) ){
bp.curBet=bp.curBet*2;
gameInfo.bet+=bp.curBet;
self.ReBet(i);
}else{
bp.curBet=bp.curBet;
gameInfo.bet+=bp.curBet;
}

}


self.UpdateCounters();

};

this.ReturnLastBets=function(){
gameInfo.bet=0;

for(var i=0; i<=156;i++){
var bp=self.cObjects['bet_places2chips'][i];

if(gameInfo.betsHistory[i]>0){
bp.curBet=gameInfo.betsHistory[i];
gameInfo.bet+=bp.curBet;
self.ReBet(i);
}

}
self.UpdateCounters();
};

this.RemoveLastBet=function(){

if(gameInfo.betsHistory2.length>0){
var bt=gameInfo.betsHistory2.pop();
var bp=self.cObjects['bet_places2chips'][bt[0]];
gameInfo.bet-=bt[1];
bp.curBet-=bt[1];

self.ReBet(bt[0]);
self.UpdateCounters();

}

};

this.ShowWinChips=function(){




for(var i=0; i<=156;i++){
var bp=self.cObjects['bet_places2chips'][i];



if(bp.curBet>0){

var numArr=this.GetNumForBet(i);
var isWin=numArr.indexOf(gameInfo.wheelNumber);

if(isWin!=-1){

/*

paytable['number']=35;
paytable['split']=17;
paytable['three']=11;
paytable['square']=8;
paytable['six']=5;
paytable['column']=2;
paytable['dozen']=2;
paytable['half']=1;


*/
var bLimit=0;
if(i<=36){
bLimit=paytable['number'];
}else if(i>36 && i<=96 ){
bLimit=paytable['split'];
}else if(i>96 && i<=110){
bLimit=paytable['three'];
}else if(i>110 && i<=133){
bLimit=paytable['square'];
}else if(i>133 && i<=144){
bLimit=paytable['six'];
}else if(i>144 && i<=150){
bLimit=paytable['column'];
}else if(i>150 && i<=156){
bLimit=paytable['half'];
}

bp.curBet=bp.curBet*bLimit;
self.ReBet(i);
}else{
bp.curBet=0;
self.ReBet(i);
}


}


}



};


this.AnimWinChips=function(){


for(var i=0; i<=156;i++){
var bp=self.cObjects['bet_places2chips'][i];

if(bp.curBet>0){

for(var j=bp.sprtBet.length-1; j>=0;j--){

	var chSpeed=RandomInt(100,200);
createjs.Tween.get(bp.sprtBet[j]).wait(j*chSpeed).to({x:500,y:800}, 300, createjs.Ease.linear);

}

}


}


};

this.GetNumForBet=function(bname){

var bLimit=new Array();
var bnArr=new Array();


for(var i=0; i<=36;i++){


bnArr[i]=new Array();
bnArr[i].push(i);
}

bnArr[37]=new Array(0,1);
bnArr[38]=new Array(1,4);
bnArr[39]=new Array(4,7);
bnArr[40]=new Array(7,10);
bnArr[41]=new Array(10,13);
bnArr[42]=new Array(13,16);
bnArr[43]=new Array(16,19);
bnArr[44]=new Array(19,22);
bnArr[45]=new Array(22,25);
bnArr[46]=new Array(25,28);
bnArr[47]=new Array(28,31);
bnArr[48]=new Array(31,34);

bnArr[49]=new Array(0,2);
bnArr[50]=new Array(2,5);
bnArr[51]=new Array(5,8);
bnArr[52]=new Array(8,11);
bnArr[53]=new Array(11,14);
bnArr[54]=new Array(14,17);
bnArr[55]=new Array(17,20);
bnArr[56]=new Array(20,23);
bnArr[57]=new Array(23,26);
bnArr[58]=new Array(26,29);
bnArr[59]=new Array(29,32);
bnArr[60]=new Array(32,35);

bnArr[61]=new Array(0,3);
bnArr[62]=new Array(3,6);
bnArr[63]=new Array(6,9);
bnArr[64]=new Array(9,12);
bnArr[65]=new Array(12,15);
bnArr[66]=new Array(15,18);
bnArr[67]=new Array(18,21);
bnArr[68]=new Array(21,24);
bnArr[69]=new Array(24,27);
bnArr[70]=new Array(27,30);
bnArr[71]=new Array(30,33);
bnArr[72]=new Array(33,36);


bnArr[73]=new Array(2,1);
bnArr[74]=new Array(5,4);
bnArr[75]=new Array(8,7);
bnArr[76]=new Array(11,10);
bnArr[77]=new Array(14,13);
bnArr[78]=new Array(17,16);
bnArr[79]=new Array(20,19);
bnArr[80]=new Array(23,22);
bnArr[81]=new Array(26,25);
bnArr[82]=new Array(29,28);
bnArr[83]=new Array(32,31);
bnArr[84]=new Array(35,34);

bnArr[85]=new Array(3,2);
bnArr[86]=new Array(6,5);
bnArr[87]=new Array(9,8);
bnArr[88]=new Array(12,11);
bnArr[89]=new Array(15,14);
bnArr[90]=new Array(18,17);
bnArr[91]=new Array(21,20);
bnArr[92]=new Array(24,23);
bnArr[93]=new Array(27,26);
bnArr[94]=new Array(30,29);
bnArr[95]=new Array(33,32);
bnArr[96]=new Array(36,35);

bnArr[97]=new Array(0,3,2);
bnArr[98]=new Array(0,1,2);

bnArr[99]=new Array(1,2,3);
bnArr[100]=new Array(4,5,6);
bnArr[101]=new Array(7,8,9);
bnArr[102]=new Array(10,11,12);
bnArr[103]=new Array(13,14,15);
bnArr[104]=new Array(16,17,18);
bnArr[105]=new Array(19,20,21);
bnArr[106]=new Array(22,23,24);
bnArr[107]=new Array(25,26,27);
bnArr[108]=new Array(28,29,30);
bnArr[109]=new Array(31,32,33);
bnArr[110]=new Array(34,35,36);

bnArr[111]=new Array(0,1,2,3);


bnArr[112]=new Array(1,2,4,5);
bnArr[113]=new Array(4,5,7,8);
bnArr[114]=new Array(7,8,10,11);
bnArr[115]=new Array(10,11,13,14);
bnArr[116]=new Array(13,14,17,16);
bnArr[117]=new Array(16,17,19,20);
bnArr[118]=new Array(19,20,22,23);
bnArr[119]=new Array(22,23,25,26);
bnArr[120]=new Array(25,26,28,29);
bnArr[121]=new Array(28,29,31,32);
bnArr[122]=new Array(31,32,34,35);


bnArr[123]=new Array(2,3,5,6);
bnArr[124]=new Array(5,6,8,9);
bnArr[125]=new Array(8,9,11,12);
bnArr[126]=new Array(11,12,14,15);
bnArr[127]=new Array(14,15,17,18);
bnArr[128]=new Array(17,18,20,21);
bnArr[129]=new Array(20,21,23,24);
bnArr[130]=new Array(23,24,26,27);
bnArr[131]=new Array(26,27,29,30);
bnArr[132]=new Array(29,30,32,33);
bnArr[133]=new Array(32,33,35,36);


bnArr[134]=new Array(1,2,3,4,5,6);
bnArr[135]=new Array(4,5,6,7,8,9);
bnArr[136]=new Array(7,8,9,10,11,12);
bnArr[137]=new Array(10,11,12,13,14,15);
bnArr[138]=new Array(13,14,15,16,17,18);
bnArr[139]=new Array(16,17,18,19,20,21);
bnArr[140]=new Array(19,20,21,22,23,24);
bnArr[141]=new Array(22,23,24,25,26,27);
bnArr[142]=new Array(25,26,27,28,29,30);
bnArr[143]=new Array(28,29,30,31,32,33);
bnArr[144]=new Array(31,32,33,34,35,36);

bnArr[145]=new Array(3,6,9,12,15,18,21,24,27,30,33,36);
bnArr[146]=new Array(2,5,8,11,14,17,20,23,26,29,32,35);
bnArr[147]=new Array(1,4,7,10,13,16,19,22,25,28,31,34);

bnArr[148]=new Array(1,2,3,4,5,6,7,8,9,10,11,12);
bnArr[149]=new Array(13,14,15,16,17,18,19,20,21,22,23,24);
bnArr[150]=new Array(25,26,27,28,29,30,31,32,33,34,35,36);

bnArr[151]=new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
bnArr[152]=new Array(2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36);
bnArr[153]=new Array(1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36);
bnArr[154]=new Array(2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35);
bnArr[155]=new Array(1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35);
bnArr[156]=new Array(19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36);



return bnArr[bname];
};

var self=this;

this.UpdateBetWnd=function(bname){

var bp=self.cObjects['bet_places2chips'][bname];

if(bp.curBet<=0){

gameUI.cObjects['bt1'].text="MINIMUM BET :";
gameUI.cObjects['btt1'].text="\n"+ gameInfo.cashType+NumFormat(betLimit[bname][0]);

gameUI.cObjects['bt2'].text="MAXIMUM BET :";
gameUI.cObjects['btt2'].text="\n"+ gameInfo.cashType+NumFormat(betLimit[bname][1]);

}else{

var bLimit=0;
var i=bname;
if(i<=36){
bLimit=paytable['number'];
}else if(i>36 && i<=96 ){
bLimit=paytable['split'];
}else if(i>96 && i<=110){
bLimit=paytable['three'];
}else if(i>110 && i<=133){
bLimit=paytable['square'];
}else if(i>133 && i<=144){
bLimit=paytable['six'];
}else if(i>144 && i<=150){
bLimit=paytable['column'];
}else if(i>150 && i<=156){
bLimit=paytable['half'];
}

gameUI.cObjects['bt1'].text="BET PLACED :";
gameUI.cObjects['btt1'].text="\n"+ gameInfo.cashType+NumFormat(bp.curBet/100);

gameUI.cObjects['bt2'].text="PAYS :";
gameUI.cObjects['btt2'].text="\n"+ gameInfo.cashType+NumFormat((bp.curBet/100)*bLimit);
}

};

this.LightBet=function(bname){

var blArr=this.GetNumForBet(bname);


	for(var i=0; i<blArr.length;i++){
	this.cObjects['bet_places'][blArr[i]].visible=true;


	}
clearTimeout(gameInfo.btIntr);

gameInfo.btIntr=setTimeout(function(){

gameUI.cObjects['bet_wnd'].x=self.cObjects['bet_places2'][bname].x-80;
gameUI.cObjects['bet_wnd'].y=self.cObjects['bet_places2'][bname].y-260;
gameUI.cObjects['bet_wnd'].visible=true;
gameUI.cObjects['bet_wnd'].alpha=0;

self.UpdateBetWnd(bname);

createjs.Tween.get(gameUI.cObjects['bet_wnd']).to({y:self.cObjects['bet_places2'][bname].y-124,alpha:1}, 200, createjs.Ease.getBackInOut(1));

},800);

};

this.LightBet2=function(bname){

var blArr=this.GetNumForBet(bname);


	for(var i=0; i<blArr.length;i++){
	this.cObjects['bet_places'][blArr[i]].visible=true;


	}
clearTimeout(gameInfo.btIntr);

};
this.HideLightBet=function(bname){

for(i=0; i<=36;i++){
	this.cObjects['bet_places'][i].visible=false;
	}

clearTimeout(gameInfo.btIntr);
gameUI.cObjects['bet_wnd'].visible=false;

};

var self=this;

this.ReBet=function(bname){


this.cObjects['bet_places2'][166].visible=false;
this.cObjects['bet_places2'][167].visible=false;
this.cObjects['bet_places2'][168].visible=false;

var bp=self.cObjects['bet_places2chips'][bname];
var bp2=self.cObjects['bet_places2'][bname];
var betsArr=new Array();
for (var key in gameInfo.betFrames) {
    if (gameInfo.betFrames.hasOwnProperty(key)){

	betsArr.push(parseFloat(key));
	}

}

betsArr.reverse();

var boxBet=bp.curBet;

///////////////
for(var i=0; i<bp.betCnt; i++){

self.cObjects['view'].removeChild(bp.sprtBet[i]);

}

delete bp.sprtBet;
bp.sprtBet=new Array();
bp.betCnt=0;
//////////////


for(var i=0; i<betsArr.length; i++){

var bet100=Math.floor(boxBet/(betsArr[i]));

boxBet=boxBet-(betsArr[i]*bet100);

for(var j=0; j<bet100; j++){

bp.sprtBet[bp.betCnt]=self.cObjects['t_chip'].clone();
bp.sprtBet[bp.betCnt].set({x:bp2.x,y:bp2.y-(bp.betCnt*3)});
bp.sprtBet[bp.betCnt].gotoAndStop(gameInfo.betFrames[betsArr[i]]);

self.cObjects['view'].addChild(bp.sprtBet[bp.betCnt]);

bp.betCnt++;
}



}

self.cObjects['view'].removeChild(self.cObjects['bet_wnd']);
self.cObjects['view'].addChild(self.cObjects['bet_wnd']);

self.UpdateBetWnd(bname);

};

this.UpdateCounters=function(bname,nom){

self.cObjects['ui_lbl_credit'].text="Cash: "+gameInfo.cashType+ NumFormat(gameInfo.credit)+"   Bet: "+gameInfo.cashType+ NumFormat(gameInfo.bet/100)+"   Win: "+gameInfo.cashType+ NumFormat(gameInfo.win);


}


this.SettingsPage=function(){


};

this.AddBet=function(bname,nom){

if(gameState!="Idle"){

return;
}





var bp=self.cObjects['bet_places2chips'][bname];
var bp2=self.cObjects['bet_places2'][bname];


if(betLimit[bname][1] <(bp.curBet+parseFloat(nom))/100 || (gameInfo.credit<(gameInfo.bet+parseFloat(nom))/100) ){

return;
}

self.UpdateBetWnd(bname);

gameInfo.betsHistory2.push(new Array(bname,parseFloat(nom)));

if(bp.curBet<=0){
PlaySound("12_tmp.SoundClass");
}else{
PlaySound("15_tmp.SoundClass");
}

bp.curBet=bp.curBet+parseFloat(nom);


bp.sprtBet[bp.betCnt]=self.cObjects['t_chip'].clone();
bp.sprtBet[bp.betCnt].set({x:bp2.x,y:bp2.y-(bp.betCnt*3)});
bp.sprtBet[bp.betCnt].gotoAndStop(gameInfo.betFrames[nom]);

self.cObjects['view'].addChild(bp.sprtBet[bp.betCnt]);

bp.betCnt++;

self.ReBet(bname);

gameInfo.bet+=parseFloat(nom);

self.UpdateCounters();

};


this.Create=function(){

////objects
this.cObjects['view']=new createjs.Container();
this.cObjects['ui_bg']=new createjs.Bitmap(this.cImages['fieldbackground']);
this.cObjects['ui_bg2']=new createjs.Bitmap(this.cImages['fieldbackground2']);
this.cObjects['ui_bg2'].set({x:0,y:670});
this.cObjects['ui_bg'].set({x:0,y:698});


this.cObjects['view'].addChild(this.cObjects['ui_bg2']);

///////////////Chip Button////////////////////


this.chipSprites=new Array();
this.chipDenom=new Array();
this.chipPos=new Array();
this.posCoordinates=new Array();

this.posCoordinates[-18]=new Object({x:624,y:778});
this.posCoordinates[-17]=new Object({x:624,y:778});
this.posCoordinates[-16]=new Object({x:624,y:778});
this.posCoordinates[-15]=new Object({x:624,y:778});
this.posCoordinates[-14]=new Object({x:624,y:778});
this.posCoordinates[-13]=new Object({x:624,y:778});
this.posCoordinates[-12]=new Object({x:624,y:778});
this.posCoordinates[-11]=new Object({x:624,y:778});
this.posCoordinates[-10]=new Object({x:320,y:720});
this.posCoordinates[-9]=new Object({x:624,y:778});
this.posCoordinates[-8]=new Object({x:624,y:778});
this.posCoordinates[-7]=new Object({x:624,y:778});
this.posCoordinates[-6]=new Object({x:624,y:778});
this.posCoordinates[-5]=new Object({x:624,y:778});
this.posCoordinates[-4]=new Object({x:624,y:778});
this.posCoordinates[-3]=new Object({x:624,y:778});
this.posCoordinates[-2]=new Object({x:624,y:778});
this.posCoordinates[-1]=new Object({x:320,y:720});
this.posCoordinates[0]=new Object({x:370,y:662});
this.posCoordinates[1]=new Object({x:438,y:662});
this.posCoordinates[2]=new Object({x:506,y:662});
this.posCoordinates[3]=new Object({x:574,y:662});
this.posCoordinates[4]=new Object({x:624,y:720});
this.posCoordinates[5]=new Object({x:624,y:778});
this.posCoordinates[6]=new Object({x:624,y:778});
this.posCoordinates[7]=new Object({x:624,y:778});
this.posCoordinates[8]=new Object({x:624,y:778});
this.posCoordinates[9]=new Object({x:624,y:778});
this.posCoordinates[10]=new Object({x:624,y:778});
this.posCoordinates[11]=new Object({x:624,y:778});
this.posCoordinates[12]=new Object({x:624,y:778});
this.posCoordinates[13]=new Object({x:624,y:778});
this.posCoordinates[14]=new Object({x:624,y:778});
this.posCoordinates[15]=new Object({x:624,y:778});
this.posCoordinates[16]=new Object({x:624,y:778});
this.posCoordinates[17]=new Object({x:624,y:778});
this.posCoordinates[18]=new Object({x:624,y:778});
this.posCoordinates[19]=new Object({x:624,y:778});
this.posCoordinates[20]=new Object({x:624,y:778});


var i=0;

for (var key in gameInfo.betFrames) {

    if (gameInfo.betFrames.hasOwnProperty(key)){////////////




var spriteSheet = new createjs.SpriteSheet({ images: [this.cImages['chips']],frames: {width:this.cImages['chips'].width, height:(this.cImages['chips'].height/30)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});
this.chipSprites[i]=new createjs.Sprite(spriteSheet);
this.chipSprites[i].gotoAndStop(gameInfo.betFrames[key]);
this.chipSprites[i].name=key;

this.cObjects['view'].addChild(this.chipSprites[i]);


this.chipSprites[i].addEventListener("mouseover",this.ChipOver);
this.chipSprites[i].addEventListener("mouseout",this.ChipOut);
this.chipSprites[i].addEventListener("click",this.ChipClick);

this.chipPos[i]=i;
this.chipSprites[i].set({x:this.posCoordinates[i].x,y:this.posCoordinates[i].y});

 i++;

}///////////////

}



////////////////////////////////
/////////////////////////////////
/*

TABLE CHIPS
*/

//////chip proto

var spriteSheet = new createjs.SpriteSheet({ images: [this.cImages['chips3d']],frames: {width:this.cImages['chips3d'].width, height:(this.cImages['chips3d'].height/30)},animations: {bet05:0}});
this.cObjects['t_chip']=new createjs.Sprite(spriteSheet);
this.cObjects['t_chip'].gotoAndStop(0);
this.cObjects['t_chip'].set({regX:16.5,regY:13});
//////////////////////////////




this.cObjects['view'].addChild(this.cObjects['ui_bg']);


////////////////////////////////
/*BETS PLACES*//*BETS PLACES*//*BETS PLACES*//*BETS PLACES*/
this.cObjects['bet_places']=new Array();
this.cObjects['bet_places2']=new Array();
this.cObjects['bet_places2chips']=new Array();

var xt=177;
var yt=422;
var sct=12.1;






for(var i=3; i<=36;i+=3){


this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 58, 44);
this.cObjects['bet_places2'][i].set({regX:29,regY:22,x:xt,y:yt,alpha:0.6,skewX:sct,alpha:0.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

this.cObjects['bet_places'][i] =  new createjs.Bitmap(this.cImages['bt']);

this.cObjects['bet_places'][i].set({regX:29,regY:22,scaleX:1.05,x:xt,y:yt,alpha:0.6,skewX:sct});
this.cObjects['view'].addChild(this.cObjects['bet_places'][i]);

xt+=61;
sct-=2.3;

}



var xt=168;
var yt=466;
var sct=12.0;

i=0;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 58, 44);
this.cObjects['bet_places2'][i].set({regX:29,regY:22,x:xt,y:yt,alpha:0.6,skewX:sct,alpha:0.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);


var xt=168;
var yt=466;
var sct=11;

for(var i=2; i<=35;i+=3){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 58, 44);
this.cObjects['bet_places2'][i].set({regX:29,regY:22,scaleX:1.09,x:xt,y:yt,alpha:0.6,skewX:sct,alpha:0.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

this.cObjects['bet_places'][i] =  new createjs.Bitmap(this.cImages['bt']);

this.cObjects['bet_places'][i].set({regX:29,regY:22,x:xt,y:yt,alpha:0.6,skewX:sct,scaleX:1.03});
this.cObjects['view'].addChild(this.cObjects['bet_places'][i]);

xt+=62.7;
sct-=2.1;

}


var xt=104;
var yt=422;
var sct=0;

/////////ZERO////////////
this.cObjects['bet_places'][0] =  new createjs.Bitmap(this.cImages['zero']);

this.cObjects['bet_places'][0].set({regX:29,regY:22,x:xt,y:yt,alpha:0.6,skewX:sct});
this.cObjects['view'].addChild(this.cObjects['bet_places'][0]);

//////////////////

var xt=160;
var yt=512;
var sct=12.0;



for(var i=1; i<=34;i+=3){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 58, 44);
this.cObjects['bet_places2'][i].set({regX:29,regY:22,x:xt,y:yt,alpha:0.6,skewX:sct,alpha:0.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

this.cObjects['bet_places'][i] =  new createjs.Bitmap(this.cImages['bt']);

this.cObjects['bet_places'][i].set({regX:29,regY:22,x:xt,y:yt,alpha:0.6,skewX:sct,scaleX:1.08,scaleY:1.04});
this.cObjects['view'].addChild(this.cObjects['bet_places'][i]);

xt+=64.2;
sct-=2.1;

}


///////////double places_line_1///////////////
var xt=126;
var yt=512;
var sct=12.0;

for(var i=37; i<=48;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 16, 34);
this.cObjects['bet_places2'][i].set({regX:8,regY:17,x:xt,y:yt,skewX:sct,alpha:0.6,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



xt+=64.5;
sct-=2.1;

}

///////////double places_line_2///////////////
var xt=137;
var yt=466;
var sct=12.0;

for(var i=49; i<=60;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 16, 34);
this.cObjects['bet_places2'][i].set({regX:8,regY:17,x:xt,y:yt,skewX:sct,alpha:0.6,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



xt+=62.6;
sct-=2.1;

}

/////////////////////

///////////double places_line_3///////////////
var xt=146;
var yt=422;
var sct=12.0;

for(var i=61; i<=72;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 16, 34);
this.cObjects['bet_places2'][i].set({regX:8,regY:17,x:xt,y:yt,skewX:sct,alpha:0.6,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



xt+=61.0;
sct-=2.1;

}

/////////////////////
///////////double places_line_1_h///////////////
var xt=164;
var yt=489;
var sct=12.0;

for(var i=73; i<=84;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 44, 10);
this.cObjects['bet_places2'][i].set({regX:22,regY:5,x:xt,y:yt,skewX:sct,alpha:0.6,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



xt+=63.4;
sct-=2.1;

}
////////////////////
///////////double places_line_2_h///////////////
var xt=172;
var yt=444;
var sct=12.0;

for(var i=85; i<=96;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#ffffcc").drawRect(0, 0, 44, 10);
this.cObjects['bet_places2'][i].set({regX:22,regY:5,x:xt,y:yt,skewX:sct,alpha:0.6,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



xt+=62.0;
sct-=2.1;

}
/////////////////////

/////////////////////
///////////three_line zero places///////////////
var xt=142;
var yt=444;
var sct=12.0;

i=97;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#00FF00").drawRect(0, 0, 16, 10);
this.cObjects['bet_places2'][i].set({regX:8,regY:5,x:xt,y:yt,skewX:sct,alpha:0.8,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);


///////////three_line zero places///////////////
var xt=132;
var yt=489;
var sct=12.0;

i=98;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#00FF00").drawRect(0, 0, 16, 10);
this.cObjects['bet_places2'][i].set({regX:8,regY:5,x:xt,y:yt,skewX:sct,alpha:0.8,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



///////////three_line places///////////////
var xt=153;
var yt=537;
var sct=12.0;

for(var i=99; i<=110;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#0066FF").drawRect(0, 0, 44, 10);
this.cObjects['bet_places2'][i].set({regX:22,regY:5,x:xt,y:yt,skewX:sct,alpha:0.8,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



xt+=65.5;
sct-=2.1;

}
////////////////////
//////////QUAD////QUAD//ZEROOO//QUAD////QUAD/1//////////////
var xt=121;
var yt=536;
var sct=12.0;

i=111;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#990099").drawRect(0, 0, 16, 10);
this.cObjects['bet_places2'][i].set({regX:8,regY:5,x:xt,y:yt,skewX:sct,alpha:0.99,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);



xt+=65.5;
sct-=2.1;

//////////QUAD////QUAD////QUAD////QUAD/1//////////////
var xt=196;
var yt=489;
var sct=12.0;
for(var i=112; i<=122;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#990099").drawRect(0, 0, 16, 10);
this.cObjects['bet_places2'][i].set({regX:8,regY:5,x:xt,y:yt,skewX:sct,alpha:0.99,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

xt+=63.4;
sct-=2.1
}


/////////////////////
//////////QUAD////QUAD////QUAD////QUAD///2////////////
var xt=202;
var yt=444;
var sct=12.0;
for(var i=123; i<=133;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#990099").drawRect(0, 0, 16, 10);
this.cObjects['bet_places2'][i].set({regX:8,regY:5,x:xt,y:yt,skewX:sct,alpha:0.8,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

xt+=62.0;
sct-=2.1
}

//////////SIX//SIX//SIX//SIX//////////////
var xt=186;
var yt=537;
var sct=12.0;
for(var i=134; i<=144;i+=1){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#FF00CC").drawRect(0, 0, 16, 10);
this.cObjects['bet_places2'][i].set({regX:8,regY:5,x:xt,y:yt,skewX:sct,alpha:0.99,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

xt+=65.4;
sct-=2.1
}


//////////3-36//////////////
var xt=910;
var yt=422;
var sct=-12.0;

i=145;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#660066").drawRect(0, 0, 58, 44);
this.cObjects['bet_places2'][i].set({regX:29,regY:22,x:xt,y:yt,skewX:sct,alpha:1.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

//////////////////////////////////////

//////////2-35//////////////
var xt=920;
var yt=466;
var sct=-12.0;

i=146;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#660066").drawRect(0, 0, 58, 44);
this.cObjects['bet_places2'][i].set({regX:29,regY:22,x:xt,y:yt,skewX:sct,alpha:1.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

//////////////////////////////////////

//////////1-34//////////////
var xt=931;
var yt=512;
var sct=-12.0;

i=147;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#660066").drawRect(0, 0, 58, 44);
this.cObjects['bet_places2'][i].set({regX:29,regY:22,x:xt,y:yt,skewX:sct,alpha:1.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

//////////////////////////////////////


//////////1-12//////////////
var xt=250;
var yt=563;
var sct=8.0;

i=148;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#333300").drawRect(0, 0, 260, 44);
this.cObjects['bet_places2'][i].set({regX:130,regY:22,x:xt,y:yt,skewX:sct,alpha:1.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

//////////////////////////////////////

//////////13-24//////////////
var xt=512;
var yt=563;
var sct=2.0;

i=149;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#333300").drawRect(0, 0, 260, 44);
this.cObjects['bet_places2'][i].set({regX:130,regY:22,x:xt,y:yt,skewX:sct,alpha:1.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

//////////////////////////////////////
//////////25-36//////////////
var xt=778;
var yt=563;
var sct=-10.0;

i=150;

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#333300").drawRect(0, 0, 260, 44);
this.cObjects['bet_places2'][i].set({regX:130,regY:22,x:xt,y:yt,skewX:sct,alpha:1.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);

//////////////////////////////////////


//////////half//////////////
var xt=174;
var yt=613;
var sct=12.0;

for(var i=151;i<=156;i++){

this.cObjects['bet_places2'][i] = new createjs.Shape();
this.cObjects['bet_places2'][i].graphics.beginFill("#000000").drawRect(0, 0, 130, 44);
this.cObjects['bet_places2'][i].set({regX:65,regY:22,x:xt,y:yt,skewX:sct,alpha:1.01,name:i});
this.cObjects['view'].addChild(this.cObjects['bet_places2'][i]);
xt+=136.0;
sct-=5.0;
}
//////////////////////////////////////

var self=this;

for(var i=0; i<=36;i++){



this.cObjects['bet_places'][i].visible=false;




}
gameUI.cObjects['bet_places2'][0].set({regX:29,regY:22,x:111,y:460,alpha:0.6,skewX:12,scaleY:2.8,alpha:0.8,name:0});
//gameUI.cObjects['bet_places'][0].set({regX:22,regY:22,x:178,y:422,alpha:0.6,skewX:12.1})


/*places |||||||||||| btn set*/
for(var i=0; i<=156;i++){

gameInfo.betsHistory[i]=0;

this.cObjects['bet_places2chips'][i]=new Object({curBet:0,sprtBet:new Array(),betCnt:0});
this.cObjects['bet_places2'][i].addEventListener("mouseover",function(ev){self.LightBet(ev.currentTarget.name);});
this.cObjects['bet_places2'][i].addEventListener("mouseout",function(ev){self.HideLightBet(ev.currentTarget.name);});
this.cObjects['bet_places2'][i].addEventListener("click",function(ev){self.AddBet(ev.currentTarget.name,gameInfo.currentBet)});
this.cObjects['bet_places2'][i].alpha=0.01;
}
/*places |||||||||||| btn set*/
////////////////////////////

this.cObjects['bet_places2'][166] = new createjs.Shape();
this.cObjects['bet_places2'][166].graphics.setStrokeStyle(8,"round").beginStroke("#EDEDBD").drawCircle(0,0, 30);
this.cObjects['bet_places2'][166].set({x:220,y:510,scaleY:0.76});
this.cObjects['bet_places2'][166].shadow=new createjs.Shadow("#FFFF00", 0, 0, 10);
this.cObjects['view'].addChild(this.cObjects['bet_places2'][166]);

this.cObjects['bet_places2'][167] = new createjs.Shape();
this.cObjects['bet_places2'][167].graphics.setStrokeStyle(2,"round").beginStroke("#EDEDBD").drawCircle(0,0, 36);
this.cObjects['bet_places2'][167].set({x:220,y:510,scaleY:0.76});
this.cObjects['bet_places2'][167].shadow=new createjs.Shadow("#FFFF00", 0, 0, 10);
this.cObjects['view'].addChild(this.cObjects['bet_places2'][167]);


this.cObjects['bet_places2'][168] = new createjs.Shape();
this.cObjects['bet_places2'][168].graphics.setStrokeStyle(1,"round").beginStroke("#EDEDBD").drawCircle(0,0, 40);
this.cObjects['bet_places2'][168].set({x:220,y:510,scaleY:0.76});
this.cObjects['bet_places2'][168].shadow=new createjs.Shadow("#FFFF00", 0, 0, 10);
this.cObjects['view'].addChild(this.cObjects['bet_places2'][168]);

this.cObjects['bet_places2'][166].visible=false;
this.cObjects['bet_places2'][167].visible=false;
this.cObjects['bet_places2'][168].visible=false;



this.cObjects['wnd_bet_c'] =  new createjs.Container();
this.cObjects['wnd_bet'] =  new createjs.Bitmap(this.cImages['wnd_bet']);


this.CreateButtons();

////////////////////////////////

/////counters

this.cObjects['ui_lbl_credit']=new createjs.Text("Cash: "+gameInfo.cashType+ NumFormat(gameInfo.credit)+"   Bet: "+gameInfo.cashType+ NumFormat(gameInfo.bet)+"   Win: "+gameInfo.cashType+ NumFormat(gameInfo.win), "14px Arial", "#FFFFFF");


this.cObjects['ui_lbl_credit'].set({textAlign:"center",x:508,y:746});


this.cObjects['view'].addChild(this.cObjects['ui_lbl_credit']);
/////////////////////////win text////////////////////

this.cObjects['win_text'] =  new createjs.Container();
this.cObjects['ui_lbl_wintext1']=new createjs.Text(""+gameInfo.cashType+"50.00", "22px PFD", "#585838");
this.cObjects['ui_lbl_wintext1'].set({textAlign:"center",outline:3});

this.cObjects['ui_lbl_wintext2']=new createjs.Text(""+gameInfo.cashType+"50.00", "22px PFD", "#F8FAE7");
this.cObjects['ui_lbl_wintext2'].set({textAlign:"center"});

this.cObjects['ui_lbl_wintext1'].shadow = new createjs.Shadow("#FFFF66", 0, 0,8);







this.cObjects['win_text'].addChild(this.cObjects['ui_lbl_wintext1']);
this.cObjects['win_text'].addChild(this.cObjects['ui_lbl_wintext2']);
this.cObjects['view'].addChild(this.cObjects['win_text']);
this.cObjects['win_text'].visible=false;



/*-----------wheel stat------------------*/

this.cObjects['wheelstat'] =  new createjs.Container();

this.cObjects['ws'] =  new createjs.Bitmap(this.cImages['wheelstat']);
this.cObjects['ws_cover'] =  new createjs.Bitmap(this.cImages['ws_cover']);
var spriteSheet = new createjs.SpriteSheet({ images: [this.cImages['pie']],frames: {width:this.cImages['pie'].width/3, height:(this.cImages['pie'].height/1)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});
var spriteSheet2 = new createjs.SpriteSheet({ images: [this.cImages['pie_btn']],frames: {width:this.cImages['pie_btn'].width/4, height:(this.cImages['pie_btn'].height/1)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});

this.cObjects['ws_pie']=new Array();
this.cObjects['ws_pie_btn']=new Array();




this.cObjects['wheelstat'].set({x:841,y:28});



this.cObjects['ws_cover'].set({x:164,y:108});


this.cObjects['wheelstat'].addChild(this.cObjects['ws']);





var cl=0;
var pnum=new Array(0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26);

for(var i=0; i<=36; i++){
this.cObjects['ws_pie'][pnum[i]]=new createjs.Sprite(spriteSheet);
this.cObjects['ws_pie'][pnum[i]].set({x:188,y:130,regY:90,regX:10,scaleX:0.70,scaleY:1,rotation:(i*9.73)});
this.cObjects['ws_pie'][pnum[i]].gotoAndStop(cl);
this.cObjects['wheelstat'].addChild(this.cObjects['ws_pie'][pnum[i]]);

this.cObjects['ws_pie_btn'][pnum[i]]=new createjs.Sprite(spriteSheet2);
this.cObjects['ws_pie_btn'][pnum[i]].set({x:188,y:130,regY:90,regX:10,scaleX:0.70,scaleY:1,rotation:(i*9.73)});
this.cObjects['ws_pie_btn'][pnum[i]].gotoAndStop(0);
this.cObjects['ws_pie_btn'][pnum[i]].name=pnum[i];

this.cObjects['ws_pie_btn'][pnum[i]].addEventListener("mouseover",function(e){e.currentTarget.gotoAndStop(2);});
this.cObjects['ws_pie_btn'][pnum[i]].addEventListener("mouseout",function(e){e.currentTarget.gotoAndStop(0);});
this.cObjects['ws_pie_btn'][pnum[i]].addEventListener("click",function(e){gameUI.AddBet(e.currentTarget.name,gameInfo.currentBet)});

this.cObjects['wheelstat'].addChild(this.cObjects['ws_pie_btn'][pnum[i]]);

if(cl==0){
cl=1;
}else if(cl==1){
cl=2;
}else if(cl==2){
cl=1;
}

}
this.cObjects['wheelstat'].addChild(this.cObjects['ws_cover']);

this.cObjects['ws_text']=new Array();
var ss=new Array("left","right","center");

for(var i=0; i<=10; i++){

this.cObjects['ws_text'][i]=new createjs.Text("1544", "13px Arial", "#FFFFFF");
this.cObjects['ws_text'][i].set({textAlign:ss[RandomInt(0,2)]});
this.cObjects['ws_text'][i].set({x:30,y:20+(i*20),lineWidth:150});
this.cObjects['ws_text'][i].text=" ";
this.cObjects['wheelstat'].addChild(this.cObjects['ws_text'][i]);
}

this.cObjects['view'].addChild(this.cObjects['wheelstat']);



///////////weather numbers stat///////////////


this.cObjects['weather'] =  new createjs.Container();


this.cObjects['weather_box_hot']=new Array();
this.cObjects['weather_box_cold']=new Array();

var spriteSheet = new createjs.SpriteSheet({ images: [this.cImages['weather_box']],frames: {width:this.cImages['weather_box'].width/3, height:(this.cImages['weather_box'].height/1)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});

//////////////////////////hot


/////////////////


////////////////////

this.cObjects['weather_num'] =  new createjs.Bitmap(this.cImages['weather_num']);

this.cObjects['weather'].set({x:887,y:28});
this.cObjects['weather'].addChild(this.cObjects['weather_num']);


///////////////////////////

this.cObjects['weather_hot_bet'] = new createjs.Shape();
this.cObjects['weather_hot_bet'].graphics.beginFill("#ffffcc").drawRect(0, 0, 100, 44);
this.cObjects['weather_hot_bet'].set({x:25,y:18,alpha:0.01});


this.cObjects['weather_cold_bet'] = new createjs.Shape();
this.cObjects['weather_cold_bet'].graphics.beginFill("#ffffcc").drawRect(0, 0, 100, 44);
this.cObjects['weather_cold_bet'].set({x:25,y:136,alpha:0.01});

this.cObjects['weather'].addChild(this.cObjects['weather_hot_bet']);
this.cObjects['weather'].addChild(this.cObjects['weather_cold_bet']);


this.cObjects['weather_cold_bet'].addEventListener("mouseover",function(ev){if(gameState=="Idle"){

for(var i=0; i<=3;i++){
gameUI.LightBet2(gameUI.cObjects['weather_box_cold'][i].name);

}

}});

this.cObjects['weather_cold_bet'].addEventListener("click",function(ev){if(gameState=="Idle"){

for(var i=0; i<=3;i++){
gameUI.AddBet(gameUI.cObjects['weather_box_cold'][i].name,gameInfo.currentBet);

}

}});

this.cObjects['weather_cold_bet'].addEventListener("mouseout",function(ev){if(gameState=="Idle"){

gameUI.HideLightBet();

}});


this.cObjects['weather_hot_bet'].addEventListener("mouseover",function(ev){if(gameState=="Idle"){

for(var i=0; i<=3;i++){
gameUI.LightBet2(gameUI.cObjects['weather_box_hot'][i].name);

}

}});

this.cObjects['weather_hot_bet'].addEventListener("click",function(ev){if(gameState=="Idle"){

for(var i=0; i<=3;i++){
gameUI.AddBet(gameUI.cObjects['weather_box_hot'][i].name,gameInfo.currentBet);

}

}});

this.cObjects['weather_hot_bet'].addEventListener("mouseout",function(ev){if(gameState=="Idle"){

gameUI.HideLightBet();

}});

////////////////////////////////////////////////

for(var i=0; i<=3;i++){
this.cObjects['weather_box_hot'][i]=new createjs.Sprite(spriteSheet);
this.cObjects['weather_box_hot'][i].set({x:20+(i*28.5),y:66});
this.cObjects['weather_box_hot'][i].gotoAndStop(RandomInt(0,1));

this.cObjects['weather_box_hot'][i+4]=new createjs.Text("1", "13px Arial", "#FFFFFF");
this.cObjects['weather_box_hot'][i+4].set({textAlign:"center"});
this.cObjects['weather_box_hot'][i+4].set({x:30+(i*28.5),y:96});
this.cObjects['weather_box_hot'][i+4].text=RandomInt(29,66);
this.cObjects['weather_box_hot'][i+8]=new createjs.Text("1", "17px Arial", "#FFFFFF");
this.cObjects['weather_box_hot'][i+8].set({textAlign:"center"});
this.cObjects['weather_box_hot'][i+8].set({x:30+(i*28.5),y:70});
this.cObjects['weather_box_hot'][i+8].text=RandomInt(1,36);

this.cObjects['weather'].addChild(this.cObjects['weather_box_hot'][i]);
this.cObjects['weather'].addChild(this.cObjects['weather_box_hot'][i+4]);
this.cObjects['weather'].addChild(this.cObjects['weather_box_hot'][i+8]);
}



////////////////////////////


for(var i=0; i<=3;i++){
this.cObjects['weather_box_cold'][i]=new createjs.Sprite(spriteSheet);
this.cObjects['weather_box_cold'][i].set({x:20+(i*28.5),y:66+119});
this.cObjects['weather_box_cold'][i].gotoAndStop(RandomInt(0,1));

this.cObjects['weather_box_cold'][i+4]=new createjs.Text("1", "13px Arial", "#FFFFFF");
this.cObjects['weather_box_cold'][i+4].set({textAlign:"center"});
this.cObjects['weather_box_cold'][i+4].set({x:30+(i*28.5),y:96+119});
this.cObjects['weather_box_cold'][i+4].text=RandomInt(1,26);
this.cObjects['weather_box_cold'][i+8]=new createjs.Text("1", "17px Arial", "#FFFFFF");
this.cObjects['weather_box_cold'][i+8].set({textAlign:"center"});
this.cObjects['weather_box_cold'][i+8].set({x:30+(i*28.5),y:72+117});
this.cObjects['weather_box_cold'][i+8].text=RandomInt(1,36);

this.cObjects['weather'].addChild(this.cObjects['weather_box_cold'][i]);
this.cObjects['weather'].addChild(this.cObjects['weather_box_cold'][i+4]);
this.cObjects['weather'].addChild(this.cObjects['weather_box_cold'][i+8]);
}
/////////////////////////////////////

var spriteSheet = new createjs.SpriteSheet({ images: [this.cImages['color_stat']],frames: {width:this.cImages['color_stat'].width/5, height:(this.cImages['color_stat'].height/1)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});

this.cObjects['ws_red_text']=new createjs.Text("40%", "25px Helvetica", "#FFFFFF");
this.cObjects['ws_red_text'].set({x:540,y:57,textAlign:"right"});
this.cObjects['ws_red_text'].shadow = new createjs.Shadow("#000000", 0, 0, 2);

this.cObjects['ws_red_text2']=new createjs.Text("Red", "13px Arial", "#FFFFFF");
this.cObjects['ws_red_text2'].set({x:536,y:97,textAlign:"right"});
this.cObjects['ws_red_text2'].shadow = new createjs.Shadow("#000000", 0, 0, 2);


this.cObjects['ws_black_text']=new createjs.Text("40%", "25px Helvetica", "#FFFFFF");
this.cObjects['ws_black_text'].set({x:345,y:57,textAlign:"left"});
this.cObjects['ws_black_text'].shadow = new createjs.Shadow("#000000", 0, 0, 2);

this.cObjects['ws_black_text2']=new createjs.Text("Black", "13px Arial", "#FFFFFF");
this.cObjects['ws_black_text2'].set({x:380,y:97,textAlign:"right"});
this.cObjects['ws_black_text2'].shadow = new createjs.Shadow("#000000", 0, 0, 2);

this.cObjects['ws_red']= new createjs.Sprite(spriteSheet);
this.cObjects['ws_red'].set({x:340,y:50});
this.cObjects['ws_red'].gotoAndStop(1);

this.cObjects['wheelstat'].addChild(this.cObjects['ws_red']);

this.cObjects['ws_green']= new createjs.Sprite(spriteSheet);
this.cObjects['ws_green'].set({x:340,y:50});
this.cObjects['ws_green'].gotoAndStop(4);

this.cObjects['wheelstat'].addChild(this.cObjects['ws_green']);

this.cObjects['ws_black']= new createjs.Sprite(spriteSheet);
this.cObjects['ws_black'].set({x:340,y:50});
this.cObjects['wheelstat'].addChild(this.cObjects['ws_black']);


this.cObjects['wheelstat'].addChild(this.cObjects['ws_red_text']);
this.cObjects['wheelstat'].addChild(this.cObjects['ws_red_text2']);

this.cObjects['wheelstat'].addChild(this.cObjects['ws_black_text']);
this.cObjects['wheelstat'].addChild(this.cObjects['ws_black_text2']);

///////////////////////////odd/even stat------------------*/


this.cObjects['ws_odd_text']=new createjs.Text("40%", "25px Helvetica", "#FFFFFF");
this.cObjects['ws_odd_text'].set({x:540,y:57+115,textAlign:"right"});
this.cObjects['ws_odd_text'].shadow = new createjs.Shadow("#000000", 0, 0, 2);

this.cObjects['ws_odd_text2']=new createjs.Text("Even", "13px Arial", "#FFFFFF");
this.cObjects['ws_odd_text2'].set({x:536,y:97+115,textAlign:"right"});
this.cObjects['ws_odd_text2'].shadow = new createjs.Shadow("#000000", 0, 0, 2);


this.cObjects['ws_even_text']=new createjs.Text("40%", "25px Helvetica", "#FFFFFF");
this.cObjects['ws_even_text'].set({x:345,y:57+115,textAlign:"left"});
this.cObjects['ws_even_text'].shadow = new createjs.Shadow("#000000", 0, 0, 2);

this.cObjects['ws_even_text2']=new createjs.Text("Odd", "13px Arial", "#FFFFFF");
this.cObjects['ws_even_text2'].set({x:380,y:97+115,textAlign:"right"});
this.cObjects['ws_even_text2'].shadow = new createjs.Shadow("#000000", 0, 0, 2);

this.cObjects['ws_odd']= new createjs.Sprite(spriteSheet);
this.cObjects['ws_odd'].set({x:340,y:50+115});
this.cObjects['ws_odd'].gotoAndStop(2);

this.cObjects['wheelstat'].addChild(this.cObjects['ws_odd']);

this.cObjects['ws_green2']= new createjs.Sprite(spriteSheet);
this.cObjects['ws_green2'].set({x:340,y:50+115});
this.cObjects['ws_green2'].gotoAndStop(4);

this.cObjects['wheelstat'].addChild(this.cObjects['ws_green2']);

this.cObjects['ws_even']= new createjs.Sprite(spriteSheet);
this.cObjects['ws_even'].set({x:340,y:50+115});
this.cObjects['ws_even'].gotoAndStop(3);
this.cObjects['wheelstat'].addChild(this.cObjects['ws_even']);


this.cObjects['wheelstat'].addChild(this.cObjects['ws_odd_text']);
this.cObjects['wheelstat'].addChild(this.cObjects['ws_odd_text2']);

this.cObjects['wheelstat'].addChild(this.cObjects['ws_even_text']);
this.cObjects['wheelstat'].addChild(this.cObjects['ws_even_text2']);


this.cObjects['ws_black'].addEventListener("click",function(e){gameUI.AddBet(153,gameInfo.currentBet)});
this.cObjects['ws_red'].addEventListener("click",function(e){gameUI.AddBet(154,gameInfo.currentBet)});

this.cObjects['ws_even'].addEventListener("click",function(e){gameUI.AddBet(152,gameInfo.currentBet)});
this.cObjects['ws_odd'].addEventListener("click",function(e){gameUI.AddBet(155,gameInfo.currentBet)});





this.cObjects['view'].addChild(this.cObjects['weather']);

/////////////////////////


this.cObjects['help_wnd'] =  new createjs.Bitmap(this.cImages['info']);

this.cObjects['help_wnd'].set({x:-270,y:370});

this.cObjects['view'].addChild(this.cObjects['help_wnd']);


///////////////////Favourite Bet//////////////////////

this.cObjects['fbet']=new createjs.Container();

this.cObjects['fbet_bg'] =  new createjs.Bitmap(this.cImages['311']);
this.cObjects['fbet'].set({x:260,y:-400});
this.cObjects['fbet'].addChild(this.cObjects['fbet_bg']);
this.cObjects['view'].addChild(this.cObjects['fbet']);


this.cObjects['ui_btnFavClose']=new ButtonUI(" ","btnInfo4",this.cImages['wnd_close'],new Image());
gameUI.cObjects['ui_btnFavClose'].cButton.set({x:446,y:-7});
this.cObjects['fbet'].addChild(this.cObjects['ui_btnFavClose'].cButton);


this.cObjects['ui_btnFavNext']=new ButtonUI(" ","btnFavNext",this.cImages['bcell2'],new Image());
gameUI.cObjects['ui_btnFavNext'].cButton.set({x:460,y:278});
this.cObjects['fbet'].addChild(this.cObjects['ui_btnFavNext'].cButton);

this.cObjects['ui_btnFavNext2']=new ButtonUI(" ","btnFavNext2",this.cImages['bcell2'],new Image());
gameUI.cObjects['ui_btnFavNext2'].cButton.set({x:280,y:278,scaleX:-1});
this.cObjects['fbet'].addChild(this.cObjects['ui_btnFavNext2'].cButton);

this.cObjects['ui_btnFavNext3']=new ButtonUI(" ","btnFavNext3",this.cImages['bcell2'],new Image());
gameUI.cObjects['ui_btnFavNext3'].cButton.set({x:216,y:278});
this.cObjects['fbet'].addChild(this.cObjects['ui_btnFavNext3'].cButton);

this.cObjects['ui_btnFavNext4']=new ButtonUI(" ","btnFavNext4",this.cImages['bcell2'],new Image());
gameUI.cObjects['ui_btnFavNext4'].cButton.set({x:36,y:278,scaleX:-1});
this.cObjects['fbet'].addChild(this.cObjects['ui_btnFavNext4'].cButton);



this.cObjects['ui_favText1']=new createjs.Text("1/1", "19px Arial", "#FFFFFF");
gameUI.cObjects['ui_favText1'].set({x:110,y:276});
this.cObjects['fbet'].addChild(this.cObjects['ui_favText1']);



this.cObjects['ui_favText2']=new createjs.Text("1/1", "19px Arial", "#FFFFFF");
gameUI.cObjects['ui_favText2'].set({x:356,y:276});
this.cObjects['fbet'].addChild(this.cObjects['ui_favText2']);



this.cObjects['sbet1']=new Button2UI("Red Splits ","sbet1",this.cImages['bcell1'],new Image(),"4");
this.cObjects['sbet1'].cButton.set({x:246,y:62});
this.cObjects['fbet'].addChild(this.cObjects['sbet1'].cButton);

this.cObjects['sbet2']=new Button2UI("Black Splits ","sbet2",this.cImages['bcell1'],new Image(),"7");
this.cObjects['sbet2'].cButton.set({x:246,y:89});
this.cObjects['fbet'].addChild(this.cObjects['sbet2'].cButton);

this.cObjects['sbet3']=new Button2UI("Orphelin Plein ","sbet3",this.cImages['bcell1'],new Image(),"8");
this.cObjects['sbet3'].cButton.set({x:246,y:116});
this.cObjects['fbet'].addChild(this.cObjects['sbet3'].cButton);

this.cObjects['sbet4']=new Button2UI("Finale Plein 0 ","sbet4",this.cImages['bcell1'],new Image(),"4");
this.cObjects['sbet4'].cButton.set({x:246,y:143});
this.cObjects['fbet'].addChild(this.cObjects['sbet4'].cButton);

this.cObjects['sbet5']=new Button2UI("Finale Plein 1 ","sbet5",this.cImages['bcell1'],new Image(),"4");
this.cObjects['sbet5'].cButton.set({x:246,y:172});
this.cObjects['fbet'].addChild(this.cObjects['sbet5'].cButton);

this.cObjects['sbet6']=new Button2UI("Finale Plein 2 ","sbet6",this.cImages['bcell1'],new Image(),"4");
this.cObjects['sbet6'].cButton.set({x:246,y:199});
this.cObjects['fbet'].addChild(this.cObjects['sbet6'].cButton);

this.cObjects['sbet7']=new Button2UI("Finale Plein 3 ","sbet7",this.cImages['bcell1'],new Image(),"4");
this.cObjects['sbet7'].cButton.set({x:246,y:226});
this.cObjects['fbet'].addChild(this.cObjects['sbet7'].cButton);

///////////////////trackbet//////////////////////

this.cObjects['trackbet']=new createjs.Container();

this.cObjects['trackbet_bg'] =  new createjs.Bitmap(this.cImages['track']);
this.cObjects['trackbet_bg'].set({x:280,y:290});


this.cObjects['trackbet1']=new createjs.Text("0", "15px Arial", "#FFFFFF");
gameUI.cObjects['trackbet1'].set({textAlign:"center",x:482,y:293});

gameInfo.trackbetCnt=0;


this.cObjects['ui_btnTrackBet1']=new ButtonUI(" ","btnTrackBet1",this.cImages['trackbet_b1'],new Image());
this.cObjects['ui_btnTrackBet2']=new ButtonUI(" ","btnTrackBet2",this.cImages['trackbet_b2'],new Image());


this.cObjects['trackbet'].addChild(this.cObjects['trackbet_bg']);
this.cObjects['trackbet'].addChild(this.cObjects['trackbet1']);

this.cObjects['trackbet'].addChild(this.cObjects['ui_btnTrackBet1'].cButton);
this.cObjects['trackbet'].addChild(this.cObjects['ui_btnTrackBet2'].cButton);
gameUI.cObjects['ui_btnTrackBet1'].cButton.set({x:446,y:293});
gameUI.cObjects['ui_btnTrackBet2'].cButton.set({x:487,y:293.5});


this.cObjects['view'].addChild(this.cObjects['trackbet']);
this.cObjects['trackbet'].visible=false;

var bCnt=1;

for(var i=1; i<=15; i++){



this.cObjects['trackbet_c'+bCnt] =  new createjs.Bitmap(this.cImages['track_c1']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:301+i*26,scaleX:0.90,y:313,alpha:0.5});

this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);

bCnt++;

}

this.cObjects['trackbet_c'+bCnt]=new createjs.Bitmap(this.cImages['track_c2']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:312+16*26,scaleX:1.1,y:307,alpha:0.5,rotation:50});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;


this.cObjects['trackbet_c'+bCnt]=new createjs.Bitmap(this.cImages['track_c2']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:348+16*26,scaleX:1.25,y:337,alpha:0.5,rotation:110});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;

this.cObjects['trackbet_c'+bCnt]=new createjs.Bitmap(this.cImages['track_c2']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:336+16*26,scaleX:1.25,y:386,alpha:0.5,rotation:169});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;

for(var i=15; i>=1; i--){

this.cObjects['trackbet_c'+bCnt] =  new createjs.Bitmap(this.cImages['track_c1']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:301+i*26,scaleX:0.90,y:366,alpha:0.5});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;
 }


this.cObjects['trackbet_c'+bCnt]=new createjs.Bitmap(this.cImages['track_c2']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:314,scaleX:1.0,y:400,alpha:0.5,rotation:230});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;


this.cObjects['trackbet_c'+bCnt]=new createjs.Bitmap(this.cImages['track_c2']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:291,scaleX:1.0,y:380,alpha:0.5,rotation:262});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;

this.cObjects['trackbet_c'+bCnt]=new createjs.Bitmap(this.cImages['track_c2']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:280,scaleX:1.0,y:340,alpha:0.5,rotation:322});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;

 this.cObjects['trackbet_c'+bCnt]=new createjs.Bitmap(this.cImages['track_c2']);
gameUI.cObjects['trackbet_c'+bCnt].set({x:298,scaleX:1.0,y:315,alpha:0.5,rotation:356});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+bCnt]);
bCnt++;

var trackbetName=new Array(-1,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26,0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5);


////////////////////////////////////////////////////////

 this.cObjects['trackbet_c'+38]=new createjs.Bitmap(this.cImages['track_c3']);
gameUI.cObjects['trackbet_c'+38].set({x:315,scaleX:1.0,y:340,alpha:0.5});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+38]);

 this.cObjects['trackbet_c'+39]=new createjs.Bitmap(this.cImages['track_c4']);
gameUI.cObjects['trackbet_c'+39].set({x:406,scaleX:1.0,y:340,alpha:0.5});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+39]);

 this.cObjects['trackbet_c'+40]=new createjs.Bitmap(this.cImages['track_c5']);
gameUI.cObjects['trackbet_c'+40].set({x:535,scaleX:1.0,y:339,alpha:0.5});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+40]);

 this.cObjects['trackbet_c'+41]=new createjs.Bitmap(this.cImages['track_c6']);
gameUI.cObjects['trackbet_c'+41].set({x:654,scaleX:1.04,y:341,alpha:0.5});
this.cObjects['trackbet'].addChild(this.cObjects['trackbet_c'+41]);

for(var i=1; i<=41; i++){

if(i<=37){

gameUI.cObjects['trackbet_c'+i].alpha=0.01;
gameUI.cObjects['trackbet_c'+i].name=trackbetName[i];
gameUI.cObjects['trackbet_c'+i].addEventListener("mouseover",function(ev){TrackbetShow(ev.currentTarget.name);});
gameUI.cObjects['trackbet_c'+i].addEventListener("mouseout",function(ev){TrackbetHide(ev.currentTarget.name);});
gameUI.cObjects['trackbet_c'+i].addEventListener("click",function(ev){TrackbetSelect(ev.currentTarget.name);});

}else{

gameUI.cObjects['trackbet_c'+i].alpha=0.01;
gameUI.cObjects['trackbet_c'+i].name=i;
gameUI.cObjects['trackbet_c'+i].addEventListener("mouseover",function(ev){TrackbetShow(ev.currentTarget.name);});
gameUI.cObjects['trackbet_c'+i].addEventListener("mouseout",function(ev){TrackbetHide(ev.currentTarget.name);});
gameUI.cObjects['trackbet_c'+i].addEventListener("click",function(ev){TrackbetSelect(ev.currentTarget.name);});

}

}




///////////betpage////////////
this.cObjects['bet_wnd'] =  new createjs.Container();
this.cObjects['bet_bg'] =  new createjs.Bitmap(this.cImages['bet_wnd']);

this.cObjects['bet_wnd'].set({x:100,y:192,visible:false});


this.cObjects['bt1']=new createjs.Text("MINIMUM BET \n "+gameInfo.cashType+"1", "12px Arial", "#FFFFFF");
this.cObjects['bt1'].set({textAlign:"center",x:84,y:10});

this.cObjects['btt1']=new createjs.Text("MINIMUM BET \n "+gameInfo.cashType+"1", "18px Arial", "#FFFFFF");
this.cObjects['btt1'].set({textAlign:"center",x:84,y:6});

this.cObjects['bt2']=new createjs.Text("MAXIMUM BET \n "+gameInfo.cashType+"10", "12px Arial", "#FFFFFF");
this.cObjects['bt2'].set({textAlign:"center",x:84,y:60});

this.cObjects['btt2']=new createjs.Text("MAXIMUM BET \n "+gameInfo.cashType+"10", "18px Arial", "#FFFFFF");
this.cObjects['btt2'].set({textAlign:"center",x:84,y:56});

this.cObjects['bet_wnd'].addChild(this.cObjects['bet_bg']);
this.cObjects['bet_wnd'].addChild(this.cObjects['bt1']);
this.cObjects['bet_wnd'].addChild(this.cObjects['btt1']);
this.cObjects['bet_wnd'].addChild(this.cObjects['bt2']);
this.cObjects['bet_wnd'].addChild(this.cObjects['btt2']);
this.cObjects['view'].addChild(this.cObjects['bet_wnd']);



///////////////////////

///////////setpage////////////
this.cObjects['set_wnd'] =  new createjs.Container();
this.cObjects['set_bg'] =  new createjs.Bitmap(this.cImages['settings']);
this.cObjects['set_wnd'].addChild(this.cObjects['set_bg']);
///////////////////////////
var spriteSheet = new createjs.SpriteSheet({ images: [this.cImages['checkbox']],frames: {width:this.cImages['checkbox'].width/2, height:(this.cImages['checkbox'].height/1)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});

for(var i=1; i<=3; i++){
this.cObjects['set_chk'+i] =  new createjs.Sprite(spriteSheet);
this.cObjects['set_chk'+i].set({x:17.5,y:54+((i-1)*32)});
this.cObjects['set_chk'+i].gotoAndStop(0);
this.cObjects['set_wnd'].addChild(this.cObjects['set_chk'+i]);
}


for(var i=4; i<=7; i++){
this.cObjects['set_chk'+i] =  new createjs.Sprite(spriteSheet);
this.cObjects['set_chk'+i].set({x:17.5,y:204+((i-4)*30.0)});
this.cObjects['set_chk'+i].gotoAndStop(1);
this.cObjects['set_wnd'].addChild(this.cObjects['set_chk'+i]);
}

for(var i=1; i<=7; i++){
this.cObjects['set_chk'+i].name=i;
this.cObjects['set_chk'+i].addEventListener("mouseover",function(ev){});
this.cObjects['set_chk'+i].addEventListener("mouseout",function(ev){});
this.cObjects['set_chk'+i].addEventListener("click",function(ev){

if(ev.currentTarget.currentFrame==0){
ev.currentTarget.gotoAndStop(1);
}else {
	ev.currentTarget.gotoAndStop(0);
}

CheckboxChange(ev.currentTarget.name,ev.currentTarget.currentFrame);

});

}

/////////soundtracks//////
this.cObjects['soundtracks']=new Array();
for(var i=0; i<5; i++){

this.cObjects['soundtracks'][i] =  new createjs.Bitmap(this.cImages['soundtrack']);
this.cObjects['soundtracks'][i].set({x:130+(i*25),y:156,alpha:0.02,name:i});
this.cObjects['set_wnd'].addChild(this.cObjects['soundtracks'][i]);

this.cObjects['soundtracks'][i].addEventListener("mouseover",function(ev){if(ev.currentTarget.alpha<1){ev.currentTarget.alpha=0.99;}});
this.cObjects['soundtracks'][i].addEventListener("mouseout",function(ev){ if(ev.currentTarget.alpha<1){ev.currentTarget.alpha=0.02;}});
this.cObjects['soundtracks'][i].addEventListener("click",function(ev){

for(var i=0; i<5; i++){
self.cObjects['soundtracks'][i].alpha=0.02;
}

ev.currentTarget.alpha=1;



if(ev.currentTarget.name==0){
PlayMusic(99);
}else if(ev.currentTarget.name==1){
PlayMusic(100);
}else if(ev.currentTarget.name==2){
PlayMusic(101);
}else if(ev.currentTarget.name==3){
PlayMusic(102);
}else if(ev.currentTarget.name==4){
PlayMusic(103);
}

});


}
self.cObjects['soundtracks'][0].alpha=1;

this.cObjects['ui_btnSettingsClose']=new ButtonUI(" ","btnSettingsClose",this.cImages['wnd_close'],new Image());

this.cObjects['set_wnd'].addChild(this.cObjects['ui_btnSettingsClose'].cButton);

//////////////////
this.cObjects['ui_btnSettingsClose'].cButton.set({x:219,y:0});

this.cObjects['set_wnd'].set({x:-8,y:292*3});


this.cObjects['view'].addChild(this.cObjects['set_wnd']);



///////////////////////

///////////autopage////////////
this.cObjects['auto_wnd'] =  new createjs.Container();
this.cObjects['auto_bg'] =  new createjs.Bitmap(this.cImages['auto']);
this.cObjects['auto_wnd'].addChild(this.cObjects['auto_bg']);

this.cObjects['auto_wnd'].set({x:-8,y:900});


this.cObjects['ui_btnAutoClose']=new ButtonUI(" ","btnAutoClose",this.cImages['wnd_close'],new Image());
this.cObjects['ui_btnAutoClose'].cButton.set({x:313,y:-1});
this.cObjects['auto_wnd'].addChild(this.cObjects['ui_btnAutoClose'].cButton);
///////////////////////////////////////////////////////
///////////////////////////
this.cObjects['ui_btnAutoStop']=new ButtonUI("STOP","btnAutoStop",this.cImages['auto_stop'],new Image());
this.cObjects['ui_btnAutoStop'].cButton.set({x:313,y:-1});


for(var i=1; i<=5; i++){

this.cObjects['ui_btnAuto'+i]=new ButtonUI(""+(i*10),"btnAuto"+i,this.cImages['auto_cell'],new Image());
this.cObjects['ui_btnAuto'+i].cButton.set({x:-30+(i*63),y:90});
this.cObjects['auto_wnd'].addChild(this.cObjects['ui_btnAuto'+i].cButton);
}

//this.cObjects['auto_wnd'].addChild(this.cObjects['ui_btnAuto'+2].cButton);
//this.cObjects['auto_wnd'].addChild(this.cObjects['ui_btnAutoStop'].cButton);
this.cObjects['ui_btnAutoStop'].cButton.set({x:-30+(3*65),y:93});






/////////////////////////////////////////

this.cObjects['view'].addChild(this.cObjects['auto_wnd']);

/////////////////////////////////////////



///////////soundpage////////////
this.cObjects['snd_wnd'] =  new createjs.Container();
this.cObjects['snd_bg'] =  new createjs.Bitmap(this.cImages['sound_set']);
this.cObjects['snd_s1'] =  new createjs.Bitmap(this.cImages['sound_set1']);
this.cObjects['snd_s2'] =  new createjs.Bitmap(this.cImages['sound_set2']);

this.cObjects['snd_s2'].set({regY:94,x:20,y:113});
this.cObjects['snd_s1'].set({regY:9.5,regX:9.5,x:23,y:18});

this.cObjects['snd_bg'].addEventListener("pressmove",function(ev){



self.cObjects['snd_s1'].y=ev.localY;

if(self.cObjects['snd_s1'].y<16){
self.cObjects['snd_s1'].y=16;
}

if(self.cObjects['snd_s1'].y>116){
self.cObjects['snd_s1'].y=116;
}

var sndVl=(100-self.cObjects['snd_s1'].y+10)/100;
self.cObjects['snd_s2'].scaleY=sndVl;

gameInfo.soundVolume=self.cObjects['snd_s2'].scaleY;
backMusic.volume=gameInfo.soundVolume;
});



this.cObjects['snd_bg'].addEventListener("mousedown",function(ev){

if(self.cObjects['snd_s1'].y<16){
self.cObjects['snd_s1'].y=16;
}

if(self.cObjects['snd_s1'].y>116){
self.cObjects['snd_s1'].y=116;
}


self.cObjects['snd_s1'].y=ev.localY;

var sndVl=(100-self.cObjects['snd_s1'].y+10)/100;
self.cObjects['snd_s2'].scaleY=sndVl;

gameInfo.soundVolume=self.cObjects['snd_s2'].scaleY;
backMusic.volume=gameInfo.soundVolume;
});


this.cObjects['snd_wnd'].set({x:30,y:800});

this.cObjects['snd_wnd'].addChild(this.cObjects['snd_bg']);
this.cObjects['snd_wnd'].addChild(this.cObjects['snd_s2']);
this.cObjects['snd_wnd'].addChild(this.cObjects['snd_s1']);

this.cObjects['view'].addChild(this.cObjects['snd_wnd']);



///////////////////////

var spriteSheet = new createjs.SpriteSheet({ images: [this.cImages['winnum']],frames: {width:this.cImages['winnum'].width/3, height:(this.cImages['winnum'].height/1)},animations: {disabled:0,enabled:1,pressed:2,onover:3}});
this.cObjects['winnum']=new createjs.Sprite(spriteSheet);
this.cObjects['winnum'].set({x:867,y:28});

this.cObjects['winnum_text']=new createjs.Text("24", "82px PFD", "#000000");
this.cObjects['winnum_text'].set({textAlign:"center",x:947,y:148,outline:3});

this.cObjects['winnum_text2']=new createjs.Text("24", "82px PFD", "#FFFFFF");
this.cObjects['winnum_text2'].set({textAlign:"center",x:947,y:148});


};







}
