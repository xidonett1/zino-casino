var sndArr=new Array();
var backMusic=new Audio();



for(var i=1;i<=103;i++){
sndArr.push(i+"_tmp.SoundClass");	
}

 createjs.Sound.registerPlugins([createjs.WebAudioPlugin]);
 createjs.Sound.alternateExtensions = ["mp3"];


var numId=new Array();

numId[0]="59_tmp.SoundClass";
numId[1]="71_tmp.SoundClass";
numId[2]="82_tmp.SoundClass";
numId[3]="90_tmp.SoundClass";
numId[4]="91_tmp.SoundClass";
numId[5]="92_tmp.SoundClass";
numId[6]="93_tmp.SoundClass";
numId[7]="94_tmp.SoundClass";
numId[8]="95_tmp.SoundClass";
numId[9]="96_tmp.SoundClass";
numId[10]="61_tmp.SoundClass";
numId[11]="62_tmp.SoundClass";
numId[12]="63_tmp.SoundClass";
numId[13]="64_tmp.SoundClass";
numId[14]="65_tmp.SoundClass";
numId[15]="66_tmp.SoundClass";
numId[16]="67_tmp.SoundClass";
numId[17]="68_tmp.SoundClass";
numId[18]="69_tmp.SoundClass";
numId[19]="70_tmp.SoundClass";
numId[20]="72_tmp.SoundClass";
numId[21]="73_tmp.SoundClass";
numId[22]="74_tmp.SoundClass";
numId[23]="75_tmp.SoundClass";
numId[24]="76_tmp.SoundClass";
numId[25]="77_tmp.SoundClass";
numId[26]="78_tmp.SoundClass";
numId[27]="79_tmp.SoundClass";
numId[28]="80_tmp.SoundClass";
numId[29]="81_tmp.SoundClass";
numId[30]="83_tmp.SoundClass";
numId[31]="84_tmp.SoundClass";
numId[32]="85_tmp.SoundClass";
numId[33]="86_tmp.SoundClass";
numId[34]="87_tmp.SoundClass";
numId[35]="88_tmp.SoundClass";
numId[36]="89_tmp.SoundClass";


function SoundsLoad(){
	
for(var i=0;i<sndArr.length;i++){	
 createjs.Sound.registerSound("sounds/"+sndArr[i]+".wav", sndArr[i]);	
 /// createjs.Sound.on("fileload", this.loadHandler, this);
}


}


function PlaySound(sid){
	if(!gameSettings['sndEffects']){
return;	
}	
var snd=createjs.Sound.play(sid);
snd.volume=gameInfo.soundVolume;
	
}
function PlayMusic(sid){
backMusic.pause();	
if(!gameSettings['sndMusic']){
return;	
}
backMusic.src="sounds/"+sid+"_tmp.SoundClass"+".wav"
backMusic.loop=true;
backMusic.volume=gameInfo.soundVolume;
backMusic.play();

}
function StopSound(sid){
	
	if(!gameSettings['sndEffects']){
return;	
}
createjs.Sound.stop(sid);

	
}


SoundsLoad();