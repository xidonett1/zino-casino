<script type='text/javascript' src='js/colorpicker/colorpicker.js'></script>

<div class="row">                            
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="head"> 
									                   <h2><?=$lang['settings_raiting_title']?></h2>
                                        <ul class="buttons">                                    
                                            <li>
                                              <a onclick="showAddRangeForm(); return false;" role="button" href="#addForm">
                                                <span class="i-plus-2"></span>
                                              </a>
                                            </li>
                                        </ul>  
                                    </div>
                                    <div class="content np">

                                        <table cellpadding="0" cellspacing="0" width="100%" id="gamers_range" class="editable oc_disable">
										
                                            <thead>
                                                <tr>               
                                                  <th><?=$lang['settings_raiting_th1']?></th>                                     
                                                  <th><?=$lang['settings_raiting_th2']?></th>
                                                  <th><?=$lang['settings_raiting_th3']?></th>
                                                  <th><?=$lang['settings_raiting_th4']?></th>
                                                  <th><?=$lang['settings_raiting_th5']?></th>
                                                  <th><?=$lang['settings_raiting_th6']?></th>
                                                  <th><?=$lang['settings_raiting_th7']?></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            <?php
                                            $sql="select * from users_rate_range where lang='$language'";
                                            $res=mysql_query($sql);
                                            while($row=mysql_fetch_assoc($res))
                                              {
                                              echo "<tr align='center' id='rate".$row['id']."'>
											            <td>".$row['level']."</td>
                                                        <td>".$row['name']."</td>
                                                        <td>".$row['range']."</td>
                                                        <td>".$row['color']."</td>
                                                        <td>".$row['pic']."</td>
                                                        <td>".$row['point_cours']."</td>
                                                        <td><a href='#' onclick='showEditRangeForm(".$row['id']."); return false;'><i class='glyphicon glyphicon-edit'></i></a></td>
                                                    </tr>";    
                                              }
                                            ?>

                                            </tbody>
                                        </table>
                                    </div>
                                 </div>  
                               </div>  
                             </div>           
                                     
                                     
                                     
<!-- Bootrstrap modal form [user balance]-->
    <div class="modal fade" id="addForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="myModalLabel3"><?=$lang['settings_raiting_add']?></h3>
                </div>
                <div class="modal-body">
                  <form>
                    <input type="hidden" name="action" value="add"/>
                    <input type="hidden" name="id" value=""/>
                    <div class="row">
                            <div class="controls-row">
                                <div class="col-md-3">
                                    <?=$lang['settings_raiting_name']?>:
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="name" value="" class="form-control"/>                        
                                </div>
                            </div>
                    </div>
                    
                    <div class="row">
                            <div class="controls-row">
                                <div class="col-md-3">
                                    <?=$lang['settings_raiting_th1']?>:
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="level" value="" class="form-control"/>                        
                                </div>
                            </div>
                    </div>
                    
                    <div class="row">
                            <div class="controls-row">
                                <div class="col-md-3">
                                    <?=$lang['settings_raiting_deposit']?>:
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="range" value="" class="form-control"/>                        
                                </div>
                            </div>
                    </div>
                    
                    <div class="row">
                            <div class="controls-row">
                                <div class="col-md-3">
                                    <?=$lang['settings_raiting_course']?>:
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="point_cours" value="" class="form-control"/>                        
                                </div>
                            </div>
                    </div>
                    
                    <div class="row">
                      <div class="controls-row">
                        <div class="col-md-3"><?=$lang['settings_raiting_color']?>:</div>
                        <div class="col-md-9">
                          <div class="input-group">
                              <span class="input-group-addon">#</span>
                              <input type="text" name="color" class="color form-control">
                          </div>                                                 
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="controls-row">
                        <div class="col-md-3"><?=$lang['settings_raiting_pic']?> :</div>
                        <div class="col-md-9">
                              <input type="text" name="pic" class="form-control">
                        </div>
                      </div>
                    </div>
                 </form>                      
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning submit-ajax">ОК</button> 
                    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><?=$lang['adm_users_edit_cancel']?></button>            
                </div>                
            </div>
        </div>
    </div>    
    <!-- EOF Bootrstrap modal form -->
    
<script>
function showAddRangeForm()
  {
  $("#addForm .modal-title").html('<?=$lang['settings_raiting_add']?>');
  $("#addForm input[type=text]").val('');
  $("#addForm input[name=action]").val('add');
  $("#addForm").modal('show');
  }

function showEditRangeForm(id)
  {
  $.get('/engine/ajax/gamer_rating.php?id='+id,function(data){
    console.log(data.success);
    if(data.success==true)
      {
      $("#addForm .modal-title").html('<?=$lang['settings_raiting_edit']?>');
      $("#addForm input[name=action]").val('edit');
      
      for( var i in data.info)
        {
        $("#addForm input[name="+i+"]").val(data.info[i]);
        }
      
      $("#addForm").modal('show');
      }
  },'json');
  
  }  


$(document).ready(function(){
   $("#addForm .submit-ajax").on('click',function(){
   
   $.post('/engine/ajax/gamer_rating.php',$("#addForm form").serialize(),function(data){
      console.log(data);
      if(data.success==true)
        {                                                                                                                                                                                                                                                                              
        if($("#addForm input[name=action]").val()=='add')
          $("#gamers_range tbody").append("<tr><td>"+$("#addForm input[name=level]").val()+"</td><td>"+$("#addForm input[name=name]").val()+"</td><td>"+$("#addForm input[name=range]").val()+"</td><td>"+$("#addForm input[name=color]").val()+"</td><td>"+$("#addForm input[name=pic]").val()+"</td><td>"+$("#addForm input[name=point_cours]").val()+"</td><td><a href='#' onclick='showEditRangeForm("+$("#addForm input[name=id]").val()+"); return false;'><i class='glyphicon glyphicon-list-alt'></i></a></td></tr>");
        else
          window.location='adm.php?a=settings&gr_id=-2';
        $("#addForm").modal('hide');
        }
      else  
        console.log(data.txt);
      },'json');
   
   });
});
</script>    
                                                  