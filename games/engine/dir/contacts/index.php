<?php
	
	$smarty->assign('user_mail', $user_info['email']);
	
	$action = isset($_GET['action']) ? htmlspecialchars(str_replace("'","",substr($_GET['action'],0,6))): false;

	if($action == "submit") {
		$name		= isset($_POST['name'])?htmlspecialchars(str_replace("'","",substr($_POST['name'],0,50))):$login;
		$user_mail	= isset($_POST['mail'])?htmlspecialchars(str_replace("'","",substr($_POST['mail'],0,50))):$user_info['email'];
		$subj		= isset($_POST['subj'])?htmlspecialchars(str_replace("'","",substr($_POST['subj'],0,100))):false;
		$textform	= isset($_POST['textform'])?htmlspecialchars(str_replace("'","",substr($_POST['textform'],0,10240))):false;
    
    $referer= str_replace('http://','',$_SERVER['HTTP_REFERER']);
    $referer= str_replace('/contacts?action=submit','',$referer);
    $referer= str_replace('/','',$referer);
    $referer= str_replace(':85','',$referer);
    $domen=get_domen();

    $error=false;

		if(!$name) {
				$_SESSION['messages'][]=array('er',$lang['err_no_login']);
        $error=true;
		}
		elseif(!$user_mail) {
				$_SESSION['messages'][]=array('er',$lang['err_mail']);
        header("location: /profile");
        die();
		}
		elseif(!$subj) {
				$_SESSION['messages'][]=array('er',$lang['err_no_subject']);
        header("location: /");
        die();
		}
		elseif(!$textform) {
				$_SESSION['messages'][]=array('er',$lang['err_in_message']);
        header("location: /");
        die();
		}
		elseif(!preg_match("/^[a-z0-9_.-]{1,20}@(([a-z0-9-]+\.)+(com|net|org|mil|edu|gov|arpa|info|biz|[a-z]{2})|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$/is",$user_mail)) {
				$_SESSION['messages'][]=array('er',$lang['err_mail_preg_match']);
        $error=true;
		} 
    if(!$error) 
      {
      $send=user_mail (9,false,array('subj'=>$subj,'text'=>$textform, 'username'=>$name, 'ip'=>$_SERVER['REMOTE_ADDR'],'usermail'=>$user_mail),$user_mail);    //1 - ИД админа
			if(!$send) {
				$_SESSION['messages'][]=array('er',$lang['err_maile_server']);
        header("location: /");
        die();		
			} else {

				$_SESSION['messages'][]=array('erok',$lang['err_send']);
        header("location: /");
        die();
				$name		= "";
				$mail		= "";
				$subj		= "";
				$textform	= "";
			}
		}
	}
	
  header('location: /');
  exit();

?>