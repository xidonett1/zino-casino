<?php
	include_once '../../../cfg.php';
	include_once '../../../ini.php';
 if(!in_array ($status, $can_game))
    die();
	
	function winlimit()
	{
	global $user_id;
	   $bank=get_bank($user_id,'alice');
	    return $bank;
	}
	
	
	
	
	$userId=$user_id;
	$user_array['login'] = $login;

	$scatter=12;
	
	$action = isset($_POST['action']) ? $_POST['action'] : 'error';
	$action = str_replace("-","", $action);
	$action = str_replace("`","", $action);
	$action = str_replace("'","", $action);

	$user_balance = floor( get_balance($userId) );

	if ($action != 'error')
	{
		$bet = isset($_POST['betline']) ? $_POST['betline'] : 0;
		$line = isset($_POST['lines']) ? $_POST['lines'] : 0;
		$allbet = $bet * $line;
	
		if ( $user_balance < 0 )
		{
		    $errorMessage = "error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт.|lobby/";
		    $action = 'error';
		}
		elseif ( $action !='state' && ($bet < 1 || 200 < $bet) )
		{
		    $action = "error";
			$errorMessage = "error|Ошибка! Ваша ставка ($bet) должна быть от 1 до 9.|lobby/";
		}
		elseif ( ($action != 'state') && ($action != 'double') && !in_array($line, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25)) )
		{
		    $action = "error";
			$errorMessage = "error|Ошибка! Выбранное вами количество линий игры ($line) должно быть от 1 до 9 (только нечётные значения).|lobby/";
		}
		elseif ( ($action == 'spin') && ($user_balance < $allbet ) )
		{
		    $action = "error";
			$errorMessage = "error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры по сделанной ставке ($allbet).|lobby/";
		}
		elseif (($action == 'freegame') && !isset($_SESSION['alice_freeGameCount']))
		{
			$action = 'error';
			$errorMessage = 'error|Ошибка! Попытка повлиять на игру. Ваш аккаунт блокирован. За подробностями обращайтесь к администратору.|user_blocked.php';
			block_user($userId);
		}
		elseif (!in_array($action, array('error', 'state', 'spin', 'double', 'freegame','bonus', 'finish','respin' )))
		{
			$action = 'error';
			$errorMessage = 'error|Ошибка! Попытка повлиять на игру. Ваш аккаунт блокирован. За подробностями обращайтесь к администратору.|user_blocked.php';
			block_user($userId);
		}
	}
	
	/*    Error   */
	if ( $action == "error" )
	{
	    $msg= isset($errorMessage) ? $errorMessage : 'error|Ошибка! Неверное действие.|lobby/';
	    ge_serv_show_str($msg);
	}
	
	/*    Init    */
	if ( $action == "state" )
	{
	$_SESSION['alice_spinMpl']=0;
	    ge_serv_show_str( "result=ok&state=0&min=1&id=$user_array[login]&balance=$user_balance");
		if (isset($_SESSION['alice_win']))
			unset($_SESSION['alice_win']);
		if (isset($_SESSION['alice_freeGameCount']))
			unset($_SESSION['alice_freeGameCount']);
		if (isset($_SESSION['alice_d']))
			unset($_SESSION['alice_d']);
			
			if (isset($_SESSION['microbank']))
			unset($_SESSION['microbank']);
			
			$_SESSION['microbank']=0;
			$_SESSION['alice_isRespin']=false;
	}

	/*   Respin  */
	if ( $action == "respin" && $_SESSION['alice_isRespin'] )
	{
	 include_once 'func.php';
	$stopRespin=false;
	$_SESSION['alice_isRespin']=false;
	
	$respinMode=$_POST['mode'];
	$respinPos=$_POST['pos'];
	
	
	$bet=$_SESSION['alice_betRespin'];
	$line=$_SESSION['alice_linesRespin'];
	$allbet=$bet*$line;
	
	
	
$psym[1]=array(0,0,0,4,15,100);
$psym[2]=array(0,0,0,4,15,100);
$psym[3]=array(0,0,0,4,15,100);
$psym[4]=array(0,0,0,7,25,200);
$psym[5]=array(0,0,0,7,25,200);
$psym[6]=array(0,0,0,7,25,200);
$psym[7]=array(0,0,0,10, 30, 300);
$psym[8]=array(0,0,0,12,50,500);
$psym[9]=array(0,0,0,15,80,750);
$psym[10]=array(0,0,0,20,100,2000);
$psym[11]=array(0,0,0,40,150,4000);
$psym[12]=array(0,0,2,5,25,200);
$psym[13]=array(0,0,0,15,80,750);
	
$lin[0]=array(2,5,8,11,14);
$lin[1]=array(1,4,7,10,13);
$lin[2]=array(3,6,9,12,15);
$lin[3]=array(1,5,9,11,13);
$lin[4]=array(3,5,7,11,15);
$lin[5]=array(2,4,7,10,14);
$lin[6]=array(2,6,9,12,14);
$lin[7]=array(1,4,8,12,15);
$lin[8]=array(3,6,8,10,13);
$lin[9]=array(2,6,8,10,14);
$lin[10]=array(2,4,8,12,14);
$lin[11]=array(1,5,8,11,13);
$lin[12]=array(3,5,8,11,15);
$lin[13]=array(1,5,7,11,13);
$lin[14]=array(3,5,9,11,15);
$lin[15]=array(2,5,7,11,14);
$lin[16]=array(2,5,9,11,14);
$lin[17]=array(1,4,9,10,13);
$lin[18]=array(3,6,7,12,15);
$lin[19]=array(1,6,9,12,13);
$lin[20]=array(3,4,7,10,15);
$lin[21]=array(2,6,7,12,14);
$lin[22]=array(2,4,9,10,14);
$lin[23]=array(1,6,7,12,13);
$lin[24]=array(3,4,9,10,15);	
	
		cange_balance($userId, $winall_old*-1);
	        change_bank($user_id,'alice',$winall_old);
	
	  while ( $am < 100000 )
	    {
	        $map_win = array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
			$winbonus = 0;
			
	        $mx2=$_SESSION['alice_RespinMap'];
 
	        for ( $k = 0; $k <= 14; ++$k ){
	            $map[$k] = $mx2[$k];
			}
			
				if(isset($_SESSION['alice_win'])){
		$winall_old=$_SESSION['alice_win'];
		}
	
			
			if($respinMode=="vmode"){
			
			$rRow=array(1,2,3,4,5,6,7,8,9,10,11);
			shuffle($rRow);
			$map[($respinPos-1)*3]=$rRow[0];
			$map[($respinPos-1)*3+1]=$rRow[1];
			$map[($respinPos-1)*3+2]=$rRow[2];
			
			}else{
			$respinPos=$respinPos-1;
			$rRow=array(1,2,3,4,5,6,7,8,9,10,11,1,2,3,4,5,6,7,8,9,10,11);
			shuffle($rRow);
			$map[0*3+$respinPos]=$rRow[0];
			$map[1*3+$respinPos]=$rRow[1];
			$map[2*3+$respinPos]=$rRow[2];
			$map[3*3+$respinPos]=$rRow[3];
			$map[4*3+$respinPos]=$rRow[4];
			
			}
			
			
			if($stopRespin){
			$map=$mx2;
			 for ( $k = 0; $k <= 14; ++$k ){
	            if($map[$k]==15 || $map[$k]==16){
				$map[$k]=14;
				} 
			}
			}
			
			
	        for ( $ln = 0; $ln <= $line-1; ++$ln )
	        {
	            $s[1] = $map[$lin[$ln][0]-1];
	            $s[2] = $map[$lin[$ln][1]-1];
	            $s[3] = $map[$lin[$ln][2]-1];
	            $s[4] = $map[$lin[$ln][3]-1];
	            $s[5] = $map[$lin[$ln][4]-1];
				
			
				
			$map_win[$ln]=WinLin($s,13,$psym,array(12),0)*$bet;
		


	        }
			
			
	        for ( $k = 1; $k <= 15; ++$k )
	        {
	            ${ "sym".$k } = $map[$k - 1];
	        }
           for ( $k = 1; $k <= 30; ++$k )
	        {
	            ${ "win".$k } = 0;
	        }
	        for ( $k = 1; $k <= 30; ++$k )
	        {
	            ${ "win".$k } = $map_win[$k - 1];
	        }
			
	        
	      
				$winall = $win1 + $win2 + $win3 + $win4 + $win5 + $win6 + $win7 + $win8 + $win9+ $win10 + $win11 + $win12 + $win13 + $win14 + $win15+ $win16+ $win17+ $win18+ $win19+ $win20+ $win21+ $win22+ $win23+ $win24+ $win25+ $win26+ $win27+ $win28+ $win29+ $win30;
	       
		   ++$am;
			
	     
			
		
			if ( $casbank < $winall)
	        {
	         $am = 10;
	        }else{
			 $am = 120000;
			}
			if ( $stopRespin)
	        {
	          $am = 120000;
	        }
	if($lc>=800){
	$stopRespin=true;
     
	  
	}
	$lc++;
	    }
		
		$winall44 = sprintf( "%01.2f", $winall );
		if($winall>0){
		
		cange_balance($userId, $winall44);
	        change_bank($user_id,'alice',$winall44*-1);
	
			$_SESSION['alice_win'] = $winall;
		}
		
		$user_balance = floor( get_balance($userId) );

		ge_serv_show_str( "&result=ok&info=|$sym1|$sym2|$sym3|$sym4|$sym5|$sym6|$sym7|$sym8|$sym9|$sym10|$sym11|$sym12|$sym13|$sym14|$sym15|$bet|$line|$allbet|$winall|0|$allbet|$winall|0|1|0&id=$user_array[login]&balance=$user_balance");

	$stat_game="alice_Respin";
		
		
		set_stat_game($userId, $user_balance, $allbet,$winall44,$stat_txt);
	}
	
	
	
	
	
	
	/*    Bonus   */
	if ( $action == "bonus" && 	$_SESSION['alice_isBonus2'] )
	{
	$bact=$_POST['bact'];
	$_SESSION['alice_isBonus2']=false;
	if($bact=="d3"){
	$crdArr=array(5,5,5,5,10,10,10,10,20,20,20,30,30,30,40,40,40,50,50,50,100,100,100,200,200,200);
	$crdArr2=$crdArr;
	shuffle($crdArr);
	$tmpWin=$crdArr[0];
	$casbank=winlimit();
	
	if($tmpWin>$casbank){
	$tmpWin=$crdArr2[rand(0,15)];
	}
	cange_balance($userId, $tmpWin);
	   
	change_bank($user_id,'alice',$tmpWin*-1);
	}
	
	
	if($bact=="d1"){
	$crdArr=array(3,3,3,3,5,5,5,5,7,7,7,7,10,10,10,10,15,15,15,15,20,20,25,25,30,30);
	
	shuffle($crdArr);
	$tmpWin=$crdArr[0];
	
	$_SESSION['alice_freeGameCount'] = $tmpWin;
	
	}
	
	
	if($bact=="d2"){
	$crdArr=array(1,1,1,1,2,2,2,2,2,3,3,3,3,4,4,4,5,5,6,6,7,8);
	
	shuffle($crdArr);
	$tmpWin=$crdArr[0];
	
		$_SESSION['alice_spinMpl'] = $tmpWin;
	    $_SESSION['alice_spinMplCnt'] = 10; 
	}
	
	  ge_serv_show_str( "result=ok&state=0&min=1&id=$user_array[login]&balance=$user_balance&bwin=$tmpWin");
	}
	
	/*    Spin   */
	if ( $action == "spin" )
	{
	    $stat_txt = "alice";
		
	    include_once 'func.php';

	    cange_balance($userId, $allbet*-1);
	    $rowb9 = get_game_settings('alice');
	    $proc4 = $rowb9['g_proc'];
	    $allbet23 = $allbet / 100 * $proc4;
	   change_bank($user_id,'alice',$allbet23);
	$pr30_shans=rand(1,3);
	$_SESSION['microbank']+=$allbet23;
	    $g_rezim = $rowb9['g_rezim'];
         $mode=$g_rezim;
	  $_SESSION['alice_isBonus2']=false; 
		
		
		
	  
        
        
      
$lin[0]=array(2,5,8,11,14);
$lin[1]=array(1,4,7,10,13);
$lin[2]=array(3,6,9,12,15);
$lin[3]=array(1,5,9,11,13);
$lin[4]=array(3,5,7,11,15);
$lin[5]=array(2,4,7,10,14);
$lin[6]=array(2,6,9,12,14);
$lin[7]=array(1,4,8,12,15);
$lin[8]=array(3,6,8,10,13);
$lin[9]=array(2,6,8,10,14);
$lin[10]=array(2,4,8,12,14);
$lin[11]=array(1,5,8,11,13);
$lin[12]=array(3,5,8,11,15);
$lin[13]=array(1,5,7,11,13);
$lin[14]=array(3,5,9,11,15);
$lin[15]=array(2,5,7,11,14);
$lin[16]=array(2,5,9,11,14);
$lin[17]=array(1,4,9,10,13);
$lin[18]=array(3,6,7,12,15);
$lin[19]=array(1,6,9,12,13);
$lin[20]=array(3,4,7,10,15);
$lin[21]=array(2,6,7,12,14);
$lin[22]=array(2,4,9,10,14);
$lin[23]=array(1,6,7,12,13);
$lin[24]=array(3,4,9,10,15);



$psym[1]=array(0,0,0,4,15,100);
$psym[2]=array(0,0,0,4,15,100);
$psym[3]=array(0,0,0,4,15,100);
$psym[4]=array(0,0,0,7,25,200);
$psym[5]=array(0,0,0,7,25,200);
$psym[6]=array(0,0,0,7,25,200);
$psym[7]=array(0,0,0,10, 30, 300);
$psym[8]=array(0,0,0,12,50,500);
$psym[9]=array(0,0,0,15,80,750);
$psym[10]=array(0,0,0,20,100,2000);
$psym[11]=array(0,0,0,40,150,4000);
$psym[12]=array(0,0,2,5,25,200);
$psym[13]=array(0,0,0,15,80,750);
	   

		
	    $shs = explode( "|", $rowb9['g_shanswin'] );
	    if ( $line == 1  )	
	    {
	        $ooo2 = $shs[0];
	    }
	    if ( $line == 3  || $line == 2)
	    {
	        $ooo2 = $shs[1];
	    }
	    if ( $line == 5 || $line == 4)
	    {
	        $ooo2 = $shs[2];
	    }
	    if ( $line == 7 || $line == 6)
	    {
	        $ooo2 = $shs[3];
	    }
	    if ( $line == 9 || $line == 8)
	    {
	        $ooo2 = $shs[4];
	    }
		 if ( $line == 11 || $line == 10)
	    {
	        $ooo2 = $shs[5];
	    }
		 if ( $line == 13 || $line == 12)
	    {
	        $ooo2 = $shs[6];
	    }
		 if ( $line == 14 || $line == 15)
	    {
	        $ooo2 = $shs[7];
	    }
		 if ( $line == 16 || $line == 17)
	    {
	        $ooo2 = $shs[8];
	    }
		 if ( $line == 18 || $line == 19)
	    {
	        $ooo2 = $shs[9];
	    }
		 if ( $line == 20 || $line == 21)
	    {
	        $ooo2 = $shs[10];
	    }
		 if ( $line == 22 || $line == 23)
	    {
	        $ooo2 = $shs[11];
	    }
		 if ( $line == 24 || $line == 25)
	    {
	        $ooo2 = $shs[12];
	    }
		 if ( $line == 26 || $line == 27)
	    {
	        $ooo2 = $shs[13];
	    }
		 if ( $line == 28 || $line == 29)
	    {
	        $ooo2 = $shs[14];
	    }
		 if ( $line == 30 || $line == 31)
	    {
	        $ooo2 = $shs[15];
	    }
	    $casbank = winlimit(); // Банк игры
      
		$bonuses=explode("|",$rowb9['g_shansbon']);
	if($_SESSION['alice_spinMplCnt'] <=0 || !isset($_SESSION['alice_spinMplCnt']) ){	
	    $rnd_bonus = rand( 1,$bonuses[0] );
		$rnd_bonus2=rand( 1,$bonuses[1] );
	    $rnd_result = rand( 1, $ooo2 ); // Шанс выиграть
	
        $rnd_respin=rand( 1, 20 );	
	
	    if ($rnd_result == 1)
	        $mas_win = 1; // Если должен выиграть
	    else
	        $mas_win = 0;

		if ( ($mas_win == 1) && ($rnd_bonus == 1) && ($casbank < $allbet * 5 + $allbet * $psym[$scatter][3]) )
	        $rnd_bonus = 0;
		if ( ($mas_win == 1) && ($casbank < $allbet * 5) )
			$mas_win = 0;
		if ( ($rnd_bonus == 1) && ($casbank < $allbet * $psym[$scatter][3]) )
			$rnd_bonus = 0;
           
	if ( $rnd_bonus2 == 1 && $casbank<50){
	$rnd_bonus2=0;	
	
	}
	if ( $rnd_bonus2 == 1 ){
	$rnd_bonus=0;
$rnd_respin=0;	
	}	
	if ( $rnd_bonus1 == 1 ){
	
    $rnd_respin=0;	
	}	
	
	}else{/////////////////////////////////////SpinMpl
	
	
	 $rnd_bonus = 0;
		$rnd_bonus2=0;
	    $rnd_result = rand( 1, $ooo2 ); // Шанс выиграть
		
		$rnd_respin=0;
		
	    if ($rnd_result == 1)
	        $mas_win = 1; // Если должен выиграть
	    else
	        $mas_win = 0;

		
		if ( ($mas_win == 1) && ($casbank < ($allbet * 5)*$_SESSION['alice_spinMpl']) )
			$mas_win = 0;
		
           
	if ( $rnd_bonus2 == 1 && $casbank<50){
	$rnd_bonus2=0;		
	}
	if ( $rnd_bonus2 == 1 ){
	$rnd_bonus=0;		
	}	
	
	}
	
	
	
	    $am = 0;
	$lc=0;
	    while ( $am < 100000 )
	    {
	        $map_win = array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0  );
			$winbonus = 0;
			
	        $mx2=get_rows($mode,0,0);
 
	        for ( $k = 0; $k <= 14; ++$k )
	            $map[$k] = $mx2[$k];
			
			
			
			if ($rnd_bonus == 1)
			{
				$tttb1 = array( 0, 1, 2 );
				shuffle($tttb1);
				$tttb2 = array( 3, 4, 5 );
				shuffle($tttb2);
		        $tttb3 = array( 6, 7, 8 );
				shuffle($tttb3);
		        $tttb4 = array( 9, 10, 11 );
				shuffle($tttb4);
				$tttb5 = array( 12, 13, 14 );
				shuffle($tttb5);
				$tempArray = array(1, 2, 3, 4, 5);
				shuffle($tempArray);
				
				if ($mas_win == 1)
				{
					do
					{
						switch ($g_rezim)
						{
							case 1:
							case 2:
								$bonusSymCount = rand(3, 4);
								break;
							default:
								$bonusSymCount = rand(3, 5);
						}
					}while ($casbank < $allbet * $psym[$scatter][$bonusSymCount]);
				}
				else
				{
					do
					{
						switch ($g_rezim)
						{
							case 1:
							case 2:
								$bonusSymCount = rand(3, 4);
								break;
							default:
								$bonusSymCount = rand(3, 5);
						}
					}while ($casbank < $allbet * $psym[$scatter][$bonusSymCount]);
				}
				
				$symBon = 0;
				for ($k = 0; $k < 15; $k++)
					if ($map[$k] == $scatter)
						$symBon += 1;
				
				for ($k = 1, $i = 0; $k <= $bonusSymCount - $symBon; $k++, $i++)
				{
					$tttbX = 'tttb'.$tempArray[$i];
					if ( ($map[${$tttbX}[0]] == $scatter) || ($map[${$tttbX}[1]] == $scatter) || ($map[${$tttbX}[2]] == $scatter) )
						$k -= 1;
					else
						$map[${$tttbX}[0]] = $scatter;
				}
				
				    if($bonusSymCount>=3){
					$_SESSION['alice_freeGameCount'] = 15;
				
					}
					
					
				$winbonus = $allbet * $psym[$scatter][$bonusSymCount];
			}
			else
			{
				$symBon = 0;
				for ($k = 0; $k < 15; $k++)
					if ($map[$k] == $scatter)
						$symBon += 1;
				
				if ($symBon == 2)
					$winbonus = $allbet * $psym[$scatter][2];
			}
			
			
			if ( $rnd_bonus2 == 1){/////////////////////bonus2 
			$rArr=array(0,1,2,3,4);
			shuffle($rArr);
			
			$map[$rArr[0]*3+rand(0,2)]=14;
			$map[$rArr[1]*3+rand(0,2)]=14;
			$map[$rArr[2]*3+rand(0,2)]=14;
			
			}
			
			if($rnd_respin==1){//////////respins
			
			$rMode=rand(1,2);
			
			if($rMode==1){//hRoll
			$pos=rand(0,2);
			$rPos=rand(0,4);
			$map[$rPos*3+$pos]=15;
			
			}else{//vRoll
			$pos=rand(0,2);
			$rPos=rand(0,4);
			$map[$rPos*3+$pos]=16;
			
			}
			
			$_SESSION['alice_isRespin']=true;
			$_SESSION['alice_betRespin']=$bet;
	        $_SESSION['alice_linesRespin']=$line;
			$_SESSION['alice_RespinMap']=$map;
			}else{
			$_SESSION['alice_isRespin']=false;
			}
			
			
	        for ( $ln = 0; $ln <= $line-1; ++$ln )
	        {
	            $s[1] = $map[$lin[$ln][0]-1];
	            $s[2] = $map[$lin[$ln][1]-1];
	            $s[3] = $map[$lin[$ln][2]-1];
	            $s[4] = $map[$lin[$ln][3]-1];
	            $s[5] = $map[$lin[$ln][4]-1];
				
			
				
			$map_win[$ln]=WinLin($s,13,$psym,array(12),0)*$bet;
			
			
			if($_SESSION['alice_spinMplCnt']>0){
		
		$map_win[$ln]*=$_SESSION['alice_spinMpl'];
		}
			
				
			
				



	        }
			
			
	        for ( $k = 1; $k <= 15; ++$k )
	        {
	            ${ "sym".$k } = $map[$k - 1];
	        }
          for ( $k = 1; $k <= 25; ++$k )
	        {
	            ${ "win".$k } = 0;
	        }
	        for ( $k = 1; $k <= 25; ++$k )
	        {
	            ${ "win".$k } = $map_win[$k - 1];
	        }
			
	        
	        if ($rnd_bonus != 1)
	        	$winall = $win1 + $win2 + $win3 + $win4 + $win5 + $win6 + $win7 + $win8 + $win9+ $win10 + $win11 + $win12 + $win13 + $win14 + $win15+ $win16+ $win17+ $win18+ $win19+ $win20 +$win21+ $win22+ $win23+ $win24+ $win25+ $winbonus;
			else
				$winall = $win1 + $win2 + $win3 + $win4 + $win5 + $win6 + $win7 + $win8 + $win9+ $win10 + $win11 + $win12 + $win13 + $win14 + $win15+ $win16+ $win17+ $win18+ $win19+ $win20+ $win21+ $win22+ $win23+ $win24+ $win25;
	        ++$am;
			
	     
			
			
			if ( $mas_win == 1 && $winall == 0 )
	        {
	            $am = 10;
	        }
			if ( $mas_win == 1 && 0 < $winall )
	        {
	            $am = 120000;
	        }
	        if ( $mas_win == 0 && $winall == 0 )
	        {
	            $am = 120000;
	        }
	        if ( $rnd_bonus == 1 && $casbank < ($winall = $winall + $winbonus) )
	        {
	            $am = 10;
	        }
			if ( $rnd_bonus != 1 && $casbank < $winall)
	        {
	            $am = 10;
	        }
	if($lc>=1000){
	$mas_win=0;
        $rnd_bonus=0;
     
$mode=6;	  
	}
	$lc++;
	    }
		
		$winall44 = sprintf( "%01.2f", $winall );
		if ( 0 < $winall )
		{
			cange_balance($userId, $winall44);
	        change_bank($user_id,'alice',$winall44*-1);
	$_SESSION['microbank']-=$winall44;
			$_SESSION['alice_win'] = $winall;
		}
		
		$user_balance = floor( get_balance($userId) );

		
		if($_SESSION['alice_spinMplCnt'] >0 || isset($_SESSION['alice_spinMplCnt']) ){	
		$spl=$_SESSION['alice_spinMpl'];
		
		
		}else{
		
		$spl=0;
		}
		
		
		
		
		
		ge_serv_show_str( "&result=ok&info=|$sym1|$sym2|$sym3|$sym4|$sym5|$sym6|$sym7|$sym8|$sym9|$sym10|$sym11|$sym12|$sym13|$sym14|$sym15|$bet|$line|$allbet|$winall|0|$allbet|$winall|0|1|0&id=$user_array[login]&balance=$user_balance&spinMpl=$spl");

		
		if($_SESSION['alice_spinMplCnt']>0){
		
		$_SESSION['alice_spinMplCnt']--;
		}
		if ( $rnd_bonus2 == 1 ){
		$_SESSION['alice_isBonus2']=true;
		}
		
		set_stat_game($userId, $user_balance, $allbet,$winall44,$stat_txt);
	}
	
	/*   Free game   */
	if ($action == 'freegame')
	{
		$stat_txt = "alice_free";
		
	    include_once 'func.php';

	    $rowb9 = get_game_settings('alice');
	    $g_rezim = $rowb9['g_rezim'];
$mode=$g_rezim;
	    
		
$lin[0]=array(2,5,8,11,14);
$lin[1]=array(1,4,7,10,13);
$lin[2]=array(3,6,9,12,15);
$lin[3]=array(1,5,9,11,13);
$lin[4]=array(3,5,7,11,15);
$lin[5]=array(2,4,7,10,14);
$lin[6]=array(2,6,9,12,14);
$lin[7]=array(1,4,8,12,15);
$lin[8]=array(3,6,8,10,13);
$lin[9]=array(2,6,8,10,14);
$lin[10]=array(2,4,8,12,14);
$lin[11]=array(1,5,8,11,13);
$lin[12]=array(3,5,8,11,15);
$lin[13]=array(1,5,7,11,13);
$lin[14]=array(3,5,9,11,15);
$lin[15]=array(2,5,7,11,14);
$lin[16]=array(2,5,9,11,14);
$lin[17]=array(1,4,9,10,13);
$lin[18]=array(3,6,7,12,15);
$lin[19]=array(1,6,9,12,13);
$lin[20]=array(3,4,7,10,15);
$lin[21]=array(2,6,7,12,14);
$lin[22]=array(2,4,9,10,14);
$lin[23]=array(1,6,7,12,13);
$lin[24]=array(3,4,9,10,15);
$lin[25]=array(1,6,8,10,15);
$lin[26]=array(3,4,8,12,13);
$lin[27]=array(2,4,9,11,15);
$lin[28]=array(1,6,8,12,13);
$lin[29]=array(3,5,7,10,14);

$psym[1]=array(0,0,0,4,15,100);
$psym[2]=array(0,0,0,4,15,100);
$psym[3]=array(0,0,0,4,15,100);
$psym[4]=array(0,0,0,7,25,200);
$psym[5]=array(0,0,0,7,25,200);
$psym[6]=array(0,0,0,7,25,200);
$psym[7]=array(0,0,0,10, 30, 300);
$psym[8]=array(0,0,0,12,50,500);
$psym[9]=array(0,0,0,15,80,750);
$psym[10]=array(0,0,0,20,100,2000);
$psym[11]=array(0,0,0,40,150,4000);
$psym[12]=array(0,0,2,5,25,200);
$psym[13]=array(0,0,0,15,80,750);
		
	    $shs = explode( "|", $rowb9['g_shanswin'] );
	    if ( $line == 1  )	
	    {
	        $ooo2 = $shs[0];
	    }
	    if ( $line == 3  || $line == 2)
	    {
	        $ooo2 = $shs[1];
	    }
	    if ( $line == 5 || $line == 4)
	    {
	        $ooo2 = $shs[2];
	    }
	    if ( $line == 7 || $line == 6)
	    {
	        $ooo2 = $shs[3];
	    }
	    if ( $line == 9 || $line == 8)
	    {
	        $ooo2 = $shs[4];
	    }
		 if ( $line == 11 || $line == 10)
	    {
	        $ooo2 = $shs[5];
	    }
		 if ( $line == 13 || $line == 12)
	    {
	        $ooo2 = $shs[6];
	    }
		 if ( $line == 14 || $line == 15)
	    {
	        $ooo2 = $shs[7];
	    }
		 if ( $line == 16 || $line == 17)
	    {
	        $ooo2 = $shs[8];
	    }
		 if ( $line == 18 || $line == 19)
	    {
	        $ooo2 = $shs[9];
	    }
		 if ( $line == 20 || $line == 21)
	    {
	        $ooo2 = $shs[10];
	    }
		 if ( $line == 22 || $line == 23)
	    {
	        $ooo2 = $shs[11];
	    }
		 if ( $line == 24 || $line == 25)
	    {
	        $ooo2 = $shs[12];
	    }
		 if ( $line == 26 || $line == 27)
	    {
	        $ooo2 = $shs[13];
	    }
		 if ( $line == 28 || $line == 29)
	    {
	        $ooo2 = $shs[14];
	    }
		 if ( $line == 30 || $line == 31)
	    {
	        $ooo2 = $shs[15];
	    }
	    $casbank = winlimit(); // Банк игры
		
	   $rnd_bonus = rand( 1, ($rowb9['g_shansbon']+200) );
		
		$rnd_wild = rand( 1, $ooo2+10 ); // Шанс выиграть
		
		
	    $rnd_result = rand( 1, $ooo2 ); // Шанс выиграть
	    if ($rnd_result == 1)
	        $mas_win = 1; // Если должен выиграть
	    else
	        $mas_win = 0;

		if ( ($mas_win == 1) && ($rnd_bonus == 1) && ($casbank < $allbet * 5 + $allbet * $psym[$scatter][4]) )
	        $rnd_bonus = 0;
		if ( ($mas_win == 1) && ($casbank < $allbet * 5) )
			$mas_win = 0;
	
			$rnd_bonus = 0;
			
	
			

	    $am = 0;
	$lc=0;
	
	$tmp_mpl=$_SESSION['freeMpl'];
	     while ( $am < 100000 )
	    {
	        $map_win = array( 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0 );
			$winbonus = 0;
			
			$bonusSymCount=0;
			
	        $mx2=get_rows($mode,0,0);

	        for ( $k = 0; $k <= 14; ++$k ){
	            $map[$k] = $mx2[$k];
			$_SESSION['freeMpl']=$tmp_mpl;
			
			if($map[$k]==$scatter){
			$bonusSymCount++;
			}
			
			}
			
			
				
				    
			
					
				$winbonus = $allbet * $psym[$scatter][$bonusSymCount];
			
			
				
			
			
	        for ( $ln = 0; $ln <= $line-1; ++$ln )
	        {
	            $s[1] = $map[$lin[$ln][0]-1];
	            $s[2] = $map[$lin[$ln][1]-1];
	            $s[3] = $map[$lin[$ln][2]-1];
	            $s[4] = $map[$lin[$ln][3]-1];
	            $s[5] = $map[$lin[$ln][4]-1];
				
		
				
				
			$map_win[$ln]=WinLin($s,13,$psym,array(12),0)*$bet;
			
			
			
			
				
			
				



	        }
			
	        for ( $k = 1; $k <= 15; ++$k )
	        {
	            ${ "sym".$k } = $map[$k - 1];
	        }
            for ( $k = 1; $k <= 30; ++$k )
	        {
	            ${ "win".$k } = 0;
	        }
	        for ( $k = 1; $k <= 30; ++$k )
	        {
	            ${ "win".$k } = $map_win[$k - 1];
	        }
			
	       
			
	        
	        	$winall = $win1 + $win2 + $win3 + $win4 + $win5 + $win6 + $win7 + $win8 + $win9+ $win10 + $win11 + $win12 + $win13 + $win14 + $win15+ $win16+ $win17+ $win18+ $win19+ $win20 +$win21+ $win22+ $win23+ $win24+ $win25+ $win26+ $win27+ $win28+ $win29+ $win30+ $winbonus;
			
	        ++$am;
	
			
			if ( $mas_win == 1 && $winall == 0 )
	        {
	            $am = 10;
	        }
			if ( $mas_win == 1 && 0 < $winall )
	        {
	            $am = 120000;
	        }
	        if ( $mas_win == 0 && $winall == 0 )
	        {
	            $am = 120000;
	        }
	
	
	
			if ( $casbank < $winall)
	        {
	            $am = 10;
	        }
	if($lc>=8000){
	$mas_win=0;
        $rnd_bonus=0;
        $mode=6;
	}
	$lc++;
	    }
		
		$winall44 = sprintf( "%01.2f", $winall );
		if ($winall > 0)
		{
			cange_balance($userId, $winall44);
	        change_bank($user_id,'alice',$winall44*-1);
	$_SESSION['microbank']-=$winall44;
			$_SESSION['alice_win'] = $_SESSION['alice_win'] + $winall;
			
		
			
		}
		
		
		$winall=$_SESSION['alice_win'];	
		
		
		
		ge_serv_show_str( "result=ok&info=|$sym1|$sym2|$sym3|$sym4|$sym5|$sym6|$sym7|$sym8|$sym9|$sym10|$sym11|$sym12|$sym13|$sym14|$sym15|$bet|$line|$allbet|$winall|0|$allbet|$winall|0|1|0&id=$user_array[login]&balance=$user_balance");
	
		
		set_stat_game($userId, $user_balance, $allbet,$winall44,$stat_txt);
		
		$_SESSION['alice_freeGameCount'] -= 1;
		if ($_SESSION['alice_freeGameCount'] == 0)
			unset($_SESSION['alice_freeGameCount']);
			
		
			
	}
	
	
	/*   Конец игры  */
	if ($action == "finish")
	{
		

		$user_balance = floor( get_balance($userId) );
		//mysql_query( "INSERT INTO stat_game VALUES(NULL,NULL,$userId,'$user_balance','$allbet','$winall44','$stat_txt')" );
		unset($_SESSION['alice_win'], $_SESSION['alice_d']);

		ge_serv_show_str( "result=ok&state=0&min=1&id=$user_array[login]&balance=$user_balance");
	}
	
	/*   Риск-игра  */
	if ( $action == "double" )
	{
		if (!isset($_SESSION['alice_win']))
		{
			ge_serv_show_str( 'error|Ошибка! Попытка повлиять на игру. Ваш аккаунт блокирован. За подробностями обращайтесь к администратору.|user_blocked.php');
			block_user($userId);
		}
		else
		{
		    $d = isset($_SESSION['alice_d']) ? $_SESSION['alice_d'] + 1 : 1;
			$_SESSION['ns_safariheat_d'] = $d;
			$winall = $_SESSION['alice_win'];
			$betBegin = $winall;
			$bet = isset($_POST['bet']) ? $_POST['bet'] : 0;
			$winall44 = sprintf( "%01.2f", $winall );
		    cange_balance($userId, $winall44*-1);
		 change_bank($user_id,'alice',$winall44);
		$_SESSION['microbank']+=$winall44;

			$row_bon=get_game_settings('alice');
			$g_shansdouble=$row_bon['g_rezerv'];
			$shans = rand( 1, $g_shansdouble );
			if ( $shans == 1 )
				$winall2 = $winall * 2;
				
			$casbank = winlimit( );
			
		    if ( $casbank < $winall2 || $shans != 1 || $d >= 255 )
		    {
				$winall2 = 0;
				unset($_SESSION['alice_d'], $_SESSION['alice_win']);
			}

			if ( $winall2 > 0 )
			{
				if ( $bet == 0 )
					$deler = 3;
				if ( $bet == 1 )
					$deler = 1;
			}

			if ( $winall2 == 0 )
			{
				if ( $bet == 0 )
					$deler = 1;
				if ( $bet == 1 )
					$deler = 3;
			}

			$stat_txt = "alice_double";
			$winall44 = sprintf( "%01.2f", $winall2 );

			ge_serv_show_str( "result=ok&info=$deler&id=$user_array[login]&balance=$user_balance");
			
			set_stat_game($userId, $user_balance, $betBegin,$winall44,$stat_txt);
			if ($winall2 > 0)
			{
				cange_balance($userId, $winall44);
			   change_bank($user_id,'alice',$winall44*-1);
			$_SESSION['microbank']-=$winall44;
				$_SESSION['alice_win'] = $winall2;
			}
		}
	}
?>