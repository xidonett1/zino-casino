{if !isset($system)}

<form id="pin" method="POST" action="/engine/dir/pay/pin/pin.php">
									<fieldset>
										<div class="block">
											<div class="title">
												<h2>{$lang['enter_PIN_CODE']}</h2>
											</div>
											<div class="form-control">
												<input type="text" name="pin_code" placeholder="{$lang['enter_pin_input']}" />
												<input type="submit" value="{$lang['button_OK']}" class="btn-blue" />
											</div>
										</div>
									</fieldset>
</form>

{/if}