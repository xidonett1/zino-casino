<div class="popup-holder-new">
	<div id="choose_reg_bonus" class="lightbox2 newreg">
     			<h3>{$lang['popup_WELCOME_BONUS_CHOICE']}</h3>
	   <form action="" method="post">
                     <ul class="sales">
                      <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="1" name="bonus"><span class="p1"></span></label>
                       <div class="saless1">{$lang['popup_GIFT_REG']}</div><br>
                       <div class="saless2">{$lang['popup_gift_reg_txt']}</div></li>
                      <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="2" name="bonus"><span class="p2"></span></label>
                       <div class="saless1">{$lang['popup_GIFT_DAY']}</div><br>
                       <div class="saless2">{$lang['popup_gift_day_txt']}</div></li>
                      <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="3" name="bonus"><span class="p3"></span></label>
                       <div class="saless1">{$lang['popup_GIFT_PAY']}</div><br>
                       <div class="saless2">{$lang['popup_gift_pay_txt']}</div></li>
                     </ul>
              <div class="green-new"><a class="btn-green" onclick="return false" href="#registration">{$lang['button_REGISTER']}!</a></div>
       </form>
	</div>
</div>
 
<div class="popup-holder">

    <div class="lightbox" id="login">
			<h3>{$lang['popup_ENTER']}</h3>
			<div class="form">
				<form action="/login" method="POST">
					<fieldset>
						<label for="login:log">{$lang['popup_login']}:*</label>
						<input type="text" name="user" id="login:log"/>
						<label for="pass:log">{$lang['popup_pass']}:*</label>
						<input type="password" name="pass" id="pass:log"/>
						<a href="#restore" class="fancybox forgot">{$lang['popup_reminder']}?</a>
            <input type="submit" value="{$lang['button_ENTER']}" class="btn-green" />
					</fieldset>
				</form>
			</div>
	</div>
		
		<div class="lightbox" id="registration">
			<h3>{$lang['popup_REGISTER']}</h3>
			<div class="form">
				<form action="/registration?action=save" method="post" class="error success">
        <input id="gift" type="hidden" name="gift">
					<fieldset>
						<label for="login:reg">{$lang['popup_login']}:*</label>
						<input type="text" name="ulogin" id="login:reg"/>
						<label for="pass:reg">{$lang['popup_pass']}:*</label>
						<input type="password" name="pass" id="pass:reg"/>
						<label for="pass2:reg">{$lang['popup_re_pass']}:*</label>
						<input type="password" name="repass" id="pass2:reg"/>
						<label for="email:reg">{$lang['popup_email']}:*</label>
						<input type="text" name="email" id="email:reg"/>
						<div class="row">
							<input type="checkbox" name="yes" value="1" id="rules"/>
							<label for="rules">{$lang['popup_yes_rules']}</label>
						</div>
            <div id='rec2' class="captcha"></div>
						<input type="submit" value="{$lang['button_REGISTER']}!" class="btn-green" />
					</fieldset>
				</form>
			</div>
		</div>
		
		<div class="lightbox" id="restore">
			<h3>{$lang['button_RECOVERY']}</h3>
			<div class="form">
				<form action="/reminder?action=send" method="post">
					<fieldset>
						<label for="login:restore">{$lang['popup_login']}:*</label>
						<input type="text" name="ulogin" id="login:restore"/>
						<label for="email:restore">{$lang['popup_email']}:*</label>
						<input type="text" name="email" id="email:restore"/>
            <div id='rec3' class="captcha"></div>
						<input type="submit" value="{$lang['button_RECOVERY']}" class="btn-green" />
					</fieldset>
				</form>
			</div>
		</div>

</div>

<script>
$(document).ready(function(){
   $(".choose_reg_bonus").on("click",function(){
      console.log(this.value);
      $("#gift").val(this.value);
      $("#choose_reg_bonus .btn-green").addClass('fancybox');
   }); 
});
</script>            
