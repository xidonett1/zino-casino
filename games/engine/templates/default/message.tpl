{if count($messages)>0}
  {foreach $messages as $message}
    <div class="message{if $message[0]=='er'} error{else} message{/if}-note">
      <p>{$message[1]}</p>
    </div>
  {/foreach}
{/if}