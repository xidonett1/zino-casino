{if $login} 
            <div class="right-block">
            <a class="btn-head-green btn-head" href="/enter">{$lang['button_PAY']}</a>
            <a class="btn-head-orange btn-head" href="/out">{$lang['button_OUT']}</a>
            <a class="btn-head-bonus" href="/enter?action=setbonus"></a>
						<div class="user-area">
							<a class="btn-logout" href="/logout" title="{$lang['auth_exit']}">{$lang['auth_exit']}</a>
							<div class="ava align-left">
							<a href="/profile"><img src="{$theme_url}/images/ava.png" width="50" height="50" alt="{$login}"/></a>
							</div>
							<ul>
								<li>
									<a href="/profile" class="title">{$lang['auth_profile']}</a>
									<p>{$login}</p>
								</li>
								<li>
									<a href="/out" class="title balance">{$lang['auth_balance']}</a>
									<p>{$balance} {$lang['auth_cur']}</p>
								</li>
								<li>
									<a href="#" class="title wager">{$lang['auth_wager']}</a>
									<p>{$user_info['wager']} {$lang['auth_bet']}</p>
								</li>								
								<li>
									<a href="/partner" class="title partner">{$lang['auth_partner']}</a><br>
									<a href="/history" class="title history">{$lang['auth_history']}</a>
								</li>
							</ul>
						</div>
			</div>
{else} 
<div class="right-block-2">
{if $config['use_ulogin']}                    
<ul class="social-sharing">
<script src="//ulogin.ru/js/ulogin.js"></script>
<div id="uLogin" data-ulogin="display=panel;theme=flat;fields=first_name,last_name;providers=
      {$config['ulogin_block']}
=;redirect_uri=http://{$config['url']}/registration?ulogin"></div>
</ul>
{/if}

<a class="btn-green fancybox show-captcha" href="#choose_reg_bonus">{$lang['button_REGISTER']}!</a>
<a class="btn-orange btn-login fancybox show-captcha" href="#login">{$lang['button_ENTER']}</a>
</div> 

{include file='dialogs.tpl'} 

{/if}