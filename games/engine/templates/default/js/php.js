function in_array(needle, haystack, strict) {   
    var found = false, key, strict = !!strict;
 
    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }
 
    return found;
}


function isset () {
	var a=arguments, l=a.length, i=0;
	 
	if (l===0) {
	    return true; 
	}
	 
	while (i!==l) {
	    if (typeof(a[i])=='undefined' || a[i]===null) { 
	        return false; 
	    } else { 
	        i++; 
	    }
	}
	return true;
}

function array_key_exists ( key, search ) {
	if( !search || (search.constructor !== Array && search.constructor !== Object) ){
		return false;
	}

	return search[key] !== undefined;
}


function is_array( mixed_var ) {
	return ( mixed_var instanceof Array );
}


function getCookie(c_name){
	 var i,x,y,ARRcookies=document.cookie.split(";");
	 for (i=0;i<ARRcookies.length;i++){
	  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
	  x=x.replace(/^\s+|\s+$/g,"");
	  if (x==c_name) {
	  return unescape(y);
	  }
	 }
	}

function utodate ( format, timestamp ) {	// Format a local time/date

	var a, jsdate = new Date(timestamp ? timestamp * 1000 : null);
	var pad = function(n, c){
		if( (n = n + "").length < c ) {
			return new Array(++c - n.length).join("0") + n;
		} else {
			return n;
		}
	};
	var txt_weekdays = ["Sunday","Monday","Tuesday","Wednesday",
		"Thursday","Friday","Saturday"];
	var txt_ordin = {1:"st",2:"nd",3:"rd",21:"st",22:"nd",23:"rd",31:"st"};
	var txt_months =  ["", "January", "February", "March", "April",
		"May", "June", "July", "August", "September", "October", "November",
		"December"];

	var f = {
		// Day
			d: function(){
				return pad(f.j(), 2);
			},
			D: function(){
				t = f.l(); return t.substr(0,3);
			},
			j: function(){
				return jsdate.getDate();
			},
			l: function(){
				return txt_weekdays[f.w()];
			},
			N: function(){
				return f.w() + 1;
			},
			S: function(){
				return txt_ordin[f.j()] ? txt_ordin[f.j()] : 'th';
			},
			w: function(){
				return jsdate.getDay();
			},
			z: function(){
				return (jsdate - new Date(jsdate.getFullYear() + "/1/1")) / 864e5 >> 0;
			},

		// Week
			W: function(){
				var a = f.z(), b = 364 + f.L() - a;
				var nd2, nd = (new Date(jsdate.getFullYear() + "/1/1").getDay() || 7) - 1;

				if(b <= 2 && ((jsdate.getDay() || 7) - 1) <= 2 - b){
					return 1;
				} else{

					if(a <= 2 && nd >= 4 && a >= (6 - nd)){
						nd2 = new Date(jsdate.getFullYear() - 1 + "/12/31");
						return date("W", Math.round(nd2.getTime()/1000));
					} else{
						return (1 + (nd <= 3 ? ((a + nd) / 7) : (a - (7 - nd)) / 7) >> 0);
					}
				}
			},

		// Month
			F: function(){
				return txt_months[f.n()];
			},
			m: function(){
				return pad(f.n(), 2);
			},
			M: function(){
				t = f.F(); return t.substr(0,3);
			},
			n: function(){
				return jsdate.getMonth() + 1;
			},
			t: function(){
				var n;
				if( (n = jsdate.getMonth() + 1) == 2 ){
					return 28 + f.L();
				} else{
					if( n & 1 && n < 8 || !(n & 1) && n > 7 ){
						return 31;
					} else{
						return 30;
					}
				}
			},

		// Year
			L: function(){
				var y = f.Y();
				return (!(y & 3) && (y % 1e2 || !(y % 4e2))) ? 1 : 0;
			},
			//o not supported yet
			Y: function(){
				return jsdate.getFullYear();
			},
			y: function(){
				return (jsdate.getFullYear() + "").slice(2);
			},

		// Time
			a: function(){
				return jsdate.getHours() > 11 ? "pm" : "am";
			},
			A: function(){
				return f.a().toUpperCase();
			},
			B: function(){
				// peter paul koch:
				var off = (jsdate.getTimezoneOffset() + 60)*60;
				var theSeconds = (jsdate.getHours() * 3600) +
								 (jsdate.getMinutes() * 60) +
								  jsdate.getSeconds() + off;
				var beat = Math.floor(theSeconds/86.4);
				if (beat > 1000) beat -= 1000;
				if (beat < 0) beat += 1000;
				if ((String(beat)).length == 1) beat = "00"+beat;
				if ((String(beat)).length == 2) beat = "0"+beat;
				return beat;
			},
			g: function(){
				return jsdate.getHours() % 12 || 12;
			},
			G: function(){
				return jsdate.getHours();
			},
			h: function(){
				return pad(f.g(), 2);
			},
			H: function(){
				return pad(jsdate.getHours(), 2);
			},
			i: function(){
				return pad(jsdate.getMinutes(), 2);
			},
			s: function(){
				return pad(jsdate.getSeconds(), 2);
			},
			//u not supported yet

		// Timezone
			//e not supported yet
			//I not supported yet
			O: function(){
			   var t = pad(Math.abs(jsdate.getTimezoneOffset()/60*100), 4);
			   if (jsdate.getTimezoneOffset() > 0) t = "-" + t; else t = "+" + t;
			   return t;
			},
			P: function(){
				var O = f.O();
				return (O.substr(0, 3) + ":" + O.substr(3, 2));
			},
			//T not supported yet
			//Z not supported yet

		// Full Date/Time
			c: function(){
				return f.Y() + "-" + f.m() + "-" + f.d() + "T" + f.h() + ":" + f.i() + ":" + f.s() + f.P();
			},
			//r not supported yet
			U: function(){
				return Math.round(jsdate.getTime()/1000);
			}
	};

	return format.replace(/[\\]?([a-zA-Z])/g, function(t, s){
		if( t!=s ){
			// escaped
			ret = s;
		} else if( f[s] ){
			// a date function exists
			ret = f[s]();
		} else{
			// nothing special
			ret = s;
		}

		return ret;
	});
}

function setCookie(title, value, exp) {
   var expdate = new Date();
   expdate.setDate(expdate.getDate() + exp);
   document.cookie = title+'='+value+';expires='+expdate.toGMTString()+';path=/';
}
function deleteCookie(title) {
  setCookie(title, "", -1);
}

// функция, которая рисует сообщение
function message(a){

	str='';
	
	if (is_array(a)){
		for (var i in a){
			str+=a[i]+'<br>';
		}	
	}
	else{
		str=a;
	}
	
	var mes_id='message-'+Math.round(Math.random()*100000);
	
	$('body').append('<div class="message_lower" id="'+mes_id+'">'+str+'</div>')
	
	$('#'+mes_id ).dialog({
		modal: true,
		width: 700,
		buttons: {
			Ok: function() {
				$(this).dialog( "close" );
				$('#'+mes_id).remove();
			}
		}
	});
	
	$('#'+mes_id).dialog("open");
	
	
	
}

function newnews (item, url){
        id='news-'+item;
        $('body').append('<div id="'+id+'"></div>');

        $('#'+id).load(url+item);

        //показываем окно

        $('#'+id ).dialog({
                modal: true,
                width: 700,
                buttons: {
                        Ok: function() {
                                $(this).dialog( "close" );
                                $('#'+id).remove();
                        }
                }
        });
}

//красивый confirm
function verify(a,f){

	str='';
	
	if (is_array(a)){
		for (var i in a){
			str+=a[i]+'<br>';
		}	
	}
	else{
		str=a;
	}
	
	id='message-'+Math.round(Math.random()*100000);
	
	$('body').append('<div id="'+id+'">'+str+'</div>')
	
	$('#'+id ).dialog({
		modal: true,
		width: 700,
		buttons: {
			Ok: function() {
				f();
				$(this).dialog( "close" );
				$('#'+id).remove();
			},
			Cancel: function() {
				$(this).dialog( "close" );
				$('#'+id).remove();
			}
			
		}
	});
	
	$('#'+id).dialog("open");

}
//красивый confirm
function verify2(a,f){

	str='';
	
	if (is_array(a)){
		for (var i in a){
			str+=a[i]+'<br>';
		}	
	}
	else{
		str=a;
	}
	
	verify2_id='message-'+Math.round(Math.random()*100000);
	
	$('body').append('<div id="'+verify2_id+'">'+str+'</div>')
	
	var yes=__('Подтвердить');
	var no=__('Отменить');
	if (langsite.lang === 'ru') {
	$('#'+verify2_id ).dialog({
		modal: true,
		width: 700,
		buttons: {
			Подтвердить: function() {
				f();
				$(this).dialog( "close" );
				$('#'+verify2_id).remove();
			},
			Отменить: function() {
				$(this).dialog( "close" );
				$('#'+verify2_id).remove();
			}
			
		}
	});
	}
	else {
	    $('#'+verify2_id ).dialog({
		modal: true,
		width: 700,
		buttons: {
			Yes: function() {
				f();
				$(this).dialog( "close" );
				$('#'+verify2_id).remove();
			},
			No: function() {
				$(this).dialog( "close" );
				$('#'+verify2_id).remove();
			}
			
		}
	});
	}
	
	$('#'+verify2_id).dialog("open");

}

//вспомогательная для закрытия окна по enter
function closedialogok(e,id,f){
	
	if(e.keyCode==13){
		
		$('#'+id).parent().find('.ui-button-text').each(function(index,el) {
		    if ($(el).html()=='Ok'){
		    	$(el).parent().click();
		    }
		});
	}
	
}

//окно со строкой с параметром
function issue(a,f){
	
	str='';
	
	if (is_array(a)){
		for (var i in a){
			str+=a[i]+'<br>';
		}	
	}
	else{
		str=a;
	}
	
	id='issue-'+Math.round(Math.random()*100000);
	
	str+='<br> <input type="text" id="input-'+id+'" onkeypress="closedialogok(event,\''+id+'\')" >'
	
	
	
	$('body').append('<div id="'+id+'">'+str+'</div>')
	
	$('#'+id ).dialog({
		modal: true,
		width: 700,
		buttons: {
			Ok: function() {
				val=$('#input-'+id).val();
				$(this).dialog( "close" );
				$('#'+id).remove();
				f(val);
			},
			Cancel: function() {
				$(this).dialog( "close" );
				$('#'+id).remove();
			}
			
		}
	});
	
	$('#'+id).dialog("open");
	
	
}



function fact(n) {
	if (n<=1){ 
		return 1
	}
	return n*fact(n-1);
}


//получение GET параметров 
function getUrlVars() {
	var vars = [], hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for(var i = 0; i < hashes.length; i++)
	{
		hash = hashes[i].split('=');
		vars[hash[0]] = hash[1];
	}
	return vars;
}

function supportsLocalStorage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
} catch (e) {
    return false;
  }
}

function ipToInt(ip) {
	if(ip.slice(-3) === '/32'){
		ip = ip.slice(0,-3);
	}
	ip = ip.split('.');
	return (ip[0] << 24) + (ip[1] << 16) + (ip[2] << 8) + (ip[3] << 0);
}

function verify3(text, func, param){
            
    var arg = null;
    if(typeof param !== 'undefined'){
        arg = param 
    }

    var id = Math.round(Math.random()*10000);

    $('body').append('<div id="message-'+id+'"><p>'+text+'</p></div>');

    var modal = $('#message-'+id); 

    modal.dialog({
        modal:true,
        buttons:[
            {
                text: __('Да'),
                click: function(){
                    func(arg);
                    modal.dialog('close');
                }
            },
            {
                text: __('Нет'),
                click: function(){
                    modal.dialog('close');
                }
            }
        ],
        close: function(){
            modal.remove();
        }
    })

    modal.dialog('open'); 
}
function verifyCancelBet(a,f){
	var str='';
	if (is_array(a)){
		for (var i in a){
			str+=a[i]+'<br>';
		}	
	}
	else{
		str=a;
	}
	var id='message-'+Math.round(Math.random()*100000);
	$('body').append('<div id="'+id+'">'+str+'</div>')
	$('#'+id ).dialog({
		modal: true,
		width: 700,
		buttons: 
			[
				{
					text: __("ДА, (хочу)"),
					click: function() {
						  f();
						  $(this).dialog( "close" );
						  $('#'+id).remove();
					}
				},
				{
					text: __("НЕТ, (не хочу)"),
					class: "no_cancel_bet",
					click: function() {
						$(this).dialog( "close" );
						$('#'+id).remove();
					}
				}
			]
	});
	
	$('#'+id).dialog("open");
}


task={
period:3000,	
id:null,	
callback:null,

create:function(type,param,callback){
	
	task.callback=callback;
	
	mes='<div id="wait19264" hidden="" style="text-align:center;">';
	mes+='<img src="/image/wait.gif"><br><br>';
	mes+='<span style="font-size:12px;">Дождитесь завершения текущей операции...</span></div>';
	
	$('body').append(mes);
	
	$('#wait19264').dialog({
						modal: true,
						width: 'auto',
						height: 'auto',
						resizable: false,
						closeOnEscape: false
					});
	$("#wait19264").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
	$('#wait19264').dialog("open");
	
	$.get('/task.php?type='+type+'&'+param, function(data){
														task.id=data;
														setTimeout(task.wait,task.period);
													}
		);
	
	
	
},

wait:function(){
	$.get('/task.php?id='+task.id, function(data){
												if(data==0 || data==1){
													setTimeout(task.wait,task.period);
												}else if(data==3){
													$('#wait19264').dialog("close");
													$('#wait19264').remove();
													message('При выполнении запроса произошла ошибка попробуйте ещё раз');
												}else{
													$('#wait19264').dialog("close");
													$('#wait19264').remove();

													task.callback(data);
												}
												
											}
		);
}
	
	
	
	
};








