<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<meta http-equiv="Cache-Control" content="no-transform" />
	<meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0" />
	<title>{$title}</title>
	<meta name="description" content="{$description}" />
    <meta name="keywords" content="{$keywords}" />
	
    
	<link media="screen" href="fixed.css" type= "text/css" rel="stylesheet" />
	<script src="src/webgl-2d.js" type="text/javascript"></script>
	

  
</head>
<body >

		

		 <div id="gameArea">
		<canvas id="canvas2"></canvas>
		<canvas id="canvas"></canvas>
	</div>
	<div id="slideUpOverlay">
		<div id="slideUp">
		</div>
	</div>
	<div id="rotateOverlay">
		<div id="rotatePanel">
			<div id="rotate">
			</div>
			<div id="rotateInfo">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="src/bellsonfireloader_00285223.js"></script>
		

	
</body>

</html>