<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{$title}</title>
	<meta name="description" content="{$description} | {$lang['copyright']}" />
    <meta name="keywords" content="{$keywords}" />
	<link media="all" rel="stylesheet" href="{$theme_url}/css/styles.css" />
	<link media="all" rel="stylesheet" href="{$theme_url}/css/vendors/owl.carousel.css" />
	<link media="all" rel="stylesheet" href="{$theme_url}/css/vendors/jquery.fancybox.css" />
    <link media="all" rel="stylesheet" href="{$theme_url}/css/flagstrap/css/flags.css" />
  
    <!--[if IE]><link media="all" rel="stylesheet" href="{$theme_url}/css/styles_ie.css" /><![endif]-->
  
	<script type="text/javascript" src="{$theme_url}/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="{$theme_url}/js/vendors/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{$theme_url}/js/vendors/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="{$theme_url}/js/jquery.main.js"></script>
    <script type="text/javascript" src="{$theme_url}/js/common.js"></script>
    <script type="text/javascript" src="{$theme_url}/js/cookies.js"></script>
	
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
 
</head>
<body>

	<div id="wrapper">
		<div class="w1">
    
    <div class="langs">
      {foreach $available_langs as $language}
        <a href="" id="{$language}"><i class="flagstrap-icon flagstrap-{$language}" style="margin-left: 5px;"></i></a>
      {/foreach}
    </div>
			<header id="header">
				<div class="header-holder">
					<strong class="logo"><a href="/">{$config['cas_name']}</a></strong>
						{include file='login.tpl'}
				</div>
				<nav id="main-nav">
					<ul>
            {foreach $game_groups as $k=>$val}
				      <li><a href="#" onclick="load_gameList({$k}); return false;">{$val}</a></li>
            {/foreach} 
					</ul>
				</nav>
			</header>
			<div id="main">
				<div class="layout">
					<div id="content">
					
			{if $messages}
              {include file='message.tpl'}
            {/if}
            
            {if isset($content_templ) && $content_templ}
              {include file=$content_templ}
            {/if}  
            
					</div>
					
					<aside id="sidebar">
        {if $config['use_blocks']}
            {if $login}
            <div class="side-block rating">
							<h3>{$lang['block_RATING']}</h3>
               <div class="rate">
			   <div class="rate_icon">
			   <a href="/exchange">
               <img src="{$theme_url}/images/{$user_info['rating_pic']}">
			   </a>
               <span class="rate_level">{$user_info['rating_level']}</span>
               </div>
               </div>
               <div class="clear"></div>
			</div>
            {/if}
		{/if}
						<div class="side-block winners">
							<h3>{$lang['block_WIN_NOW']}</h3>
							<ul>
                {foreach $last_wins as $wins}
                <li>
				<a href="/games/{$wins.path}/{$wins.game}/real">
                    <img class="align-left" src="{$theme_url}/ico/{$wins.game}.png" width="50" height="50" alt="{$wins.title}" />
				</a>
									<div class="text">
										<div class="summ">
											<b>{$wins.win}</b>
										</div>
										<strong class="title">{$wins.title}</strong>
										<p>{$lang['block_gamers']} {$wins.login}</p>
									</div>  
				        </li>
                {/foreach}
							</ul>
						</div>
            {if $payout_block}
						<div class="side-block payout">
							<h3>{$lang['block_WIN_BEST']}</h3>
							<ul>
              {foreach $payout_block as $payout}
								<li>
                <a href="/games/{$payout.g_path}/{$payout.game}/real">
					<img class="align-left" src="{$theme_url}/ico/{$payout.game}.png" width="80" height="63" alt="{$payout.g_title}" />
				</a>
									<div class="text">
										<div class="summ">
											<b>{$payout.suma}</b>
										</div>
										<strong class="title">{$payout.g_title}</strong>
										<p>{$lang['block_bet']} {$payout.stav}</p>
									</div>  
								</li>
               {/foreach} 
								
							</ul>
						</div>
            {/if}
						<div class="side-block contacts">
							<h3>{$lang['block_CONTACT']}</h3>
							<dl>
                {if $config.contact_phone}
								<dt>
									<strong class="tel">{$lang['block_phone']}:</strong>
								</dt>
								<dd>{$config.contact_phone}</dd>
                {/if}
        {if $config['use_blocks']}
                {if $config.contact_mail}
								<dt>                     
									<strong class="email">{$lang['block_email']}:</strong>
								</dt>
								<dd><a href="mailto:{$config.contact_mail}">{$config.contact_mail}</a></dd>
                {/if}
                {if $config.contact_icq}
								<dt>
									<strong class="icq">{$lang['block_icq']}:</strong>
								</dt>
								<dd>{$config.contact_icq}</dd>
                {/if}
		{/if}
								<dt>
									<strong class="support">{$lang['block_support']}:</strong>
								</dt>
								<dd><a class="fancybox" href="{if $login}#support{else}#login{/if}">{$lang['block_write_to_us']}</a></dd>
							</dl>
						</div>
						<p></p>
					</aside>
				</div>
        {if $config['use_blocks']}				
        {if $content_templ=='index.tpl'}
  
			  <div class="text-intro" id="text-intro">
					<h2>{$title}</h2>
					<p>{$sub_title}</p>
					<div class="slide">
                    {$content}
					</div>
					<div class="btn-holder">
						<a class="opener" href="" title=""></a>
					</div>
				</div>
        {/if}
        {/if}		
			</div>
        {if $config['use_blocks']}
			<footer id="footer">
				<ul class="payments">
					<li><a href="/paymethods" class="wm" title="webmoney"></a></li>
					<li><a href="/paymethods" class="yad" title="yandex dengi"></a></li>
					<li><a href="/paymethods" class="pb" title="privat24"></a></li>
					<li><a href="/paymethods" class="dm" title="Деньги@mail.ru"></a></li>
					<li><a href="/paymethods" class="lq" title="liqpay"></a></li>
					<li><a href="/paymethods" class="mr" title="moneta.ru"></a></li>
					<li><a href="/paymethods" class="ii" title="ИИ"></a></li>
					<li><a href="/paymethods" class="ak" title="АльфаКлик"></a></li>
					<li><a href="/paymethods" class="qiwi" title="qiwi"></a></li>
					<li><a href="/paymethods" class="visa" title="Visa"></a></li>
					<li><a href="/paymethods" class="mc" title="MasterCard"></a></li>
				</ul>
				<div class="columns">
					<div class="col">
						<h3>{$lang['menu_ABOUT']}</h3>
						<ul>							
							<li><a href="/guaranty">{$lang['menu_guaranty']}</a></li>
							<li><a href="/rules">{$lang['menu_privacy']}</a></li>
						</ul>
						<ul>							
							<li><a href="/bonuses">{$lang['menu_bonuses']}</a></li>
							<li><a href="/paymethods">{$lang['menu_paymethods']}</a></li>
						</ul>
						<ul>
						    <li><a href="/license">{$lang['menu_license']}</a></li>
							<li><a href="/conditions">{$lang['menu_conditions']}</a></li>						
						</ul>
					</div>
					<div class="col">
						<h3>{$lang['menu_YOUR_MENU']}</h3>
						<ul>
							<li><a class="fancybox" href="{if $login}/profile{else}#login{/if}">{$lang['menu_profile']}</a></li>
							<li><a class="fancybox" href="{if $login}/history{else}#login{/if}">{$lang['menu_history']}</a></li>
						</ul>
						<ul>
						    <li><a class="fancybox" href="{if $login}/out{else}#login{/if}">{$lang['menu_out']}</a></li>
							<li><a class="fancybox" href="{if $login}/enter{else}#login{/if}">{$lang['menu_enter']}</a></li>		
						</ul>
						<ul>
						    <li><a class="fancybox" href="{if $login}/partner{else}#login{/if}">{$lang['menu_partner']}</a></li>
							<li><a class="fancybox" href="{if $login}/enter?action=setbonus{else}#login{/if}">{$lang['menu_set_bonus']}</a></li>
						</ul>
					</div>
					<div class="col right">
						<h3>{$lang['menu_OPTIONAL']}</h3>
						<ul>
						    <li><a href="/return">{$lang['menu_return']}</a></li>
							<li><a href="/jp">{$lang['menu_jp']}</a></li>
						</ul>
						<ul>
							<li><a href="/faq">{$lang['menu_faq']}</a></li>
							<li><a href="/news">{$lang['menu_news']}</a></li>
						</ul>
					</div>
				</div>
				<div class="bottom-block">
					<p class="copy">{$lang['copyright']}</p>
					<ul class="icons">
						<li><img src="{$theme_url}/images/ico01.png" width="44" height="21" alt=""/></li>
						<li><img src="{$theme_url}/images/ico02.png" width="37" height="24" alt=""/></li>
						<li><img src="{$theme_url}/images/ico03.png" width="43" height="22" alt=""/></li>
						<li><img src="{$theme_url}/images/ico04.png" width="42" height="17" alt=""/></li>
						<li><img src="{$theme_url}/images/ico05.png" width="43" height="21" alt=""/></li>
						<li><img src="{$theme_url}/images/ico06.png" width="43" height="20" alt=""/></li>
					</ul>
				</div>
			</footer>
        {/if}
		</div>
	</div>

<div class="popup-holder">
	
		<div class="lightbox" id="support">
			<h3>{$lang['popup_SUPPORT']}</h3>
			<div class="form">
				<form action="/contacts?action=submit" method="post">
					<fieldset>
						<label for="theme">{$lang['popup_subject']}:*</label>
						<input type="text" name="subj" id="theme"/>
						<label for="text">{$lang['popup_message']}:*</label>
						<textarea name="textform" id="text" cols="30" rows="10"></textarea>
						<input type="submit" value="{$lang['button_SEND']}" class="btn-green" />
					</fieldset>
				</form>
			</div>
		</div>
		
</div>
  
{if isset($game_slider)}
          <div class="games-description" id="games-slider1" style="display: none">
							<ul class="slideset">
                {foreach $game_slider as $game}
                <li>
									<div class="item align-left">
										<a href=""><img src="{$theme_url}/ico/{$game.g_name}.png" width="165" height="130" alt=""/></a>
										<div class="buttons">
											<a class="btn-green" href="/games/{$game.g_path}/{$game.g_name}/">{$lang['button_REAL']}!</a>
    								
										</div>
									</div>
									<div class="text-holder">
										<h2>{$game.g_title}</h2>
										<p>{$game.g_desc}</p>
										<br /><br />
										<div class="rating-area">
											<span class="label">{$lang['block_Rating_stars']}:</span>
											<div class="rating">
												<div class="rating-value" style="width: 100%;"></div>
											</div>
										</div>
									</div>
				</li>
                {/foreach}
							</ul>
							<script type="text/javascript">
								$(document).ready(function(){
									$("#games-slider .slideset").owlCarousel({
										items:2,
										autoplay:true,
										loop:true,
										autoplayTimeout:5000,
										autoplayHoverPause:true
									});
                  
								});
							</script>
		  </div> 
{/if}  
</body>

<script>

function load_gameList(gr_id)
  {
  if($("#content").length)
    {
  $.ajax({
				url: "/engine/ajax/game_list.php?game_group="+gr_id,
				dataType: "json",
			 	cache: false,
				success: function(data){
        if(gr_id==-1)
          {
          $("#content").html('<div class="content"><div class="games-list">'+
							'</div></div>');
          }
        else
          {
          $("#content").html('<div class="content"><div class="games-list">'+
							'<div class="item item-games">'+
							'<span class="text">'+data.title+'</span>'+
							'</div></div></div>'+
					        '<img src="{$theme_url}/promo/{$cur_lang}_banner.jpg" width="900" height="300" /><br /><br />');
          }  
        
       
        
          $.each(data.games,function()
                {
                str='<div class="item">'
								str+='<img src="{$theme_url}/ico/'+this.g_name+'.png" width="165" height="130" alt=""/>'
								str+='<div class="buttons">'
								str+='	<a class="btn-green" href="/games/'+this.g_path+'/'+this.g_name+'/real">{$lang['button_REAL']}!</a>'
							//	str+='	<a class="btn-orange small" href="/games/'+this.g_path+'/'+this.g_name+'/demo">{$lang['button_DEMO']}</a>'
								str+='</div>'
							  str+='</div>'
                
			
                
                $("#content .content .games-list").append(str);
                });
        var item_width=165;
        var item_height=130;
               
        var width=$("#content .content").innerWidth();
        var height=$("#sidebar").height()-20-$("#content .content").height();
        var add_games_count= Math.floor(height/item_height) *  Math.floor(width/item_width) -2;
          if (add_games_count>0)
            {
                                                
          $.ajax({
				    url: "/engine/ajax/game_list.php?count="+add_games_count,
				    dataType: "json",
			 	    cache: false,
				    success: function(data){
              
              $("#content").append('<div class="content"><div id="other_games" class="games-list">'+
							'<div class="item item-other-games">'+
							'<span class="text">{$lang['corner_OTHER_GAME']}</span>'+
							'</div></div></div>');
              
              $.each(data.games,function()
                {
                str='<div class="item">'
								str+='<img src="{$theme_url}/ico/'+this.g_name+'.png" width="165" height="130" alt=""/>'
								str+='<div class="buttons">'
								str+='	<a class="btn-green" href="/games/'+this.g_path+'/'+this.g_name+'/real">{$lang['button_REAL']}!</a>'
							//	str+='	<a class="btn-orange small" href="/games/'+this.g_path+'/'+this.g_name+'/demo">{$lang['button_DEMO']}</a>'
								str+='</div>'
							  str+='</div>'
                
			
                
                $("#other_games").append(str);
                }); 
              }
            });  
            }
         
        }
			});
    }  
        else
        {
        location.href="/";
        }  
  return false;
  }
  
$(document).ready(function()
    {
      {if ($status==5 ||$status==6) && !$user_info['gift']}
        $.fancybox(
	  '  <div id="gift" class="lightbox2 newreg" style="display: block">'+
    '   <h3>{$lang['popup_WELCOME_BONUS_CHOICE']}</h3>'+
    '     <form action="/profile?action=save_gift" method="post">'+
    '       <ul class="sales">'+
    '         <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="1" name="bonus"><span class="p1"></span></label>'+
    '         <div class="saless1">{$lang['popup_GIFT_REG']}</div><br>'+
    '         <div class="saless2">{$lang['popup_gift_reg_txt']}</div></li>'+
    '         <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="2" name="bonus"><span class="p2"></span></label>'+
    '         <div class="saless1">{$lang['popup_GIFT_DAY']}</div><br>'+
    '         <div class="saless2">{$lang['popup_gift_day_txt']}</div></li>'+
    '         <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="3" name="bonus"><span class="p3"></span></label>'+
    '         <div class="saless1">{$lang['popup_GIFT_PAY']}</div><br>'+
    '         <div class="saless2">{$lang['popup_gift_pay_txt']}</div></li>'+
    '       </ul>'+
    '       <div class="green-new"><input type="submit" class="btn-green" value="{$lang['button_SAVE']}"></div>'+
    '     </form>'+
    '  </div>',
		{
      padding	: 0,
      modal: true,
		helpers : {
			overlay: {
				css: { 'background': 'rgba(0,0,0,.8)'},
				locked: true
			}
		}
		}
	);
        
        $("#gift").trigger('click');
      {/if}  
    
    if($("#content .content").height()< $("#sidebar").height()-20)
      $("#content .content").height($("#sidebar").height()-20);   
    });  
</script>

</html>