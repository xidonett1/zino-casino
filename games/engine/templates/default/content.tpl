<div class="content">
						<div class="refill article">
							<div class="heading">
								<h1>{$title}</h1>
								<div class="texts">
									<p>{$sub_title}</p>
								</div>
							</div>
							<div class="article-holder">
                           {$content}
							</div>
						</div>
</div>