<h2>{$lang['out_title']}</h2>
<div class="userForm">
            <form action="/out?action=save" method="post">

                <div class="label grid_2">{$lang['out_SUM_IN']}:</div>
                <div class="field grid_3">
                    <input type="text" name="sum" placeholder="0.00" value='{$real_balance}'>
                </div>
                <div class="clear"></div>
                
                <div class="label grid_2">{$lang['out_PS']}:</div>
                <div class="field grid_3">
                    <select name="ps">
						            {foreach $ps as $ps_id=>$ps_title}
                          <option value="{$ps_id}">{$ps_title}</option>
                        {/foreach}
										</select>
                </div>
                <div class="clear"></div>

                <a tabindex="" onclick="$(this).parents('form').submit()" class="n_btn btn_green" href="#">
                    <span><i class="ref-iconcheckmark"></i>{$lang['button_OK']}</span>
                </a>
            </form>
        </div>