function initCasinoJSONServer() {
	casinoJsonServer = jQuery.Zend.jsonrpc({
		url: '/rpc/server/json/1.0/portal/gameservice',
		async: true
	});
}

function forceCloseActiveGames() {
	if (typeof(casinoJsonServer) == 'undefined')
		initCasinoJSONServer();

	casinoJsonServer.forceClosePlayerGames({
		success: function (ret, id, method) {
			var jsonServer = jQuery.Zend.jsonrpc({
				url: '/rpc/server/json/1.0/platform/finance',
				async: true
			});
		},
		exceptionHandler: function (err) {}
	});
}

function unblockUserAccount(link)
{
	var form = link.closest('form');
	form.append('<input type="hidden" name="unblock" value="1"/>');
	form.submit();
}
function selectCountry($select) {
    var countryCode = $select.val();
    var $selectedElement = $select.find('option:selected');
    var phoneCode = $selectedElement.attr('title');
    $('#countryPhoneCode span').css('background-image', 'url("/static/img/countries/' + countryCode + '.png")');
    $('#countryPhoneCode span').html('+' + phoneCode);

}
function resizeBannersCarousel()
{
	if ($(window).width() < 980)
		$('.bannersCarousel').height($(window).width() * 250 / 980);
	else
		$('.bannersCarousel').height(250);
}

function refreshBalance(jsonServer, all) {
    all = typeof all !== 'undefined' ? all : false;
    jsonServer.amount(all, {
        success: function (ret, id, method) {
            if (typeof ret.error != 'undefined') {
                return;
            }
            for (i = 0; i < ret.length; i++) {
                if (typeof ret[i]["total"] != 'undefined') {
                    $('span#real_amount2').html(ret[i]["total"]);
                }
            }
        }
    });
}

$(document).ready(function(){
	$('#registration-login-modal').bind('reveal:close', function () {
		$('#registration-login-modal .success').html('');
		$('#registration-login-modal .error').html('');
		enableScaling();
	});

	(function($) {
		// Extend reveal goto slide functionality
		$.fn.gotoSlide = function (revealGotoSlideIndex){

			// Show selected welcome offer only for Registration tab
			if (revealGotoSlideIndex != 0)
				$('.selectedWelcomeOffer').hide();
			else
			{
				if (getCookie('welcome_offer'))
					$('.selectedWelcomeOffer').show();
				else
				{
					$('a.close-reveal-modal').click();
					return;
				}
			}

			$('.rlm-slide').hide();
			$('.rlm-slide:eq('+ revealGotoSlideIndex +')').show();
			window.scrollTo(0, 0);
		};
	})(jQuery);

	resizeBannersCarousel();


    // Check hash parameter for account blocked popup
    if ($.inArray(window.location.hash, ['#blocked', '#blocked_by_user']) != -1)
    {
        var blockedByUser = window.location.hash == '#blocked_by_user';
        window.location.hash = "";
        var errorContId = (blockedByUser) ? 'blockedAccountMsg1' : 'blockedAccountMsg2';
        $('#loginForm .error').html($('#'+errorContId).html());
        setTimeout('showLoginRequired();', 100);
    }


});


$(window).resize(function () {
	resizeBannersCarousel();
});