//----Font from localStorage
var isShowLoader = true;
(function() {
    var cache,
        insert_fonts = "",
        i,
        i_max,
        fonts =[
            {
                name:'font-iconmoon',
                md5:'b1d9175875a4995b3fb5529e3c9ffcf2'
            },
            {
                name:'font-cuprum',
                md5:'137e613d7e6bcb5205697b9cf64ce777'
            }
        ];

    function insertFont(value) {
        var style = document.createElement('style');
        style.innerHTML = value;
        document.head.appendChild(style);
        isShowLoader = false;
    }

    // PRE-RENDER
    try {
        for (i = 0, i_max = fonts.length; i < i_max; i++) {
            cache = window.localStorage.getItem(fonts[i].name);
            if (cache) {
                cache = JSON.parse(cache);
                if (cache.md5 == fonts[i].md5) {
                    insert_fonts += cache.value+' ';
                } else {
                    // Busting cache when md5 doesn't match
                    window.localStorage.removeItem(fonts[i].name);
                    cache = null;
                }
            }
        }
        if (insert_fonts.length > 0) {
            insertFont(insert_fonts);
        }
    } catch(e) {
        // Most likely LocalStorage disabled
        console.log('error',e);
    }

    // POST-RENDER
    if (!cache) {
        // Fonts not in LocalStorage or md5 did not match
        window.addEventListener('load', function() {
            var request = new XMLHttpRequest(),
                response,
                i,
                i_max;
            request.open('GET', '/engine/templates/mobile/static/fonts/v/0.321/fonts.json', true);
            request.onload = function() {
                var insert_fonts = "";
                if (this.status == 200) {
                    try {
                        response = JSON.parse(this.response);

                        for (i = 0, i_max = response.length; i < i_max; i++) {
                            insert_fonts += response[i].value+' ';
                            window.localStorage.setItem(response[i].name,  JSON.stringify(response[i]));
                        }

                        if (insert_fonts.length > 0) {
                            insertFont(insert_fonts);
                        }
                    } catch(e) {
                        // LocalStorage is probably full
                        console.log('error',e);
                    }
                }
            };
            request.send();
        });
    }
})();

// Отключаем реакцию на hover при прокрутке страницы. Уменьшаем вероятность лагов.
function disableHover()
{
var body = $('body'),
    timerBody;

$(window).bind('mousewheel', function(e){
    if (!$('.details').is(':visible')) {
    clearTimeout(timerBody);

    if(!body.hasClass("disable-hover")) {
        body.addClass( "disable-hover" )
    }

    timerBody = setTimeout(function(){
        body.removeClass("disable-hover");
    },500);
    }
});
}


function showLoader()
{
    $('#overl').fadeIn('slow');
    $('#loader').fadeIn('slow');
}

function hideLoader()
{
    $('#loader').delay(100).fadeOut('slow');
    $('#overl').delay(100).fadeOut('slow');
}

function hideLoaderSlow()
{
    $('#loader').delay(200).fadeOut('slow');
    $('#overl').delay(200).fadeOut('slow');
}

function showLoginPopup(slideIndex)
{
    if (typeof(slideIndex) == 'undefined')
        slideIndex = 1;
    
    $('#overl').fadeIn('slow');
    
    $('.reveal-modal .rlm-slide').each(function(index)
      {
      if(index==slideIndex)
        {
        $(this).show();
        }
      else
        $(this).hide();  
      }
    );
   $('#registration-login-modal').slideDown(); 
}

function showGiftPopup()
  {
  $('#overl').fadeIn('slow');
  $('#gift-modal').slideDown();
  }

function setGift(gift,uid)
  {
  if(uid>0)
    {
    location.href="/profile?action=save_gift&bonus="+gift;
    }
  else
    {
    $('#registration-login-modal input[name=gift]').val(gift);
    $('#gift-modal').slideUp();
    showLoginPopup(0);
    }
  }

$(document).ready(function () {
        
        $(document).on('click','a.close-reveal-modal',function(){$('#registration-login-modal').slideUp();$('#gift-modal').slideUp();$('#overl').delay(100).fadeOut('slow')});
} );   


function Ualert(text,type)
  {
  
  type= type || 'alert-danger';
  $("#alt").attr('class','alert '+type);
  $("#alert strong").html(text);
  $("#alert").show();
  }    

function showProfilePopup() {
    "use strict";
    showWelcomePopup()
}

function showWelcomePopup(e) {
    "use strict";
    ("undefined" == typeof e && (e = "profile"), loadTabContent(e), $("#visibleOverlay").show(), $("#welcomePopup").show())
}


function hideSystemPopup(e) {
    "use strict";
    $("#" + e).hide(), $("#visibleOverlay").hide(), $("body").removeClass("noScroll"), "number" == typeof systemPopupShownInt && clearInterval(systemPopupShownInt), enableScaling()
}

function loadTabContent(e) {
    var t = '<img src="'+theme_url+'/images/preloader.gif" alt=""/>';
    
    
    
    $("ul.tabs li").removeClass("selected"); 
    $(".tabContent").hide();
    console.log(e);
    if("profile" === e)
      {
        $("#profileTab").addClass("selected");
        $("#profileTabCont").show();
        /*casinoJsonServer.getProfilePage({
        success: function(e) {
            $("#profileTabCont").html(e)
        }
    })*/}
    else if("inpay" === e){
        console.log($("#inpayTabCont"));
        $("#inpayTab").addClass("selected");
        $("#inpayTabCont").show();
      }
    else if("outpay" === e){
        $("#outpayTab").addClass("selected"); 
        $("#outpayTabCont").show(); 
        }
    else if("points" === e){
        $("#pointsTab").addClass("selected"); 
        $("#pointsTabCont").show(); 
        }    
}