<h2>{$lang['exchange_title']}</h2>
<div class="userForm">
            <form action="?action=exchange" method='post'>

                <div class="label grid_1" style="margin-top: 0px;">{$lang['exchange_sum']}:</div>
                <div class="grid_3" style="margin-top: 12px;">
                    <span>{$point_pay} </span>
                </div>
                <div class="clear"></div>
                
                <div class="label grid_1">{$lang['exchange_cours']}:</div>
                <div class="grid_3">
                    <span class="link">{$point_cours}</span>
                </div>
                <div class="clear"></div>
                
                <div class="label grid_2">{$lang['exchange_pounts_ex']}:</div>
                <div class="field grid_3">
                    <input type="text" name="sumpoints" class="dk_f_input" placeholder="0" value="{$point_pay}">
                </div>
                <div class="clear"></div>

                <a tabindex="" onclick="$(this).parents('form').submit()" class="n_btn btn_green" href="#">
                    <span><i class="ref-iconcheckmark"></i>{$lang['button_OK']}</span>
                </a>
            </form>
        </div>