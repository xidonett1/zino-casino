<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="cache-control" content="no-cache"/>
	<title>{$title}</title>
	<meta name="description" content="{$description}"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="{$theme_url}/js/cookies.js"></script>
    <script type="text/javascript" src="{$theme_url}/assets/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$theme_url}/assets/flagstrap/js/jquery.flagstrap.min.js"></script>
    
    <script type="text/javascript" src="{$theme_url}/js/common.js"></script>

	  <link media="all" rel="stylesheet" href="{$theme_url}/assets/bootstrap.min.css" />
    <link media="all" rel="stylesheet" href="{$theme_url}/assets/flagstrap/css/flags.css" />
  
    <link rel="stylesheet" href="{$theme_url}/css/animation.css" />
    <link rel="stylesheet" type="text/css" href="{$theme_url}/css/common.min.css"/>
	
    <!-- FlexSlider-->
    <script type="text/javascript" src="{$theme_url}/js/TweenMax.min.js"></script>
	  <script>
      var theme_url='{$theme_url}';
    </script>
</head>
<body>
<div id="visibleOverlay"></div>

<div id="overl"></div>
<div id="loader">
    loading...    <div id="spinningSquaresG">
        <div id="spinningSquaresG_1" class="spinningSquaresG"></div>
        <div id="spinningSquaresG_2" class="spinningSquaresG"></div>
        <div id="spinningSquaresG_3" class="spinningSquaresG"></div>
        <div id="spinningSquaresG_4" class="spinningSquaresG"></div>
        <div id="spinningSquaresG_5" class="spinningSquaresG"></div>
        <div id="spinningSquaresG_6" class="spinningSquaresG"></div>
        <div id="spinningSquaresG_7" class="spinningSquaresG"></div>
        <div id="spinningSquaresG_8" class="spinningSquaresG"></div>
    </div>
</div>

{if $user_id}
<div class="attentionPopup1" id="welcomePopup" style="display: none;">
	<div class="ref_noise">
		<a onclick="hideSystemPopup('welcomePopup')" href="javascript:void(0)" class="closeLink">
			<img src="{$theme_url}/images/close.png" class="close">
		</a>
		<ul class="tabs">
			<li onclick="loadTabContent('profile')" id="profileTab" class="selected"><i class="ref-iconuser"></i>{$lang['menu_profile']}</li>
			<li onclick="loadTabContent('inpay')" id="inpayTab" class=""><i class="ref-iconwallet"></i>{$lang['menu_enter']}</li>
			<li onclick="loadTabContent('outpay')" id="outpayTab" class=""><i class="ref-icongift3"></i>{$lang['menu_out']}</li>
      <li onclick="loadTabContent('points')" id="pointsTab" class=""><i class="ref-iconlibrary"></i>{$lang['menu_exchange']}</li>
		</ul>

		<div class="tabContent" id="profileTabCont" style="display: block;"><div class="verifyprofile staticPage">
        
        <h2>{$lang['popup_PROFILE_DATA']}</h2>

        <div class="userForm">
            <form method="post" id="profileForm" action="/profile?action=save">
                                <input type="submit" style="display: none" value="{$lang['button_SAVE']}" id="profileFormSubmit">

                <div class="label grid_2">{$lang['popup_login']}:</div>
                <div class="field grid_3">
                    <input type="text" value="{$user_info['login']}" name="name" disabled="disabled">
                    <div class="dnd-error ref_notify"></div>
                </div>
                <div class="clear"></div>

                <div class="label grid_2">{$lang['popup_email']}:</div>
                <div class="field grid_3">
                    <input type="text" value="{$user_info['email']}" name="surname" disabled="disabled">
                    <div class="dnd-error ref_notify"></div>
                </div>
                <div class="clear"></div>
                <div class="label grid_2">{$lang['popup_qiwi']}:</div>
                <div class="field grid_3">
                    <input type="text" value="{$user_info['qiwi']}" name="qiwi" >
                    <div class="dnd-error ref_notify"></div>
                </div>
                <div class="clear"></div>
                <div class="label grid_2">{$lang['popup_wmr']}:</div>
                <div class="field grid_3">
                    <input type="text" value="{$user_info['wmr']}" name="wmr" >
                    <div class="dnd-error ref_notify"></div>
                </div>
                
                <div class="clear"></div>
                <a tabindex="" onclick="$(this).parents('form').submit()" class="n_btn btn_green" href="#">
                    <span><i class="ref-iconcheckmark"></i>{$lang['button_SAVE']}</span>
                </a>
            </form>
            <div class="dnd-errorMsg-verify-profile"></div>
        </div>
    
</div>

</div>

		<div style="display: none;" class="ref_m_deposit tabContent" id="inpayTabCont">
      <div class="staticPage paymentMethods ajax">

      {foreach $pay_mods as $mod}
        {include file="inpay/$mod.tpl"}
      {/foreach}

      </div>
    </div>

		<div style="display: none;" class="tabContent" id="outpayTabCont">
			{include file="out.tpl"}
		</div>
    
    <div style="display: none;" class="tabContent" id="pointsTabCont">
			{include file="points.tpl"}
		</div>
	</div>
</div>
{/if}

    <div id="index">

        <!--Header Starts -->
<div id="header" >
   <div class="strip">
      <!--Logo and Icons-->
      <div class="icons f_left" >
         <a href="javascript:void(0)" class="showMenu black">
             <i class="ref-iconalign-justify"></i>
             <span>{$lang['menu']}</span>
         </a>
      </div>
    <div style="text-align: left;">  	  
    <div id="langs">
      
    </div>
    </div>	  

      <!--/Logo and Icons-->
   </div>
</div>
<!-- /Header -->

{if $login} 

<div class="userTopMenu slideout-menu">
    <table>
        <tbody><tr>
            <td onclick="showPaymentMethods()">           
                    <span class="caption">{$lang['auth_balance']}</span><br>
					<a href="#">
                    <span id="real_amount2" class="value">{$balance} {$lang['auth_CUR']}</span>
					</a>
            </td>
            <td onclick="showBonusCodesPopup()">
                            <span class="caption">{$lang['auth_wager']}</span><br>
                            <span class="value" id="bonus_amount2">{$user_info['wager']} {$lang['auth_bet']}</span>
                        <div id="bonusinfo">
                                                                                                        </div>
            </td>


            <td onclick="showCpPopup()">
                <span class="caption">{$lang['auth_bonus']}</span><br>
				<a href="/enter?action=setbonus">
                <span class="value" id="cp_balance">{$lang['auth_set_bonus']}</span>
				</a>
            </td>
        </tr>
    </tbody></table>
</div>

{/if}

<div id="registration-login-modal" class="reveal-modal ref_modal r-ani">


<div id="ref_modal_cont">
<div class="ref_noise">
    
        <div id="rlm-slides">
            <div class="rlm-slide">
                <p class="rlm-title">{$lang['popup_REGISTER']}</p>
                <form id="signupForm" class="dnd-signup" action="/registration?action=save" method="post" >
                    <input name="checked" value="0" type="hidden">
                    <input name="gift" value="0" type="hidden">
                    <div class="registration-actions-container">
                        <div class="registration-container">
                            <label for="email">{$lang['popup_login']}:*</label>
                            <input id="ulogin" name="ulogin" placeholder="{$lang['popup_login']}" type="text">
                        </div>
                        
                        <div class="registration-container">
                            <label for="pass">{$lang['popup_pass']}:*</label>
                            <input id="pass" name="pass" placeholder="{$lang['popup_pass']}" type="password">
                        </div>
                        
                        <div class="registration-container">
                            <label for="repass">{$lang['popup_re_pass']}:*</label>
                            <input id="repass" name="repass" placeholder="{$lang['popup_re_pass']}" type="password">
                        </div>
                        
                        <div class="registration-container">
                            <label for="email">{$lang['popup_email']}:*</label>
                            <input id="email" name="email" placeholder="{$lang['popup_email']}" type="email">
                        </div>

                        <div class="agreement">
                            <div class="agr_1">
                                <input id="agr_1" name="yes" value="1" type="checkbox">
                                <label for="agr_1">
                                    {$lang['popup_yes_rules']} </label>
                            </div>

                        </div>
                        <div class="mob_clear"></div>
                        <div class="error"></div>
                        <div class="mob_clear"></div>
                    </div>
                </form>
                <a href="javascript:void(0)" class="green-action-button"  onclick="$('#signupForm').submit()" >{$lang['button_REGISTER']}</a>
                <div class="clear"></div>
                <a href="javascript:void(0)" title="{$lang['popup_ENTER']}" class="rlm-login-btn grey-button" onclick="showLoginPopup(1)" >{$lang['popup_ENTER']}</a>
            </div>
            <div class="rlm-slide">
                <p class="rlm-title">{$lang['popup_ENTER']}</p>
                <form id="rlm-auth" name="auth" method="post" action="/login">
                    <div class="login-actions-container">
                        <div class="login-container">
                            <input name="mode" value="signin" type="hidden">
                            
                            <div class="auth-form">
                                <div class="af-inputs">
                                    <label for="rlm-login-function">{$lang['popup_login']}:*</label>									
                                    <input name="user" placeholder="{$lang['popup_login']}" type="text">
									
                                    <label for="rlm-password-function">{$lang['popup_pass']}:*</label>
                                    <input name="pass" placeholder="{$lang['popup_pass']}" type="password">
                                </div>
                            </div>
                        </div>

                        <div class="mob_clear"></div>
                        <div id="ref_login-error" class="error"></div>
                        <div class="mob_clear"></div>

                        <div class="clear"></div>
                    </div>
                </form>

                <a class="green-action-button" onclick="$('#rlm-auth').submit()" >{$lang['popup_ENTER']}</a>
                <div class="clear"></div>
                <a class="forgotten-login grey-button" href="javascript:void(0)" onclick="showLoginPopup(2)" >{$lang['button_RECOVERY']}</a>
                <div class="clear"></div>
                <a href="javascript:void(0)" title="{$lang['popup_REGISTER']}" class="rlm-register-btn grey-button" onclick="showGiftPopup()" >{$lang['popup_REGISTER']}</a>
            </div>
            <div class="rlm-slide">
                <p class="rlm-title">{$lang['button_RECOVERY']}</p>
                <form id="rlm-restore" method="post" action="/reminder?action=send">
                    <div class="login-actions-container">
                        <div class="login-container">
                            <input name="mode" value="signin" type="hidden">
                            
                            <div class="restore-form">
                                <div class="af-inputs">
                                    <label for="rlm-login-function">{$lang['popup_login']}:*</label>									
                                    <input name="ulogin" placeholder="{$lang['popup_login']}" type="text">
									
                                    <label for="rlm-password-function">{$lang['popup_email']}:*</label>
                                    <input name="email" placeholder="{$lang['popup_email']}" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="mob_clear"></div>
                        <div id="ref_login-error" class="error"></div>
                        <div class="mob_clear"></div>

                        <div class="clear"></div>
                    </div>
                </form>
                <a class="green-action-button" onclick="$('#rlm-restore').submit()">{$lang['button_RECOVERY']}</a>
                <div class="clear"></div>
                <a href="javascript:void(0)" title="{$lang['popup_ENTER']}" class="rlm-login-btn grey-button" onclick="showLoginPopup(1)">{$lang['popup_ENTER']}</a>
                <div class="clear"></div>
                <a href="javascript:void(0)" title="{$lang['popup_REGISTER']}" class="rlm-register-btn grey-button" onclick="showGiftPopup()">{$lang['popup_REGISTER']}</a>
            </div>
        </div>

        <a title="" class="close-reveal-modal">×</a>

    

</div>
</div>
</div>

<div class="reveal-modal ref_modal r-ani" id="gift-modal">
    <div class="ref_noise">
        <a class="close-reveal-modal">×</a>

        <h1 style="z-index: 0; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">{$lang['popup_REGISTER']}</h1>

        <div class="choiceHint" style="z-index: 0; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">{$lang['popup_Welcome_gift_choice']}</div>

        <div class="offers">
                            

        <div onclick="setGift(1,{$user_id})" class="wo offer offerFS" style="z-index: 0; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">

        <div class="offerImg offerFS ru" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"></div>
        <div class="offerBack" style="opacity: 0.7; z-index: 0; transform: matrix(0.95, 0, 0, 0.95, 0, 0);">

        </div>
        <div class="offerName">{$lang['popup_gift_REG']}</div>
        <div class="clear"></div>


        <div class="tooltip bounceIn woop3 r-a">
            <div class="info">
                <p>{$lang['popup_gift_reg_txt']}</p>

                <div class="arrow"></div>
            </div>
        </div>
    </div>


                            

        <div onclick="setGift(2,{$user_id})" class="wo offer offerDB" style="z-index: 0; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">

        <div class="offerImg offerDB ru" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"></div>
        <div class="offerBack" style="opacity: 0.7; z-index: 0; transform: matrix(0.95, 0, 0, 0.95, 0, 0);">

        </div>
        <div class="offerName">{$lang['popup_gift_DAY']}</div>
        <div class="clear"></div>


        <div class="tooltip bounceIn woop3 r-a">
            <div class="info">
                <p>{$lang['popup_gift_day_txt']}</p>

                <div class="arrow"></div>
            </div>
        </div>
    </div>


        <div onclick="setGift(3,{$user_id})" class="wo offer offerCP" style="z-index: 0; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">

        <div class="offerImg offerCP ru" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"></div>
        <div class="offerBack" style="opacity: 0.7; z-index: 0; transform: matrix(0.95, 0, 0, 0.95, 0, 0);">

        </div>
        <div class="offerName">{$lang['popup_gift_PAY']}</div>
        <div class="clear"></div>


        <div class="tooltip bounceIn woop3 r-a">
            <div class="info">
                <p>{$lang['popup_gift_pay_txt']}</p>

                <div class="arrow"></div>
            </div>
        </div>
    </div>


                    </div>
    </div>
</div>  

<div class="clear"></div>

			{if $messages}
              {include file='message.tpl'}
            {/if}

            {if isset($content_templ) && $content_templ}
              {include file=$content_templ}
            {/if}

            <!--Footer Starts -->
<div id="footer">
    <div class="strip">
    <div id="license">
        <a href="#" >
            <img src="{$theme_url}/images/im18.png" alt="" border="0" width="32px" height="32px"/>
        </a>
        <a href="#" >
            <img src="{$theme_url}/images/license.png" alt="" border="0" width="105px" height="43px"/>
        </a>
    </div>
    </div>
</div>
<!-- /Footer -->

    </div>
<!-- Left Side Slide Area Starts -->
<div id="menu">

<div class="clear"></div>

<ul id="leftmenu">
{if !$login} 
    <li>
        <a href="javascript:void(0)" onclick="showGiftPopup();">
            <span><i class="ref-iconuser-plus"></i></span>
            {$lang['button_REGISTER']}</a>
    </li>
    <li>
        <a onclick="showLoginPopup(1)" href="javascript:void(0)">
            <span><i class="ref-iconuser-check"></i></span>
            {$lang['button_ENTER']}</a>
    </li>
{/if}    
    <li>
      <a href="/" >
      <span><i class="ref-icondice"></i></span>{$lang['button_REAL']}
      </a>
	</li>
        
    <li>
      <a href="/tournament">
      <span><i class="ref-iconstudy"></i></span>{$lang['menu_TOURNAMENT']}</a>
    </li>
    
{if $login}			
	<li>
      <a href="/" onclick="showProfilePopup();return false;">
      <span><i class="ref-iconuser"></i></span>{$lang['button_PROFILE']}
      </a>
    </li>
    <li>
      <a onclick="showWelcomePopup('points');" href="javascript:void(0)" >
      <span><i class="ref-iconlibrary"></i></span>{$lang['menu_exchange']}
      </a>
    </li>	
    <li>
      <a href="/logout" >
      <span><i class="ref-iconexit"></i></span>{$lang['button_EXIT']}
      </a>
	</li>			
{/if}        
</ul>

</div>

<script type="text/javascript">

    $(document).ready(function () {
        {if ($status==5 ||$status==6) && !$user_info['gift']}
          showGiftPopup();
          $("#gift-modal .close-reveal-modal").on("click", function(){
            return false;
          })
        {/if}
        hideLoaderSlow();
        
        $('#langs').flagStrap({
        countries: {
            {foreach $available_langs as $language}
            "{$language}":"{$language}",
            {/foreach}
        },
        selectedCountry: "{$cur_lang}",
        buttonSize: "btn-xs",
        buttonType: "btn-info",
        labelMargin: "5px",
        scrollable: false,
        scrollableHeight: "300px"
    });

$("#langs").on('click','a',function(){
  setCookie ('lang',$(this).attr('data-val'), Math.round(new Date().getTime() / 1000)+604800, '/');
  location.href="/";
});    



        $('#leftmenu a').click(function() {
                closeMenu();
        });
        $('.showMenu').click(function() {
            if ($("#index").hasClass("menuOpen")) {
                closeMenu();
            }
            else {
                showMenu();
            }
        });

        $('#visibleOverlay').click(function() {
            if ($("#index").hasClass("menuOpen")) {
                closeMenu();
            }

        });
        TweenMax.to($("#menu"), 0.1, {
            delay: 0,
            x:-400,
            opacity:0,
            ease: Power3.easeIn,
        });


    });
    var menuAni =  TweenMax.to($("#index"), 0.8, {
        delay: 0,
        className:"+=menuOpen",
        paused:true,
        ease: Power3.easeIn,
    });
    var menuShow =  TweenMax.to($("#menu"), 0.3, {
        delay: 0,
        x:0,
        paused:true,
        opacity:1,
        ease: Power3.easeIn,
    });
    function showMenu() {
        menuShow.play();
        menuAni.play();
        $("#visibleOverlay").show();
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
    function closeMenu() {
        menuAni.reverse();
        menuShow.reverse();
        $("#visibleOverlay").hide();
    }
</script>

</body>
</html>
