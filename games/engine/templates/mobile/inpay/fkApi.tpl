{*<script src="http://yandex.st/jquery/1.6.0/jquery.min.js"></script>*}

<style>
.block .row:nth-child(even) .title{
  background: #1d384b;
}

.block .row:nth-child(even){
  background: #eee;
}

.title{
  float: left;
  width: 120px;
}

.form-control{
  width: 280px;
  margin-left: 136px;
}
</style>

<script type="text/javascript">
var orgShow = $.fn.show;
$.fn.show = function()
{
    $(this).trigger( 'myOnShowEvent' );
    orgShow.apply( this, arguments );
    return this;
}

    var min = {$config['enter_from']};
    var max = {$config['enter_to']};
    var hash = '';
    var url = '/engine/ajax/fkApi.php';
    function calculate() {
        var re = /[^0-9\.]/gi;

        var desc = $('#desc').val();
        var sum = $('#sum').val();

        if (re.test(sum)) {
            sum = sum.replace(re, '');
            $('#oa').val(sum);
        }
        if (sum < min) {
            $('#error').html('Сумма должна быть больше '+min).show();
            $('#submit').attr("disabled", "disabled");
            return false;
        } else if (sum > max) {
            $('#error').html('Сумма должна быть меньше '+max).show();
            $('#submit').attr("disabled", "disabled");
            return false;
        }else {
            $('#error').html('').hide();
        }
        if (desc.length < 1) {
            $('#error').html('Необходимо ввести номер заявки').show();
            return false;
        }
        $.get(url+'?action=prepare_once&l='+desc+'&oa='+sum, function(data) {
            re_anwer = /<hash>([0-9a-z]+)<\/hash>/gi;
            hash = re_anwer.exec(data)[1];
            $('#s').val(hash);
            $('#submit').removeAttr("disabled");

        });
    }

    function fetch_form() {
          $("#payment_form_sorce").empty();
          if($("#cur :selected").val()==0)
            {
            $("#payment_form_sorce").html("Нужно выбрать вид платежа");
            return false;
            }
          var senddata = "action=gen_form&cur="+$('#cur').val()+"&amount="+$('#sum').val()+"&from=merchant&"+$('#from_cur_input').attr('name')+"="+$('#from_cur_input').val();
          $("#fkApi .preloader").show();
          $.post("/engine/ajax/fkApi.php",  senddata ,
            function(data) {
                $("#fkApi .preloader").hide();
                data = base64_decode(data);
                $("#payment_form_sorce").html(data);
                //$("#submit").remove();
            }
        );  
        
        return false;
    }

    function load_more(id) {
        if (id == 63) $('#from_cur').html('<div class="title">Ваш # в QIWI (79xxx234567)</div><div class="form-control"><input type="text" name="qiwi_account" id="from_cur_input" value="{$user_info['qiwi']}"></div>').show();
        else if (id == 63) $('#from_cur').html('<div class="title">Номер счета cash4wm.ru (WM123456789)</div><div class="form-control"><input type="text" name="cashru"  id="from_cur_input"></div>').show();
        else if (id == 82 || id == 83 || id == 84) $('#from_cur').html('<div class="title">Номер телефона (9xxx234567)</div><div class="form-control"><input type="text" name="phone"  id="from_cur_input"></div>').show();
        else if (id == 79 || id == 80 || id == 81) $('#from_cur').html('<div class="title">Счёт в банке или номер карты<br><br>Фамилия Имя Отчество</div><div class="form-control"><input type="text" name="bank_account"  id="from_cur_input"> <br><br><input type="text" name="bank_user"></div>').show();
        else $('#from_cur').html('').hide();
    }

    function base64_decode (data) {
        var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            dec = "",
            tmp_arr = [];

        if (!data) {
            return data;
        }

        data += '';

        do { // unpack four hexets into three octets using index points in b64
            h1 = b64.indexOf(data.charAt(i++));
            h2 = b64.indexOf(data.charAt(i++));
            h3 = b64.indexOf(data.charAt(i++));
            h4 = b64.indexOf(data.charAt(i++));

            bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

            o1 = bits >> 16 & 0xff;
            o2 = bits >> 8 & 0xff;
            o3 = bits & 0xff;

            if (h3 == 64) {
                tmp_arr[ac++] = String.fromCharCode(o1);
            } else if (h4 == 64) {
                tmp_arr[ac++] = String.fromCharCode(o1, o2);
            } else {
                tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
            }
        } while (i < data.length);

        dec = tmp_arr.join('');
        dec = this.utf8_decode(dec);

        return dec;
    }

    function utf8_decode (str_data) {
        var tmp_arr = [],
            i = 0,
            ac = 0,
            c1 = 0,
            c2 = 0,
            c3 = 0;

        str_data += '';

        while (i < str_data.length) {
            c1 = str_data.charCodeAt(i);
            if (c1 < 128) {
                tmp_arr[ac++] = String.fromCharCode(c1);
                i++;
            } else if (c1 > 191 && c1 < 224) {
                c2 = str_data.charCodeAt(i + 1);
                tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = str_data.charCodeAt(i + 1);
                c3 = str_data.charCodeAt(i + 2);
                tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }

        return tmp_arr.join('');
    }
    
    $(document).ready(function(){
    $('#welcomePopup').on( "myOnShowEvent", function()
      {
      if ($("#cur option").length==0)
        {
        
        $("#fkApi_inpay").find(".preloader").css('display','block');
        $.get('/engine/ajax/fkApi.php',function(data){
          $("#fkApi_inpay").find(".preloader").css('display','none');
          console.log(data);

        for (key in data.curs){
          $("#cur").append( $('<option value="'+data.curs[key].id+'">'+data.curs[key].name+'</option>'));
        };
        $("div.cur").show();
        $('#fkApi_submit').removeAttr("disabled");
      },'json');
        }
      });

    });    
 
    
</script>
<h2>Оплата через <a href="http://wwww.free-kassa.ru">free-kassa.ru</a></h2>

{if $check_order}

    <script type="text/javascript">
        var ret_url = '{$success_url}';
        var i=15;
        $(document).ready(function(){
            setInterval(function() { check_order()},15000);
            setInterval(function() { countdown()},1000);
        });
        function check_order() {
            $('#check_result').html('&nbsp;');
            $.get('{$current_url}?check=1&order_id='+{$_GET['order_id']}, function(data) {
                if (data == '1') window.location.href = ret_url;
                else $('#check_result').html(data);
            });
            i = 15;
        }

        function countdown() {
            if (i==1) i=1;
            $('#countdown').html(i-1);
            i = i-1;
        }
        
    </script>
    <div align="center">
        <h2 style="color: red">НЕ ЗАКРЫВАЙТЕ ЭТУ СТРАНИЦУ!</h2>
        Проверяется оплата счета #{$_GET['order_id']}<br>
        <img src="/images/loading.gif">
        <div id="countdown">
        </div>
        <div id="check_result">
            Подождите, идет выставление счета...
        </div>
        <br><br>

        {if (isset($_GET['open']) && $_GET['open'] == '63')}
            <h2> <a href="http://w.qiwi.ru" target="_blank">ПЕРЕЙТИ НА САЙТ "QIWI кошелек" для оплаты</a></h2>
        {/if}
        <br><br>

    </div>
{else}
<div class="userForm" id="fkApi_inpay">
    <div id="error" class="message error-note" style="display: none"></div>
    <form method=GET action="http://www.free-kassa.ru/merchant/cash.php" onsubmit="fetch_form(); return false;" id="fkApi">
    <input type="hidden" name="m" value="{$config[fkApi_merchant_id]}">
    <input type="hidden" name="s" id="s" value="0">
		
        <div class="label grid_2">{$lang['enter_SUM_ENTER']}:</div>
        
        <div class="field grid_3"><input type="text" name="oa" id="sum" id="oa" value="{$config['enter_from']}" ></div>
            
        <div class="cur" style="display: none">
          <div class="label grid_2">Валюта платежа*:</div>
          <div class="field grid_3">
                    <select name="cur" id="cur" onchange="load_more(this.value)">
                          {foreach $curs as $item}
                            <option value="{$item['id']}">{$item['name']}</option>
                          {/foreach}
                        </select>
          </div>
            
            <div class="field" id="from_cur"></div>
            
        </div>

        

        <div id="dialog_profile_error_data" class="dialog_error" style="display: block;"></div>
        <a tabindex="" onclick="$(this).parents('form').submit()" class="n_btn btn_green" id="fkApi_submit" href="#">
            <span><i class="ref-iconcheckmark"></i>{$lang['button_PAY']}</span>
        </a>
        <div class="preloader">&nbsp;</div>

    </form>
    </div>
{/if}
<div id="payment_form_sorce">
</div>
