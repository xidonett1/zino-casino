{if !isset($system)}
    <h2>{$lang['enter_PIN_CODE_title']}</h2>
        <div class="userForm">
            <form id="pin" method="POST" action="/engine/dir/pay/pin/pin.php">

                <div class="label grid_2">{$lang['enter_PIN_CODE']}:</div>
                <div class="field grid_3">
                    <input type="text" name="pin_code" placeholder="{$lang['enter_pin_input']}">
                    <div class="dnd-error ref_notify"></div>
                </div>
                <div class="clear"></div>

                <a tabindex="" onclick="$(this).parents('form').submit()" class="n_btn btn_green" href="#">
                    <span><i class="ref-iconcheckmark"></i>{$lang['button_OK']}</span>
                </a>
            </form>
        </div>

{*<form id="pin" method="POST" action="/engine/dir/pay/pin/pin.php">
									<fieldset>
										<div class="block">
											<div class="title">
												<h2>{$lang['enter_PIN_CODE']}</h2>
											</div>
											<div class="form-control">
												<input type="text" name="pin_code" placeholder="{$lang['enter_pin_input']}" />
												<input type="submit" value="{$lang['button_OK']}" class="btn-blue" />
											</div>
										</div>
									</fieldset>
</form>
*}
{/if}