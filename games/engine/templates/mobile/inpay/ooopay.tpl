{if isset($m)}		

<div class="text news-content">
<h2><center>{$lang['enter_confirm']} <b>{$oa}</b> {$lang['enter_cur']}</center></h2>
</div>

<div class="divider35px"></div>

<form id="ooopay" method="get"  action="https://www.ooopay.org/page/payments/">	
        <input type="hidden" name="m" value="{$m}"/>
        <input type="hidden" name="amount" value="{$oa}" />
        {*<input type="hidden" name="s" value="{$s}" />*}
        <input type="hidden" name="order_id" value="{$o}" />
        <input type="hidden" name="lang" value="ru" />
        <input type="hidden" name="currency" value="USD" />
</form>

<a class="reg-btn green-gradient ts-green" href="#" onClick="document.getElementById('ooopay').submit(); return false;"><i class="icon reg-big"></i>{$lang['button_PAY']}</a>

{else if !isset($system)}

<h2>{$lang['enter_ooopay_title']}</h2>

<form id= "ooopay" method="POST" action="/{if isset($ge)}{$ge}{else}enter{/if}?action=send&system=ooopay">
<div id="profile-page">
                <div class="profile-form">

<div class="border-grey email-data">
<div class="left">
<div class="field email">
<label for="profile-email">{$lang['enter_SUM_ENTER']}</label>
<input type="text" name="money" value="{$config['enter_from']}"/>
</div>
<div class="button"><a href="#" onClick="document.getElementById('ooopay').submit(); return false;"><button type="button">{$lang['button_PAY']}</button></a></div>
</div>
</div>
				
                </div>
</div>
</form>

{/if}		