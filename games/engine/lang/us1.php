<?php

// GOLDSVET 5.1 - максимальная

// Игровая часть 30.10.2016

// для не авторизованных

$lang['err_no_access']='you must be logged in to access this page';

// registration

$lang['err_all_input']='you need to fill out all the fields of this form';
$lang['err_pass2']='password does not match the confirm password';
$lang['err_mail']='enter your e-mail address';
$lang['err_mail_preg_match']='enter a valid e-mail address';
$lang['err_in_login']='you are already logged in';
$lang['err_login_3_20']='username must be 3-20 characters long';
$lang['err_rules']='you need to accept the rules of the system';
$lang['err_mail_30']='e-mail must be no longer than 30 characters';
$lang['err_wmr_13']='purse number consists of 13 characters';
$lang['err_wmr_r']='purse number starts with the letter R';
$lang['err_captcha']='security code is incorrect';
$lang['err_login_duble']='enter another username';
$lang['err_wmr_duble']='enter another purse number';
$lang['err_mail_duble']='enter another e-mail';
$lang['err_no_reg_ip']='your IP address is already in use';

// login

$lang['err_login_pass']='incorrect username or password';
$lang['err_no_login']='no username';
$lang['err_no_pass']='no password';
$lang['err_no_login_no_pass']='no username and password';
$lang['err_auth']='user is already logged in';
$lang['err_block']='your username is blocked';
$lang['err_no_user']='user is not found';
$lang['err_no_real']='The real-money games will be available <br />after signing up';

// reminder

$lang['err_in_auth']='you are already logged in';
$lang['err_no_mail']='e-mail is not found';
$lang['err_no_send']='failed email delivery';
$lang['err_new_pass']='new password has been sent to your e-mail';

// contacts

$lang['err_no_subject']='no subject';
$lang['err_in_message']='enter the message';
$lang['err_maile_server']='e-mail server error';
$lang['err_send']='your message has been sent';

// enter

$lang['err_max_bonus']='you have maximum bonus amount';
$lang['err_bonus']='you get a bonus of <b>{bonus}</b> credits';
$lang['err_off_bonus']='bonuses are not available now';
$lang['err_no_bonus']='you have already got your today\'s bonus';
$lang['err_no_wager']='Enable wager to get a bonus';
$lang['err_min_max_enter']='you can make a deposit from <b>%min</b> to <b>%max</b> at a time';
$lang['err_no_enter']='deposits are temporarily unavailable';
$lang['err_no_bonus_ip']='your IP have already got a bonus';

// news

$lang['err_no_news']='the item is not found, it may be removed';

// out

$lang['err_no_pay']='you had no deposits this month, the withdrawal is impossible';
$lang['err_no_weger']='you have not met wagering requirements for the amount of bets';
$lang['err_no_ps']='fill out the payment details in your account';
$lang['err_activ_email']='activate your e-mail address';
$lang['err_activ_phone']='activate your phone number';
$lang['err_no_out']='withdrawal is temporarily unavailable';
$lang['err_min_max_out']='enter a valid amount [ <b>{min}</b> - <b>{max}</b> ]';
$lang['err_no_access_ps']='the selected payment system is not available now';
$lang['err_no_money']='not enough money in your account';
$lang['err_in_ps']='choose a payment system';
$lang['err_yes_out']='your application is accepted and will be processed soon';
$lang['err_no_send_out']='failed to send an application for withdrawal';
$lang['err_wait_out']='you already have an unprocessed application for withdrawal';

// pay

$lang['err_redirect']='you will be redirected to the homepage in 10 seconds';
$lang['err_yes_pin']='your PIN code is processed successfully';
$lang['err_no_pin_search']='PIN code is not found';
$lang['err_no_pin']='incorrect PIN code';
$lang['err_pin_block']='no more PIN code input attempts left, your account is suspended';
$lang['err_pin_stay']='input attempts left';
$lang['pay']['userbal']='player\'s account balance';
$lang['pay']['success2']='deposit made';

// profile

$lang['err_email_activated']='e-mail is activated';
$lang['err_code']='incorrect code';
$lang['err_phone_activated']='phone number is activated';
$lang['err_phone_duble']='enter another phone number';
$lang['err_save']='your data has been successfully saved';
$lang['err_db']='data input error';

// games/index.php

$lang['err_game_block']='account is suspended, contact customer support';
$lang['err_no_access_game']='available for players';
$lang['err_no_start_game']='game start file not found';
$lang['err_no_game']='failed to start the game';

// index.php

$lang['err_404']='page is not found';
$lang['err_activate_yes']='activation successfully completed';
$lang['err_activate_no']='activation error';
$lang['err_ajax']='server error';

// main

$lang['main_LK']='PERSONAL ACCOUNT';
$lang['main_wellcome']='Welcome';
$lang['main_showGames']='Show more games';
$lang['main_jackpot']='Jackpot';
$lang['main_tomain']='Go to the homepage';
$lang['footer_paysys']='Payment methods';
$lang['footer_paysys1']='+ more than <strong>20 ways!</strong>';

// exchange

$lang['err_no_points']='not enough points';
$lang['err_yes_points']='<b>".$paypoints."</b> points were exchanged for <b>$sum</b> руб.';
$lang['err_minimal_points']='there should be more <b>100</b> points';

// games

$lang['games_list']='games list';

// Страница новостей /news

$lang['news_more']='show details';

// Страница партнерской программы /partner

$lang['partner_REF_URL']='YOUR AFFILIATE LINK';
$lang['partner_ref']='partner';
$lang['partner_all_out']='total amount paid';
$lang['partner_to_enter']='received';
$lang['partner_yes_out']='ready for payment';
$lang['partner_no_ref']='you have not invited anyone yet';
$lang['partner_ref_title']='partners invited by you';
$lang['partner_transfer_to_balance']='transfer to the game account';

// Страница статистики /history

$lang['history_WITHDRAWL']='WITHDRAWAL REQUEST';
$lang['history_HISTORY_PAY']='PAYMENT HISTORY';
$lang['history_HISTORY_LOGIN']='ACTIVITY HISTORY';
$lang['history_GAME_LOG']='LATEST GAMES';
$lang['history_date']='date';
$lang['history_order_amout']='order amount';
$lang['history_payout']='payout amount';
$lang['history_inv']='invoice';
$lang['history_PS']='PS';
$lang['history_status']='status';
$lang['history_IP']='IP';
$lang['history_game']='game';
$lang['history_bet']='bet';
$lang['history_win']='gain';
$lang['history_no_requests']='no requests';
$lang['history_no_enter']='no deposits';
$lang['history_no_login']='no authorizations';
$lang['history_no_game']='no game statistics';
$lang['history_statuses'][0]='processing';
$lang['history_statuses'][1]='processing';
$lang['history_statuses'][2]='success';
$lang['history_statuses'][3]='returned';
$lang['history_statuses'][4]='denied';
$lang['history_ps']['wmr']='WebMoney WMR';
$lang['history_ps']['qiwi']='QIWI';

// Страница обмена поинтов /exchange

$lang['exchange_TOTAL_BALANCE_POINTS']='CURRENT POINTS BALANCE';
$lang['exchange_COURSE']='EXCHANGE RATE';
$lang['exchange_POINTS_EX']='GIVE OUT POINTS <BR />FOR EXCHANGE';
$lang['exchange_EX_POINTS']='YOU GET';
$lang['exchange_cur']='RUB.';

// Страница выплаты /out

$lang['out_SUM_IN']='GAIN WITHDRAWAL <BR />AMOUNT';
$lang['out_PS']='PAYMENT SERVICE';
$lang['out_ps_select']='choose a payment service';
$lang['out_QIWI']='QIWI';
$lang['out_WMR']='WebMoney WMR';

// Страница пополнения счета /enter

$lang['enter_PIN_CODE_title']='PIN code';
$lang['enter_PIN_CODE']='PIN CODE<BR />INPUT';
$lang['enter_pin_input']='enter PIN code';
$lang['enter_SUM_ENTER']='DEPOSIT <BR />AMOUNT';
$lang['enter_confirm']='you make a deposit to your game account of';
$lang['enter_cur']='RUB.';

// Страница профиля /profile

$lang['profile_AVATAR']='USERPIC';
$lang['profile_LOGIN']='USERNAME';
$lang['profile_ID']='ID';
$lang['profile_E-MAIL']='E-MAIL';
$lang['profile_CHANGE_PASS']='PASSWORD <BR />CHANGE';
$lang['profile_PASS']='PASSWORD';
$lang['profile_RE_PASS']='CONFIRMATION';
$lang['profile_YOUR_E-MAIL']='YOUR E-MAIL';
$lang['profile_PHONE_QIWI']='PHONE NUMBER<BR />QIWI PURSE';
$lang['profile_WMR']='WEBMONEY PURSE <BR />WMR';
$lang['profile_CODE']='CODE';
$lang['profile_new_pass']='enter a new password';
$lang['profile_new_pass2']='confirm a password';
$lang['profile_wmr_input']='WMR purse number';
$lang['profile_phone_input']='phone number';
$lang['profile_code_input']='activation code';
$lang['profile_email_input']='enter your e-mail';
$lang['profile_use_wager']='GET BONUSES';

// Блок авторизации

$lang['auth_profile']='Profile';
$lang['auth_balance']='Balance';
$lang['auth_wager']='Wager';
$lang['auth_partner']='Partners';
$lang['auth_history']='Statistics';
$lang['auth_cur']='RUB.';
$lang['auth_bet']='bet';
$lang['auth_guest']='Guest';
$lang['auth_exit']='Exit';

// Попап окна popup

$lang['popup_MSG']='MESSAGE';
$lang['popup_PAY']='DEPOSIT';
$lang['popup_OUT']='WITHDRAWAL';
$lang['popup_REGISTER']='SIGN UP';
$lang['popup_login']='Username';
$lang['popup_pass']='Password';
$lang['popup_re_pass']='Confirm password';
$lang['popup_email']='E-mail';
$lang['popup_qiwi']='QIWI';
$lang['popup_wmr']='WMR';
$lang['popup_yes_rules']='I accept the <a href="/rules">rules</a> of a gambling club';
$lang['popup_ENTER']='ENTER THE WEBSITE';
$lang['popup_reminder']='Forgot the password';
$lang['popup_PASS_RECOVERY']='PASSWORD RECOVERY';
$lang['popup_SUPPORT']='CUSTOMR SUPPORT';
$lang['popup_subject']='Subject of the message';
$lang['popup_message']='Text of the message';
$lang['popup_WELCOME_BONUS_CHOICE']='WELCOME BONUS OF YOUR CHOICE';
$lang['popup_gift_register']='sign-up bonus,<br />the most advantageous for the beginners';
$lang['popup_GIFT_REG']='SIGN-UP BONUS';
$lang['popup_gift_reg_txt']='instant deposit';
$lang['popup_GIFT_DAY']='DAILY BONUS';
$lang['popup_gift_day_txt']='every day a new bonus';
$lang['popup_GIFT_PAY']='FIRST DEPOSIT';
$lang['popup_gift_pay_txt']='double up your payment';

// Кнопки button

$lang['button_EXCHANGE']='EXCHANGE';
$lang['button_AUTH']='AUTHORIZATION';
$lang['button_REGISTER']='SIGN UP';
$lang['button_ENTER']='LOG IN';
$lang['button_EXIT']='EXIT';
$lang['button_PROFILE']='PROFILE';
$lang['button_RECOVERY']='RECOVER';
$lang['button_OK']='OK';
$lang['button_SAVE']='SAVE';
$lang['button_CONFIRM']='ACCEPT';
$lang['button_SEND']='SEND';
$lang['button_OUT']='WITHDRAW';
$lang['button_PAY']='DEPOSIT';
$lang['button_COPY']='COPY';
$lang['button_REAL']='PLAY';
$lang['button_DEMO']='DEMO';

// Уголок прямоугольный

$lang['corner_OTHER_GAME']='OTHER GAMES';

// Блоки с права block и слайдер и соц. кнопки

$lang['block_WIN_NOW']='WHO IS WINNING NOW';
$lang['block_WIN_BEST']='THE BEST GAINS';
$lang['block_CONTACT']='CONTACT INFO';
$lang['block_phone']='phone';
$lang['block_email']='e-mail';
$lang['block_icq']='icq';
$lang['block_support']='support';
$lang['block_write_to_us']='contact us';
$lang['block_RATING']='RATING';
$lang['block_Rating_stars']='Rating';
$lang['block_gamers']='player';
$lang['block_bet']='bet';

// Меню с низу menu

$lang['menu_ABOUT']='ABOUT US';
$lang['menu_guaranty']='Guarantees';
$lang['menu_privacy']='Privacy policy';
$lang['menu_bonuses']='Bonus policy';
$lang['menu_paymethods']='Payment services';
$lang['menu_license']='License';
$lang['menu_conditions']='Agreement';
$lang['menu_YOUR_MENU']='YOUR MENU';
$lang['menu_profile']='Profile';
$lang['menu_history']='Statistics';
$lang['menu_out']='Withdraw';
$lang['menu_enter']='Deposit';
$lang['menu_partner']='For partners';
$lang['menu_set_bonus']='Get bonus';
$lang['menu_OPTIONAL']='ADDITIONAL';
$lang['menu_faq']='Help';
$lang['menu_news']='News';
$lang['menu_jp']='Jackpot';
$lang['menu_return']='Cashback';

//платежки

$lang['pay']['success']='payment is completed';
$lang['pay']['fail']='payment is not completed';
$lang['pay']['not_found']='payment is not found';
$lang['pay']['fail_sign']='digital signature error';
$lang['pay']['error']='payment ended with error';



////////////////////////////////////////////////////////////////////////////
/////////////////////////////// АДМИН ПАНЕЛЬ ///////////////////////////////
////////////////////////////////////////////////////////////////////////////



// админ панель - Шапка

$lang['admin_head_hello']='Hi';
$lang['admin_head_ip']='IP';
$lang['admin_head_group']='Your group';

// админ панель - группы пользователей

$lang['user_group'][1]='Director';
$lang['user_group'][2]='Dealer';
$lang['user_group'][3]='Manager';
$lang['user_group'][4]='Cashier';
$lang['user_group'][5]='Player';

// админ панель - главное меню

$lang['adminmenu']['users']='Gambling accounts';
$lang['adminmenu']['jp']='Jackpot';
$lang['adminmenu']['game']='Games settings';
$lang['adminmenu']['settings']='System settings';
$lang['adminmenu']['report']='Reports';
$lang['adminmenu']['game_log']='Game logs';
$lang['adminmenu']['pin']='PIN code';
$lang['adminmenu']['email']='E-mail pattern';
$lang['adminmenu']['sms']='Message pattern';
$lang['adminmenu']['mailing']='Messaging';
$lang['adminmenu']['notify']='Mini-messages';
$lang['adminmenu']['edit']='Payment processing';
$lang['adminmenu']['news']='News';
$lang['adminmenu']['pages']='Pages';
$lang['adminmenu']['articles']='Articles';
$lang['adminmenu']['tournament']='Tournaments';
$lang['adminmenu']['links']='External systems';
$lang['adminmenu']['exit']='Exit';
$lang['adminmenu']['outpay_detail']='Outgoing payment';
$lang['adminmenu']['edit_content']='Edit section';
$lang['adminmenu']['add_page']='Add section';
$lang['adminmenu']['add_article']='Add article';
$lang['adminmenu']['edit_news']='Edit news';

// админ панель - репорт меню

$lang['reportmenu'][1]='General data';
$lang['reportmenu'][2]='Payments';
$lang['reportmenu'][5]='Active players';
$lang['reportmenu'][6]='Players history';
$lang['reportmenu'][7]='Game logging';
$lang['reportmenu'][10]='Refunds';
$lang['reportmenu'][11]='Settings log';
$lang['reportmenu'][13]='Payoffs';
$lang['reportmenu'][14]='Lotto circulations';
$lang['reportmenu'][15]='Lotto bets';

// админ панель - субменю настроек

$lang['settings_group'][1]='General settings';
$lang['settings_group'][2]='Payment and bonuses';
$lang['settings_group'][3]='Return percent';
$lang['settings_group'][4]='Game interface';
$lang['settings_group'][5]='Input and output';
$lang['settings_group'][7]='Bonuses for players';
$lang['settings_group'][8]='Bot statistics';
$lang['settings_group'][9]='Contact info';
$lang['settings_group'][13]='PIN codes';
$lang['settings_group'][14]='SMS settings';
$lang['settings_group'][15]='PS FK';
$lang['settings_group'][11]='FK settings';
$lang['settings_group'][16]='SMTP settings';
$lang['settings_group'][17]='SMTP settings';
$lang['settings_group']['returns']='Refunds for players';
$lang['settings_group']['other_settings']='Different settings';
$lang['settings_group']['gamers_rating']='Players\' rating';

// админ панель - настройки рейтинги

$lang['settings_raiting_title']='Players\' rating';
$lang['settings_raiting_th1']='Level';
$lang['settings_raiting_th2']='Name';
$lang['settings_raiting_th3']='Deposit amount';
$lang['settings_raiting_th4']='Color';
$lang['settings_raiting_th5']='Picture';
$lang['settings_raiting_th6']='Exchange rate';
$lang['settings_raiting_th7']='Action';
$lang['settings_raiting_add']='Add rating';
$lang['settings_raiting_edit']='Edit rating';
$lang['settings_raiting_name']='Name';
$lang['settings_raiting_deposit']='Deposit amount';
$lang['settings_raiting_course']='Exchange rate';
$lang['settings_raiting_color']='Color';
$lang['settings_raiting_pic']='Picture';

// админ панель - настройки

$lang['settings_title']['cas_name']='Casino name';
$lang['settings_title']['url']='URL or IP';
$lang['settings_title']['num']='Amount of data displayed on the page';
$lang['settings_title']['spin_koef']='Number of paid spins per credit';
$lang['settings_title']['win_koef']='Guarantee multiplier, after the end of spin limit';
$lang['settings_title']['payed_spins_fixed']='Fixed spin limit';
$lang['settings_title']['payed_spins_val']='Spin limit';
$lang['settings_title']['bank_type']='Gambling banks working hours';
$lang['settings_title']['spin_bank_perc']='SB percentage';
$lang['settings_title']['bonus_bank_perc']='BB percentage';
$lang['settings_title']['double_bank_perc']='DB percentage';
$lang['settings_title']['online_timeout']='Keep player online when inactive (min)';
$lang['settings_title']['refresh_timeout']='Updating (min)';
$lang['settings_title']['denomination']='Denomination';
$lang['settings_title']['debug']='Record logs';
$lang['settings_title']['ref_perc']='Referral percentage';
$lang['settings_title']['use_ulogin']='Use authorization <b>uLogin.ru</b>';
$lang['settings_title']['ulogin_block']='Authorization buttons <b>uLogin.ru</b>';
$lang['settings_title']['use_captcha']='Use captcha';
$lang['settings_title']['activate_mail']='Ask for e-mail activation';
$lang['settings_title']['activate_phone']='Ask for phone number activation';
$lang['settings_title']['reg_ip_check']='Number of signups from one IP address';
$lang['settings_title']['is_block']='Ban casino';
$lang['settings_title']['block_reason']='The reasons of casino ban';
$lang['settings_title']['game_use_logo']='Use logo when loading games';
$lang['settings_title']['game_use_bg']='Use custom background when loading games';
$lang['settings_title']['game_use_fullscreen']='Show FULL SCREEN button in games';
$lang['settings_title']['game_use_sound']='Show SOUND button in games';
$lang['settings_title']['sms_use_sms']='Inform via SMS';
$lang['settings_title']['sms_https_login']='Username for authorization protocol';
$lang['settings_title']['sms_https_password']='Password for authorization protocol';
$lang['settings_title']['fk_use']='Use PS FREE-KASSA';
$lang['settings_title']['fk_merchant_id']='Shop ID';
$lang['settings_title']['fk_merchant_key']='Secret word 1';
$lang['settings_title']['fk_merchant_key2']='Secret word 2';
$lang['settings_title']['pin_use']='Use PIN code';
$lang['settings_title']['allow_outpay']='Allow withdrawal';
$lang['settings_title']['monthly_outpay']='Do not allow to deduce, without Deposit in the current month';
$lang['settings_title']['allow_wmr']='Allow withdrawal via WMR';
$lang['settings_title']['allow_qiwi']='Allow withdrawal via QIWI';
$lang['settings_title']['outpay_tax_perc']='Withdrawal deduction percentage';
$lang['settings_title']['outpay_tax_sum']='Withdrawal deduction amount';
$lang['settings_title']['minout']='Minimum withdrawal amount';
$lang['settings_title']['maxout']='Maximum withdrawal amount';
$lang['settings_title']['enter_from']='Minimum deposit amount';
$lang['settings_title']['enter_to']='Maximum deposit amount';
$lang['settings_title']['bonus_from']='Minimum bonus';
$lang['settings_title']['bonus_to']='Maximum bonus';
$lang['settings_title']['bonus_daily']='Daily bonus';
$lang['settings_title']['bonus_limit']='Bonus limit - daily';
$lang['settings_title']['bonus_check_ip']='Bonus grant limit by IP';
$lang['settings_title']['dep_bonus_limit']='Bonus limit - deposit';
$lang['settings_title']['fdep_bonus']='Bonus on first deposit';
$lang['settings_title']['fdep_bonus_val']='First deposit bonus, percent';
$lang['settings_title']['dep_bonus_1']='Second deposit bonus, percent';
$lang['settings_title']['dep_bonus_2']='Second deposit bonus, percent';
$lang['settings_title']['dep_bonus_3']='Second deposit bonus, percent';
$lang['settings_title']['reg_bon']='Sign-up bonus';
$lang['settings_title']['reg_bon_ref']='Referral sign up bonus';
$lang['settings_title']['act_bon_ref']='Referral activation bonus';
$lang['settings_title']['act_bon']='E-mail and phone number activation bonus';
$lang['settings_title']['botstat_timeout']='Updating BOT data statistics (ms)';
$lang['settings_title']['botstat_logins']='Usernames to be displayed in statistics';
$lang['settings_title']['botstat_stav']='Possible bets in statistics';
$lang['settings_title']['botstat_win']='Possible gains in statistics';
$lang['settings_title']['contact_phone']='Phone number';
$lang['settings_title']['contact_mail']='E-mail';
$lang['settings_title']['contact_icq']='ICQ';
$lang['settings_title']['adm_email']='System e-mail';
$lang['settings_title']['mail_from']='From';
$lang['settings_title']['mail_reply']='Reply';
$lang['settings_title']['mail_type']='Sending method';
$lang['settings_title']['mail_count']='Messages during session';
$lang['settings_title']['mail_period']='Time limit';
$lang['settings_title']['mail_period_count']='Message limit';
$lang['settings_title']['mail_smtp_host']='SMTP host';
$lang['settings_title']['mail_smtp_port']='SMTP port';
$lang['settings_title']['mail_smtp_auth']='SMTP authorization';
$lang['settings_title']['mail_smtp_user']='SMTP username';
$lang['settings_title']['mail_smtp_pass']='SMTP password';
$lang['settings_title']['returns_min']='Minimum deposit amount';
$lang['settings_title']['returns_max']='Maximum deposit amount';
$lang['settings_title']['returns_percent']='Refund percentage';
$lang['settings_title']['returns_percent_room']='Deduction percentage per bet';
$lang['settings_title']['returns_balance_room']='Refund balance';
$lang['settings_title']['returns_balans_finish']='Maximum refund balance';
$lang['settings_title']['returns_winners_room']='Winner';
$lang['settings_title']['default_lang']='Default language, ru or us';
$lang['settings_title']['bets']='Possible bets';
$lang['settings_title']['use_gamer_raiting']='Players ratig';
$lang['settings_title']['game_block_count']='Number of games in the first set';
$lang['settings_title']['hide_news_date']='Hide the date of the item';
$lang['settings_title']['points_pay']='Points for deposit';
$lang['settings_title']['mail_text_type']='Body of the message';

//форма авторизации

$lang['adm_login_title']='Users management system';
$lang['login']='Username';
$lang['pass']='Password';
$lang['adm_form_enter']='Log in';
$lang['enter_login_pass']='Enter username / password';
$lang['enterCor_login_pass']='Enter valid username / password';
$lang['adm_not_access']='You have no access to this dashboard';
$lang['page_not_access']='You have no access to this page';
$lang['not_auth']='You need to log in';

// админ панель - users

$lang['adm_users_head']='Players and cashiers';
$lang['adm_users_login']='Username';
$lang['adm_users_jp']='Jackpot';
$lang['adm_users_balance']='Account balance';
$lang['adm_users_plus']='Deposit';
$lang['adm_users_minus']='Withdraw';
$lang['adm_users_points']='Points';
$lang['adm_users_status']='Status';
$lang['adm_users_spin']='Spin';
$lang['adm_users_guarantee']='Guarantee';
$lang['adm_users_limit']='Limit';
$lang['adm_users_m_activ']='E';
$lang['adm_users_p_activ']='P';
$lang['adm_users_block']='B';
$lang['adm_users_del']='R';
$lang['adm_users_search']='search';
$lang['adm_users_search_input']='username, ip, e-mail, phone number';
$lang['adm_users_edit_title']='Edit player';
$lang['adm_users_add_title']='Add player or cashier';
$lang['adm_users_add_login']='Username';
$lang['adm_users_add_pass']='Password';
$lang['adm_users_add_mail']='E-mail';
$lang['adm_users_add_phone']='Phone number';
$lang['adm_users_add_wmr']='WebMoney WMR';
$lang['adm_users_add_cashier']='Cashier';
$lang['adm_users_add_cancel']='CANCEL';
$lang['adm_users_edit_head']='Edit balance';
$lang['adm_users_edit_amount']='Amount';
$lang['adm_users_edit_select']='Choose';
$lang['adm_users_edit_cancel']='CANCEL';
$lang['adm_users_del_head']='Are you sure to remove the user?';
$lang['adm_users_del_body']='The user and all the connected information will be removed. Are you sure?';
$lang['adm_users_yes']='Yes';
$lang['adm_users_no']='No';
$lang['adm_users_bots']='Bots';

$lang['adm_users_action'][0]='na';
$lang['adm_users_action'][1]='LK';
$lang['adm_users_action'][2]='game';
$lang['adm_users_action'][3]='suspended';
$lang['adm_users_enter_sum']='Enter amount';

// админ панель - jp

$lang['adm_jp_head_1']='Jackpot denomination';
$lang['adm_jp_real_sum']='Show real Jackpot amount';
$lang['adm_jp_head_2']='Jackpot';
$lang['adm_jp_title']='Deposit to Jackpot';
$lang['adm_jp_amount']='Amount';
$lang['adm_jp_select']='Choose';
$lang['adm_jp_cancel']='CANCEL';
$lang['adm_jp_edit_title']='Edit Jackpot';
$lang['adm_jp_edit_name']='Name';
$lang['adm_jp_edit_win']='Result amount';
$lang['adm_jp_edit_percent']='Percent';
$lang['adm_jp_edit_win_chance']='Prev. result';
$lang['adm_jp_edit_spin']='Spin numbers';
$lang['adm_jp_date']='Date';
$lang['adm_jp_logs']='Logs';
$lang['adm_jp_balance']='Account balance';
$lang['adm_jp_win']='Result amount';
$lang['adm_jp_percent']='Percent';
$lang['adm_jp_win_chance']='Prev. result';
$lang['adm_jp_spin']='Spin number';
$lang['adm_jp_win_player']='Player';
$lang['adm_jp_deposit']='Deposit';
$lang['adm_jp_edit']='Edit';
$lang['adm_jp_head_log']='Jackpot logs';
$lang['adm_jp_log_date']='Date';
$lang['adm_jp_log_login']='Username';
$lang['adm_jp_log_log']='Logs';
$lang['adm_jp_no_records']='No data';
$lang['adm_jp_balanceUp']='a deposit has been made of';
$lang['adm_jp_editmsg']='Edited <b>%jack_name%</b> payment amount <b>%jack_sum%</b>, deduction percentage <b>%jack_percent%</b>';

// админ панель - 

$lang['adm_pages_tableHead_1']='Name';
$lang['adm_pages_tableHead_2']='URL';
$lang['adm_pages_tableHead_3']='Action';
$lang['adm_add_page_title']='Create static page';
$lang['adm_pages_title']='Create and edit pages';
$lang['adm_add_page_tableHead_1']='Link';
$lang['adm_add_page_tableHead_2']='Title';
$lang['adm_add_page_tableHead_3']='Subtitle';
$lang['adm_add_page_tableHead_4']='Keywords';
$lang['adm_add_page_tableHead_5']='Page description';
$lang['adm_edit_content_title']='Create static page';
$lang['adm_edit_content_tableHead_1']='Title';
$lang['adm_edit_content_tableHead_2']='Subtitle';
$lang['adm_edit_content_tableHead_3']='Keywords';
$lang['adm_edit_content_tableHead_4']='Page description';
$lang['adm_pages_msg_1']='Page is not specified';
$lang['adm_pages_msg_2']='Page is removed';

// админ панель - 

$lang['adm_news_title']='Create and edit news';
$lang['adm_news_tableHead_1']='Title';
$lang['adm_news_tableHead_2']='Keywords';
$lang['adm_news_tableHead_3']='Page description';
$lang['adm_news_yes']='Yes';
$lang['adm_news_no']='No';
$lang['adm_news_modalTitle']='Remove an item';
$lang['adm_news_modalBody']='The item will be removed. Are you sure?';

// админ панель - 

$lang['adm_edit_news_title']='Create and edit news';
$lang['adm_edit_news_tableHead_1']='Title';
$lang['adm_edit_news_tableHead_2']='Keywords';
$lang['adm_edit_news_tableHead_3']='Page description';

// админ панель - 

$lang['adm_mailing_title']='Messaging';
$lang['adm_mailing_tableHead_1']='Title';
$lang['adm_mailing_title1']='Messages sent';
$lang['adm_mailing_table1Head_1']='Name';
$lang['adm_mailing_table1Head_2']='Send date';
$lang['adm_mailing_table1Head_3']='Messages in the list';
$lang['adm_mailing_table1Head_4']='Action';
$lang['adm_mailing_noMsg']='Messages have not been sent yet';
$lang['adm_mailing_test']='Test';

// админ панель - 

$lang['adm_sms_phone']='Phone number';
$lang['adm_sms_txt']='Text';
$lang['adm_sms_title1']='Sign up';
$lang['adm_sms_title2']='Deposit';
$lang['adm_sms_title3']='Withdrawal';

// админ панель - 

$lang['adm_email_title']='Title';

// админ панель - 

$lang['adm_email_title1']='Sign up';
$lang['adm_email_title2']='New password';
$lang['adm_email_title4']='Deposit';
$lang['adm_email_title5']='The bill is paid';
$lang['adm_email_title7']='Payment denied';
$lang['adm_email_title8']='Account activation';
$lang['adm_email_title9']='Support';
$lang['adm_email_title10']='Tournament start';
$lang['adm_email_title11']='Tournament end';

// админ панель - 

$lang['adm_pin_title']='PIN code generator';
$lang['adm_pin_tableHead_1']='Amount';
$lang['adm_pin_tableHead_2']='Nominal';
$lang['adm_pin_btn_1']='Generate';
$lang['adm_pin_title1']='PIN codes statistics';
$lang['adm_pin_table1Head_1']='PIN codes generated';
$lang['adm_pin_table1Head_2']='Nominal';
$lang['adm_pin_btn_2']='Unload';
$lang['adm_pin_title2']='PIN codes';
$lang['adm_pin_table2Head_1']='PIN code';
$lang['adm_pin_table2Head_2']='Amount';
$lang['adm_pin_table2Head_3']='Date';
$lang['adm_pin_table2Head_4']='Status';
$lang['adm_pin_noPin']='no data';
$lang['adm_pin_status1']='No';
$lang['adm_pin_status2']='No';
$lang['adm_pin_table3Head_1']='ID';
$lang['adm_pin_table3Head_2']='Password';
$lang['adm_pin_table3Head_3']='PIN code';
$lang['adm_pin_table3Head_4']='Creation time';
$lang['adm_pin_table3Head_5']='Amount';
$lang['adm_pin_table3Head_6']='Status';
$lang['adm_pin_table3Head_7']='Activation time';
$lang['adm_pin_noScratch']='No scratch cards yet';
$lang['adm_pin_status_0']='No';
$lang['adm_pin_status_1']='No';
$lang['adm_pin_status_2']='Player';
$lang['adm_pin_status_3']='Pin';

// админ панель - 

$lang['adm_report_filter']='Filter';
$lang['adm_report_date']='Date';
$lang['adm_report_time']='Time';
$lang['adm_report_KAS']='CASH DEPARTMENT';
$lang['adm_report_INCOME']='INCOME';
$lang['adm_report_receipt']='Receipts';
$lang['adm_report_outcome']='Expenditures';
$lang['adm_report_rest']='Account balance';
$lang['adm_report_income']='Income';
$lang['adm_report_np']='ND';
$lang['adm_report_login']='Username';
$lang['adm_report_ps']='PS';
$lang['adm_report_paysys']='Payment service';
$lang['adm_report_bill']='Account';
$lang['adm_report_sum']='Amount';
$lang['adm_report_status']='Condition';
$lang['adm_report_gamer']='Player';
$lang['adm_report_regtime']='Sign up';
$lang['adm_report_authtime']='Log in';
$lang['adm_report_balance']='Account balance';
$lang['adm_report_bonus']='Bonus';
$lang['adm_report_title']='Name';
$lang['adm_report_data']='Data';
$lang['adm_report_os']='OS';
$lang['adm_report_useragent']='Sign-up useragent';
$lang['adm_report_useragent1']='Log-in useragent';
$lang['adm_report_userip']='Sign-up IP';
$lang['adm_report_action']='Action';
$lang['adm_report_inpay']='deposit';
$lang['adm_report_outpay']='withdrawal';
$lang['adm_report_game']='Game';
$lang['adm_report_bet']='Bet';
$lang['adm_report_win']='Gain';
$lang['adm_report_denom']='Denom.';
$lang['adm_report_gamebalance']='Game balance';
$lang['adm_report_return']='Refund';
$lang['adm_report_setting']='Setting';
$lang['adm_report_old']='Before';
$lang['adm_report_new']='After';
$lang['adm_report_inv']='Invoice';
$lang['adm_report_type']='Type';
$lang['adm_report_lotodraw']='Run';
$lang['adm_report_sumbet']='Bets amount';
$lang['adm_report_sumwin']='Gains amount';
$lang['adm_report_winballs']='Matching balls';
$lang['adm_report_coef']='Index';
$lang['adm_report_no_data']='no data';

$lang['adm_outpay_detail_invoice']='Invoice';
$lang['adm_outpay_detail_title']='Latest player\'s deposits';
$lang['adm_outpay_detail_table1Head_1']='Date';
$lang['adm_outpay_detail_table1Head_2']='Amount';
$lang['adm_outpay_detail_table1Head_3']='PS';
$lang['adm_outpay_detail_table1Head_4']='Invoice';
$lang['adm_outpay_detail_table1Head_5']='Status';
$lang['adm_outpay_detail_title2']='Latest played games';
$lang['adm_outpay_detail_table2Head_1']='Date';
$lang['adm_outpay_detail_table2Head_2']='Account balance';
$lang['adm_outpay_detail_table2Head_3']='Bet';
$lang['adm_outpay_detail_table2Head_4']='Gain';
$lang['adm_outpay_detail_table2Head_5']='Game';
$lang['adm_outpay_detail_noData']='no data';
$lang['adm_outpay_detail_cancel']='Cancel';
$lang['adm_outpay_detail_confirm']='Paid';
$lang['adm_outpay_detail_yes']='Yes';
$lang['adm_outpay_detail_no']='No';
$lang['adm_outpay_detail_popupTitle']='Cancel payment';
$lang['adm_outpay_detail_popupBody']='The payment will be cancelled. Are you sure?';
$lang['adm_outpay_detail_popupTitle2']='Confirm payment.';
$lang['adm_outpay_detail_popupBody2']='The payment will be made. Are you sure?';

$lang['adm_game_title']='Gambling banks';
$lang['adm_game_table1Head_1']='Min. bet';
$lang['adm_game_table1Head_2']='Max. bet';
$lang['adm_game_table1Head_3']='SB';
$lang['adm_game_table1Head_4']='BB';
$lang['adm_game_table1Head_5']='DESKTOP BANK';
$lang['adm_game_table1Head_6']='LOTO BANK';
$lang['adm_game_table1Head_7']='TRAFFIC BANK';
$lang['adm_game_title2']='not lost deposit balance';
$lang['adm_game_title3']='Gain guarantee';
$lang['adm_game_table3Head_1']='Spin guarantee';
$lang['adm_game_table3Head_2']='Bonus guarantee';
$lang['adm_game_title4']='Games';
$lang['adm_game_table4Head_1']='Game';
$lang['adm_game_table4Head_2']='Description';
$lang['adm_game_table4Head_3']='Action';
$lang['adm_game_title5']='Change bank';
$lang['adm_game_title6']='Enter amount';
$lang['adm_game_title7']='or choose';
$lang['adm_game_cancel']='Cancel';
$lang['adm_game_off']='Disable';
$lang['adm_game_on']='Enable';
$lang['adm_game_spincell']='spin slot';
$lang['adm_game_bonuscell']='bonus slot';
$lang['adm_game_balance']= 'balance';
$lang['adm_game_cellwin']= 'sink';

$lang['adm_game_name']='Name game';
$lang['adm_game_reels']='Reels';
$lang['adm_game_lines']='Lines';
$lang['adm_game_bonus']='Bonus';
$lang['adm_game_freespins']='Free spins';
$lang['adm_game_double']='Double';
$lang['adm_game_in']='In';
$lang['adm_game_out']='Out';
$lang['adm_game_total']='Total';
$lang['adm_game_action']='Action';

$lang['adm_game_desc_lines']='lines';
$lang['adm_game_desc_line']='line';
$lang['adm_game_desc_freespin']='free spins with multiplication';
$lang['adm_game_desc_freespin_']='free spins';
$lang['adm_game_desc_superbonus']='bonus and super bonus game';
$lang['adm_game_desc_superbonus_']='bonuses and super bonus game';
$lang['adm_game_desc_helmet']='There is a helmet';
$lang['adm_game_desc_fire']='There is a fire extinguisher';
$lang['adm_game_desc_bonus']='bonus game';
$lang['adm_game_desc_bonus_']='bonus games';
$lang['adm_game_desc_umbrella']='There is an umbrella';
$lang['adm_game_desc_reels']='drums';
$lang['adm_game_desc_jocker']='Joker';
$lang['adm_game_desc_caribpoker']='Caribbean Poker, single-player';
$lang['adm_game_desc_blackjack']='7 deck Blackjack';
$lang['adm_game_desc_roulette']='Single zero roulette';

$lang['adm_msg_1']='fill out all fields';
$lang['adm_msg_2']='link must consist of latin letters a - z and numbers 0 - 9';
$lang['adm_msg_3']='no special characters in the link allowed';
$lang['adm_msg_4']='this link already exists';
$lang['adm_msg_5']='page';
$lang['adm_msg_6']='created';
$lang['adm_msg_7']='set record access, file cfg.php - chmod 666';
$lang['adm_msg_8']='no record access in the root directory';
$lang['adm_msg_9']='disable SAFE MODE on the server';
$lang['adm_msg_10']='changes are saved';
$lang['adm_msg_11']='enter a title';
$lang['adm_msg_12']='enter your text';
$lang['adm_msg_13']='data is saved';
$lang['adm_msg_14']='MySql error';
$lang['adm_msg_15']='the section is unavailable';
$lang['adm_msg_16']='open a session';
$lang['adm_msg_17']='bank settings are saved';
$lang['adm_msg_18']='warranty settings are saved';
$lang['adm_msg_19']='create a pit';
$lang['adm_msg_20']='Bank is changed, new balance is';
$lang['adm_msg_21']='game is added to a slider';
$lang['adm_msg_22']='game is removed from a slider';
$lang['adm_msg_23']='game is on';
$lang['adm_msg_24']='game is off';
$lang['adm_msg_25']='enter a title and text of your message';
$lang['adm_msg_26']='item is added';
$lang['adm_msg_27']='item is removed';
$lang['adm_msg_28']='PIN code is generated';
$lang['adm_msg_29']='data input error';
$lang['adm_msg_30']='access denied';
$lang['adm_msg_31']='minimum denomination is 0.01';
$lang['adm_msg_32']='the group is added';
$lang['adm_msg_33']='player cannot be removed';
$lang['adm_msg_34']='no username';
$lang['adm_msg_35']='no password';
$lang['adm_msg_36']='no e-mail';
$lang['adm_msg_37']='incorrect e-mail';
$lang['adm_msg_38']='player %username% is created, player\'s safety PIN code is %pin%';
$lang['adm_msg_39']='choose another username';
$lang['adm_msg_40']='choose another e-mail';
$lang['adm_msg_41']='choose another phone number';
$lang['adm_msg_42']='the group is unavailable';
$lang['adm_msg_43']='comment is added';
$lang['adm_msg_44']='Player gets Jackpot';
$lang['adm_msg_45']='not enough parameters';
$lang['adm_msg_46']='Player\'s Jackpot is cancelled';
$lang['adm_msg_47']='The user is blocked';
$lang['adm_msg_48']='The user is unblocked';
$lang['adm_msg_49']='spin is reset';
$lang['adm_msg_50']='spin limit is changed';
$lang['adm_msg_51']='guarantee is changed';
$lang['adm_msg_52']='a deposit has been made of';
$lang['adm_msg_53']='a charge-off has been made of';
$lang['adm_msg_54']='No data';
$lang['adm_msg_55']='Tournament is added';
$lang['adm_msg_56']='Tournament is edited';

$lang['adm_popup_cancel']='Cancel';

// Копирайт

$lang['copyright']='casinofull';

$lang['gift_error']='you have chosen another bonus during sign up';

$lang['menu']='Menu';

$lang['adm_edit_game_title']='Edit texts of the game';
$lang['adm_edit_game_tableHead_1']='Title';
$lang['adm_edit_game_tableHead_2']='Description';
$lang['adm_edit_game_tableHead_3']='Keywords';
$lang['adm_edit_game_tableHead_4']='Text input field';

//импорт юзеров

$lang['adm_import_title']='Import users from a text file';
$lang['adm_import_file']='File';
$lang['adm_import_file_format']='File format';
$lang['adm_import_browse']='Choose';
$lang['adm_import_balance']='Account balance';
$lang['adm_import_balance_bonus']='Balance (bonus)';
$lang['adm_import_wager']='Wager';
$lang['adm_import_points']='Points';
$lang['adm_import_mailing']='Send e-mails to the added users';
$lang['adm_import_msg']='%num% users added';

//adm/game_log

$lang['adm_log_title']='Games logs';
$lang['adm_log_username']='Player\'s username';
$lang['adm_log_game']='Game';
$lang['adm_log_date']='Date';
$lang['adm_log_strings']='Log lines';
$lang['adm_log_flash']='Flash window';
$lang['adm_log_view']='View';
$lang['adm_log_loading']='Loading';
$lang['adm_log_select_string']='Select lines';

// lobby

$lang['lobby_exit']='EXIT';
$lang['lobby_payment']='CASH DEPARTMENT';
$lang['lobby_pay_label']='CASH DEPARTMENT';
$lang['lobby_pay_collect']='WITHDRAW';
$lang['lobby_pay_add']='DEPOSIT';
$lang['lobby_pay_pin_accept']='PIN code accepted! You have made a deposit of';
$lang['lobby_pay_collect_wnd_lbl']='Withdrawal';
$lang['lobby_pay_collect_wnd_text']='Click OK for payment. Withdrawal amount does not include denomination !';
$lang['lobby_pay_in_wnd_lbl']='Deposit';
$lang['lobby_pay_in_wnd_text']='Enter PIN code to make a deposit';
$lang['lobby_pay_in_wnd_text2']='Enter a deposit amount';
$lang['lobby_set_label']='SETTINGS';
$lang['lobby_set_main']='MAIN';
$lang['lobby_set_save']='Settings saved';
$lang['lobby_set_lang_wnd_lbl']='User interface language';
$lang['lobby_set_lang_wnd_text']='Choose user interface language';
$lang['lobby_set_denom_wnd_lbl']='Denomination';
$lang['lobby_set_denom_wnd_text']='Choose denomination!';

?>