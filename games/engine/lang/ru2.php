<?php

// casinofull - максимальная

// Игровая часть 30.10.2016

// для не авторизованных

$lang['err_no_access']='You must be logged in to access this page.';

// registration

$lang['err_all_input']='you need to fill in all the fields in this form';
$lang['err_pass2']='password and confirmation do not match';
$lang['err_mail']='enter e-mail address';
$lang['err_mail_preg_match']='enter the correct e-mail address';
$lang['err_in_login']='you are already logged in';
$lang['err_login_3_20']='login must contain from 3 to 20 characters';
$lang['err_rules']='you must agree to the rules of the system';
$lang['err_mail_30']='e-mail must contain up to 30 characters';
$lang['err_wmr_13']='wallet must be 13 characters long';
$lang['err_wmr_r']='wallet must begin with the letter R';
$lang['err_captcha']='security code entered incorrectly';
$lang['err_login_duble']='enter another login';
$lang['err_wmr_duble']='enter another wallet';
$lang['err_mail_duble']='enter another e-mail';
$lang['err_no_reg_ip']='from your IP already registered';

// login

$lang['err_login_pass']='invalid username or password';
$lang['err_no_login']='no login specified';
$lang['err_no_pass']='no password specified';
$lang['err_no_login_no_pass']='login and password are not specified';
$lang['err_auth']='this user is already authorized';
$lang['err_block']='your login is blocked';
$lang['err_no_user']='user not found';
$lang['err_no_real']='The game for real money will be available <br /> after registration';

// reminder

$lang['err_in_auth']='you are already logged in';
$lang['err_no_mail']='the email you entered is not found';
$lang['err_no_send']='cannot send message';
$lang['err_new_pass']='a new password has been sent to your e-mail';

// contacts

$lang['err_no_subject']='no subject specified';
$lang['err_in_message']='enter message';
$lang['err_maile_server']='mail server error';
$lang['err_send']='your message has been sent';

// enter

$lang['err_max_bonus']='you have the maximum number of bonuses';
$lang['err_bonus']='you get a bonus of <b> {bonus} </b> credits';
$lang['err_off_bonus']='bonuses are currently disabled';
$lang['err_no_bonus']='you already received a bonus today';
$lang['err_no_wager']='Turn on the wager to get the bonus';
$lang['err_min_max_enter']='replenishment is possible from <b>% min </b> to <b>% max </b> at a time';
$lang['err_no_enter']='receiving funds has been temporarily disabled';
$lang['err_no_bonus_ip']='received a bonus from your IP today';

// news

$lang['err_no_news']='this news was not found, perhaps it has already been deleted';

// out

$lang['err_no_pay']='you didn’t have any deposits this month, withdrawal is impossible';
$lang['err_no_weger']='you have not used up your wager for the amount of bets';
$lang['err_no_ps']='fill in the payment system data in the profile';
$lang['err_activ_email']='activate e-mail address';
$lang['err_activ_phone']='activate phone';
$lang['err_no_out']='withdrawal of funds is temporarily impossible';
$lang['err_min_max_out']='enter the correct amount [<b> {min} </b> - <b> {max} </b>]';
$lang['err_no_access_ps']='the selected PS is not available at the moment';
$lang['err_no_money']='you do not have so much money on the balance';
$lang['err_in_ps']='select payment system';
$lang['err_yes_out']='your application has been accepted and will be processed soon';
$lang['err_no_send_out']='Unable to send withdrawal request';
$lang['err_wait_out']='you already have a raw withdrawal request';

// pay

$lang['err_redirect']='after 10 seconds you will be redirected to the main page';
$lang['err_yes_pin']='your PIN code has been processed successfully';
$lang['err_no_pin_search']='this PIN code was not found';
$lang['err_no_pin']='you entered the wrong PIN code';
$lang['err_pin_block']='you have exhausted attempts to enter a PIN code, your account has been blocked';
$lang['err_pin_stay']='left input attempts';
$lang['pay'] ['userbal']='balance playerа';
$lang['pay'] ['success2']='replenished by the amount';

// profile

$lang['err_email_activated']='mail activated';
$lang['err_code']='invalid code entered';
$lang['err_phone_activated']='phone activated';
$lang['err_phone_duble']='enter another phone';
$lang['err_save']='your data was saved successfully';
$lang['err_db']='error writing data';

// games / index.php

$lang['err_game_block']='account is blocked, contact support';
$lang['err_no_access_game']='available to players';
$lang['err_no_start_game']='the game start file was not found';
$lang['err_no_game']='Unable to start the game';

// index.php

$lang['err_404']='page not found';
$lang['err_activate_yes']='activation was successful';
$lang['err_activate_no']='activation error';
$lang['err_ajax']='server error';

// main

$lang['main_LK']='PERSONAL OFFICE';
$lang['main_wellcome']='Welcome';
$lang['main_showGames']='Show more games';
$lang['main_jackpot']='Jackpot';
$lang['main_tomain']='Go to Home Page';
$lang['footer_paysys']='Accepted for payment';
$lang['footer_paysys1']='+ even more <strong> 20 ways! </strong>';

// exchange

$lang['err_no_points']='not enough points';
$lang['err_yes_points']='<b> ". $ paypoints." </b> points were exchanged for <b> $ sum </b> rubles';
$lang['err_minimal_points']='must be greater than <b> 100 </b> points';

// games

$lang['games_list']='list of games';

// News Page / news

$lang['news_more']='more';

// Страница партнерской программы /partner

$lang['partner_REF_URL']='YOUR PARTNER LINK';
$lang['partner_ref']='partner';
$lang['partner_all_out']='total paid';
$lang['partner_to_enter']='accrued';
$lang['partner_yes_out']='ready to pay';
$lang['partner_no_ref']='you haven’t invited anyone yet';
$lang['partner_ref_title']='partners you invited';
$lang['partner_transfer_to_balance']='transfer to game account';

// Страница статистики /history

$lang['history_WITHDRAWL']='REQUEST FOR OUTPUT';
$lang['history_HISTORY_PAY']='PAYMENT HISTORY';
$lang['history_HISTORY_LOGIN']='HISTORY OF ACTIVITY';
$lang['history_GAME_LOG']='LAST GAMES';
$lang['history_date']='date';
$lang['history_order_amout']='order amount';
$lang['history_payout']='payout amount';
$lang['history_inv']='invoice';
$lang['history_PS']='PS';
$lang['history_status']='status';
$lang['history_IP']='IP';
$lang['history_game']='game';
$lang['history_bet']='bet';
$lang['history_win']='win';
$lang['history_no_requests']='no requests';
$lang['history_no_enter']='no replenishment';
$lang['history_no_login']='no authorization';
$lang['history_no_game']='Statistics on games is missing';
$lang['history_statuses'] [0]='processing';
$lang['history_statuses'] [1]='processing';
$lang['history_statuses'] [2]='successful';
$lang['history_statuses'] [3]='returned';
$lang['history_statuses'] [4]='denied';
$lang['history_ps'] ['wmr']='WebMoney WMR';
$lang['history_ps'] ['qiwi']='QIWI';

// Страница обмена поинтов /exchange

$lang['exchange_TOTAL_BALANCE_POINTS']='CURRENT BALANCE OF POINTS';
$lang['exchange_COURSE']='EXCHANGE RATE';
$lang['exchange_POINTS_EX']='GIVE <BR /> POINTS FOR EXCHANGE';
$lang['exchange_EX_POINTS']='HOW MUCH YOU WILL GET';
$lang['exchange_cur']='cre.';

// Страница выплаты /out

$lang['out_SUM_IN']='SUM FOR OUTPUT <BR /> WINS';
$lang['out_PS']='PAYMENT SYSTEM';
$lang['out_ps_select']='select payment system';
$lang['out_QIWI']='QIWI';
$lang['out_WMR']='WebMoney WMR';

// Account replenishment page / enter

$lang['enter_PIN_CODE_title']='PIN code';
$lang['enter_PIN_CODE']='ENTER <BR /> CODE PIN';
$lang['enter_pin_input']='enter PIN code';
$lang['enter_SUM_ENTER']='AMOUNT <BR /> SUPPLIES';
$lang['enter_confirm']='you replenish the game account for the amount of';
$lang['enter_cur']='cre.';

// profile page / profile

$lang['profile_AVATAR']='AVATAR';
$lang['profile_LOGIN']='LOGIN';
$lang['profile_ID']='ID';
$lang['profile_E-MAIL']='MAIL';
$lang['profile_CHANGE_PASS']='CHANGE <BR /> PASSWORD';
$lang['profile_PASS']='PASSWORD';
$lang['profile_RE_PASS']='ACKNOWLEDGMENT';
$lang['profile_YOUR_E-MAIL']='YOUR MAIL';
$lang['profile_PHONE_QIWI']='PHONE <BR /> QIWI WALLET';
$lang['profile_WMR']='WEB MANI WALLET <BR /> WMR';
$lang['profile_CODE']='CODE';
$lang['profile_new_pass']='enter new password';
$lang['profile_new_pass2']='re-enter password';
$lang['profile_wmr_input']='WMR wallet number';
$lang['profile_phone_input']='phone number';
$lang['profile_code_input']='activation code';
$lang['profile_email_input']='enter your mail';
$lang['profile_use_wager']='GET BONUSES';

// Authorization block

$lang['auth_profile']='PROFILE';
$lang['auth_balance']='Balance';
$lang['auth_wager']='Wager';
$lang['auth_partner']='Affiliates';
$lang['auth_history']='Statistics';
$lang['auth_cur']='cre.';
$lang['auth_bet']='st.';
$lang['auth_guest']='Guest';
$lang['auth_exit']='OUTPUT';

// Popup window popup

$lang['popup_MSG']='MESSAGE';
$lang['popup_PAY']='ADDING BALANCE';
$lang['popup_OUT']='WITHDRAWAL OF FUNDS';
$lang['popup_REGISTER']='registration';
$lang['popup_login']='Login';
$lang['popup_pass']='Password';
$lang['popup_re_pass']='Retype password';
$lang['popup_email']='Mail';
$lang['popup_qiwi']='QIWI';
$lang['popup_wmr']='WMR';
$lang['popup_yes_rules']='I agree with the <a href="/rules"> rules </a> of the gaming club';
$lang['popup_ENTER']='sign in ON SITE';
$lang['popup_reminder']='Forgot your password';
$lang['popup_PASS_RECOVERY']='RESET PASSWORD';
$lang['popup_SUPPORT']='SERVICE SUPPORT';
$lang['popup_subject']='Post subject';
$lang['popup_message']='Message text';
$lang['popup_WELCOME_BONUS_CHOICE']='WELCOME BONUS FOR YOUR CHOICE';
$lang['popup_gift_register']='signup bonus, <br /> the most beneficial for beginners';
$lang['popup_GIFT_REG']='REGISTRATION BONUS';
$lang['popup_gift_reg_txt']='instant enrollment';
$lang['popup_GIFT_DAY']='DAILY BONUS';
$lang['popup_gift_day_txt']='every day, a new bonus';
$lang['popup_GIFT_PAY']='FIRST TOP UP';
$lang['popup_gift_pay_txt']='double your payment';
// Кнопки button

$lang['button_EXCHANGE']='EXCHANGE';
$lang['button_AUTH']='AUTHORIZATION';
$lang['button_REGISTER']='registration';
$lang['button_ENTER']='sign in';
$lang['button_EXIT']='OUTPUT';
$lang['button_PROFILE']='PROFILE';
$lang['button_RECOVERY']='RESET';
$lang['button_OK']='OK';
$lang['button_SAVE']='SAVE';
$lang['button_CONFIRM']='CONFIRM';
$lang['button_SEND']='SEND';
$lang['button_OUT']='config';
$lang['button_PAY']='cash';
$lang['button_COPY']='COPY';
$lang['button_REAL']='PLAY';
$lang['button_DEMO']='DEMO';

// The corner is rectangular

$lang['corner_OTHER_GAME']='OTHER GAMES';

// Blocks with the right block and slider and social. buttons

$lang['block_WIN_NOW']='WIN NOW';
$lang['block_WIN_BEST']='BEST WINNINGS';
$lang['block_CONTACT']='Suport';
$lang['block_phone']='mobile';
$lang['block_email']='e-mail';
$lang['block_icq']='skype';
$lang['block_support']='suport';
$lang['block_write_to_us']='Write to us';
$lang['block_RATING']='RATING';
$lang['block_Rating_stars']='RATING';
$lang['block_gamers']='player';
$lang['block_bet']='bet';

// Menu From The Bottom Menu

$lang['menu_ABOUT']='ABOUT US';
$lang['menu_guaranty']='Warranties';
$lang['menu_privacy']='Privacy';
$lang['menu_bonuses']='Bonus policy';
$lang['menu_paymethods']='Payment systems';
$lang['menu_license']='License';
$lang['menu_conditions']='Agreement';
$lang['menu_YOUR_MENU']='YOUR MENU';
$lang['menu_profile']='PROFILE';
$lang['menu_history']='Statistics';
$lang['menu_out']='Withdraw';
$lang['menu_enter']='Top up';
$lang['menu_partner']='Affiliates';
$lang['menu_set_bonus']='Get Bonus';
$lang['menu_OPTIONAL']='OPTIONAL';
$lang['menu_faq']='Help';
$lang['menu_news']='News';
$lang['menu_jp']='Jackpot';
$lang['menu_return']='Cashback';

// payments

$lang['pay'] ['success']='payment completed successfully';
$lang['pay'] ['fail']='payment is not completed';
$lang['pay'] ['not_found']='payment not found';
$lang['pay'] ['fail_sign']='digital signature error';
$lang['pay'] ['error']='payment failed;';


////////////////////////////////////////////////////////////////////////////
/////////////////////////////// АДМИН ПАНЕЛЬ ///////////////////////////////
////////////////////////////////////////////////////////////////////////////



// админ панель - Шапка

$lang['admin_head_hello']='Hello';
$lang['admin_head_ip']='IP';
$lang['admin_head_group']='Your group';

// admin panel - user groups

$lang['user_group'] [1]='Director';
$lang['user_group'] [2]='Dealer';
$lang['user_group'] [3]='Manager';
$lang['user_group'] [4]='Cashier';
$lang['user_group'] [5]='player';

// admin panel - main menu

$lang['adminmenu'] ['users']='Game Accounts';
$lang['adminmenu'] ['jp']='Jack Pot';
$lang['adminmenu'] ['game']='Game Settings';
$lang['adminmenu'] ['settings']='System Settings';
$lang['adminmenu'] ['report']='Reports';
$lang['adminmenu'] ['game_log']='Game logs';
$lang['adminmenu'] ['pin']='PIN code';
$lang['adminmenu'] ['email']='Email Template';
$lang['adminmenu'] ['sms']='SMS template';
$lang['adminmenu'] ['mailing']='Distribution of messages';
$lang['adminmenu'] ['notify']='Mini Messages';
$lang['adminmenu'] ['edit']='Payment processing';
$lang['adminmenu'] ['news']='News';
$lang['adminmenu'] ['pages']='Pages';
$lang['adminmenu'] ['articles']='Articles';
$lang['adminmenu'] ['tournament']='Tournaments';
$lang['adminmenu'] ['links']='External systems';
$lang['adminmenu'] ['exit']='OUTPUT';
$lang['adminmenu'] ['outpay_detail']='Outgoing payment';
$lang['adminmenu'] ['edit_content']='Edit section';
$lang['adminmenu'] ['add_page']='Add section';
$lang['adminmenu'] ['add_article']='Add article';
$lang['adminmenu'] ['edit_news']='Edit News';

// admin panel - menu report

$lang['reportmenu'] [1]='General data';
$lang['reportmenu'] [2]='Payouts';
$lang['reportmenu'] [5]='Active playersand';
$lang['reportmenu'] [6]='Player history';
$lang['reportmenu'] [7]='Game Logging';
$lang['reportmenu'] [10]='Refunds';
$lang['reportmenu'] [11]='Settings log';
$lang['reportmenu'] [13]='Payments';
$lang['reportmenu'] [14]='Lottery draws';
$lang['reportmenu'] [15]='Lotto Bet';

// admin panel - submenu of settings

$lang['settings_group'] [1]='General settings';
$lang['settings_group'] [2]='Payment and bonuses';
$lang['settings_group'] [3]='Percentage of return';
$lang['settings_group'] [4]='Games Interface';
$lang['settings_group'] [5]='Input and output';
$lang['settings_group'] [7]='Bonuses to players';
$lang['settings_group'] [8]='Statistics';
$lang['settings_group'] [9]='Suport';
$lang['settings_group'] [13]='pin codes';
$lang['settings_group'] [14]='SMS settings';
$lang['settings_group'] [15]='PS FREE-KASSA';
$lang['settings_group'] [11]='Configure FK';
$lang['settings_group'] [16]='SMTP settings';
$lang['settings_group'] [17]='SMTP settings';
$lang['settings_group'] ['returns']='Returns to players';
$lang['settings_group'] ['other_settings']='Miscellaneous settings';
$lang['settings_group'] ['gamers_rating']='RATING players';

// admin panel - RATING and settings

$lang['settings_raiting_title']='RATING players';
$lang['settings_raiting_th1']='Level';
$lang['settings_raiting_th2']='Title';
$lang['settings_raiting_th3']='Deposit Amount';
$lang['settings_raiting_th4']='Color';
$lang['settings_raiting_th5']='Image';
$lang['settings_raiting_th6']='Exchange rate';
$lang['settings_raiting_th7']='Action';
$lang['settings_raiting_add']='Add RATING';
$lang['settings_raiting_edit']='Edit RATING';
$lang['settings_raiting_name']='Title';
$lang['settings_raiting_deposit']='Deposit Amount';
$lang['settings_raiting_course']='Exchange Rate';
$lang['settings_raiting_color']='Color';
$lang['settings_raiting_pic']='Image';

// админ панель - настройки

$lang['settings_title'] ['cas_name']='Casino Name';
$lang['settings_title'] ['url']='URL or IP';
$lang['settings_title'] ['num']='The amount of output on the page';
$lang['settings_title'] ['spin_koef']='Number of paid spins for 1 credit';
$lang['settings_title'] ['win_koef']='Warranty multiplier, upon completion of the spin limit';
$lang['settings_title'] ['payed_spins_fixed']='Fixed spin limit';
$lang['settings_title'] ['payed_spins_val']='Spin Limit';
$lang['settings_title'] ['bank_type']='Game banks operating mode';
$lang['settings_title'] ['spin_bank_perc']='Percentage of Sat';
$lang['settings_title'] ['bonus_bank_perc']='Percentage of BB';
$lang['settings_title'] ['double_bank_perc']='Percentage of DB';
$lang['settings_title'] ['online_timeout']='Keep player online while idle (min)';
$lang['settings_title'] ['refresh_timeout']='Refresh data (min)';
$lang['settings_title'] ['denomination']='Denomination';
$lang['settings_title'] ['debug']='Record logs';
$lang['settings_title'] ['ref_perc']='Percentage of referrals';
$lang['settings_title'] ['use_ulogin']='Use authorization <b> uLogin.ru </b>';
$lang['settings_title'] ['ulogin_block']='Authorization buttons <b> uLogin.ru </b>';
$lang['settings_title'] ['use_captcha']='Use captcha';
$lang['settings_title'] ['activate_mail']='Require activation of e-mail';
$lang['settings_title'] ['activate_phone']='Require phone activation';
$lang['settings_title'] ['reg_ip_check']='The number of registrations from one IP address';
$lang['settings_title'] ['is_block']='Block the casino';
$lang['settings_title'] ['block_reason']='Reason for blocking a casino';
$lang['settings_title'] ['game_use_logo']='Use logo when downloading games';
$lang['settings_title'] ['game_use_bg']='Use your background when loading games';
$lang['settings_title'] ['game_use_fullscreen']='Show the FULL SCREEN button in games';
$lang['settings_title'] ['game_use_sound']='Show the SOUND button in games';
$lang['settings_title'] ['sms_use_sms']='Use SMS informing';
$lang['settings_title'] ['sms_https_login']='Login for authorization protocol';
$lang['settings_title'] ['sms_https_password']='Password for authorization protocol';
$lang['settings_title'] ['fk_use']='Use PS FREE-KASSA';
$lang['settings_title'] ['fk_merchant_id']='Store ID';
$lang['settings_title'] ['fk_merchant_key']='Secret Word 1';
$lang['settings_title'] ['fk_merchant_key2']='Secret Word 2';
$lang['settings_title'] ['pin_use']='Use PIN code';
$lang['settings_title'] ['allow_outpay']='Allow withdrawal of funds';
$lang['settings_title'] ['monthly_outpay']='It is forbidden to withdraw without a deposit in the current month';
$lang['settings_title'] ['allow_wmr']='Allow withdrawal of funds to WMR';
$lang['settings_title'] ['allow_qiwi']='Allow withdrawals to QIWI';
$lang['settings_title'] ['outpay_tax_perc']='Percentage withholding from the amount of withdrawal';
$lang['settings_title'] ['outpay_tax_sum']='Retention amount from the withdrawal amount';
$lang['settings_title'] ['minout']='The minimum amount to withdraw';
$lang['settings_title'] ['maxout']='Maximum amount for withdrawal';
$lang['settings_title'] ['enter_from']='The minimum amount to replenish';
$lang['settings_title'] ['enter_to']='Maximum amount to top up';
$lang['settings_title'] ['bonus_from']='Minimum Bonus';
$lang['settings_title'] ['bonus_to']='Maximum bonus';
$lang['settings_title'] ['bonus_daily']='Daily Bonus';
$lang['settings_title'] ['bonus_limit']='Bonus limit - daily';
$lang['settings_title'] ['bonus_check_ip']='Limit the issuance of bonuses by IP';
$lang['settings_title'] ['dep_bonus_limit']='Bonus limit - deposit';
$lang['settings_title'] ['fdep_bonus']='First Deposit Bonus';
$lang['settings_title'] ['fdep_bonus_val']='Bonus for the first deposit, in percent';
$lang['settings_title'] ['dep_bonus_1']='Bonus for the second replenishment, in percent';
$lang['settings_title'] ['dep_bonus_2']='Bonus for the second replenishment, in percent';
$lang['settings_title'] ['dep_bonus_3']='Bonus for the second replenishment, in percent';
$lang['settings_title'] ['reg_bon']='Registration Bonus';
$lang['settings_title'] ['reg_bon_ref']='Bonus for registering a referral';
$lang['settings_title'] ['act_bon_ref']='Referral activation bonus';
$lang['settings_title'] ['act_bon']='Bonus for activation of e-mail and phone';
$lang['settings_title'] ['botstat_timeout']='Update BOT statistics (ms)';
$lang['settings_title'] ['botstat_logins']='Logins to display in statistics';
$lang['settings_title'] ['botstat_stav']='Possible rates in statistics';
$lang['settings_title'] ['botstat_win']='Possible winnings in statistics';
$lang['settings_title'] ['contact_phone']='Phone';
$lang['settings_title'] ['contact_mail']='E-mail';
$lang['settings_title'] ['contact_icq']='ICQ';
$lang['settings_title'] ['adm_email']='System e-mail';
$lang['settings_title'] ['mail_from']='From whom';
$lang['settings_title'] ['mail_reply']='Reply';
$lang['settings_title'] ['mail_type']='Send Method';
$lang['settings_title'] ['mail_count']='Messages per connection';
$lang['settings_title'] ['mail_period']='Time Limit';
$lang['settings_title'] ['mail_period_count']='Message Limit';
$lang['settings_title'] ['mail_smtp_host']='SMTP Host';
$lang['settings_title'] ['mail_smtp_port']='SMTP Port';
$lang['settings_title'] ['mail_smtp_auth']='SMTP authorization';
$lang['settings_title'] ['mail_smtp_user']='SMTP Login';
$lang['settings_title'] ['mail_smtp_pass']='SMTP Password';
$lang['settings_title'] ['returns_min']='The minimum deposit amount';
$lang['settings_title'] ['returns_max']='Maximum recharge amount';
$lang['settings_title'] ['returns_percent']='Return percentage';
$lang['settings_title'] ['returns_percent_room']='Percentage of deduction from each bid';
$lang['settings_title'] ['returns_balance_room']='Balance of return';
$lang['settings_title'] ['returns_balans_finish']='Maximum return balance';
$lang['settings_title'] ['returns_winners_room']='Winner';
$lang['settings_title'] ['default_lang']='Language by default, ru or us';
$lang['settings_title'] ['bets']='Possible bids';
$lang['settings_title'] ['use_gamer_raiting']='RATING players';
$lang['settings_title'] ['game_block_count']='The number of games in the first block';
$lang['settings_title'] ['hide_news_date']='Hide news date';
$lang['settings_title'] ['points_pay']='Points for replenishment';
$lang['settings_title'] ['mail_text_type']='Body of the letter';

//authorization form

$lang['adm_login_title']='User Management System';
$lang['login']='Login';
$lang['pass']='Password';
$lang['adm_form_enter']='Login';
$lang['enter_login_pass']='Enter username / password';
$lang['enterCor_login_pass']='Enter the correct username / password';
$lang['adm_not_access']='This control panel is not available to you';
$lang['page_not_access']='You do not have access to this page';
$lang['not_auth']='Login required';

// админ панель - users

$lang['adm_users_head']='playerand cashiers';
$lang['adm_users_login']='Login';
$lang['adm_users_jp']='Jack Pot';
$lang['adm_users_balance']='Balance';
$lang['adm_users_plus']='Top up';
$lang['adm_users_minus']='config';
$lang['adm_users_points']='Points';
$lang['adm_users_status']='Status';
$lang['adm_users_spin']='Spin';
$lang['adm_users_guarantee']='Warranty';
$lang['adm_users_limit']='Limit';
$lang['adm_users_m_activ']='P';
$lang['adm_users_p_activ']='T';
$lang['adm_users_block']='B';
$lang['adm_users_del']='Y';
$lang['adm_users_search']='search';
$lang['adm_users_search_input']='login, ip, e-mail, phone';
$lang['adm_users_edit_title']='Editing a playera';
$lang['adm_users_add_title']='Add player or cashier';
$lang['adm_users_add_login']='Login';
$lang['adm_users_add_pass']='Password';
$lang['adm_users_add_mail']='E-mail';
$lang['adm_users_add_phone']='Phone';
$lang['adm_users_add_wmr']='WebMoney WMR';
$lang['adm_users_add_cashier']='Cashier';
$lang['adm_users_add_cancel']='CANCEL';
$lang['adm_users_edit_head']='Edit balance';
$lang['adm_users_edit_amount']='Amount';
$lang['adm_users_edit_select']='Select';
$lang['adm_users_edit_cancel']='CANCEL';
$lang['adm_users_del_head']='Delete user?';
$lang['adm_users_del_body']='The user and all information on him will be deleted. Are you sure?';
$lang['adm_users_yes']='Yes';
$lang['adm_users_no']='No';
$lang['adm_users_bots']='Bots';

$lang ['adm_users_action'] [0]='na';
$lang ['adm_users_action'] [1]='LK';
$lang ['adm_users_action'] [2]='game';
$lang ['adm_users_action'] [3]='frozen';
$lang ['adm_users_enter_sum']='Enter the amount';

// админ панель - jp

$lang['adm_jp_head_1']='Jack pot denomination';
$lang['adm_jp_real_sum']='Show real jackpot amountа';
$lang['adm_jp_head_2']='Jack Pot';
$lang['adm_jp_title']='Recharge Jack Pot';
$lang['adm_jp_amount']='Amount';
$lang['adm_jp_select']='Select';
$lang['adm_jp_cancel']='CANCEL';
$lang['adm_jp_edit_title']='Edit Jack Pot';
$lang['adm_jp_edit_name']='Name';
$lang['adm_jp_edit_win']='Drop Amount';
$lang['adm_jp_edit_percent']='Percentage';
$lang['adm_jp_edit_win_chance']='Prev dropping out';
$lang['adm_jp_edit_spin']='Spin Numbers';
$lang['adm_jp_date']='Date';
$lang['adm_jp_logs']='Logs';
$lang['adm_jp_balance']='Balance';
$lang['adm_jp_win']='Drop Amount';
$lang['adm_jp_percent']='Percentage';
$lang['adm_jp_win_chance']='Prev dropping out';
$lang['adm_jp_spin']='Spin Number';
$lang['adm_jp_win_player']='player';
$lang['adm_jp_deposit']='Recharge';
$lang['adm_jp_edit']='Edit';
$lang['adm_jp_head_log']='Jack Pot Logs';
$lang['adm_jp_log_date']='Date';
$lang['adm_jp_log_login']='Login';
$lang['adm_jp_log_log']='Logs';
$lang['adm_jp_no_records']='no data';
$lang['adm_jp_balanceUp']='balance replenished by the amount';
$lang['adm_jp_editmsg']='Edited <b>% jack_name% </b> payout amount <b>% jack_sum% </b>, percentage of deductions <b>% jack_percent% </b>';

// админ панель - 

$lang['adm_pages_tableHead_1']='Title';
$lang['adm_pages_tableHead_2']='URL';
$lang['adm_pages_tableHead_3']='Action';
$lang['adm_add_page_title']='Create a static page';
$lang['adm_pages_title']='Create and edit pages';
$lang['adm_add_page_tableHead_1']='Link';
$lang['adm_add_page_tableHead_2']='Title';
$lang['adm_add_page_tableHead_3']='Subheading';
$lang['adm_add_page_tableHead_4']='Keywords';
$lang['adm_add_page_tableHead_5']='Page Description';
$lang['adm_edit_content_title']='Create a static page';
$lang['adm_edit_content_tableHead_1']='Title';
$lang['adm_edit_content_tableHead_2']='Subheading';
$lang['adm_edit_content_tableHead_3']='Keywords';
$lang['adm_edit_content_tableHead_4']='Page Description';
$lang['adm_pages_msg_1']='No page specified';
$lang['adm_pages_msg_2']='Page deleted';

// Admin panel -

$lang['adm_news_title']='Create and edit news';
$lang['adm_news_tableHead_1']='Title';
$lang['adm_news_tableHead_2']='Keywords';
$lang['adm_news_tableHead_3']='Page Description';
$lang['adm_news_yes']='Yes';
$lang['adm_news_no']='No';
$lang['adm_news_modalTitle']='Delete news';
$lang['adm_news_modalBody']='The news will be deleted. Are you sure?';

// Admin panel -

$lang['adm_edit_news_title']='Create and edit news';
$lang['adm_edit_news_tableHead_1']='Title';
$lang['adm_edit_news_tableHead_2']='Keywords';
$lang['adm_edit_news_tableHead_3']='Page Description';

// админ панель - 

$lang['adm_mailing_title']='Mailing List';
$lang['adm_mailing_tableHead_1']='Title';
$lang['adm_mailing_title1']='Sent messages';
$lang['adm_mailing_table1Head_1']='Name';
$lang['adm_mailing_table1Head_2']='Submission Date';
$lang['adm_mailing_table1Head_3']='Mailing List';
$lang['adm_mailing_table1Head_4']='Action';
$lang['adm_mailing_noMsg']='messages have not yet been sent';
$lang['adm_mailing_test']='Test';

// Admin panel -

$lang['adm_sms_phone']='Phone';
$lang['adm_sms_txt']='Text';
$lang['adm_sms_title1']='registration';
$lang['adm_sms_title2']='Top-up';
$lang['adm_sms_title3']='Payout';

// Admin panel -

$lang['adm_email_title']='Title';

// Admin panel -

$lang['adm_email_title1']='registration';
$lang['adm_email_title2']='New password';
$lang['adm_email_title4']='Top-up';
$lang['adm_email_title5']='Invoice paid';
$lang['adm_email_title7']='Denial of payment';
$lang['adm_email_title8']='Account Activation';
$lang['adm_email_title9']='Support';
$lang['adm_email_title10']='Start of the tournament';
$lang['adm_email_title11']='End the tournament';
// админ панель - 

$lang['adm_pin_title']='PIN code generator';
$lang['adm_pin_tableHead_1']='Quantity';
$lang['adm_pin_tableHead_2']='Denomination';
$lang['adm_pin_btn_1']='Generate';
$lang['adm_pin_title1']='Statistics PIN codes';
$lang['adm_pin_table1Head_1']='Generated PIN codes';
$lang['adm_pin_table1Head_2']='Denomination';
$lang['adm_pin_btn_2']='Unload';
$lang['adm_pin_title2']='pin codes';
$lang['adm_pin_table2Head_1']='PIN code';
$lang['adm_pin_table2Head_2']='Amount';
$lang['adm_pin_table2Head_3']='Date';
$lang['adm_pin_table2Head_4']='Status';
$lang['adm_pin_noPin']='No PIN codes';
$lang['adm_pin_status1']='No';
$lang['adm_pin_status2']='No';
$lang['adm_pin_table3Head_1']='ID';
$lang['adm_pin_table3Head_2']='Password';
$lang['adm_pin_table3Head_3']='PIN code';
$lang['adm_pin_table3Head_4']='Creation Time';
$lang['adm_pin_table3Head_5']='Amount';
$lang['adm_pin_table3Head_6']='Status';
$lang['adm_pin_table3Head_7']='Activation time';
$lang['adm_pin_noScratch']='No scratch cards yet';
$lang['adm_pin_status_0']='No';
$lang['adm_pin_status_1']='No';
$lang['adm_pin_status_2']='player';
$lang['adm_pin_status_3']='Pin';

// админ панель - 

$lang['adm_report_filter']='Filter';
$lang['adm_report_date']='Date';
$lang['adm_report_time']='Time';
$lang['adm_report_KAS']='Cashier';
$lang['adm_report_INCOME']='INCOME';
$lang['adm_report_receipt']='Coming';
$lang['adm_report_outcome']='Consumption';
$lang['adm_report_rest']='Remain';
$lang['adm_report_income']='Income';
$lang['adm_report_np']='NP';
$lang['adm_report_login']='Login';
$lang['adm_report_ps']='PS';
$lang['adm_report_paysys']='Payment system';
$lang['adm_report_bill']='Account';
$lang['adm_report_sum']='Amount';
$lang['adm_report_status']='Status';
$lang['adm_report_gamer']='player';
$lang['adm_report_regtime']='registration';
$lang['adm_report_authtime']='Authorization';
$lang['adm_report_balance']='Balance';
$lang['adm_report_bonus']='Bonus';
$lang['adm_report_title']='Title';
$lang['adm_report_data']='Data';
$lang['adm_report_os']='OS';
$lang['adm_report_useragent']='User Registration Agent';
$lang['adm_report_useragent1']='User authorization agent';
$lang['adm_report_userip']='Registration IP';
$lang['adm_report_action']='Action';
$lang['adm_report_inpay']='recharge';
$lang['adm_report_outpay']='withdrawal';
$lang['adm_report_game']='Game';
$lang['adm_report_bet']='bet';
$lang['adm_report_win']='Win';
$lang['adm_report_denom']='Denom.';
$lang['adm_report_gamebalance']='Game balance';
$lang['adm_report_return']='Return';
$lang['adm_report_setting']='Settings';
$lang['adm_report_old']='Was';
$lang['adm_report_new']='Now';
$lang['adm_report_inv']='Invoice';
$lang['adm_report_type']='Type';
$lang['adm_report_lotodraw']='Circulation';
$lang['adm_report_sumbet']='Amount of bets';
$lang['adm_report_sumwin']='Amount of winnings';
$lang['adm_report_winballs']='Matching Balls';
$lang['adm_report_coef']='Odds';
$lang['adm_report_no_data']='no data';

$lang['adm_outpay_detail_invoice']='Invoice';
$lang['adm_outpay_detail_title']='Recent player replenishment';
$lang['adm_outpay_detail_table1Head_1']='Date';
$lang['adm_outpay_detail_table1Head_2']='Amount';
$lang['adm_outpay_detail_table1Head_3']='PS';
$lang['adm_outpay_detail_table1Head_4']='Invoice';
$lang['adm_outpay_detail_table1Head_5']='Status';
$lang['adm_outpay_detail_title2']='Last Played Games';
$lang['adm_outpay_detail_table2Head_1']='Date';
$lang['adm_outpay_detail_table2Head_2']='Balance';
$lang['adm_outpay_detail_table2Head_3']='bet';
$lang['adm_outpay_detail_table2Head_4']='Win';
$lang['adm_outpay_detail_table2Head_5']='Game';
$lang['adm_outpay_detail_noData']='no data';
$lang['adm_outpay_detail_cancel']='Cancel';
$lang['adm_outpay_detail_confirm']='Paid';
$lang['adm_outpay_detail_yes']='Yes';
$lang['adm_outpay_detail_no']='No';
$lang['adm_outpay_detail_popupTitle']='Cancel payment';
$lang['adm_outpay_detail_popupBody']='Payment will be canceled. Are you sure?';
$lang['adm_outpay_detail_popupTitle2']='Confirm payment';
$lang['adm_outpay_detail_popupBody2']='Payment will be paid. Are you sure?';

$lang['adm_game_title']='Game banks';
$lang['adm_game_table1Head_1']='Min. bet ';
$lang['adm_game_table1Head_2']='Max. bet ';
$lang['adm_game_table1Head_3']='Sat';
$lang['adm_game_table1Head_4']='BB';
$lang['adm_game_table1Head_5']='TABLE BANK';
$lang['adm_game_table1Head_6']='LOTTO BANK';
$lang['adm_game_table1Head_7']='TRAFFIC BANK';
$lang['adm_game_title2']='not lost balance of replenishment';
$lang['adm_game_title3']='Guaranteed winnings';
$lang['adm_game_table3Head_1']='Spin guarantee';
$lang['adm_game_table3Head_2']='Bonus Guarantee';
$lang['adm_game_title4']='Games';
$lang['adm_game_table4Head_1']='Game';
$lang['adm_game_table4Head_2']='Description';
$lang['adm_game_table4Head_3']='Action';
$lang['adm_game_title5']='Change bank';
$lang['adm_game_title6']='Enter the amount';
$lang['adm_game_title7']='or select';
$lang['adm_game_cancel']='Cancel';
$lang['adm_game_off']='Turn off';
$lang['adm_game_on']='Enable';
$lang['adm_game_spincell']='spin cell';
$lang['adm_game_bonuscell']='bonus cell';
$lang['adm_game_balance']='balance';
$lang['adm_game_cellwin']='drain';

$lang['adm_game_name']='Game Name';
$lang['adm_game_reels']='Drums';
$lang['adm_game_lines']='Lines';
$lang['adm_game_bonus']='Bonuses';
$lang['adm_game_freespins']='Free Spins';
$lang['adm_game_double']='Doubling';
$lang['adm_game_in']='Logged in';
$lang['adm_game_out']='Out';
$lang['adm_game_total']='Total';
$lang['adm_game_action']='Action';

$lang['adm_game_desc_lines']='lines';
$lang['adm_game_desc_line']='line';
$lang['adm_game_desc_freespin']='free free spins with multiplication';
$lang['adm_game_desc_freespin _']='free free spins';
$lang['adm_game_desc_superbonus']='bonus and super bonus game';
$lang['adm_game_desc_superbonus _']='bonus and super bonus game';
$lang['adm_game_desc_helmet']='There is a helmet';
$lang['adm_game_desc_fire']='There is a fire extinguisher';
$lang['adm_game_desc_bonus']='bonus game';
$lang['adm_game_desc_bonus _']='game bonus';
$lang['adm_game_desc_umbrella']='There is an umbrella';
$lang['adm_game_desc_reels']='reels';
$lang['adm_game_desc_jocker']='Joker';
$lang['adm_game_desc_caribpoker']='Carb Poker Single Player';
$lang['adm_game_desc_blackjack']='Black Jack for 7 Sleeves';
$lang['adm_game_desc_roulette']='Single player roulette with zero';

$lang['adm_msg_1']='fill in all fields';
$lang['adm_msg_2']='in the link only Latin characters a - z and numbers 0 - 9';
$lang['adm_msg_3']='special characters are not allowed in the link';
$lang['adm_msg_4']='this link already exists';
$lang['adm_msg_5']='page';
$lang['adm_msg_6']='created';
$lang['adm_msg_7']='set write permissions, cfg.php file - chmod 666';
$lang['adm_msg_8']='no write permissions, in the root directory';
$lang['adm_msg_9']='disable SAFE MODE on the server';
$lang['adm_msg_10']='changes saved';
$lang['adm_msg_11']='enter the title';
$lang['adm_msg_12']='enter the text';
$lang['adm_msg_13']='data saved';
$lang['adm_msg_14']='error MySql';
$lang['adm_msg_15']='section not available';
$lang['adm_msg_16']='open shift';
$lang['adm_msg_17']='bank settings saved';
$lang['adm_msg_18']='warranty settings saved';
$lang['adm_msg_19']='create a room';
$lang['adm_msg_20']='Bank changed, new balance';
$lang['adm_msg_21']='game added to slider';
$lang['adm_msg_22']='game removed from slider';
$lang['adm_msg_23']='game is on';
$lang['adm_msg_24']='game is off';
$lang['adm_msg_25']='enter the title and text of the message';
$lang['adm_msg_26']='news added';
$lang['adm_msg_27']='news deleted';
$lang['adm_msg_28']='PIN code generated';
$lang['adm_msg_29']='data entry error';
$lang['adm_msg_30']='access denied';
$lang['adm_msg_31']='minimum denomination is 0.01';
$lang['adm_msg_32']='group added';
$lang['adm_msg_33']='player cannot be deleted';
$lang['adm_msg_34']='login not specified';
$lang['adm_msg_35']='no password specified';
$lang['adm_msg_36']='not specified e-mail';
$lang['adm_msg_37']='invalid email';
$lang['adm_msg_38']='player% username% created, security PIN playera% pin%';
$lang['adm_msg_39']='choose another login';
$lang['adm_msg_40']='choose another e-mail';
$lang['adm_msg_41']='choose another phone';
$lang['adm_msg_42']='group not available';
$lang['adm_msg_43']='comment added';
$lang['adm_msg_44']='Jackpot assigned to player';
$lang['adm_msg_45']='not enough parameters';
$lang['adm_msg_46']='canceled Jack Pot playera';
$lang['adm_msg_47']='User is blocked';
$lang['adm_msg_48']='User Unblocked';
$lang['adm_msg_49']='spin reset';
$lang['adm_msg_50']='spin limit changed';
$lang['adm_msg_51']='warranty changed';
$lang['adm_msg_52']='balance replenished by the amount';
$lang['adm_msg_53']='the amount deducted from the balance';
$lang['adm_msg_54']='no data';
$lang['adm_msg_55']='Tournament added';
$lang['adm_msg_56']='Tournament edited';

$lang['adm_popup_cancel']='Cancel';

// Copyright

$lang['copyright']='casinofull';

$lang['gift_error']='you selected another bonus during registration';

$lang['menu']='Menu';

$lang['adm_edit_game_title']='Editing game texts';
$lang['adm_edit_game_tableHead_1']='Title';
$lang['adm_edit_game_tableHead_2']='Description';
$lang['adm_edit_game_tableHead_3']='Keyword';
$lang['adm_edit_game_tableHead_4']='Text input field';

//импорт юзеров

$lang['adm_import_title']='Import users from a text file';
$lang['adm_import_file']='File';
$lang['adm_import_file_format']='File format';
$lang['adm_import_browse']='Select';
$lang['adm_import_balance']='Balance';
$lang['adm_import_balance_bonus']='Balance (bonus)';
$lang['adm_import_wager']='Wager';
$lang['adm_import_points']='Points';
$lang['adm_import_mailing']='Send emails to added users';
$lang['adm_import_msg']='Added% num% users';

// adm / game_log

$lang['adm_log_title']='Game Logs';
$lang['adm_log_username']='Login playera';
$lang['adm_log_game']='Game';
$lang['adm_log_date']='Date';
$lang['adm_log_strings']='Log lines';
$lang['adm_log_flash']='Flash window';
$lang['adm_log_view']='View';
$lang['adm_log_loading']='Download';
$lang['adm_log_select_string']='Need to select lines';

// lobby

$lang['lobby_exit']='OUTPUT';
$lang['lobby_payment']='Cashier';
$lang['lobby_pay_label']='CASH';
$lang['lobby_pay_collect']='GET';
$lang['lobby_pay_add']='cash';
$lang['lobby_pay_pin_accept']='Pin code accepted! Your balance is credited to ';
$lang['lobby_pay_collect_wnd_lbl']='Withdraw funds';
$lang['lobby_pay_collect_wnd_text']='Click OK to issue. The withdrawal amount is indicated without denomination! ';
$lang['lobby_pay_in_wnd_lbl']='Refill balance';
$lang['lobby_pay_in_wnd_text']='Enter the pin code to replenish the balance';
$lang['lobby_pay_in_wnd_text2']='Enter the amount to replenish the balance';
$lang['lobby_set_label']='SETTINGS';
$lang['lobby_set_main']='BASIC';
$lang['lobby_set_save']='Settings saved';
$lang['lobby_set_lang_wnd_lbl']='Interface language';
$lang['lobby_set_lang_wnd_text']='Select an interface language';
$lang['lobby_set_denom_wnd_lbl']='Denomination';
$lang['lobby_set_denom_wnd_text']='Choose a denomination!';

?>