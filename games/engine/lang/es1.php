<?php

// GOLDSVET 5.1 - максимальная

// Игровая часть 30.10.2016

// для не авторизованных

$lang['err_no_access']='debe iniciar sesión o registrarse para acceder a esta página';

// registration

$lang['err_all_input']='debe llenar todos los campos de este formulario';
$lang['err_pass2']='contraseña y confirmación no coinciden';
$lang['err_mail']='introduzca el e-mail';
$lang['err_mail_preg_match']='introduzca el e-mail correcto';
$lang['err_in_login']='ya está autorizado';
$lang['err_login_3_20']='nombre de usuario debe contener entre 3 y 20 caracteres';
$lang['err_rules']='debe estar de acuerdo con las reglas del sistema';
$lang['err_mail_30']='e-mail debe contener hasta 30 caracteres';
$lang['err_wmr_13']='el monedero debe constar de 13 caracteres';
$lang['err_wmr_r']='el monedero debe comenzar con la letra R';
$lang['err_captcha']='el código de seguridad introducido es incorrecto';
$lang['err_login_duble']='introduzca otro nombre del usuario';
$lang['err_wmr_duble']='introduzca otro monedero';
$lang['err_mail_duble']='introduzca otro e-mail';
$lang['err_no_reg_ip']='de ese IP ya se habían registrado';

// login

$lang['err_login_pass']='nombre del usuario o contraseña incorrecto';
$lang['err_no_login']='no ha introducido el nombre del usuario';
$lang['err_no_pass']='no ha introducido la contraseña';
$lang['err_no_login_no_pass']='no ha introducido el nombre del usuario y contraseña';
$lang['err_auth']='este usuario ya está registrado';
$lang['err_block']='su nombre del usuario está bloqueado';
$lang['err_no_user']='el usuario no encontrado';
$lang['err_no_real']='el juego con dinero real será disponible<br />después de registro';

// reminder

$lang['err_in_auth']='ya está autorizado';
$lang['err_no_mail']='el e-mail introducido no encontrado';
$lang['err_no_send']='no se puede enviar la carta';
$lang['err_new_pass']='la contraseña nueva había sido enviada a su e-mail';

// contacts

$lang['err_no_subject']='el tema no especificado';
$lang['err_in_message']='introduzca el mensaje';
$lang['err_maile_server']='error del servidor postal';
$lang['err_send']='su mensaje se ha enviado';

// enter

$lang['err_max_bonus']='tiene el número máximo de bonos';
$lang['err_bonus']='usted recibe un bono en total de <b>{bonus}</b> créditos';
$lang['err_off_bonus']='en ese momento los bonos están deshabilitados';
$lang['err_no_bonus']='ya ha recibido el bono hoy';
$lang['err_no_wager']='Para recibir el bono conecta apuesta';
$lang['err_min_max_enter']='la reposición es posible de <b>%min</b> hasta <b>%max</b> a la vez';
$lang['err_no_enter']='recepción de dinero temporalmente desactivado';
$lang['err_no_bonus_ip']='con su dirección de IP hoy ya recibieron el bono';

// news

$lang['err_no_news']='esta noticia no se encuentra, tal vez ya se ha eliminado';

// out

$lang['err_no_pay']='usted no hizo recargas este mes, la extracción imposible';
$lang['err_no_weger']='usted tiene las apuestas no consumidas en el importe de las apuestas';
$lang['err_no_ps']='rellene en el perfil los datos de los sistemas de pago';
$lang['err_activ_email']='active el e-mail';
$lang['err_activ_phone']='active el teléfono';
$lang['err_no_out']='la extracción de fondos temporalmente imposible';
$lang['err_min_max_out']='escriba la cantidad correcta [ <b>{min}</b> - <b>{max}</b> ]';
$lang['err_no_access_ps']='SP seleccionada no está disponible en este momento ';
$lang['err_no_money']='no tiene tanto dinero en el balance ';
$lang['err_in_ps']='seleccione el sistema de pago';
$lang['err_yes_out']='su solicitud fue aceptada y en breve será procesada';
$lang['err_no_send_out']='no se puede enviar la solicitud de extracción de fondos';
$lang['err_wait_out']='ya tiene la solicitud de extracción de fondos no procesada';

// pay

$lang['err_redirect']='en 10 segundos se le redirigirá a la página principal';
$lang['err_yes_pin']='su pin se ha procesado correctamente';
$lang['err_no_pin_search']='este pin no se ha encontrado';
$lang['err_no_pin']='ha introducido el pin incorrecto';
$lang['err_pin_block']='usted ha agotado los intentos de introducir el pin, su cuenta está bloqueada';
$lang['err_pin_stay']='queda intentos de entrada';
$lang['pay']['userbal']='balance del jugador';
$lang['pay']['success2']='financiado con la cantidad';

// profile

$lang['err_email_activated']='correo activado';
$lang['err_code']='código introducido es incorrecto';
$lang['err_phone_activated']='teléfono activado';
$lang['err_phone_duble']='introduzca otro número del teléfono';
$lang['err_save']='sus datos se guardaron correctamente';
$lang['err_db']='error al escribir los datos';

// games/index.php

$lang['err_game_block']='cuenta bloqueada, póngase en contacto con soporte';
$lang['err_no_access_game']='está disponible a los jugadores';
$lang['err_no_start_game']='no se ha encontrado el archivo de inicio del juego';
$lang['err_no_game']='imposible iniciar el juego';

// index.php

$lang['err_404']='página no encontrada';
$lang['err_activate_yes']='la activación se realizó con éxito';
$lang['err_activate_no']='error al activar';
$lang['err_ajax']='error del servidor';

// main

$lang['main_LK']='GABINETE PERSONAL';
$lang['main_wellcome']='Bienvenido';
$lang['main_showGames']='Mostrar más juegos';
$lang['main_jackpot']='Jackpot';
$lang['main_tomain']='A la página principal';
$lang['footer_paysys']='Aceptamos pagos con';
$lang['footer_paysys1']='más de <strong>¡20 modos!</strong>';

// exchange

$lang['err_no_points']='no es suficiente de puntos';
$lang['err_yes_points']='<b>".$paypoints."</b> puntos cotizan en la <b>$sum</b> руб.';
$lang['err_minimal_points']='debe ser mayor que <b>100</b> puntos';

// games

$lang['games_list']='lista de juegos';

// �������� �������� /news

$lang['news_more']='más';

// �������� ����������� ��������� /partner

$lang['partner_REF_URL']='SU ENLACE DE AFILIADOS';
$lang['partner_ref']='afiliado';
$lang['partner_all_out']='pagado total';
$lang['partner_to_enter']='depositado';
$lang['partner_yes_out']='está listo para el pago';
$lang['partner_no_ref']='hasta ahora no invitó a nadie';
$lang['partner_ref_title']='sus afiliados invitados';
$lang['partner_transfer_to_balance']='transferir a cuenta del juego';

// �������� ���������� /history

$lang['history_WITHDRAWL']='SOLICITUD DE EXTRACCIÓN';
$lang['history_HISTORY_PAY']='HISTORIAL DE PAGOS';
$lang['history_HISTORY_LOGIN']='HISTORIAL DE ACTIVIDAD';
$lang['history_GAME_LOG']='ÚLTIMOS JUEGOS';
$lang['history_date']='fecha';
$lang['history_order_amout']='total de solicitud';
$lang['history_payout']='total de pago';
$lang['history_inv']='factura';
$lang['history_PS']='SP';
$lang['history_status']='estatus';
$lang['history_IP']='IP';
$lang['history_game']='juego';
$lang['history_bet']='apuesta';
$lang['history_win']='premio';
$lang['history_no_requests']='no hay consultas';
$lang['history_no_enter']='no hay depositos';
$lang['history_no_login']='no hay autorizaciones';
$lang['history_no_game']='las estadísticas de juego no existen';
$lang['history_statuses'][0]='en proceso';
$lang['history_statuses'][1]='en proceso';
$lang['history_statuses'][2]='con éxito';
$lang['history_statuses'][3]='devuelto';
$lang['history_statuses'][4]='negado';
$lang['history_ps']['wmr']='WebMoney WMR';
$lang['history_ps']['qiwi']='QIWI';

// �������� ������ ������� /exchange

$lang['exchange_TOTAL_BALANCE_POINTS']='EL SALDO DE BONOS';
$lang['exchange_COURSE']='EL TIPO DE CAMBIO';
$lang['exchange_POINTS_EX']='DAR BONOS <BR />PARA CAMBIAR';
$lang['exchange_EX_POINTS']='CUANTO RECIBIRÁ';
$lang['exchange_cur']='rub.';

// �������� ������� /out

$lang['out_SUM_IN']='EXTRACCIÓN <BR />DEL PREMIO';
$lang['out_PS']='SISTEMA DEL PAGO';
$lang['out_ps_select']='seleccione el sistema de pago';
$lang['out_QIWI']='QIWI';
$lang['out_WMR']='WebMoney WMR';

// �������� ���������� ����� /enter

$lang['enter_PIN_CODE_title']='PIN';
$lang['enter_PIN_CODE']='INTRODUCIR <BR />PIN';
$lang['enter_pin_input']='introduzca el pin';
$lang['enter_SUM_ENTER']='TOTAL <BR />DE DEPOSITO';
$lang['enter_confirm']='usted deposita en la cuenta del juego ';
$lang['enter_cur']='rub.';

// �������� ������� /profile

$lang['profile_AVATAR']='AVATAR';
$lang['profile_LOGIN']='ENTRAR';
$lang['profile_ID']='ID';
$lang['profile_E-MAIL']='CORREO';
$lang['profile_CHANGE_PASS']='CAMBIO <BR />DE CONTRASEÑA';
$lang['profile_PASS']='CONTRASEÑA';
$lang['profile_RE_PASS']='CONFIRMACIÓN';
$lang['profile_YOUR_E-MAIL']='SU CORREO';
$lang['profile_PHONE_QIWI']='TELÉFONO <BR />QIWI MONEDERO';
$lang['profile_WMR']='WEBMONEY MONEDERO <BR />WMR';
$lang['profile_CODE']='CÓDIGO';
$lang['profile_new_pass']='introduzca contraseña nueva';
$lang['profile_new_pass2']='repita la contraseña';
$lang['profile_wmr_input']='número de monedero WMR';
$lang['profile_phone_input']='número de teléfono';
$lang['profile_code_input']='código de activación';
$lang['profile_email_input']='introduzca su correo';
$lang['profile_use_wager']='RECIBIR BONOS';

// ���� �����������

$lang['auth_profile']='Perfil';
$lang['auth_balance']='Balance';
$lang['auth_wager']='Apuesta';
$lang['auth_partner']='Los socios';
$lang['auth_history']='Estadística';
$lang['auth_cur']='rub.';
$lang['auth_bet']='st.';
$lang['auth_guest']='Invitado';
$lang['auth_exit']='Salir';

// ����� ���� popup

$lang['popup_MSG']='MENSAJE';
$lang['popup_PAY']='DEPOSITAR AL BALANCE';
$lang['popup_OUT']='EXTRACCIÓN DE FONDOS';
$lang['popup_REGISTER']='REGISTRARSE';
$lang['popup_login']='Usuario';
$lang['popup_pass']='Contraseña';
$lang['popup_re_pass']='Repita la contraseña';
$lang['popup_email']='Correo';
$lang['popup_qiwi']='QIWI';
$lang['popup_wmr']='WMR';
$lang['popup_yes_rules']='Estoy de acuerdo con <a href="/rules">reglas</a> de club de juegos';
$lang['popup_ENTER']='ENTRAR';
$lang['popup_reminder']='Olvidó la contraseña';
$lang['popup_PASS_RECOVERY']='RECUPERACIÓN DE CONTRASEÑA';
$lang['popup_SUPPORT']='SOPORTE';
$lang['popup_subject']='Tema del mensaje';
$lang['popup_message']='Texto del mensaje';
$lang['popup_WELCOME_BONUS_CHOICE']='BONO DE BIENVENIDA A SU SELECCIÓN';
$lang['popup_gift_register']='el bono de bienvenida<br />más conveniente para principiantes';
$lang['popup_GIFT_REG']='BONO DE BIENVENIDA';
$lang['popup_gift_reg_txt']='la inscripción inmediata';
$lang['popup_GIFT_DAY']='BONO DIARIO';
$lang['popup_gift_day_txt']='cada día bono nuevo';
$lang['popup_GIFT_PAY']='PRIMER DEPOSITO';
$lang['popup_gift_pay_txt']='duplica tu pago';

// ������ button

$lang['button_EXCHANGE']='CAMBIAR';
$lang['button_AUTH']='AUTORIZACIÓN';
$lang['button_REGISTER']='REGISTRARSE';
$lang['button_ENTER']='ENTRAR';
$lang['button_EXIT']='SALIR';
$lang['button_PROFILE']='PERFIL';
$lang['button_RECOVERY']='RECUPERAR';
$lang['button_OK']='OK';
$lang['button_SAVE']='GUARDAR';
$lang['button_CONFIRM']='CONFIRMAR';
$lang['button_SEND']='ENVIAR';
$lang['button_OUT']='EXTRAER';
$lang['button_PAY']='DEPOSITAR';
$lang['button_COPY']='COPIAR';
$lang['button_REAL']='JUGAR';
$lang['button_DEMO']='DEMO';

// ������ �������������

$lang['corner_OTHER_GAME']='OTROS JUEGOS';

// ����� � ����� block � ������� � ���. ������

$lang['block_WIN_NOW']='AHORA GANAN';
$lang['block_WIN_BEST']='MEJORES PREMIOS';
$lang['block_CONTACT']='CONTACTOS';
$lang['block_phone']='teléfono';
$lang['block_email']='correo';
$lang['block_icq']='icq';
$lang['block_support']='soporte';
$lang['block_write_to_us']='contactarnos';
$lang['block_RATING']='RANKING';
$lang['block_Rating_stars']='Ranking';
$lang['block_gamers']='jugador';
$lang['block_bet']='apuesta';

// ���� � ���� menu

$lang['menu_ABOUT']='SOBRE NOSOTROS';
$lang['menu_guaranty']='Garantías';
$lang['menu_privacy']='Confidencialidad';
$lang['menu_bonuses']='Política de bonos';
$lang['menu_paymethods']='Sistemas de pago';
$lang['menu_license']='Licencia';
$lang['menu_conditions']='Acuerdo';
$lang['menu_YOUR_MENU']='SU MENÚ';
$lang['menu_profile']='Perfil';
$lang['menu_history']='Estadística';
$lang['menu_out']='Extraer';
$lang['menu_enter']='Depositar';
$lang['menu_partner']='A los afiliados';
$lang['menu_set_bonus']='Recibir bono';
$lang['menu_OPTIONAL']='MÁS';
$lang['menu_faq']='Ayuda';
$lang['menu_news']='Noticias';
$lang['menu_jp']='Jackpot';
$lang['menu_return']='Cashback';

//��������

$lang['pay']['success']='el pago se ha realizado correctamente';
$lang['pay']['fail']='el pago no se ha completado';
$lang['pay']['not_found']='el pago no se ha encontrado';
$lang['pay']['fail_sign']='error de firma digital';
$lang['pay']['error']='el pago se realizó con falla';



////////////////////////////////////////////////////////////////////////////
/////////////////////////////// ����� ������ ///////////////////////////////
////////////////////////////////////////////////////////////////////////////



// ����� ������ - �����

$lang['admin_head_hello']='Hola';
$lang['admin_head_ip']='IP';
$lang['admin_head_group']='Su grupo';

// ����� ������ - ������ �������������

$lang['user_group'][1]='Director';
$lang['user_group'][2]='Dealer';
$lang['user_group'][3]='Manager';
$lang['user_group'][4]='Cajero';
$lang['user_group'][5]='Jugador';

// ����� ������ - ������� ����

$lang['adminmenu']['users']='Cuentas de juegos';
$lang['adminmenu']['jp']='Jackpot';
$lang['adminmenu']['game']='Configuración de juegos';
$lang['adminmenu']['settings']='Configuración del sistema';
$lang['adminmenu']['report']='Informes';
$lang['adminmenu']['game_log']='Logos de juegos';
$lang['adminmenu']['pin']='PIN';
$lang['adminmenu']['email']='Plantilla de e-mail';
$lang['adminmenu']['sms']='Plantilla de sms';
$lang['adminmenu']['mailing']='Mailing';
$lang['adminmenu']['notify']='Mini mensajes';
$lang['adminmenu']['edit']='Procesamiento de pagos';
$lang['adminmenu']['news']='Noticias';
$lang['adminmenu']['pages']='Páginas';
$lang['adminmenu']['articles']='Artículos';
$lang['adminmenu']['tournament']='Torneos';
$lang['adminmenu']['links']='Sistemas externos';
$lang['adminmenu']['exit']='Salir';
$lang['adminmenu']['outpay_detail']='Pago efectuado';
$lang['adminmenu']['edit_content']='Editar sección';
$lang['adminmenu']['add_page']='Agregar una sección ';
$lang['adminmenu']['add_article']='Agregar artículo';
$lang['adminmenu']['edit_news']='Editar noticias';

// ����� ������ - ������ ����

$lang['reportmenu'][1]='Datos generales';
$lang['reportmenu'][2]='Pagos';
$lang['reportmenu'][5]='Jugadores activos';
$lang['reportmenu'][6]='Historial de jugadores';
$lang['reportmenu'][7]='Logo de juego';
$lang['reportmenu'][10]='Reembolsos';
$lang['reportmenu'][11]='Logo de configuración';
$lang['reportmenu'][13]='Pagos';
$lang['reportmenu'][14]='Sorteos de lotería';
$lang['reportmenu'][15]='Apuestas de lotería';

// ����� ������ - ������� ��������

$lang['settings_group'][1]='Configuración general';
$lang['settings_group'][2]='Pago y bonos';
$lang['settings_group'][3]='Porcentaje de retroceso';
$lang['settings_group'][4]='Interfaz de juegos';
$lang['settings_group'][5]='Deposito y extracción';
$lang['settings_group'][7]='Bonos a jugadores';
$lang['settings_group'][8]='Bot estadísticas';
$lang['settings_group'][9]='Contactos';
$lang['settings_group'][13]='PIN';
$lang['settings_group'][14]='Configuración de sms';
$lang['settings_group'][15]='SP FREE-KASSA';
$lang['settings_group'][11]='Configuración FK';
$lang['settings_group'][16]='Configuración SMTP';
$lang['settings_group'][17]='Configuración SMTP';
$lang['settings_group']['returns']='Reembolsos a los jugadores';
$lang['settings_group']['other_settings']='Diferentes ajustes';
$lang['settings_group']['gamers_rating']='Ranking de jugadores';

// ����� ������ - ��������� ��������

$lang['settings_raiting_title']='Ranking de jugadores';
$lang['settings_raiting_th1']='Nivel';
$lang['settings_raiting_th2']='Nombre';
$lang['settings_raiting_th3']='Total de deposito';
$lang['settings_raiting_th4']='Color';
$lang['settings_raiting_th5']='Imagen';
$lang['settings_raiting_th6']='Tipo de cambio';
$lang['settings_raiting_th7']='Acción';
$lang['settings_raiting_add']='Agregar ranking';
$lang['settings_raiting_edit']='Editar ranking';
$lang['settings_raiting_name']='Nombre';
$lang['settings_raiting_deposit']='Deposito';
$lang['settings_raiting_course']='Tipo de cambio';
$lang['settings_raiting_color']='Color';
$lang['settings_raiting_pic']='Imagen';

// ����� ������ - ���������

$lang['settings_title']['cas_name']='Nombre de casino';
$lang['settings_title']['url']='URL o IP';
$lang['settings_title']['num']='El número de resultados en la página';
$lang['settings_title']['spin_koef']='El número de spin pagados por 1 crédito';
$lang['settings_title']['win_koef']='El multiplicador de la garantía,  por la terminación de spin límite';
$lang['settings_title']['payed_spins_fixed']='Spin límite fijo';
$lang['settings_title']['payed_spins_val']='Spin límite';
$lang['settings_title']['bank_type']='Horario de los bancos de juego';
$lang['settings_title']['spin_bank_perc']='Porcentaje SB';
$lang['settings_title']['bonus_bank_perc']='Porcentaje BB';
$lang['settings_title']['double_bank_perc']='Porcentaje DB';
$lang['settings_title']['online_timeout']='Mantener al jugador en  línea, sin jugar (min.)';
$lang['settings_title']['refresh_timeout']='Renovación de datos (min.)';
$lang['settings_title']['denomination']='Denominación';
$lang['settings_title']['debug']='Anotar logos';
$lang['settings_title']['ref_perc']='Porcentaje a los afiliados';
$lang['settings_title']['use_ulogin']='Usar autorización <b>uLogin.ru</b>';
$lang['settings_title']['ulogin_block']='Botones de autorización';
$lang['settings_title']['use_captcha']='Usar captcha';
$lang['settings_title']['activate_mail']='Exigir la activación de e-mail';
$lang['settings_title']['activate_phone']='Exigir la activación de teléfono';
$lang['settings_title']['reg_ip_check']='Número de registros del mismo IP';
$lang['settings_title']['is_block']='Bloquear casino';
$lang['settings_title']['block_reason']='Causa del bloqueo de casino';
$lang['settings_title']['game_use_logo']='Usar el logo al descargar los juegos';
$lang['settings_title']['game_use_bg']='Usar fondo propio al descargar los juegos';
$lang['settings_title']['game_use_fullscreen']='Mostrar botón FULL SCREEN en juegos';
$lang['settings_title']['game_use_sound']='Mostrar botón SOUND en juegos';
$lang['settings_title']['sms_use_sms']='Usar SMS informes';
$lang['settings_title']['sms_https_login']='Nombre de usuario para el protocolo de autorización';
$lang['settings_title']['sms_https_password']='Contraseña para el protocolo de autorización';
$lang['settings_title']['fk_use']='Usar SP FREE-KASSA';
$lang['settings_title']['fk_merchant_id']='ID de tienda';
$lang['settings_title']['fk_merchant_key']='Palabra secreta 1';
$lang['settings_title']['fk_merchant_key2']='Palabra secreta 2';
$lang['settings_title']['pin_use']='Usar PIN';
$lang['settings_title']['allow_outpay']='Permitir extracción de fondos';
$lang['settings_title']['monthly_outpay']='Está prohibido mostrar sin depósito en el mes actual';
$lang['settings_title']['allow_wmr']='Permitir extracción de fondos a WMR';
$lang['settings_title']['allow_qiwi']='Permitir extracción de fondos a QIWI';
$lang['settings_title']['outpay_tax_perc']='El porcentaje de retención de extracción';
$lang['settings_title']['outpay_tax_sum']='El importe de retención de extracción';
$lang['settings_title']['minout']='Importe mínimo de extracción';
$lang['settings_title']['maxout']='Importe máximo de extracción';
$lang['settings_title']['enter_from']='Importe mínimo de deposito';
$lang['settings_title']['enter_to']='Importe máximo de deposito';
$lang['settings_title']['bonus_from']='Bono mínimo';
$lang['settings_title']['bonus_to']='Bono máximo';
$lang['settings_title']['bonus_daily']='Bono diario';
$lang['settings_title']['bonus_limit']='Bono límite - diario';
$lang['settings_title']['bonus_check_ip']='Limitación de la concesión de bonos segun IP';
$lang['settings_title']['dep_bonus_limit']='Bono límite - de deposito';
$lang['settings_title']['fdep_bonus']='Bono de primer depósito';
$lang['settings_title']['fdep_bonus_val']='Bono de primer deposito en porcientos';
$lang['settings_title']['dep_bonus_1']='Bono de segundo deposito en porcientos';
$lang['settings_title']['dep_bonus_2']='Bono de segundo deposito en porcientos';
$lang['settings_title']['dep_bonus_3']='Bono de segundo deposito en porcientos';
$lang['settings_title']['reg_bon']='Bono de bienvenida';
$lang['settings_title']['reg_bon_ref']='Bono de bienvenida de afiliado';
$lang['settings_title']['act_bon_ref']='Bono de activación de afiliado';
$lang['settings_title']['act_bon']='Bono de activación de e-mail y teléfono';
$lang['settings_title']['botstat_timeout']='Renovación de datos de BOT estadística (ms)';
$lang['settings_title']['botstat_logins']='Inicios de sesión de usuario para la visualización de datos en las estadísticas ';
$lang['settings_title']['botstat_stav']='Apuestas posibles en las estadísticas';
$lang['settings_title']['botstat_win']='Premios posibles en las estadísticas';
$lang['settings_title']['contact_phone']='Teléfono';
$lang['settings_title']['contact_mail']='E-mail';
$lang['settings_title']['contact_icq']='ICQ';
$lang['settings_title']['adm_email']='E-mail de sistema';
$lang['settings_title']['mail_from']='De quien';
$lang['settings_title']['mail_reply']='Contestar';
$lang['settings_title']['mail_type']='Modo de envío';
$lang['settings_title']['mail_count']='Mensajes durante la conección';
$lang['settings_title']['mail_period']='Límite de tiempo';
$lang['settings_title']['mail_period_count']='Límite de mensajes';
$lang['settings_title']['mail_smtp_host']='Hosting SMTP';
$lang['settings_title']['mail_smtp_port']='Puerto SMTP';
$lang['settings_title']['mail_smtp_auth']='SMTP autorización';
$lang['settings_title']['mail_smtp_user']='Nombre de usuario SMTP';
$lang['settings_title']['mail_smtp_pass']='Contraseña SMTP';
$lang['settings_title']['returns_min']='Importe mínimo de deposito';
$lang['settings_title']['returns_max']='Importe máximo de deposito';
$lang['settings_title']['returns_percent']='Porcentaje de reembolso';
$lang['settings_title']['returns_percent_room']='Porcentaje de pago de cada apuesta';
$lang['settings_title']['returns_balance_room']='Balance de reembolso';
$lang['settings_title']['returns_balans_finish']='Balance de reembolso máximo';
$lang['settings_title']['returns_winners_room']='Ganador';
$lang['settings_title']['default_lang']='El idioma de la cesación de pagos, ru o us';
$lang['settings_title']['bets']='Apuestas posibles';
$lang['settings_title']['use_gamer_raiting']='Ranking de jugadores';
$lang['settings_title']['game_block_count']='Número de juegos en el primer bloque';
$lang['settings_title']['hide_news_date']='Ocultar fecha de la noticia';
$lang['settings_title']['points_pay']='Bonos de deposito';
$lang['settings_title']['mail_text_type']='Carta';

//����� �����������

$lang['adm_login_title']='Sistema de gestión de usuarios';
$lang['login']='Usuario';
$lang['pass']='Contraseña';
$lang['adm_form_enter']='Entrar';
$lang['enter_login_pass']='Introduzca nombre del usuario/contraseña';
$lang['enterCor_login_pass']='Introduzca nombre del usuario/contraseña correcto';
$lang['adm_not_access']='Para usted no está disponible este panel de control';
$lang['page_not_access']='No tiene acceso a esta página';
$lang['not_auth']='Debe autorizarse';

// ����� ������ - users

$lang['adm_users_head']='Jugadores y cajeros';
$lang['adm_users_login']='Usuario';
$lang['adm_users_jp']='Jackpot';
$lang['adm_users_balance']='Balance';
$lang['adm_users_plus']='Depositar';
$lang['adm_users_minus']='Extraer';
$lang['adm_users_points']='Bonos';
$lang['adm_users_status']='Estatus';
$lang['adm_users_spin']='Spin';
$lang['adm_users_guarantee']='Garantía';
$lang['adm_users_limit']='Límite';
$lang['adm_users_m_activ']='C';
$lang['adm_users_p_activ']='T';
$lang['adm_users_block']='B';
$lang['adm_users_del']='E';
$lang['adm_users_search']='búsqueda';
$lang['adm_users_search_input']='usuario, ip, e-mail, teléfono';
$lang['adm_users_edit_title']='Editar al jugador';
$lang['adm_users_add_title']='Agregar jugador o cajero';
$lang['adm_users_add_login']='Usuario';
$lang['adm_users_add_pass']='Contraseña';
$lang['adm_users_add_mail']='E-mail';
$lang['adm_users_add_phone']='Teléfono';
$lang['adm_users_add_wmr']='Webmoney WMR';
$lang['adm_users_add_cashier']='Cajero';
$lang['adm_users_add_cancel']='CANCELAR';
$lang['adm_users_edit_head']='Editar balance';
$lang['adm_users_edit_amount']='Total';
$lang['adm_users_edit_select']='Seleccione';
$lang['adm_users_edit_cancel']='CANCELAR';
$lang['adm_users_del_head']='¿Eliminar al usuario?';
$lang['adm_users_del_body']='El usuario y toda la información sobre el serán eliminados. ¿Está seguro?';
$lang['adm_users_yes']='Sí';
$lang['adm_users_no']='No';
$lang['adm_users_bots']='BOTS';

$lang['adm_users_action'][0]='na';
$lang['adm_users_action'][1]='GP';
$lang['adm_users_action'][2]='juego';
$lang['adm_users_action'][3]='congelado';
$lang['adm_users_enter_sum']='Introduzca el importe';

// ����� ������ - jp

$lang['adm_jp_head_1']='Jackpot denominación';
$lang['adm_jp_real_sum']='Mostrar el importe real de Jackpot';
$lang['adm_jp_head_2']='Jackpot';
$lang['adm_jp_title']='Depositar a Jackpot';
$lang['adm_jp_amount']='Importe';
$lang['adm_jp_select']='Seleccione';
$lang['adm_jp_cancel']='CANCELAR';
$lang['adm_jp_edit_title']='Editar Jackpot';
$lang['adm_jp_edit_name']='Nombre';
$lang['adm_jp_edit_win']='Importe de giro';
$lang['adm_jp_edit_percent']='Porcentaje';
$lang['adm_jp_edit_win_chance']='Giro anterior';
$lang['adm_jp_edit_spin']='Números de spin';
$lang['adm_jp_date']='Fecha';
$lang['adm_jp_logs']='Logos';
$lang['adm_jp_balance']='Balance';
$lang['adm_jp_win']='Importe de giro';
$lang['adm_jp_percent']='Porcentaje';
$lang['adm_jp_win_chance']='Giro anterior';
$lang['adm_jp_spin']='Número de spin';
$lang['adm_jp_win_player']='Jugador';
$lang['adm_jp_deposit']='Depositar';
$lang['adm_jp_edit']='Editar';
$lang['adm_jp_head_log']='Logos de Jackpot';
$lang['adm_jp_log_date']='Fecha ';
$lang['adm_jp_log_login']='Usuario';
$lang['adm_jp_log_log']='Logos';
$lang['adm_jp_no_records']='no hay datos';
$lang['adm_jp_balanceUp']='En balance se deposito el importe de';
$lang['adm_jp_editmsg']='Editado <b>%jack_name%</b> el importe de pago <b>%jack_sum%</b>, porcentaje de extracciones <b>%jack_percent%</b>';

// ����� ������ - 

$lang['adm_pages_tableHead_1']='Nombre';
$lang['adm_pages_tableHead_2']='URL';
$lang['adm_pages_tableHead_3']='Acción';
$lang['adm_add_page_title']='Creación de página de estadística';
$lang['adm_pages_title']='Creación y edición de páginas';
$lang['adm_add_page_tableHead_1']='Enlace';
$lang['adm_add_page_tableHead_2']='Título';
$lang['adm_add_page_tableHead_3']='Subtitulo';
$lang['adm_add_page_tableHead_4']='Palabras claves';
$lang['adm_add_page_tableHead_5']='Descripción de página';
$lang['adm_edit_content_title']='Creación de página de estadística';
$lang['adm_edit_content_tableHead_1']='Título';
$lang['adm_edit_content_tableHead_2']='Subtítulo';
$lang['adm_edit_content_tableHead_3']='Palabras claves';
$lang['adm_edit_content_tableHead_4']='Descripción de página';
$lang['adm_pages_msg_1']='Página no indicada';
$lang['adm_pages_msg_2']='Página eliminada';

// ����� ������ - 

$lang['adm_news_title']='Creación y edición de noticias';
$lang['adm_news_tableHead_1']='Título';
$lang['adm_news_tableHead_2']='Palabras claves';
$lang['adm_news_tableHead_3']='Descripción de página';
$lang['adm_news_yes']='Sí';
$lang['adm_news_no']='No';
$lang['adm_news_modalTitle']='Eliminar noticia';
$lang['adm_news_modalBody']='La noticia será eliminada. ¿Está seguro?';

// ����� ������ - 

$lang['adm_edit_news_title']='Creación y edición de noticias';
$lang['adm_edit_news_tableHead_1']='Título';
$lang['adm_edit_news_tableHead_2']='Palabras claves';
$lang['adm_edit_news_tableHead_3']='Descripción de página';

// ����� ������ - 

$lang['adm_mailing_title']='Mailing';
$lang['adm_mailing_tableHead_1']='Título';
$lang['adm_mailing_title1']='Mensajes enviados';
$lang['adm_mailing_table1Head_1']='Nombre';
$lang['adm_mailing_table1Head_2']='Fecha de envío';
$lang['adm_mailing_table1Head_3']='Cartas en mailing';
$lang['adm_mailing_table1Head_4']='Acción';
$lang['adm_mailing_noMsg']='los mensajes aún no enviados';
$lang['adm_mailing_test']='Test';

// ����� ������ - 

$lang['adm_sms_phone']='Teléfono';
$lang['adm_sms_txt']='Texto';
$lang['adm_sms_title1']='Registrarse';
$lang['adm_sms_title2']='Deposito';
$lang['adm_sms_title3']='Pago';

// ����� ������ - 

$lang['adm_email_title']='Título';

// ����� ������ - 

$lang['adm_email_title1']='Registrarse';
$lang['adm_email_title2']='Contraseña nueva';
$lang['adm_email_title4']='Deposito';
$lang['adm_email_title5']='Factura pagada';
$lang['adm_email_title7']='Pago negado';
$lang['adm_email_title8']='Activación de cuenta';
$lang['adm_email_title9']='Soporte';
$lang['adm_email_title10']='Comienzo del torneo';
$lang['adm_email_title11']='Finalización del torneo';

// ����� ������ - 

$lang['adm_pin_title']='Generador de los PIN';
$lang['adm_pin_tableHead_1']='Número';
$lang['adm_pin_tableHead_2']='Clasificación';
$lang['adm_pin_btn_1']='Generar';
$lang['adm_pin_title1']='Estadística de los PIN';
$lang['adm_pin_table1Head_1']='Generado los PIN';
$lang['adm_pin_table1Head_2']='Clasificación';
$lang['adm_pin_btn_2']='Descargar';
$lang['adm_pin_title2']='PIN';
$lang['adm_pin_table2Head_1']='PIN';
$lang['adm_pin_table2Head_2']='Importe';
$lang['adm_pin_table2Head_3']='Fecha';
$lang['adm_pin_table2Head_4']='Estatus';
$lang['adm_pin_noPin']='PIN no encontrados';
$lang['adm_pin_status1']='No';
$lang['adm_pin_status2']='No';
$lang['adm_pin_table3Head_1']='ID';
$lang['adm_pin_table3Head_2']='Contraseña';
$lang['adm_pin_table3Head_3']='PIN';
$lang['adm_pin_table3Head_4']='Fecha de creación';
$lang['adm_pin_table3Head_5']='Importe';
$lang['adm_pin_table3Head_6']='Estatus';
$lang['adm_pin_table3Head_7']='Fecha de activación';
$lang['adm_pin_noScratch']='Tarjetas de rasca y gana aún no hay';
$lang['adm_pin_status_0']='No';
$lang['adm_pin_status_1']='No';
$lang['adm_pin_status_2']='Jugador';
$lang['adm_pin_status_3']='Pin';

// ����� ������ - 

$lang['adm_report_filter']='Filtro';
$lang['adm_report_date']='Fecha';
$lang['adm_report_time']='Hora';
$lang['adm_report_KAS']='CAJA';
$lang['adm_report_INCOME']='INGRESOS';
$lang['adm_report_receipt']='Entrante';
$lang['adm_report_outcome']='Consumo';
$lang['adm_report_rest']='Balance';
$lang['adm_report_income']='Ingresos';
$lang['adm_report_np']='NP';
$lang['adm_report_login']='Usuario';
$lang['adm_report_ps']='SP';
$lang['adm_report_paysys']='Sistema de pago';
$lang['adm_report_bill']='Cuenta';
$lang['adm_report_sum']='Total';
$lang['adm_report_status']='Estado';
$lang['adm_report_gamer']='Jugador';
$lang['adm_report_regtime']='Registrarse';
$lang['adm_report_authtime']='Autorizarse';
$lang['adm_report_balance']='Balance';
$lang['adm_report_bonus']='De bonos';
$lang['adm_report_title']='Nombre';
$lang['adm_report_data']='Datos';
$lang['adm_report_os']='Soporte';
$lang['adm_report_useragent']='User Agent de registro';
$lang['adm_report_useragent1']='User Agent de autorizaciones';
$lang['adm_report_userip']='IP de registro';
$lang['adm_report_action']='Acción';
$lang['adm_report_inpay']='deposito';
$lang['adm_report_outpay']='extracción';
$lang['adm_report_game']='Juego';
$lang['adm_report_bet']='Apuesta';
$lang['adm_report_win']='Premio';
$lang['adm_report_denom']='Denominación';
$lang['adm_report_gamebalance']='Balance del juego';
$lang['adm_report_return']='Reembolso';
$lang['adm_report_setting']='Configuración';
$lang['adm_report_old']='Había';
$lang['adm_report_new']='Hay ahora';
$lang['adm_report_inv']='Factura';
$lang['adm_report_type']='Tipo';
$lang['adm_report_lotodraw']='Circulación';
$lang['adm_report_sumbet']='Importe de apuestas';
$lang['adm_report_sumwin']='Importe de premios';
$lang['adm_report_winballs']='Bolas coincidentes';
$lang['adm_report_coef']='Coeficiente';
$lang['adm_report_no_data']='no hay datos';

$lang['adm_outpay_detail_invoice']='Factura';
$lang['adm_outpay_detail_title']='Últimos depositos del jugador';
$lang['adm_outpay_detail_table1Head_1']='Fecha';
$lang['adm_outpay_detail_table1Head_2']='Importe';
$lang['adm_outpay_detail_table1Head_3']='SP';
$lang['adm_outpay_detail_table1Head_4']='Factura';
$lang['adm_outpay_detail_table1Head_5']='Estatus';
$lang['adm_outpay_detail_title2']='Últimos juegos jugados';
$lang['adm_outpay_detail_table2Head_1']='Fecha';
$lang['adm_outpay_detail_table2Head_2']='Balance';
$lang['adm_outpay_detail_table2Head_3']='Apuesta';
$lang['adm_outpay_detail_table2Head_4']='Premio';
$lang['adm_outpay_detail_table2Head_5']='Juego';
$lang['adm_outpay_detail_noData']='no hay datos';
$lang['adm_outpay_detail_cancel']='Cancelar';
$lang['adm_outpay_detail_confirm']='Pagado';
$lang['adm_outpay_detail_yes']='Sí';
$lang['adm_outpay_detail_no']='No';
$lang['adm_outpay_detail_popupTitle']='Cancelar pago';
$lang['adm_outpay_detail_popupBody']='El pago será cancelado. ¿Está seguro?';
$lang['adm_outpay_detail_popupTitle2']='Confirmar pago';
$lang['adm_outpay_detail_popupBody2']='El pago será efectuado. ¿Está seguro?';

$lang['adm_game_title']='Bancos de juego';
$lang['adm_game_table1Head_1']='Apuesta mín.';
$lang['adm_game_table1Head_2']='Apuesta máx.';
$lang['adm_game_table1Head_3']='SB';
$lang['adm_game_table1Head_4']='BB';
$lang['adm_game_table1Head_5']='BANCO DE MESA';
$lang['adm_game_table1Head_6']='BANCO DE LOTERÍA';
$lang['adm_game_table1Head_7']='BANCO DE TRÁFICO';
$lang['adm_game_title2']='saldo de depositos no perdido';
$lang['adm_game_title3']='Garantía de premios';
$lang['adm_game_table3Head_1']='Spin garantía';
$lang['adm_game_table3Head_2']='Bono garantía';
$lang['adm_game_title4']='Juegos';
$lang['adm_game_table4Head_1']='Juego';
$lang['adm_game_table4Head_2']='Descripción';
$lang['adm_game_table4Head_3']='Acción';
$lang['adm_game_title5']='Cambio de banco';
$lang['adm_game_title6']='Introduzca el importe';
$lang['adm_game_title7']='o seleccione';
$lang['adm_game_cancel']='Cancelar';
$lang['adm_game_off']='Apagar';
$lang['adm_game_on']='Encender';
$lang['adm_game_spincell']='Célula splin';
$lang['adm_game_bonuscell']='Célula bono';
$lang['adm_game_balance']='balance';
$lang['adm_game_cellwin']='eliminación de';

$lang['adm_game_name']='El nombre del juego';
$lang['adm_game_reels']='Los tambores';
$lang['adm_game_lines']='Línea';
$lang['adm_game_bonus']='Bonos';
$lang['adm_game_freespins']='Tiradas gratis';
$lang['adm_game_double']='Duplicación';
$lang['adm_game_in']='Ha entrado';
$lang['adm_game_out']='Ha salido';
$lang['adm_game_total']='Por todo';
$lang['adm_game_action']='Acción';

$lang['adm_game_desc_lines']='líneas';
$lang['adm_game_desc_line']='línea';
$lang['adm_game_desc_freespin']='free spins gratuitos con la multiplicación';
$lang['adm_game_desc_freespin_']='free spins gratuitos';
$lang['adm_game_desc_superbonus']='juego de bono y super bono';
$lang['adm_game_desc_superbonus_']='juego de bono y super bono';
$lang['adm_game_desc_helmet']='Hay un casco';
$lang['adm_game_desc_fire']='Hay un extintor de incendios';
$lang['adm_game_desc_bonus']='juego de bono';
$lang['adm_game_desc_bonus_']='juegos de bono';
$lang['adm_game_desc_umbrella']='Hay un paraguas';
$lang['adm_game_desc_reels']='de tambores';
$lang['adm_game_desc_jocker']='Joker';
$lang['adm_game_desc_caribpoker']='Póker caribeño, de un solo jugador';
$lang['adm_game_desc_blackjack']='Blackjack de 7 mazos';
$lang['adm_game_desc_roulette']='Ruleta con un solo jugador';

$lang['adm_msg_1']='rellene todos los campos';
$lang['adm_msg_2']='en el enlace sólo se permiten los caracteres a - z y números del 0 al 9';
$lang['adm_msg_3']='en el enlace están prohibidos caracteres especiales';
$lang['adm_msg_4']='este enlace ya existe';
$lang['adm_msg_5']='página';
$lang['adm_msg_6']='creada';
$lang['adm_msg_7']='establezca los permisos de grabación, el archivo cfg.php - chmod 666';
$lang['adm_msg_8']='no tiene permisos de escritura en el directorio raíz';
$lang['adm_msg_9']='desconecte el SAFE MODE en el servidor';
$lang['adm_msg_10']='cambios se han guardados';
$lang['adm_msg_11']='introduzca el título';
$lang['adm_msg_12']='introduzca el texto';
$lang['adm_msg_13']='datos se han guardados';
$lang['adm_msg_14']='error MySql';
$lang['adm_msg_15']='la sección no está disponible';
$lang['adm_msg_16']='abra el cambio de';
$lang['adm_msg_17']='configuraciones de bancos se han guardados';
$lang['adm_msg_18']='configuraciones de garantía se han guardados';
$lang['adm_msg_19']='cree la sala';
$lang['adm_msg_20']='Banco cambiado, balance nuevo';
$lang['adm_msg_21']='juego añadido al slider';
$lang['adm_msg_22']='juego eliminado del slider';
$lang['adm_msg_23']='juego activado';
$lang['adm_msg_24']='juego desactivado';
$lang['adm_msg_25']='introduzca el título y texto del mensaje';
$lang['adm_msg_26']='noticia agregada';
$lang['adm_msg_27']='noticia eliminada';
$lang['adm_msg_28']='PIN generado';
$lang['adm_msg_29']='error de entrada de datos';
$lang['adm_msg_30']='acceso denegado';
$lang['adm_msg_31']='denominación mínima 0.01';
$lang['adm_msg_32']='grupo agregado';
$lang['adm_msg_33']='el jugador no puede ser eliminado';
$lang['adm_msg_34']='nombre de usuario no especificado';
$lang['adm_msg_35']='contraseña no especificada';
$lang['adm_msg_36']='e-mail no especificado';
$lang['adm_msg_37']='e-mail incorrecto';
$lang['adm_msg_38']='jugador %username% creado, PIN de seguridad del jugador %pin%';
$lang['adm_msg_39']='seleccione otro nombre del usuario';
$lang['adm_msg_40']='seleccione otro e-mail';
$lang['adm_msg_41']='seleccione otro teléfono';
$lang['adm_msg_42']='grupo no está disponible';
$lang['adm_msg_43']='comentario agregado';
$lang['adm_msg_44']='Jackpot asignado al jugador';
$lang['adm_msg_45']='falta de opciones';
$lang['adm_msg_46']='Jackpot del jugador está cancelado';
$lang['adm_msg_47']='Usuario bloqueado';
$lang['adm_msg_48']='Usuario desbloqueado';
$lang['adm_msg_49']='spin eliminado';
$lang['adm_msg_50']='límite de spins se ha cambiado';
$lang['adm_msg_51']='garantía se ha cambiado';
$lang['adm_msg_52']='al balance se ha depositado el importe de';
$lang['adm_msg_53']='del balance se ha extraido el importe de';
$lang['adm_msg_54']='no hay datos';
$lang['adm_msg_55']='Torneo agregado';
$lang['adm_msg_56']='Torneo editado';

$lang['adm_popup_cancel']='Cancelar';

// ��������

$lang['copyright']='casinofull';

$lang['gift_error']='al registrarse usted eligió otro bono';

$lang['menu']='Menú';

$lang['adm_edit_game_title']='Edición de los textos del juego';
$lang['adm_edit_game_tableHead_1']='Título';
$lang['adm_edit_game_tableHead_2']='Descripción';
$lang['adm_edit_game_tableHead_3']='Palabras claves';
$lang['adm_edit_game_tableHead_4']='Campo para texto';

//������ ������

$lang['adm_import_title']='Importación de usuarios desde un archivo de texto';
$lang['adm_import_file']='Archivo';
$lang['adm_import_file_format']='Formato del archivo';
$lang['adm_import_browse']='Seleccionar';
$lang['adm_import_balance']='Balance';
$lang['adm_import_balance_bonus']='Balance (bono)';
$lang['adm_import_wager']='Apuesta';
$lang['adm_import_points']='Bonos';
$lang['adm_import_mailing']='Enviar los e-mail a los usuarios agregados';
$lang['adm_import_msg']='Agregado %num% usuarios';

//adm/game_log

$lang['adm_log_title']='Logos de juego';
$lang['adm_log_username']='Nombre del usuario del jugador';
$lang['adm_log_game']='Juego';
$lang['adm_log_date']='Fecha';
$lang['adm_log_strings']='Línea del logo';
$lang['adm_log_flash']='Flash ventana';
$lang['adm_log_view']='Repaso';
$lang['adm_log_loading']='Cargando';
$lang['adm_log_select_string']='Debe seleccionar la línea';

// lobby

$lang['lobby_exit']='SALIR';
$lang['lobby_payment']='CAJA';
$lang['lobby_pay_label']='CAJA';
$lang['lobby_pay_collect']='COBRAR';
$lang['lobby_pay_add']='DEPOSITAR';
$lang['lobby_pay_pin_accept']='¡Pin aprobado! A su balance se ha acreditado ';
$lang['lobby_pay_collect_wnd_lbl']='Extracción de fondos';
$lang['lobby_pay_collect_wnd_text']='Para el pago haga clic en aceptar. ¡Monto de extracción se especifica sin denominación!';
$lang['lobby_pay_in_wnd_lbl']='Depositar al balance';
$lang['lobby_pay_in_wnd_text']='Introduzca el pin para realizar el deposito';
$lang['lobby_pay_in_wnd_text2']='Introduzca el importe para realizar el deposito';
$lang['lobby_set_label']='AJUSTES';
$lang['lobby_set_main']='PRINCIPALES';
$lang['lobby_set_save']='Ajustes se han guardados';
$lang['lobby_set_lang_wnd_lbl']='Idioma de la interfaz';
$lang['lobby_set_lang_wnd_text']='Seleccione el idioma de la interfaz';
$lang['lobby_set_denom_wnd_lbl']='Denominación';
$lang['lobby_set_denom_wnd_text']='¡Seleccione la denominación!';

?>