<?php

// GOLDSVET 5.1 - максимальная

// Игровая часть 30.10.2016

// для не авторизованных

$lang['err_no_access']='вы должны авторизоваться для доступа к этой странице';

// registration

$lang['err_all_input']='вам необходимо заполнить все поля в данной форме';
$lang['err_pass2']='пароль и подтверждение не совпадают';
$lang['err_mail']='введите e-mail адрес';
$lang['err_mail_preg_match']='введите правильный e-mail адрес';
$lang['err_in_login']='вы уже авторизованы';
$lang['err_login_3_20']='логин должен содержать от 3 до 20 символов';
$lang['err_rules']='вы должны согласиться с правилами системы';
$lang['err_mail_30']='e-mail должен содержать до 30 символов';
$lang['err_wmr_13']='кошелек должен состоять из 13 символов';
$lang['err_wmr_r']='кошелек должен начинаться с буквы R';
$lang['err_captcha']='код безопасности введен не верно';
$lang['err_login_duble']='введите другой логин';
$lang['err_wmr_duble']='введите другой кошелек';
$lang['err_mail_duble']='введите другой e-mail';
$lang['err_no_reg_ip']='с вашего IP уже регистрировались';

// login

$lang['err_login_pass']='не верный логин или пароль';
$lang['err_no_login']='не указан логин';
$lang['err_no_pass']='не указан пароль';
$lang['err_no_login_no_pass']='не указан логин и пароль';
$lang['err_auth']='данный юзер уже авторизован';
$lang['err_block']='ваш логин заблокирован';
$lang['err_no_user']='пользователь не найден';
$lang['err_no_real']='Игра на реальные деньги будет доступна<br />после регистрации';

// reminder

$lang['err_in_auth']='вы уже авторизованы';
$lang['err_no_mail']='введенный вами e-mail не найден';
$lang['err_no_send']='не удается отправить письмо';
$lang['err_new_pass']='новый пароль был выслан на ваш e-mail';

// contacts

$lang['err_no_subject']='не указана тема';
$lang['err_in_message']='введите сообщение';
$lang['err_maile_server']='ошибка почтового сервера';
$lang['err_send']='ваше сообщение отправлено';

// enter

$lang['err_max_bonus']='у вас максимальное количество бонусов';
$lang['err_bonus']='вы получаете бонус в сумме <b>{bonus}</b> кредитов';
$lang['err_off_bonus']='в данный момент бонусы отключены';
$lang['err_no_bonus']='вы сегодня уже получили бонус';
$lang['err_no_wager']='Для получения бонуса включите вейджер';
$lang['err_min_max_enter']='пополнение возможно от <b>%min</b> до <b>%max</b> за один раз';
$lang['err_no_enter']='прием денежных средств временно отключен';
$lang['err_no_bonus_ip']='с вашего IP сегодня получали бонус';

// news

$lang['err_no_news']='данная новость не найдена, возможно она уже удалена';

// out

$lang['err_no_pay']='у вас не было пополнений в этом месяце, вывод невозможен';
$lang['err_no_weger']='у вас не израсходован вейджер на сумму ставок';
$lang['err_no_ps']='заполните в профиле данные платежных систем';
$lang['err_activ_email']='активируйте e-mail адрес';
$lang['err_activ_phone']='активируйте телефон';
$lang['err_no_out']='вывод средств временно невозможен';
$lang['err_min_max_out']='введите корректную сумму [ <b>{min}</b> - <b>{max}</b> ]';
$lang['err_no_access_ps']='выбранная ПС не доступна в данный момент';
$lang['err_no_money']='у вас нет столько денег на балансе';
$lang['err_in_ps']='выберите платежную систему';
$lang['err_yes_out']='ваша заявка принята и в ближайшее время будет обработана';
$lang['err_no_send_out']='не удается отправить заявку на снятие денег';
$lang['err_wait_out']='у вас уже есть необработанная заявка на вывод средств';

// pay

$lang['err_redirect']='через 10 секунд вы будете перенаправлены на главную страницу';
$lang['err_yes_pin']='ваш ПИН код успешно обработан';
$lang['err_no_pin_search']='данный ПИН код не найден';
$lang['err_no_pin']='вы ввели не правильный ПИН код';
$lang['err_pin_block']='вы исчерпали попытки ввести ПИН код, ваш аккаунт заблокирован';
$lang['err_pin_stay']='осталось попыток ввода';
$lang['pay']['userbal']='баланс playerа';
$lang['pay']['success2']='пополнен на сумму';

// profile

$lang['err_email_activated']='почта активирована';
$lang['err_code']='введен не верный код';
$lang['err_phone_activated']='телефон активирован';
$lang['err_phone_duble']='введите другой телефон';
$lang['err_save']='ваши данные успешно сохранены';
$lang['err_db']='ошибка записи данных';

// games/index.php

$lang['err_game_block']='аккаунт заблокирован, обратитесь в поддержку';
$lang['err_no_access_game']='доступно playerам';
$lang['err_no_start_game']='не найден стартовый файл игры';
$lang['err_no_game']='невозможно запустить игру';

// index.php

$lang['err_404']='страница не найдена';
$lang['err_activate_yes']='активация прошла успешно';
$lang['err_activate_no']='ошибка активации';
$lang['err_ajax']='ошибка на сервере';

// main

$lang['main_LK']='ЛИЧНЫЙ КАБИНЕТ';
$lang['main_wellcome']='Добро пожаловать';
$lang['main_showGames']='Показать еще игры';
$lang['main_jackpot']='Джекпот';
$lang['main_tomain']='На главную страницу';
$lang['footer_paysys']='Принимаем к оплате';
$lang['footer_paysys1']='+ еще более <strong>20 способов!</strong>';

// exchange

$lang['err_no_points']='недостаточно поинтов';
$lang['err_yes_points']='<b>".$paypoints."</b> поинтов обменяли на <b>$sum</b> руб.';
$lang['err_minimal_points']='должно быть больше <b>100</b> поинтов';

// games

$lang['games_list']='список игр';

// Страница новостей /news

$lang['news_more']='подробнее';

// Страница партнерской программы /partner

$lang['partner_REF_URL']='ВАША ПАРТНЕРСКАЯ ССЫЛКА';
$lang['partner_ref']='партнер';
$lang['partner_all_out']='всего выплачено';
$lang['partner_to_enter']='начислено';
$lang['partner_yes_out']='готово к выплате';
$lang['partner_no_ref']='вы пока никого не пригласили';
$lang['partner_ref_title']='приглашенные вами партнеры';
$lang['partner_transfer_to_balance']='перевести на игровой счет';

// Страница статистики /history

$lang['history_WITHDRAWL']='ЗАПРОС НА ВЫВОД';
$lang['history_HISTORY_PAY']='ИСТОРИЯ ПЛАТЕЖЕЙ';
$lang['history_HISTORY_LOGIN']='ИСТОРИЯ АКТИВНОСТИ';
$lang['history_GAME_LOG']='ПОСЛЕДНИЕ ИГРЫ';
$lang['history_date']='дата';
$lang['history_order_amout']='сумма заказа';
$lang['history_payout']='сумма выплаты';
$lang['history_inv']='инвойс';
$lang['history_PS']='ПС';
$lang['history_status']='статус';
$lang['history_IP']='ИП';
$lang['history_game']='игра';
$lang['history_bet']='bet';
$lang['history_win']='выигрыш';
$lang['history_no_requests']='запросов нет';
$lang['history_no_enter']='пополнений нет';
$lang['history_no_login']='авторизаций нет';
$lang['history_no_game']='Statistics по играм отсутствует';
$lang['history_statuses'][0]='обработка';
$lang['history_statuses'][1]='обработка';
$lang['history_statuses'][2]='успешно';
$lang['history_statuses'][3]='возвращено';
$lang['history_statuses'][4]='отказано';
$lang['history_ps']['wmr']='WebMoney WMR';
$lang['history_ps']['qiwi']='QIWI';

// Страница обмена поинтов /exchange

$lang['exchange_TOTAL_BALANCE_POINTS']='ТЕКУЩИЙ БАЛАНС ПОИНТОВ';
$lang['exchange_COURSE']='КУРС ОБМЕНА';
$lang['exchange_POINTS_EX']='ОТДАТЬ ПОИНТЫ <BR />ДЛЯ ОБМЕНА';
$lang['exchange_EX_POINTS']='СКОЛЬКО ВЫ ПОЛУЧИТЕ';
$lang['exchange_cur']='руб.';

// Страница выплаты /out

$lang['out_SUM_IN']='СУММА ДЛЯ ВЫВОДА <BR />ВЫИГРЫША';
$lang['out_PS']='ПЛАТЕЖНАЯ СИСТЕМА';
$lang['out_ps_select']='выберите платежную систему';
$lang['out_QIWI']='QIWI';
$lang['out_WMR']='WebMoney WMR';

// Страница пополнения счета /enter

$lang['enter_PIN_CODE_title']='ПИН код';
$lang['enter_PIN_CODE']='ВВОД <BR />ПИН КОДА';
$lang['enter_pin_input']='введите ПИН код';
$lang['enter_SUM_ENTER']='СУММА <BR />ПОПОЛНЕНИЯ';
$lang['enter_confirm']='вы пополняете игровой счет на сумму';
$lang['enter_cur']='руб.';

// Страница профиля /profile

$lang['profile_AVATAR']='АВАТАР';
$lang['profile_LOGIN']='ЛОГИН';
$lang['profile_ID']='ИД';
$lang['profile_E-MAIL']='ПОЧТА';
$lang['profile_CHANGE_PASS']='ИЗМЕНЕНИЕ <BR />ПАРОЛЯ';
$lang['profile_PASS']='ПАРОЛЬ';
$lang['profile_RE_PASS']='ПОДТВЕРЖДЕНИЕ';
$lang['profile_YOUR_E-MAIL']='ВАША ПОЧТА';
$lang['profile_PHONE_QIWI']='ТЕЛЕФОН <BR />QIWI КОШЕЛЕК';
$lang['profile_WMR']='ВЕБ МАНИ КОШЕЛЕК <BR />WMR';
$lang['profile_CODE']='КОД';
$lang['profile_new_pass']='введите новый пароль';
$lang['profile_new_pass2']='повторите ввод пароля';
$lang['profile_wmr_input']='номер WMR кошелька';
$lang['profile_phone_input']='номер телефона';
$lang['profile_code_input']='код активации';
$lang['profile_email_input']='введите вашу почту';
$lang['profile_use_wager']='ПОЛУЧАТЬ БОНУСЫ';

// Блок авторизации

$lang['auth_profile']='PROFILE';
$lang['auth_balance']='Баланс';
$lang['auth_wager']='Вейджер';
$lang['auth_partner']='Партнерам';
$lang['auth_history']='Statistics';
$lang['auth_cur']='руб.';
$lang['auth_bet']='ст.';
$lang['auth_guest']='Гость';
$lang['auth_exit']='OUTPUT';

// Попап окна popup

$lang['popup_MSG']='СООБЩЕНИЕ';
$lang['popup_PAY']='ПОПОЛНЕНИЕ БАЛАНСА';
$lang['popup_OUT']='ВЫВОД СРЕДСТВ';
$lang['popup_REGISTER']='registration';
$lang['popup_login']='Логин';
$lang['popup_pass']='Пароль';
$lang['popup_re_pass']='Повторите пароль';
$lang['popup_email']='Почта';
$lang['popup_qiwi']='QIWI';
$lang['popup_wmr']='WMR';
$lang['popup_yes_rules']='Согласен с <a href="/rules">правилами</a> игрового клуба';
$lang['popup_ENTER']='sign in НА САЙТ';
$lang['popup_reminder']='Забыли пароль';
$lang['popup_PASS_RECOVERY']='ВОССТАНОВЛЕНИЕ ПАРОЛЯ';
$lang['popup_SUPPORT']='СЛУЖБА ПОДДЕРЖКА';
$lang['popup_subject']='Тема сообщения';
$lang['popup_message']='Текст сообщения';
$lang['popup_WELCOME_BONUS_CHOICE']='ПРИВЕТСВЕННЫЙ БОНУС НА ВАШ ВЫБОР';
$lang['popup_gift_register']='бонус за регистрацию,<br />самый выгодный для новичков';
$lang['popup_GIFT_REG']='БОНУС ЗА РЕГИСТРАЦИЮ';
$lang['popup_gift_reg_txt']='моментальное зачисление';
$lang['popup_GIFT_DAY']='ЕЖЕДНЕВНЫЙ БОНУС';
$lang['popup_gift_day_txt']='каждый день, новый бонус';
$lang['popup_GIFT_PAY']='ПЕРВОЕ ПОПОЛНЕНИЕ';
$lang['popup_gift_pay_txt']='удвой свой платеж';

// Кнопки button

$lang['button_EXCHANGE']='ОБМЕНЯТЬ';
$lang['button_AUTH']='АВТОРИЗАЦИЯ';
$lang['button_REGISTER']='registration';
$lang['button_ENTER']='sign in';
$lang['button_EXIT']='OUTPUT';
$lang['button_PROFILE']='PROFILE';
$lang['button_RECOVERY']='ВОССТАНОВИТЬ';
$lang['button_OK']='ОК';
$lang['button_SAVE']='СОХРАНИТЬ';
$lang['button_CONFIRM']='ПОДТВЕРДИТЬ';
$lang['button_SEND']='ОТПРАВИТЬ';
$lang['button_OUT']='СНЯТЬ';
$lang['button_PAY']='ПОПОЛНИТЬ';
$lang['button_COPY']='КОПИРВОАТЬ';
$lang['button_REAL']='ИГРАТЬ';
$lang['button_DEMO']='ДЕМО';

// Уголок прямоугольный

$lang['corner_OTHER_GAME']='ДРУГИЕ ИГРЫ';

// Блоки с права block и слайдер и соц. кнопки

$lang['block_WIN_NOW']='WIN NOW';
$lang['block_WIN_BEST']='BEST WINNINGS';
$lang['block_CONTACT']='Suport';
$lang['block_phone']='mobile';
$lang['block_email']='e-mail';
$lang['block_icq']='skype';
$lang['block_support']='suport';
$lang['block_write_to_us']='Write to us';
$lang['block_RATING']='RATING';
$lang['block_Rating_stars']='RATING';
$lang['block_gamers']='player';
$lang['block_bet']='bet';

// Меню с низу menu

$lang['menu_ABOUT']='О НАС';
$lang['menu_guaranty']='Гарантии';
$lang['menu_privacy']='Конфиденциальность';
$lang['menu_bonuses']='Бонусная политика';
$lang['menu_paymethods']='Платежные системы';
$lang['menu_license']='Лицензия';
$lang['menu_conditions']='Соглашение';
$lang['menu_YOUR_MENU']='ВАШЕ МЕНЮ';
$lang['menu_profile']='PROFILE';
$lang['menu_history']='Statistics';
$lang['menu_out']='Вывести';
$lang['menu_enter']='Пополнить';
$lang['menu_partner']='Партнерам';
$lang['menu_set_bonus']='Получить бонус';
$lang['menu_OPTIONAL']='ДОПОЛНИТЕЛЬНО';
$lang['menu_faq']='Помощь';
$lang['menu_news']='Новости';
$lang['menu_jp']='Джекпот';
$lang['menu_return']='Кэшбэк';

//платежки

$lang['pay']['success']='платеж завершен успешно';
$lang['pay']['fail']='платеж не завершен';
$lang['pay']['not_found']='платеж не найден';
$lang['pay']['fail_sign']='ошибка цифровой подписи';
$lang['pay']['error']='платеж завершился с ошибкой';



////////////////////////////////////////////////////////////////////////////
/////////////////////////////// АДМИН ПАНЕЛЬ ///////////////////////////////
////////////////////////////////////////////////////////////////////////////



// админ панель - Шапка

$lang['admin_head_hello']='Привет';
$lang['admin_head_ip']='IP';
$lang['admin_head_group']='Ваша группа';

// админ панель - группы пользователей

$lang['user_group'][1]='Директор';
$lang['user_group'][2]='Дилер';
$lang['user_group'][3]='Менеджер';
$lang['user_group'][4]='Кассир';
$lang['user_group'][5]='player';

// админ панель - главное меню

$lang['adminmenu']['users']='Игровые аккаунты';
$lang['adminmenu']['jp']='Джек Пот';
$lang['adminmenu']['game']='Настройка игр';
$lang['adminmenu']['settings']='Настройка системы';
$lang['adminmenu']['report']='Отчеты';
$lang['adminmenu']['game_log']='Логи игр';
$lang['adminmenu']['pin']='ПИН код';
$lang['adminmenu']['email']='Шаблон e-mail';
$lang['adminmenu']['sms']='Шаблон смс';
$lang['adminmenu']['mailing']='Рассылка сообщений';
$lang['adminmenu']['notify']='Мини сообщения';
$lang['adminmenu']['edit']='Обработка выплаты';
$lang['adminmenu']['news']='Новости';
$lang['adminmenu']['pages']='Страницы';
$lang['adminmenu']['articles']='Статьи';
$lang['adminmenu']['tournament']='Турниры';
$lang['adminmenu']['links']='Внешние системы';
$lang['adminmenu']['exit']='OUTPUT';
$lang['adminmenu']['outpay_detail']='Исходящий платеж';
$lang['adminmenu']['edit_content']='Редактировать раздел';
$lang['adminmenu']['add_page']='Добавить раздел';
$lang['adminmenu']['add_article']='Добавить статью';
$lang['adminmenu']['edit_news']='Редактировать новости';

// админ панель - репорт меню

$lang['reportmenu'][1]='Общие данные';
$lang['reportmenu'][2]='Выплаты';
$lang['reportmenu'][5]='Активные playerи';
$lang['reportmenu'][6]='История playerов';
$lang['reportmenu'][7]='Логирование игр';
$lang['reportmenu'][10]='Возвраты';
$lang['reportmenu'][11]='Лог настроек';
$lang['reportmenu'][13]='Платежи';
$lang['reportmenu'][14]='Лото тиражи';
$lang['reportmenu'][15]='Лото ставки';

// админ панель - субменю настроек

$lang['settings_group'][1]='Общие настройки';
$lang['settings_group'][2]='Оплата и бонусы';
$lang['settings_group'][3]='Процент отдачи';
$lang['settings_group'][4]='Интерфейс игр';
$lang['settings_group'][5]='Ввод и вывод';
$lang['settings_group'][7]='Бонусы playerам';
$lang['settings_group'][8]='Statistics';
$lang['settings_group'][9]='Suport';
$lang['settings_group'][13]='pin codes';
$lang['settings_group'][14]='СМС настройки';
$lang['settings_group'][15]='ПС FREE-KASSA';
$lang['settings_group'][11]='Настройка FK';
$lang['settings_group'][16]='СМТП настройки';
$lang['settings_group'][17]='СМТП настройки';
$lang['settings_group']['returns']='Возвраты playerам';
$lang['settings_group']['other_settings']='Разные настройки';
$lang['settings_group']['gamers_rating']='RATING playerов';

// админ панель - настройки RATINGи

$lang['settings_raiting_title']='RATING playerов';
$lang['settings_raiting_th1']='Уровень';
$lang['settings_raiting_th2']='Название';
$lang['settings_raiting_th3']='Сумма депозита';
$lang['settings_raiting_th4']='Цвет';
$lang['settings_raiting_th5']='Картинка';
$lang['settings_raiting_th6']='Курс обмена';
$lang['settings_raiting_th7']='Действие';
$lang['settings_raiting_add']='Добавить RATING';
$lang['settings_raiting_edit']='Редактировать RATING';
$lang['settings_raiting_name']='Название';
$lang['settings_raiting_deposit']='Сумма депозита';
$lang['settings_raiting_course']='Курс обмена';
$lang['settings_raiting_color']='Цвет';
$lang['settings_raiting_pic']='Картинка';

// админ панель - настройки

$lang['settings_title']['cas_name']='Название казино';
$lang['settings_title']['url']='URL или IP';
$lang['settings_title']['num']='Количество выводимых данных на странице';
$lang['settings_title']['spin_koef']='Количество оплаченных спинов за 1 кредит';
$lang['settings_title']['win_koef']='Множитель гарантии, по завершению спин лимита';
$lang['settings_title']['payed_spins_fixed']='Фиксированный спин лимит';
$lang['settings_title']['payed_spins_val']='Спин лимит';
$lang['settings_title']['bank_type']='Режим работы игровых банков';
$lang['settings_title']['spin_bank_perc']='Процент СБ';
$lang['settings_title']['bonus_bank_perc']='Процент ББ';
$lang['settings_title']['double_bank_perc']='Процент ДБ';
$lang['settings_title']['online_timeout']='Держать playerа в онлайне, при простое (мин)';
$lang['settings_title']['refresh_timeout']='Обнолвение данных (мин)';
$lang['settings_title']['denomination']='Деноминация';
$lang['settings_title']['debug']='Записывать логи';
$lang['settings_title']['ref_perc']='Процент рефералам';
$lang['settings_title']['use_ulogin']='Использовать авторизацию <b>uLogin.ru</b>';
$lang['settings_title']['ulogin_block']='Кнопки авторизации <b>uLogin.ru</b>';
$lang['settings_title']['use_captcha']='Использовать капчу';
$lang['settings_title']['activate_mail']='Требовать активацию e-mail';
$lang['settings_title']['activate_phone']='Требовать активацию телефона';
$lang['settings_title']['reg_ip_check']='Количество регистраций с одного IP адреса';
$lang['settings_title']['is_block']='Заблокировать казино';
$lang['settings_title']['block_reason']='Причина блокировки казино';
$lang['settings_title']['game_use_logo']='Использовать логотип при загрузке игр';
$lang['settings_title']['game_use_bg']='Использовать свой фон при загрузке игр';
$lang['settings_title']['game_use_fullscreen']='Показывать кнопку FULL SCREEN в играх';
$lang['settings_title']['game_use_sound']='Показывать кнопку SOUND в играх';
$lang['settings_title']['sms_use_sms']='Использовать СМС информирование';
$lang['settings_title']['sms_https_login']='Логин для протокола авторизации';
$lang['settings_title']['sms_https_password']='Пароль для протокола авторизации';
$lang['settings_title']['fk_use']='Использовать ПС FREE-KASSA';
$lang['settings_title']['fk_merchant_id']='ИД магазина';
$lang['settings_title']['fk_merchant_key']='Секретное слово 1';
$lang['settings_title']['fk_merchant_key2']='Секретное слово 2';
$lang['settings_title']['pin_use']='Использовать ПИН код';
$lang['settings_title']['allow_outpay']='Разрешить вывод средств';
$lang['settings_title']['monthly_outpay']='Запрещено выводить без депозита в текущем месяце';
$lang['settings_title']['allow_wmr']='Разрешить вывод средств на WMR';
$lang['settings_title']['allow_qiwi']='Разрешить вывод средств на QIWI';
$lang['settings_title']['outpay_tax_perc']='Процент удержания с суммы вывода';
$lang['settings_title']['outpay_tax_sum']='Сумма удержания с суммы вывода';
$lang['settings_title']['minout']='Минимальная сумма для вывода';
$lang['settings_title']['maxout']='Максимальная сумма для вывода';
$lang['settings_title']['enter_from']='Минимальная сумма для пополнения';
$lang['settings_title']['enter_to']='Максимальная сумма для пополнения';
$lang['settings_title']['bonus_from']='Минимальный бонус';
$lang['settings_title']['bonus_to']='Максимальный бонус';
$lang['settings_title']['bonus_daily']='Ежедневный бонус';
$lang['settings_title']['bonus_limit']='Бонус лимит - ежедневный';
$lang['settings_title']['bonus_check_ip']='Ограничение выдачи бонусов по IP';
$lang['settings_title']['dep_bonus_limit']='Бонус лимит - депозитный';
$lang['settings_title']['fdep_bonus']='Бонус на первое пополнение';
$lang['settings_title']['fdep_bonus_val']='Бонус на первое пополнение, в процентах';
$lang['settings_title']['dep_bonus_1']='Бонус на второе пополнение, в процентах';
$lang['settings_title']['dep_bonus_2']='Бонус на второе пополнение, в процентах';
$lang['settings_title']['dep_bonus_3']='Бонус на второе пополнение, в процентах';
$lang['settings_title']['reg_bon']='Бонус за регистрацию';
$lang['settings_title']['reg_bon_ref']='Бонус за регистрацию реферала';
$lang['settings_title']['act_bon_ref']='Бонус за активацию реферала';
$lang['settings_title']['act_bon']='Бонус за активацию e-mail и телефона';
$lang['settings_title']['botstat_timeout']='Обновление данных БОТ статистики (мс)';
$lang['settings_title']['botstat_logins']='Логины для отображения в статистике';
$lang['settings_title']['botstat_stav']='Возможные ставки в статистике';
$lang['settings_title']['botstat_win']='Возможные выигрыши в статистике';
$lang['settings_title']['contact_phone']='Телефон';
$lang['settings_title']['contact_mail']='E-mail';
$lang['settings_title']['contact_icq']='ICQ';
$lang['settings_title']['adm_email']='Системный e-mail';
$lang['settings_title']['mail_from']='От кого';
$lang['settings_title']['mail_reply']='Ответить';
$lang['settings_title']['mail_type']='Метод отправки';
$lang['settings_title']['mail_count']='Сообщений за соединение';
$lang['settings_title']['mail_period']='Лимит времени';
$lang['settings_title']['mail_period_count']='Лимит сообщений';
$lang['settings_title']['mail_smtp_host']='Хост SMTP';
$lang['settings_title']['mail_smtp_port']='Порт SMTP';
$lang['settings_title']['mail_smtp_auth']='SMTP авторизация';
$lang['settings_title']['mail_smtp_user']='Логин SMTP';
$lang['settings_title']['mail_smtp_pass']='Пароль SMTP';
$lang['settings_title']['returns_min']='Минимальная сумма пополнения';
$lang['settings_title']['returns_max']='Максимальная сумма пополнения';
$lang['settings_title']['returns_percent']='Процент возврата';
$lang['settings_title']['returns_percent_room']='Процент отчисления с каждой ставки';
$lang['settings_title']['returns_balance_room']='Баланс возврата';
$lang['settings_title']['returns_balans_finish']='Максимальный баланс возврата';
$lang['settings_title']['returns_winners_room']='Победитель';
$lang['settings_title']['default_lang']='Язык по дефолту, ru или us';
$lang['settings_title']['bets']='Возможные ставки';
$lang['settings_title']['use_gamer_raiting']='RATING playerов';
$lang['settings_title']['game_block_count']='Количество игр в первом блоке';
$lang['settings_title']['hide_news_date']='Спрятать дату новости';
$lang['settings_title']['points_pay']='Поинты за пополнение';
$lang['settings_title']['mail_text_type']='Тело письма';

//форма авторизации

$lang['adm_login_title']='Cистема управления пользователями';
$lang['login']='Логин';
$lang['pass']='Пароль';
$lang['adm_form_enter']='Войти';
$lang['enter_login_pass']='Введите логин / пароль';
$lang['enterCor_login_pass']='Введите правильный логин / пароль';
$lang['adm_not_access']='Вам не доступна данная панель управления';
$lang['page_not_access']='У Вас нет доступа к этой странице';
$lang['not_auth']='Необходимо авторизоваться';

// админ панель - users

$lang['adm_users_head']='playerи и кассиры';
$lang['adm_users_login']='Логин';
$lang['adm_users_jp']='Джек Пот';
$lang['adm_users_balance']='Баланс';
$lang['adm_users_plus']='Пополнить';
$lang['adm_users_minus']='Снять';
$lang['adm_users_points']='Поинты';
$lang['adm_users_status']='Статус';
$lang['adm_users_spin']='Спин';
$lang['adm_users_guarantee']='Гарантия';
$lang['adm_users_limit']='Лимит';
$lang['adm_users_m_activ']='П';
$lang['adm_users_p_activ']='Т';
$lang['adm_users_block']='Б';
$lang['adm_users_del']='У';
$lang['adm_users_search']='поиск';
$lang['adm_users_search_input']='логин, ип, e-mail, телефон';
$lang['adm_users_edit_title']='Редактирвоание playerа';
$lang['adm_users_add_title']='Добавить playerа или кассира';
$lang['adm_users_add_login']='Логин';
$lang['adm_users_add_pass']='Пароль';
$lang['adm_users_add_mail']='E-mail';
$lang['adm_users_add_phone']='Телефон';
$lang['adm_users_add_wmr']='WebMoney WMR';
$lang['adm_users_add_cashier']='Кассир';
$lang['adm_users_add_cancel']='ОТМЕНА';
$lang['adm_users_edit_head']='Редактирвоать баланс';
$lang['adm_users_edit_amount']='Сумма';
$lang['adm_users_edit_select']='Выберите';
$lang['adm_users_edit_cancel']='ОТМЕНА';
$lang['adm_users_del_head']='Удалить пользователя?';
$lang['adm_users_del_body']='Пользователь и вся информация по нему будут удалены. Вы уверены?';
$lang['adm_users_yes']='Да';
$lang['adm_users_no']='Нет';
$lang['adm_users_bots']='Боты';

$lang['adm_users_action'][0]='na';
$lang['adm_users_action'][1]='ЛК';
$lang['adm_users_action'][2]='игра';
$lang['adm_users_action'][3]='заморожен';
$lang['adm_users_enter_sum']='Введите сумму';

// админ панель - jp

$lang['adm_jp_head_1']='Джек Пот деноминация';
$lang['adm_jp_real_sum']='Показывать реальную сумму Джек Пота';
$lang['adm_jp_head_2']='Джек Пот';
$lang['adm_jp_title']='Пополнить Джек Пот';
$lang['adm_jp_amount']='Сумма';
$lang['adm_jp_select']='Выберите';
$lang['adm_jp_cancel']='ОТМЕНА';
$lang['adm_jp_edit_title']='Редактирвоать Джек Пот';
$lang['adm_jp_edit_name']='Название';
$lang['adm_jp_edit_win']='Сумма выпадения';
$lang['adm_jp_edit_percent']='Процент';
$lang['adm_jp_edit_win_chance']='Пред. выпадение';
$lang['adm_jp_edit_spin']='Номера спина';
$lang['adm_jp_date']='Дата';
$lang['adm_jp_logs']='Логи';
$lang['adm_jp_balance']='Баланс';
$lang['adm_jp_win']='Сумма выпадения';
$lang['adm_jp_percent']='Процент';
$lang['adm_jp_win_chance']='Пред. выпадение';
$lang['adm_jp_spin']='Номер спина';
$lang['adm_jp_win_player']='player';
$lang['adm_jp_deposit']='Пополнить';
$lang['adm_jp_edit']='Редактирвоать';
$lang['adm_jp_head_log']='Логи Джек Пота';
$lang['adm_jp_log_date']='Дата';
$lang['adm_jp_log_login']='Логин';
$lang['adm_jp_log_log']='Логи';
$lang['adm_jp_no_records']='нет данных';
$lang['adm_jp_balanceUp']='баланс пополнен на сумму';
$lang['adm_jp_editmsg']='Отредактирован <b>%jack_name%</b> сумма выплаты <b>%jack_sum%</b>, процент отчислений <b>%jack_percent%</b>';

// админ панель - 

$lang['adm_pages_tableHead_1']='Название';
$lang['adm_pages_tableHead_2']='URL';
$lang['adm_pages_tableHead_3']='Действие';
$lang['adm_add_page_title']='Создание статической страницы';
$lang['adm_pages_title']='Создание и редактирование страниц';
$lang['adm_add_page_tableHead_1']='Ссылка';
$lang['adm_add_page_tableHead_2']='Заголовок';
$lang['adm_add_page_tableHead_3']='Подзаголовок';
$lang['adm_add_page_tableHead_4']='Ключевые слова';
$lang['adm_add_page_tableHead_5']='Описание страницы';
$lang['adm_edit_content_title']='Создание статической страницы';
$lang['adm_edit_content_tableHead_1']='Заголовок';
$lang['adm_edit_content_tableHead_2']='Подзаголовок';
$lang['adm_edit_content_tableHead_3']='Ключевые слова';
$lang['adm_edit_content_tableHead_4']='Описание страницы';
$lang['adm_pages_msg_1']='Не указана страница';
$lang['adm_pages_msg_2']='Страница удалена';

// админ панель - 

$lang['adm_news_title']='Создание и редактирование новостей';
$lang['adm_news_tableHead_1']='Заголовок';
$lang['adm_news_tableHead_2']='Ключевые слова';
$lang['adm_news_tableHead_3']='Описание страницы';
$lang['adm_news_yes']='Да';
$lang['adm_news_no']='Нет';
$lang['adm_news_modalTitle']='Удалить новость';
$lang['adm_news_modalBody']='Новость будет удалена. Вы уверены?';

// админ панель - 

$lang['adm_edit_news_title']='Создание и редактирование новостей';
$lang['adm_edit_news_tableHead_1']='Заголовок';
$lang['adm_edit_news_tableHead_2']='Ключевые слова';
$lang['adm_edit_news_tableHead_3']='Описание страницы';

// админ панель - 

$lang['adm_mailing_title']='Рассылка сообщений';
$lang['adm_mailing_tableHead_1']='Заголовок';
$lang['adm_mailing_title1']='Отправленные сообщения';
$lang['adm_mailing_table1Head_1']='Название';
$lang['adm_mailing_table1Head_2']='Дата отправки';
$lang['adm_mailing_table1Head_3']='Писем в рассылке';
$lang['adm_mailing_table1Head_4']='Действие';
$lang['adm_mailing_noMsg']='сообщения еще не отправлялись';
$lang['adm_mailing_test']='Тест';

// админ панель - 

$lang['adm_sms_phone']='Телефон';
$lang['adm_sms_txt']='Текст';
$lang['adm_sms_title1']='registration';
$lang['adm_sms_title2']='Пополнение';
$lang['adm_sms_title3']='Выплата';

// админ панель - 

$lang['adm_email_title']='Заголовок';

// админ панель - 

$lang['adm_email_title1']='registration';
$lang['adm_email_title2']='Новый пароль';
$lang['adm_email_title4']='Пополнение';
$lang['adm_email_title5']='Счёт оплачен';
$lang['adm_email_title7']='Отказ в выплате';
$lang['adm_email_title8']='Активация аккаунта';
$lang['adm_email_title9']='Поддержка';
$lang['adm_email_title10']='Старт турнира';
$lang['adm_email_title11']='Завершение турнира';

// админ панель - 

$lang['adm_pin_title']='Генератор ПИН кодов';
$lang['adm_pin_tableHead_1']='Количество';
$lang['adm_pin_tableHead_2']='Номинал';
$lang['adm_pin_btn_1']='Генерировать';
$lang['adm_pin_title1']='Statistics ПИН кодов';
$lang['adm_pin_table1Head_1']='Сгенерировано ПИН кодов';
$lang['adm_pin_table1Head_2']='Номинал';
$lang['adm_pin_btn_2']='Выгрузить';
$lang['adm_pin_title2']='pin codes';
$lang['adm_pin_table2Head_1']='ПИН код';
$lang['adm_pin_table2Head_2']='Сумма';
$lang['adm_pin_table2Head_3']='Дата';
$lang['adm_pin_table2Head_4']='Статус';
$lang['adm_pin_noPin']='ПИН кодов нет';
$lang['adm_pin_status1']='Нет';
$lang['adm_pin_status2']='Нет';
$lang['adm_pin_table3Head_1']='ID';
$lang['adm_pin_table3Head_2']='Пароль';
$lang['adm_pin_table3Head_3']='ПИН код';
$lang['adm_pin_table3Head_4']='Время создания';
$lang['adm_pin_table3Head_5']='Сумма';
$lang['adm_pin_table3Head_6']='Статус';
$lang['adm_pin_table3Head_7']='Время активации';
$lang['adm_pin_noScratch']='Скретч-карт пока нет';
$lang['adm_pin_status_0']='Нет';
$lang['adm_pin_status_1']='Нет';
$lang['adm_pin_status_2']='player';
$lang['adm_pin_status_3']='Пин';

// админ панель - 

$lang['adm_report_filter']='Фильтр';
$lang['adm_report_date']='Дата';
$lang['adm_report_time']='Время';
$lang['adm_report_KAS']='КАССА';
$lang['adm_report_INCOME']='ДОХОД';
$lang['adm_report_receipt']='Приход';
$lang['adm_report_outcome']='Расход';
$lang['adm_report_rest']='Остаток';
$lang['adm_report_income']='Доход';
$lang['adm_report_np']='НП';
$lang['adm_report_login']='Логин';
$lang['adm_report_ps']='ПС';
$lang['adm_report_paysys']='Платежная система';
$lang['adm_report_bill']='Счет';
$lang['adm_report_sum']='Сумма';
$lang['adm_report_status']='Состояние';
$lang['adm_report_gamer']='player';
$lang['adm_report_regtime']='registration';
$lang['adm_report_authtime']='Авторизация';
$lang['adm_report_balance']='Баланс';
$lang['adm_report_bonus']='Бонусные';
$lang['adm_report_title']='Название';
$lang['adm_report_data']='Данные';
$lang['adm_report_os']='ОС';
$lang['adm_report_useragent']='Юзерагент регистрации';
$lang['adm_report_useragent1']='Юзерагент авторизаций';
$lang['adm_report_userip']='ИП регистрации';
$lang['adm_report_action']='Действие';
$lang['adm_report_inpay']='пополнение';
$lang['adm_report_outpay']='снятие';
$lang['adm_report_game']='Игра';
$lang['adm_report_bet']='bet';
$lang['adm_report_win']='Выигрыш';
$lang['adm_report_denom']='Деном.';
$lang['adm_report_gamebalance']='Баланс игры';
$lang['adm_report_return']='Возврат';
$lang['adm_report_setting']='Настройка';
$lang['adm_report_old']='Было';
$lang['adm_report_new']='Стало';
$lang['adm_report_inv']='Инвойс';
$lang['adm_report_type']='Тип';
$lang['adm_report_lotodraw']='Тираж';
$lang['adm_report_sumbet']='Сумма ставок';
$lang['adm_report_sumwin']='Сумма выигрышей';
$lang['adm_report_winballs']='Совпавших шаров';
$lang['adm_report_coef']='Коэффициент';
$lang['adm_report_no_data']='нет данных';

$lang['adm_outpay_detail_invoice']='Инвойс';
$lang['adm_outpay_detail_title']='Последние пополнения playerа';
$lang['adm_outpay_detail_table1Head_1']='Дата';
$lang['adm_outpay_detail_table1Head_2']='Сумма';
$lang['adm_outpay_detail_table1Head_3']='ПС';
$lang['adm_outpay_detail_table1Head_4']='Инвойс';
$lang['adm_outpay_detail_table1Head_5']='Статус';
$lang['adm_outpay_detail_title2']='Последние сыгранные игры';
$lang['adm_outpay_detail_table2Head_1']='Дата';
$lang['adm_outpay_detail_table2Head_2']='Баланс';
$lang['adm_outpay_detail_table2Head_3']='bet';
$lang['adm_outpay_detail_table2Head_4']='Выигрыш';
$lang['adm_outpay_detail_table2Head_5']='Игра';
$lang['adm_outpay_detail_noData']='данных нет';
$lang['adm_outpay_detail_cancel']='Отменить';
$lang['adm_outpay_detail_confirm']='Выплачено';
$lang['adm_outpay_detail_yes']='Да';
$lang['adm_outpay_detail_no']='Нет';
$lang['adm_outpay_detail_popupTitle']='Отменить платеж';
$lang['adm_outpay_detail_popupBody']='Платеж будет отменен. Вы уверены?';
$lang['adm_outpay_detail_popupTitle2']='Подтвердите платеж';
$lang['adm_outpay_detail_popupBody2']='Платеж будет выплачен. Вы уверены?';

$lang['adm_game_title']='Игровые банки';
$lang['adm_game_table1Head_1']='Мин. bet';
$lang['adm_game_table1Head_2']='Макс. bet';
$lang['adm_game_table1Head_3']='СБ';
$lang['adm_game_table1Head_4']='ББ';
$lang['adm_game_table1Head_5']='НАСТОЛЬНЫЙ БАНК';
$lang['adm_game_table1Head_6']='ЛОТО БАНК';
$lang['adm_game_table1Head_7']='ТРАФИК БАНК';
$lang['adm_game_title2']='не проигранный остаток пополнений';
$lang['adm_game_title3']='Гарантия выигрышей';
$lang['adm_game_table3Head_1']='Спин гарантия';
$lang['adm_game_table3Head_2']='Бонус гарантия';
$lang['adm_game_title4']='Игры';
$lang['adm_game_table4Head_1']='Игра';
$lang['adm_game_table4Head_2']='Описание';
$lang['adm_game_table4Head_3']='Действие';
$lang['adm_game_title5']='Изменение банка';
$lang['adm_game_title6']='Введите сумму';
$lang['adm_game_title7']='или выберите';
$lang['adm_game_cancel']='Отмена';
$lang['adm_game_off']='Выключить';
$lang['adm_game_on']='Включить';
$lang['adm_game_spincell']='спин-ячейка';
$lang['adm_game_bonuscell']='бонус-ячейка';
$lang['adm_game_balance']='баланс';
$lang['adm_game_cellwin']='слив';

$lang['adm_game_name']='Название игры';
$lang['adm_game_reels']='Барабаны';
$lang['adm_game_lines']='Линии';
$lang['adm_game_bonus']='Бонусы';
$lang['adm_game_freespins']='Фриспины';
$lang['adm_game_double']='Удвоение';
$lang['adm_game_in']='Вошло';
$lang['adm_game_out']='Вышло';
$lang['adm_game_total']='Итого';
$lang['adm_game_action']='Действие';

$lang['adm_game_desc_lines']='линий';
$lang['adm_game_desc_line']='линия';
$lang['adm_game_desc_freespin']='бесплатные фриспины с умножением';
$lang['adm_game_desc_freespin_']='бесплатные фриспины';
$lang['adm_game_desc_superbonus']='бонус и супер бонус игра';
$lang['adm_game_desc_superbonus_']='бонуса и супер бонус игра';
$lang['adm_game_desc_helmet']='Имеется каска';
$lang['adm_game_desc_fire']='Имеется огнетушитель';
$lang['adm_game_desc_bonus']='бонус игра';
$lang['adm_game_desc_bonus_']='бонус игры';
$lang['adm_game_desc_umbrella']='Имеется зонтик';
$lang['adm_game_desc_reels']='барабанов';
$lang['adm_game_desc_jocker']='Джокер';
$lang['adm_game_desc_caribpoker']='Карбиский покер, однопользовательский';
$lang['adm_game_desc_blackjack']='Блэк джек на 7 рукавов';
$lang['adm_game_desc_roulette']='Однопользовательская рулетка с зеро';

$lang['adm_msg_1']='заполните все поля';
$lang['adm_msg_2']='в ссылке разрешены только латинские символы a - z и цифры 0 - 9';
$lang['adm_msg_3']='в ссылке запрещены спецсимволы';
$lang['adm_msg_4']='данная ссылка уже существует';
$lang['adm_msg_5']='страница';
$lang['adm_msg_6']='создана';
$lang['adm_msg_7']='установите права на запись, файл cfg.php - chmod 666';
$lang['adm_msg_8']='нет прав на запись, в корневой директории';
$lang['adm_msg_9']='отключите SAFE MODE на сервере';
$lang['adm_msg_10']='изменения сохранены';
$lang['adm_msg_11']='введите заголовок';
$lang['adm_msg_12']='введите текст';
$lang['adm_msg_13']='данные сохранены';
$lang['adm_msg_14']='ошибка MySql';
$lang['adm_msg_15']='раздел недоступен';
$lang['adm_msg_16']='откройте смену';
$lang['adm_msg_17']='настройки банков сохранены';
$lang['adm_msg_18']='настройки гарантии сохранены';
$lang['adm_msg_19']='создайте зал';
$lang['adm_msg_20']='Банк изменен, новый баланс';
$lang['adm_msg_21']='игра добавлена в слайдер';
$lang['adm_msg_22']='игра удалена из слайдера';
$lang['adm_msg_23']='игра включена';
$lang['adm_msg_24']='игра выключена';
$lang['adm_msg_25']='введите заголовок и текст сообщения';
$lang['adm_msg_26']='новость добавлена';
$lang['adm_msg_27']='новость удалена';
$lang['adm_msg_28']='ПИН код сгенерирован';
$lang['adm_msg_29']='ошибка ввода данных';
$lang['adm_msg_30']='доступ запрещен';
$lang['adm_msg_31']='минимальная деноминация 0.01';
$lang['adm_msg_32']='группа добавлена';
$lang['adm_msg_33']='player не может быть удален';
$lang['adm_msg_34']='не указан логин';
$lang['adm_msg_35']='не указан пароль';
$lang['adm_msg_36']='не указан e-mail';
$lang['adm_msg_37']='не верный e-mail';
$lang['adm_msg_38']='player %username% создан, ПИН безопасности playerа %pin%';
$lang['adm_msg_39']='выберете другой логин';
$lang['adm_msg_40']='выберете другой e-mail';
$lang['adm_msg_41']='выберете другой телефон';
$lang['adm_msg_42']='группа недоступна';
$lang['adm_msg_43']='комментарий добавлен';
$lang['adm_msg_44']='назначен Джек Пот playerу';
$lang['adm_msg_45']='не хватает параметров';
$lang['adm_msg_46']='отменен Джек Пота playerа';
$lang['adm_msg_47']='Пользователь заблокирован';
$lang['adm_msg_48']='Пользователь разблокирован';
$lang['adm_msg_49']='спин сброшен';
$lang['adm_msg_50']='лимит спинов изменен';
$lang['adm_msg_51']='гарантия изменена';
$lang['adm_msg_52']='баланс пополнен на сумму';
$lang['adm_msg_53']='с баланса списана сумма ';
$lang['adm_msg_54']='нет данных';
$lang['adm_msg_55']='Турнир добавлен';
$lang['adm_msg_56']='Турнир отредактирован';

$lang['adm_popup_cancel']='Отмена';

// Копирайт

$lang['copyright']='casinofull';

$lang['gift_error']='при регистрации вы выбрали другой бонус';

$lang['menu']='Меню';

$lang['adm_edit_game_title']='Редактирование текстов игры';
$lang['adm_edit_game_tableHead_1']='Заголовок';
$lang['adm_edit_game_tableHead_2']='Описание';
$lang['adm_edit_game_tableHead_3']='Ключевики';
$lang['adm_edit_game_tableHead_4']='Поле ввода текста';

//импорт юзеров

$lang['adm_import_title']='Импорт пользователей из текстового файла';
$lang['adm_import_file']='Файл';
$lang['adm_import_file_format']='Формат файла';
$lang['adm_import_browse']='Выбрать';
$lang['adm_import_balance']='Баланс';
$lang['adm_import_balance_bonus']='Баланс(бонус)';
$lang['adm_import_wager']='Вейджер';
$lang['adm_import_points']='Поинты';
$lang['adm_import_mailing']='Разослать емайлы добавленным юзерам';
$lang['adm_import_msg']='Добавлено %num% пользователей';

//adm/game_log

$lang['adm_log_title']='Логи игр';
$lang['adm_log_username']='Логин playerа';
$lang['adm_log_game']='Игра';
$lang['adm_log_date']='Дата';
$lang['adm_log_strings']='Строки лога';
$lang['adm_log_flash']='Flash-окно';
$lang['adm_log_view']='Просмотр';
$lang['adm_log_loading']='Загружаем';
$lang['adm_log_select_string']='Нужно выбрать строки';

// lobby

$lang['lobby_exit']='OUTPUT';
$lang['lobby_payment']='КАССА';
$lang['lobby_pay_label']='КАССА';
$lang['lobby_pay_collect']='ПОЛУЧИТЬ';
$lang['lobby_pay_add']='ПОПОЛНИТЬ';
$lang['lobby_pay_pin_accept']='Пин-код принят! Ваш баланс пополнен на ';
$lang['lobby_pay_collect_wnd_lbl']='Вывод средств';
$lang['lobby_pay_collect_wnd_text']='Для выдачи нажмите OK. Сумма снятия указана без деноминации !';
$lang['lobby_pay_in_wnd_lbl']='Пополнение баланса';
$lang['lobby_pay_in_wnd_text']='Введите пин код для пополнения баланса';
$lang['lobby_pay_in_wnd_text2']='Введите сумму для пополнения баланса';
$lang['lobby_set_label']='НАСТРОЙКИ';
$lang['lobby_set_main']='ОСНОВНЫЕ';
$lang['lobby_set_save']='Настройки сохранены';
$lang['lobby_set_lang_wnd_lbl']='Язык интерфейса';
$lang['lobby_set_lang_wnd_text']='Выберите язык интерфейса';
$lang['lobby_set_denom_wnd_lbl']='Деноминация';
$lang['lobby_set_denom_wnd_text']='Выберите деноминацию!';

?>