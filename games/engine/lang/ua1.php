<?php

// GOLDSVET 5.1 - максимальная

// Игровая часть 30.10.2016

// для не авторизованных

$lang['err_no_access']='вам необхідно авторизуватись для доступу до цієї сторінки';

// registration

$lang['err_all_input']='вам необхідно заповнити всі поля цієї форми';
$lang['err_pass2']='пароль і підтвердження не співпадають';
$lang['err_mail']='введіть e-mail адресу';
$lang['err_mail_preg_match']='введіть правильну e-mail адресу';
$lang['err_in_login']='ви вже авторизовані';
$lang['err_login_3_20']='логін має містити 3-20 символів';
$lang['err_rules']='ви маєте погодитися з правилами системи';
$lang['err_mail_30']='e-mail має містити до 30 символів';
$lang['err_wmr_13']='гаманець має містити 13 символів';
$lang['err_wmr_r']='гаманець має починатись з літери R';
$lang['err_captcha']='код безпеки введено не вірно';
$lang['err_login_duble']='оберіть інший логін';
$lang['err_wmr_duble']='введіть інший гаманець';
$lang['err_mail_duble']='введіть інший e-mail';
$lang['err_no_reg_ip']='з вашого IP вже зареєструвались';

// login

$lang['err_login_pass']='невірний логін або пароль';
$lang['err_no_login']='не вказано логін';
$lang['err_no_pass']='не вказано пароль';
$lang['err_no_login_no_pass']='не вказано логін та пароль';
$lang['err_auth']='цей юзер вже авторизувався';
$lang['err_block']='ваш логін заблоковано';
$lang['err_no_user']='користувача не знайдено';
$lang['err_no_real']='Гра на реальні гроші буде доступна<br />після реєстрації';

// reminder

$lang['err_in_auth']='ви вже авторизовані';
$lang['err_no_mail']='введений вами e-mail не знайдено';
$lang['err_no_send']='не вдається надіслати листа';
$lang['err_new_pass']='новий пароль вислано на ваш e-mail';

// contacts

$lang['err_no_subject']='не вказана тема';
$lang['err_in_message']='введіть повідомлення';
$lang['err_maile_server']='помилка поштового серверу';
$lang['err_send']='ваше повідомлення надіслано';

// enter

$lang['err_max_bonus']='у вас максимальна кількість бонусів';
$lang['err_bonus']='ви отримуєте бонус на суму <b>{bonus}</b> кредитів';
$lang['err_off_bonus']='на даний момент бонуси вимкнено';
$lang['err_no_bonus']='ви сьогодні вже отримали бонус';
$lang['err_no_wager']='Для отримання бонуса ввімкніть вейджер';
$lang['err_min_max_enter']='поповнення можливе від <b>%min</b> до <b>%max</b> за один раз';
$lang['err_no_enter']='отримання грошових коштів тимчасово вимкнено';
$lang['err_no_bonus_ip']='з вашого IP сьогодні вже отримано бонус';

// news

$lang['err_no_news']='цієї новини не знайдено, можливо, її вже видалено';

// out

$lang['err_no_pay']='у вас не було поповнень в цьому місяці, вивести кошти неможливо';
$lang['err_no_weger']='у вас не витрачено вейджер на суму ставок';
$lang['err_no_ps']='заповніть дані про платіжні системи у профілі';
$lang['err_activ_email']='активуйте e-mail адресу';
$lang['err_activ_phone']='активуйте телефон';
$lang['err_no_out']='вивести кошти тимчасово неможливо';
$lang['err_min_max_out']='введіть коректну суму [ <b>{min}</b> - <b>{max}</b> ]';
$lang['err_no_access_ps']='обрана ПС на даний момент не доступна';
$lang['err_no_money']='у вас недостатньо грошей на балансі';
$lang['err_in_ps']='оберіть платіжну систему';
$lang['err_yes_out']='вашу заявку прийнято і буде оброблено найближчим часом';
$lang['err_no_send_out']='не вдається відправити заявку на виведення коштів';
$lang['err_wait_out']='у вас вже є необроблена заявка на виведення коштів';

// pay

$lang['err_redirect']='через 10 секунд вас буде перенаправлено на головну сторінку';
$lang['err_yes_pin']='ваш ПІН код успішно оброблено';
$lang['err_no_pin_search']='даний ПІН код не знайдено';
$lang['err_no_pin']='ви ввели невірний ПІН код';
$lang['err_pin_block']='ви вичерпали спроби введення ПІН коду, ваш акаунт заблоковано';
$lang['err_pin_stay']='залишилось спроб введення';
$lang['pay']['userbal']='баланс гравця';
$lang['pay']['success2']='поповнено на суму';

// profile

$lang['err_email_activated']='пошту активовано';
$lang['err_code']='введно невірний код';
$lang['err_phone_activated']='телефон активовано';
$lang['err_phone_duble']='введіть інший телефон';
$lang['err_save']='ваші дані успішно збережено';
$lang['err_db']='помилка запису даних';

// games/index.php

$lang['err_game_block']='акаунт заблоковано, зверніться до служби підтримки';
$lang['err_no_access_game']='доступно гравцям';
$lang['err_no_start_game']='не знайдено стартовий файл гри';
$lang['err_no_game']='неможливо запустити гру';

// index.php

$lang['err_404']='сторінку не знайдено';
$lang['err_activate_yes']='активація пройшла успішно';
$lang['err_activate_no']='помилка активації';
$lang['err_ajax']='помилка на сервері';

// main

$lang['main_LK']='ОСОБИСТИЙ КАБІНЕТ';
$lang['main_wellcome']='Ласкаво просимо';
$lang['main_showGames']='Показати ще ігри';
$lang['main_jackpot']='Джекпот';
$lang['main_tomain']='На головну сторінку';
$lang['footer_paysys']='Приймаємо до сплати';
$lang['footer_paysys1']='+ ще більш ніж <strong>20 способів!</strong>';

// exchange

$lang['err_no_points']='недостатньо поінтів';
$lang['err_yes_points']='<b>".$paypoints."</b> поінтів обміняли на <b>$sum</b> руб.';
$lang['err_minimal_points']='повинно бути більше <b>100</b> поінтів';

// games

$lang['games_list']='список ігр';

// Страница новостей /news

$lang['news_more']='докладніше';

// Страница партнерской программы /partner

$lang['partner_REF_URL']='ВАШЕ ПАРТНЕРСЬКЕ ПОСИЛАННЯ';
$lang['partner_ref']='партнер';
$lang['partner_all_out']='всього сплачено';
$lang['partner_to_enter']='нараховано';
$lang['partner_yes_out']='готово до виплати';
$lang['partner_no_ref']='ви поки що нікого не запросили';
$lang['partner_ref_title']='запрошені вами партнери';
$lang['partner_transfer_to_balance']='перевести на гральний рахунок';

// Страница статистики /history

$lang['history_WITHDRAWL']='ЗАПИТ НА ВИВЕДЕННЯ';
$lang['history_HISTORY_PAY']='ІСТОРІЯ ПЛАТЕЖІВ';
$lang['history_HISTORY_LOGIN']='ІСТОРІЯ АКТИВНОСТІ';
$lang['history_GAME_LOG']='ОСТАННІ ІГРИ';
$lang['history_date']='дата';
$lang['history_order_amout']='сума замовлення';
$lang['history_payout']='сума виплати';
$lang['history_inv']='інвойс';
$lang['history_PS']='ПС';
$lang['history_status']='статус';
$lang['history_IP']='ІП';
$lang['history_game']='гра';
$lang['history_bet']='ставка';
$lang['history_win']='виграш';
$lang['history_no_requests']='запитів немає';
$lang['history_no_enter']='поповнень немає';
$lang['history_no_login']='авторизацій немає';
$lang['history_no_game']='статистика по іграм відсутня';
$lang['history_statuses'][0]='обробка';
$lang['history_statuses'][1]='обробка';
$lang['history_statuses'][2]='успішно';
$lang['history_statuses'][3]='повернено';
$lang['history_statuses'][4]='відмовлено';
$lang['history_ps']['wmr']='WebMoney WMR';
$lang['history_ps']['qiwi']='QIWI';

// Страница обмена поинтов /exchange

$lang['exchange_TOTAL_BALANCE_POINTS']='ПОТОЧНИЙ БАЛАНС ПОІНТІВ';
$lang['exchange_COURSE']='КУРС ОБМІНУ';
$lang['exchange_POINTS_EX']='ВІДДАТИ ПОІНТИ <BR />ДЛЯ ОБМІНУ';
$lang['exchange_EX_POINTS']='СКІЛЬКИ ВИ ОТРИМАЄТЕ';
$lang['exchange_cur']='руб.';

// Страница выплаты /out

$lang['out_SUM_IN']='СУМА ДЛЯ ВИВЕДЕННЯ <BR />ВИГРАШУ';
$lang['out_PS']='ПЛАТІЖНА СИСТЕМА';
$lang['out_ps_select']='оберіть платіжну систему';
$lang['out_QIWI']='QIWI';
$lang['out_WMR']='WebMoney WMR';

// Страница пополнения счета /enter

$lang['enter_PIN_CODE_title']='ПІН код';
$lang['enter_PIN_CODE']='ВВЕДЕННЯ <BR />ПІН КОДУ';
$lang['enter_pin_input']='введіть ПІН код';
$lang['enter_SUM_ENTER']='СУМА <BR />ПОПОВНЕННЯ';
$lang['enter_confirm']='ви поповнюєте гральний рахунок на суму';
$lang['enter_cur']='руб.';

// Страница профиля /profile

$lang['profile_AVATAR']='АВАТАР';
$lang['profile_LOGIN']='ЛОГІН';
$lang['profile_ID']='ІД';
$lang['profile_E-MAIL']='ПОШТА';
$lang['profile_CHANGE_PASS']='ЗМІНА <BR />ПАРОЛЮ';
$lang['profile_PASS']='ПАРОЛЬ';
$lang['profile_RE_PASS']='ПІДТВЕРДЖЕННЯ';
$lang['profile_YOUR_E-MAIL']='ВАША ПОШТА';
$lang['profile_PHONE_QIWI']='ТЕЛЕФОН <BR />QIWI ГАМАНЕЦЬ';
$lang['profile_WMR']='ВЕБ МАНІ ГАМАНЕЦЬ <BR />WMR';
$lang['profile_CODE']='КОД';
$lang['profile_new_pass']='введіть новий пароль';
$lang['profile_new_pass2']='повторіть введення паролю';
$lang['profile_wmr_input']='номер WMR гаманця';
$lang['profile_phone_input']='номер телефону';
$lang['profile_code_input']='код активації';
$lang['profile_email_input']='введіть вашу пошту';
$lang['profile_use_wager']='ОТРИМУВАТИ БОНУСИ';

// Блок авторизации

$lang['auth_profile']='Профіль';
$lang['auth_balance']='Баланс';
$lang['auth_wager']='Вейджер';
$lang['auth_partner']='Партнерам';
$lang['auth_history']='Статистика';
$lang['auth_cur']='руб.';
$lang['auth_bet']='ст.';
$lang['auth_guest']='Гість';
$lang['auth_exit']='Вихід';

// Попап окна popup

$lang['popup_MSG']='ПОВІДОМЛЕННЯ';
$lang['popup_PAY']='ПОПОВНЕННЯ БАЛАНСУ';
$lang['popup_OUT']='ВИВЕДЕННЯ КОШТІВ';
$lang['popup_REGISTER']='РЕЄСТРАЦІЯ';
$lang['popup_login']='Логін';
$lang['popup_pass']='Пароль';
$lang['popup_re_pass']='Повторіть пароль';
$lang['popup_email']='Пошта';
$lang['popup_qiwi']='QIWI';
$lang['popup_wmr']='WMR';
$lang['popup_yes_rules']='Погоджуюсь з <a href="/rules">правилами</a> грального клубу';
$lang['popup_ENTER']='ВХІД НА САЙТ';
$lang['popup_reminder']='Забули пароль';
$lang['popup_PASS_RECOVERY']='ВІДНОВЛЕННЯ ПАРОЛЮ';
$lang['popup_SUPPORT']='СЛУЖБА ПІДТРИМКИ';
$lang['popup_subject']='Тема повідомлення';
$lang['popup_message']='Текст повідомлення';
$lang['popup_WELCOME_BONUS_CHOICE']='ПРИВІТАЛЬНИЙ БОНУС НА ВАШ ВИБІР';
$lang['popup_gift_register']='бонус за реєстрацію,<br /> найвигідніший для новачків';
$lang['popup_GIFT_REG']='БОНУС ЗА РЕЄСТРАЦІЮ';
$lang['popup_gift_reg_txt']='моментальне зарахування';
$lang['popup_GIFT_DAY']='ЩОДЕННИЙ БОНУС';
$lang['popup_gift_day_txt']='кожного дня новий бонус';
$lang['popup_GIFT_PAY']='ПЕРШЕ ПОПОВНЕННЯ';
$lang['popup_gift_pay_txt']='подвоюй свій платіж';

// Кнопки button

$lang['button_EXCHANGE']='ОБМІНЯТИ';
$lang['button_AUTH']='АВТОРИЗАЦІЯ';
$lang['button_REGISTER']='РЕЄСТРАЦІЯ';
$lang['button_ENTER']='ВХІД';
$lang['button_EXIT']='ВИХІД';
$lang['button_PROFILE']='ПРОФІЛЬ';
$lang['button_RECOVERY']='ВІДНОВИТИ';
$lang['button_OK']='ОК';
$lang['button_SAVE']='ЗБЕРЕГТИ';
$lang['button_CONFIRM']='ПІДТВЕРДИТИ';
$lang['button_SEND']='ВІДПРАВИТИ';
$lang['button_OUT']='ЗНЯТИ';
$lang['button_PAY']='ПОПОВНИТИ';
$lang['button_COPY']='КОПІЮВАТИ';
$lang['button_REAL']='ГРАТИ';
$lang['button_DEMO']='ДЕМО';

// Уголок прямоугольный

$lang['corner_OTHER_GAME']='ІНШІ ІГРИ';

// Блоки с права block и слайдер и соц. кнопки

$lang['block_WIN_NOW']='ЗАРАЗ ВИГРАЮТЬ';
$lang['block_WIN_BEST']='НАЙКРАЩІ ВИГРАШІ';
$lang['block_CONTACT']='КОНТАКТИ';
$lang['block_phone']='телефон';
$lang['block_email']='пошта';
$lang['block_icq']='icq';
$lang['block_support']='підтримка';
$lang['block_write_to_us']='написати нам';
$lang['block_RATING']='РЕЙТИНГ';
$lang['block_Rating_stars']='Рейтинг';
$lang['block_gamers']='гравець';
$lang['block_bet']='ставка';

// Меню с низу menu

$lang['menu_ABOUT']='ПРО НАС';
$lang['menu_guaranty']='Гарантії';
$lang['menu_privacy']='Конфіденційність';
$lang['menu_bonuses']='Бонусна політика';
$lang['menu_paymethods']='Платіжні системи';
$lang['menu_license']='Ліцензія';
$lang['menu_conditions']='Погодження';
$lang['menu_YOUR_MENU']='ВАШЕ МЕНЮ';
$lang['menu_profile']='Профіль';
$lang['menu_history']='Статистика';
$lang['menu_out']='Вивести';
$lang['menu_enter']='Поповнити';
$lang['menu_partner']='Партнерам';
$lang['menu_set_bonus']='Отримати бонус';
$lang['menu_OPTIONAL']='ДОДАТКОВО';
$lang['menu_faq']='Допомога';
$lang['menu_news']='Новини';
$lang['menu_jp']='Джекпот';
$lang['menu_return']='Кешбек';

//платежки

$lang['pay']['success']='платіж завершено успішно';
$lang['pay']['fail']='платіж не завершено';
$lang['pay']['not_found']='платіж не знайдено';
$lang['pay']['fail_sign']='помилка цифрового підпису';
$lang['pay']['error']='платіж завершено з помилкою';



////////////////////////////////////////////////////////////////////////////
/////////////////////////////// АДМИН ПАНЕЛЬ ///////////////////////////////
////////////////////////////////////////////////////////////////////////////



// админ панель - Шапка

$lang['admin_head_hello']='Привіт';
$lang['admin_head_ip']='IP';
$lang['admin_head_group']='Ваша група';

// админ панель - группы пользователей

$lang['user_group'][1]='Директор';
$lang['user_group'][2]='Дилер';
$lang['user_group'][3]='Менеджер';
$lang['user_group'][4]='Касир';
$lang['user_group'][5]='Гравець';

// админ панель - главное меню

$lang['adminmenu']['users']='Гральні акаунти';
$lang['adminmenu']['jp']='Джек Пот';
$lang['adminmenu']['game']='Налаштування ігор';
$lang['adminmenu']['settings']='Налаштування системи';
$lang['adminmenu']['report']='Звіти';
$lang['adminmenu']['game_log']='Логи ігор';
$lang['adminmenu']['pin']='ПІН код';
$lang['adminmenu']['email']='Шаблон e-mail';
$lang['adminmenu']['sms']='Шаблон смс';
$lang['adminmenu']['mailing']='Розсилка повідомлень';
$lang['adminmenu']['notify']='Міні повідомлення';
$lang['adminmenu']['edit']='Обробка виплати';
$lang['adminmenu']['news']='Новини';
$lang['adminmenu']['pages']='Сторінки';
$lang['adminmenu']['articles']='Статті';
$lang['adminmenu']['tournament']='Турніри';
$lang['adminmenu']['links']='Зовнішні системи';
$lang['adminmenu']['exit']='Вихід';
$lang['adminmenu']['outpay_detail']='Вихідний платіж';
$lang['adminmenu']['edit_content']='Редагувати розділ';
$lang['adminmenu']['add_page']='Додати розділ';
$lang['adminmenu']['add_article']='Додати статтю';
$lang['adminmenu']['edit_news']='Редагувати новини';

// админ панель - репорт меню

$lang['reportmenu'][1]='Загальні дані';
$lang['reportmenu'][2]='Виплати';
$lang['reportmenu'][5]='Активні гравці';
$lang['reportmenu'][6]='Історія гравців';
$lang['reportmenu'][7]='Логування ігор';
$lang['reportmenu'][10]='Повернення';
$lang['reportmenu'][11]='Лог налаштувань';
$lang['reportmenu'][13]='Платежі';
$lang['reportmenu'][14]='Лото тиражі';
$lang['reportmenu'][15]='Лото ставки';

// админ панель - субменю настроек

$lang['settings_group'][1]='Загальні налаштування';
$lang['settings_group'][2]='Оплата і бонуси';
$lang['settings_group'][3]='Відсоток віддачі';
$lang['settings_group'][4]='Інтерфейс ігор';
$lang['settings_group'][5]='Введення и виведення';
$lang['settings_group'][7]='Бонуси гравцям';
$lang['settings_group'][8]='Бот статистика';
$lang['settings_group'][9]='Контакти';
$lang['settings_group'][13]='ПІН коди';
$lang['settings_group'][14]='СМС налаштування';
$lang['settings_group'][15]='ПС FREE-KASSA';
$lang['settings_group'][11]='Налаштування FK';
$lang['settings_group'][16]='СМТП налаштування';
$lang['settings_group'][17]='СМТП налаштування';
$lang['settings_group']['returns']='Повернення гравцям';
$lang['settings_group']['other_settings']='Різні налаштування';
$lang['settings_group']['gamers_rating']='Рейтинг гравців';

// админ панель - настройки рейтинги

$lang['settings_raiting_title']='Рейтинг гравців';
$lang['settings_raiting_th1']='Рівень';
$lang['settings_raiting_th2']='Назва';
$lang['settings_raiting_th3']='Сума депозиту';
$lang['settings_raiting_th4']='Колір';
$lang['settings_raiting_th5']='Зображення';
$lang['settings_raiting_th6']='Курс обміну';
$lang['settings_raiting_th7']='Дія';
$lang['settings_raiting_add']='Додати рейтинг';
$lang['settings_raiting_edit']='Редагувати рейтинг';
$lang['settings_raiting_name']='Назва';
$lang['settings_raiting_deposit']='Сума депозиту';
$lang['settings_raiting_course']='Курс обміну';
$lang['settings_raiting_color']='Колір';
$lang['settings_raiting_pic']='Зображення';

// админ панель - настройки

$lang['settings_title']['cas_name']='Назва казино';
$lang['settings_title']['url']='URL або IP';
$lang['settings_title']['num']='Кількість даних, що виводяться на сторінці';
$lang['settings_title']['spin_koef']='Кількість сплачених спінів за 1 кредит';
$lang['settings_title']['win_koef']='Множник гарантії за завершенням спін ліміту';
$lang['settings_title']['payed_spins_fixed']='Фиксований спін ліміт';
$lang['settings_title']['payed_spins_val']='Спін ліміт';
$lang['settings_title']['bank_type']='Режим роботи гральних банків';
$lang['settings_title']['spin_bank_perc']='Відсоток СБ';
$lang['settings_title']['bonus_bank_perc']='Відсоток ББ';
$lang['settings_title']['double_bank_perc']='Відсоток ДБ';
$lang['settings_title']['online_timeout']='Тримати гравця онлайн при простої (хв.)';
$lang['settings_title']['refresh_timeout']='Оновлкння даних (хв.)';
$lang['settings_title']['denomination']='Деномінація';
$lang['settings_title']['debug']='Записувати логи';
$lang['settings_title']['ref_perc']='Відсоток рефералам';
$lang['settings_title']['use_ulogin']='Використовувати авторизацію <b>uLogin.ru</b>';
$lang['settings_title']['ulogin_block']='Кнопки авторизації <b>uLogin.ru</b>';
$lang['settings_title']['use_captcha']='Використовувати капчу';
$lang['settings_title']['activate_mail']='Вимагати активацію e-mail';
$lang['settings_title']['activate_phone']='Вимагати активацію телефону';
$lang['settings_title']['reg_ip_check']='Кількість реєстрацій з одної IP адреси';
$lang['settings_title']['is_block']='Блокувати казино';
$lang['settings_title']['block_reason']='Причина блокування казино';
$lang['settings_title']['game_use_logo']='Використовувати логотип при завантаженні ігор';
$lang['settings_title']['game_use_bg']='Використовувати свій фон при завантаженні ігор';
$lang['settings_title']['game_use_fullscreen']='Показувати кнопку FULL SCREEN в іграх';
$lang['settings_title']['game_use_sound']='Показувати кнопку SOUND в іграх';
$lang['settings_title']['sms_use_sms']='Використовувати СМС інформування';
$lang['settings_title']['sms_https_login']='Логін для протоколу авторизації';
$lang['settings_title']['sms_https_password']='Пароль для протоколу авторизації';
$lang['settings_title']['fk_use']='Використовувати ПС FREE-KASSA';
$lang['settings_title']['fk_merchant_id']='ІД крамниці';
$lang['settings_title']['fk_merchant_key']='Таємне слово 1';
$lang['settings_title']['fk_merchant_key2']='Таємне  слово 2';
$lang['settings_title']['pin_use']='Використовувати ПІН код';
$lang['settings_title']['allow_outpay']='Дозволити виведення коштів';
$lang['settings_title']['monthly_outpay']='Заборонено виводити без депозиту в поточному місяці';
$lang['settings_title']['allow_wmr']='Дозволити виведення коштів на WMR';
$lang['settings_title']['allow_qiwi']='Дозволити виведення коштів на QIWI';
$lang['settings_title']['outpay_tax_perc']='Відсоток утримання з суми виведення';
$lang['settings_title']['outpay_tax_sum']='Сума утримання з суми виведення';
$lang['settings_title']['minout']='Мінімальна сума для виведення';
$lang['settings_title']['maxout']='Максимальна сума для виведення';
$lang['settings_title']['enter_from']='Мінімальна сума для поповнення';
$lang['settings_title']['enter_to']='Максимальна сума для поповнення';
$lang['settings_title']['bonus_from']='Мінімальний бонус';
$lang['settings_title']['bonus_to']='Максимальний бонус';
$lang['settings_title']['bonus_daily']='Щоденний бонус';
$lang['settings_title']['bonus_limit']='Бонус ліміт - щоденний';
$lang['settings_title']['bonus_check_ip']='Обмеження видавання бонусів за IP';
$lang['settings_title']['dep_bonus_limit']='Бонус ліміт - депозитний';
$lang['settings_title']['fdep_bonus']='Бонус на перше поповнення';
$lang['settings_title']['fdep_bonus_val']='Бонус на перше поповнення, у відсотках';
$lang['settings_title']['dep_bonus_1']='Бонус на друге поповнення, у відсотках';
$lang['settings_title']['dep_bonus_2']='Бонус на друге поповнення, у відсотках';
$lang['settings_title']['dep_bonus_3']='Бонус на друге поповнення, у відсотках';
$lang['settings_title']['reg_bon']='Бонус за реєстрацію';
$lang['settings_title']['reg_bon_ref']='Бонус за реєстрацію рефералу';
$lang['settings_title']['act_bon_ref']='Бонус за активацію рефералу';
$lang['settings_title']['act_bon']='Бонус за активацію e-mail і телефону';
$lang['settings_title']['botstat_timeout']='Обновлення даних БОТ статистики (мс)';
$lang['settings_title']['botstat_logins']='Логіни для відображення у статистиці';
$lang['settings_title']['botstat_stav']='Можливі ставки у статистиці';
$lang['settings_title']['botstat_win']='Можливі виграші у статистиці';
$lang['settings_title']['contact_phone']='Телефон';
$lang['settings_title']['contact_mail']='E-mail';
$lang['settings_title']['contact_icq']='ICQ';
$lang['settings_title']['adm_email']='Системний e-mail';
$lang['settings_title']['mail_from']='Від кого';
$lang['settings_title']['mail_reply']='Відповісти';
$lang['settings_title']['mail_type']='Метод відправлення';
$lang['settings_title']['mail_count']='Повідомлень за з\'єднання';
$lang['settings_title']['mail_period']='Ліміт часу';
$lang['settings_title']['mail_period_count']='Ліміт повідомлень';
$lang['settings_title']['mail_smtp_host']='Хост SMTP';
$lang['settings_title']['mail_smtp_port']='Порт SMTP';
$lang['settings_title']['mail_smtp_auth']='SMTP авторизація';
$lang['settings_title']['mail_smtp_user']='Логін SMTP';
$lang['settings_title']['mail_smtp_pass']='Пароль SMTP';
$lang['settings_title']['returns_min']='Мінімальна сума поповнення';
$lang['settings_title']['returns_max']='Максимальна сума поповнення';
$lang['settings_title']['returns_percent']='Відсоток повернення';
$lang['settings_title']['returns_percent_room']='Відсоток відрахування з кожної ставки';
$lang['settings_title']['returns_balance_room']='Баланс повернення';
$lang['settings_title']['returns_balans_finish']='Максимальний баланс повернення';
$lang['settings_title']['returns_winners_room']='Переможець';
$lang['settings_title']['default_lang']='Мова за дефолтом, ru або us';
$lang['settings_title']['bets']='Можливі ставки';
$lang['settings_title']['use_gamer_raiting']='Рейтинг гравців';
$lang['settings_title']['game_block_count']='Кількість ігор у першому блоці';
$lang['settings_title']['hide_news_date']='Сховати дату новини';
$lang['settings_title']['points_pay']='Поінти за поповнення';
$lang['settings_title']['mail_text_type']='Тіло листа';

//форма авторизации

$lang['adm_login_title']='Cистема керування користувачами';
$lang['login']='Логін';
$lang['pass']='Пароль';
$lang['adm_form_enter']='Увійти';
$lang['enter_login_pass']='Введіть логін / пароль';
$lang['enterCor_login_pass']='Введіть правильний логін / пароль';
$lang['adm_not_access']='У Вас немає доступу до цієї панелі керування';
$lang['page_not_access']='У Вас немає доступу до цієї сторінки';
$lang['not_auth']='Необхідно авторизуватися';

// админ панель - users

$lang['adm_users_head']='Гравці і касири';
$lang['adm_users_login']='Логін';
$lang['adm_users_jp']='Джек Пот';
$lang['adm_users_balance']='Баланс';
$lang['adm_users_plus']='Поповнити';
$lang['adm_users_minus']='Зняти';
$lang['adm_users_points']='Поінти';
$lang['adm_users_status']='Статус';
$lang['adm_users_spin']='Спін';
$lang['adm_users_guarantee']='Гарантія';
$lang['adm_users_limit']='Ліміт';
$lang['adm_users_m_activ']='П';
$lang['adm_users_p_activ']='Т';
$lang['adm_users_block']='Б';
$lang['adm_users_del']='В';
$lang['adm_users_search']='пошук';
$lang['adm_users_search_input']='логін, іп, e-mail, телефон';
$lang['adm_users_edit_title']='Редагування гравця';
$lang['adm_users_add_title']='Додати гравця або касира';
$lang['adm_users_add_login']='Логін';
$lang['adm_users_add_pass']='Пароль';
$lang['adm_users_add_mail']='E-mail';
$lang['adm_users_add_phone']='Телефон';
$lang['adm_users_add_wmr']='WebMoney WMR';
$lang['adm_users_add_cashier']='Касир';
$lang['adm_users_add_cancel']='СКАСУВАННЯ';
$lang['adm_users_edit_head']='Редагувати баланс';
$lang['adm_users_edit_amount']='Сума';
$lang['adm_users_edit_select']='Оберіть';
$lang['adm_users_edit_cancel']='СКАСУВАННЯ';
$lang['adm_users_del_head']='Видалити користувача?';
$lang['adm_users_del_body']='Користувача і всю інформацію щодо нього буде видалено. Ви впевнені?';
$lang['adm_users_yes']='Так';
$lang['adm_users_no']='Ні';
$lang['adm_users_bots']='Боти';

$lang['adm_users_action'][0]='na';
$lang['adm_users_action'][1]='ОК';
$lang['adm_users_action'][2]='гра';
$lang['adm_users_action'][3]='Заморожено';
$lang['adm_users_enter_sum']='Введіть суму';

// админ панель - jp

$lang['adm_jp_head_1']='Джек Пот деномінація';
$lang['adm_jp_real_sum']='Показати реальну суму Джек Поту';
$lang['adm_jp_head_2']='Джек Пот';
$lang['adm_jp_title']='Поповнити Джек Пот';
$lang['adm_jp_amount']='Сума';
$lang['adm_jp_select']='Оберіть';
$lang['adm_jp_cancel']='СКАСУВАННЯ';
$lang['adm_jp_edit_title']='Редагувати Джек Пот';
$lang['adm_jp_edit_name']='Назва';
$lang['adm_jp_edit_win']='Сума випадання';
$lang['adm_jp_edit_percent']='Відсоток';
$lang['adm_jp_edit_win_chance']='Попер. випадання';
$lang['adm_jp_edit_spin']='Номери спіну';
$lang['adm_jp_date']='Дата';
$lang['adm_jp_logs']='Логи';
$lang['adm_jp_balance']='Баланс';
$lang['adm_jp_win']='Сума випадання';
$lang['adm_jp_percent']='Відсоток';
$lang['adm_jp_win_chance']='Попер. випадання';
$lang['adm_jp_spin']='Номер спіну';
$lang['adm_jp_win_player']='Гравець';
$lang['adm_jp_deposit']='Поповнити';
$lang['adm_jp_edit']='Редагувати';
$lang['adm_jp_head_log']='Логи Джек Поту';
$lang['adm_jp_log_date']='Дата';
$lang['adm_jp_log_login']='Логін';
$lang['adm_jp_log_log']='Логи';
$lang['adm_jp_no_records']='немає даних';
$lang['adm_jp_balanceUp']='баланс поповнено на суму';
$lang['adm_jp_editmsg']='Відредагован <b>%jack_name%</b> сума виплати <b>%jack_sum%</b>, відсоток нарахувань <b>%jack_percent%</b>';

// админ панель - 

$lang['adm_pages_tableHead_1']='Назва';
$lang['adm_pages_tableHead_2']='URL';
$lang['adm_pages_tableHead_3']='Дія';
$lang['adm_add_page_title']='Створення статичної сторінки';
$lang['adm_pages_title']='Створення і редагування сторінок';
$lang['adm_add_page_tableHead_1']='Посилання';
$lang['adm_add_page_tableHead_2']='Заголовок';
$lang['adm_add_page_tableHead_3']='Підзаголовок';
$lang['adm_add_page_tableHead_4']='Ключові слова';
$lang['adm_add_page_tableHead_5']='Опис сторінки';
$lang['adm_edit_content_title']='Створення статичної сторінки';
$lang['adm_edit_content_tableHead_1']='Заголовок';
$lang['adm_edit_content_tableHead_2']='Підзаголовок';
$lang['adm_edit_content_tableHead_3']='Ключові слова';
$lang['adm_edit_content_tableHead_4']='Опис сторінки';
$lang['adm_pages_msg_1']='Не вказано сторінку';
$lang['adm_pages_msg_2']='Сторінку видалено';

// админ панель - 

$lang['adm_news_title']='Створення і редагування новин';
$lang['adm_news_tableHead_1']='Заголовок';
$lang['adm_news_tableHead_2']='Ключові слова';
$lang['adm_news_tableHead_3']='Опис сторінки';
$lang['adm_news_yes']='Так';
$lang['adm_news_no']='Ні';
$lang['adm_news_modalTitle']='Видалити новину';
$lang['adm_news_modalBody']='Новину буде видалено. Ви впевнені?';

// админ панель - 

$lang['adm_edit_news_title']='Створення і редагування новин';
$lang['adm_edit_news_tableHead_1']='Заголовок';
$lang['adm_edit_news_tableHead_2']='Ключові слова';
$lang['adm_edit_news_tableHead_3']='Опис сторінки';

// админ панель - 

$lang['adm_mailing_title']='Розсилка повідомлень';
$lang['adm_mailing_tableHead_1']='Заголовок';
$lang['adm_mailing_title1']='Відправлені повідомлення';
$lang['adm_mailing_table1Head_1']='Назва';
$lang['adm_mailing_table1Head_2']='Дата відправлення';
$lang['adm_mailing_table1Head_3']='Листів у розсильці';
$lang['adm_mailing_table1Head_4']='Дія';
$lang['adm_mailing_noMsg']='Повідомлення ще не відправлялись';
$lang['adm_mailing_test']='Тест';

// админ панель - 

$lang['adm_sms_phone']='Телефон';
$lang['adm_sms_txt']='Текст';
$lang['adm_sms_title1']='Реєстрація';
$lang['adm_sms_title2']='Поповнення';
$lang['adm_sms_title3']='Виплата';

// админ панель - 

$lang['adm_email_title']='Заголовок';

// админ панель - 

$lang['adm_email_title1']='Реєстрація';
$lang['adm_email_title2']='Новий пароль';
$lang['adm_email_title4']='Поповнення';
$lang['adm_email_title5']='Рахунок оплачено';
$lang['adm_email_title7']='Відмова у виплаті';
$lang['adm_email_title8']='Активація акаунту';
$lang['adm_email_title9']='Підтримка';
$lang['adm_email_title10']='Старт турніру';
$lang['adm_email_title11']='Завершення турніру';

// админ панель - 

$lang['adm_pin_title']='Генератор ПІН кодів';
$lang['adm_pin_tableHead_1']='Кількість';
$lang['adm_pin_tableHead_2']='Номінал';
$lang['adm_pin_btn_1']='Генерувати';
$lang['adm_pin_title1']='Статистика ПІН кодів';
$lang['adm_pin_table1Head_1']='Сгенеровано ПІН кодів';
$lang['adm_pin_table1Head_2']='Номінал';
$lang['adm_pin_btn_2']='Вивантажити';
$lang['adm_pin_title2']='ПІН коди';
$lang['adm_pin_table2Head_1']='ПІН код';
$lang['adm_pin_table2Head_2']='Сума';
$lang['adm_pin_table2Head_3']='Дата';
$lang['adm_pin_table2Head_4']='Статус';
$lang['adm_pin_noPin']='ПІН кодів немає';
$lang['adm_pin_status1']='Ні';
$lang['adm_pin_status2']='Ні';
$lang['adm_pin_table3Head_1']='ID';
$lang['adm_pin_table3Head_2']='Пароль';
$lang['adm_pin_table3Head_3']='ПІН код';
$lang['adm_pin_table3Head_4']='Час створення';
$lang['adm_pin_table3Head_5']='Сума';
$lang['adm_pin_table3Head_6']='Статус';
$lang['adm_pin_table3Head_7']='Час активації';
$lang['adm_pin_noScratch']='Скретч-карток поки що немає';
$lang['adm_pin_status_0']='Ні';
$lang['adm_pin_status_1']='Ні';
$lang['adm_pin_status_2']='Гравець';
$lang['adm_pin_status_3']='Пін';

// админ панель - 

$lang['adm_report_filter']='Фільтр';
$lang['adm_report_date']='Дата';
$lang['adm_report_time']='Час';
$lang['adm_report_KAS']='КАСА';
$lang['adm_report_INCOME']='ПРИБУТОК';
$lang['adm_report_receipt']='Прихід';
$lang['adm_report_outcome']='Розхід';
$lang['adm_report_rest']='Залишок';
$lang['adm_report_income']='Прибуток';
$lang['adm_report_np']='НП';
$lang['adm_report_login']='Логін';
$lang['adm_report_ps']='ПС';
$lang['adm_report_paysys']='Платіжна система';
$lang['adm_report_bill']='Рахунок';
$lang['adm_report_sum']='Сума';
$lang['adm_report_status']='Стан';
$lang['adm_report_gamer']='Гравець';
$lang['adm_report_regtime']='Реєстрація';
$lang['adm_report_authtime']='Авторизація';
$lang['adm_report_balance']='Баланс';
$lang['adm_report_bonus']='Бонусні';
$lang['adm_report_title']='Назва';
$lang['adm_report_data']='Дані';
$lang['adm_report_os']='ОС';
$lang['adm_report_useragent']='Юзерагент реєстрації';
$lang['adm_report_useragent1']='Юзерагент авторизацій';
$lang['adm_report_userip']='ІП реєстрації';
$lang['adm_report_action']='Дія';
$lang['adm_report_inpay']='поповнення';
$lang['adm_report_outpay']='зняття';
$lang['adm_report_game']='Гра';
$lang['adm_report_bet']='Ставка';
$lang['adm_report_win']='Виграш';
$lang['adm_report_denom']='Деном.';
$lang['adm_report_gamebalance']='Баланс гри';
$lang['adm_report_return']='Повернення';
$lang['adm_report_setting']='Налаштування';
$lang['adm_report_old']='Було';
$lang['adm_report_new']='Стало';
$lang['adm_report_inv']='Інвойс';
$lang['adm_report_type']='Тип';
$lang['adm_report_lotodraw']='Тираж';
$lang['adm_report_sumbet']='Сума ставок';
$lang['adm_report_sumwin']='Сума виграшів';
$lang['adm_report_winballs']='Шарів, що співпали';
$lang['adm_report_coef']='Коефіцієнт';
$lang['adm_report_no_data']='немає даних';

$lang['adm_outpay_detail_invoice']='Інвойс';
$lang['adm_outpay_detail_title']='Останні поповнення гравця';
$lang['adm_outpay_detail_table1Head_1']='Дата';
$lang['adm_outpay_detail_table1Head_2']='Сума';
$lang['adm_outpay_detail_table1Head_3']='ПС';
$lang['adm_outpay_detail_table1Head_4']='Інвойс';
$lang['adm_outpay_detail_table1Head_5']='Статус';
$lang['adm_outpay_detail_title2']='Останні зіграні ігри';
$lang['adm_outpay_detail_table2Head_1']='Дата';
$lang['adm_outpay_detail_table2Head_2']='Баланс';
$lang['adm_outpay_detail_table2Head_3']='Ставка';
$lang['adm_outpay_detail_table2Head_4']='Виграш';
$lang['adm_outpay_detail_table2Head_5']='Гра';
$lang['adm_outpay_detail_noData']='даних немає';
$lang['adm_outpay_detail_cancel']='Скасувати';
$lang['adm_outpay_detail_confirm']='Виплачено';
$lang['adm_outpay_detail_yes']='Так';
$lang['adm_outpay_detail_no']='Ні';
$lang['adm_outpay_detail_popupTitle']='Скасувати платіж';
$lang['adm_outpay_detail_popupBody']='Платіж буде скасовано. Ви впевнені?';
$lang['adm_outpay_detail_popupTitle2']='Підтвердіть платіж';
$lang['adm_outpay_detail_popupBody2']='Платіж буде сплачено. Ви впевнені?';

$lang['adm_game_title']='Гральні банки';
$lang['adm_game_table1Head_1']='Мін. ставка';
$lang['adm_game_table1Head_2']='Макс. ставка';
$lang['adm_game_table1Head_3']='СБ';
$lang['adm_game_table1Head_4']='ББ';
$lang['adm_game_table1Head_5']='НАСТІЛЬНИЙ БАНК';
$lang['adm_game_table1Head_6']='ЛОТО БАНК';
$lang['adm_game_table1Head_7']='ТРАФІК БАНК';
$lang['adm_game_title2']='не програний залишок поповнень';
$lang['adm_game_title3']='Гарантія виграшів';
$lang['adm_game_table3Head_1']='Спін гарантія';
$lang['adm_game_table3Head_2']='Бонус гарантія';
$lang['adm_game_title4']='Ігри';
$lang['adm_game_table4Head_1']='Гра';
$lang['adm_game_table4Head_2']='Опис';
$lang['adm_game_table4Head_3']='Дія';
$lang['adm_game_title5']='Зміна банку';
$lang['adm_game_title6']='Введіть суму';
$lang['adm_game_title7']='або оберіть';
$lang['adm_game_cancel']='Скасування';
$lang['adm_game_off']='Вимкнути';
$lang['adm_game_on']='Ввімкнути';
$lang['adm_game_spincell']='спін-комірка';
$lang['adm_game_bonuscell']='бонус-комірка';
$lang['adm_game_balance']= 'баланс';
$lang['adm_game_cellwin']= 'злив';

$lang['adm_game_name']='Назва гри';
$lang['adm_game_reels']='Барабани';
$lang['adm_game_lines']='Лінії';
$lang['adm_game_bonus']='Бонуси';
$lang['adm_game_freespins']='Фриспины';
$lang['adm_game_double']='Подвоєння';
$lang['adm_game_in']='Увійшло';
$lang['adm_game_out']='Вийшло';
$lang['adm_game_total']='Разом';
$lang['adm_game_action']='Діло';

$lang['adm_game_desc_lines']='линій';
$lang['adm_game_desc_line']='линія';
$lang['adm_game_desc_freespin']='безкоштовні фріспіни з помноженням';
$lang['adm_game_desc_freespin_']='безкоштовні фріспіни';
$lang['adm_game_desc_superbonus']='бонус и супер бонус гра';
$lang['adm_game_desc_superbonus_']='бонуси и супер бонус гра';
$lang['adm_game_desc_helmet']='Є каска';
$lang['adm_game_desc_fire']='Є вогнегасник';
$lang['adm_game_desc_bonus']='бонус гра';
$lang['adm_game_desc_bonus_']='бонус ігри';
$lang['adm_game_desc_umbrella']='Є парасолька';
$lang['adm_game_desc_reels']='барабанів';
$lang['adm_game_desc_jocker']='Джокер';
$lang['adm_game_desc_caribpoker']='Карибський покер, на одного користувача';
$lang['adm_game_desc_blackjack']='Блек джек на 7 рукавів';
$lang['adm_game_desc_roulette']='Рулетка с зеро на одного користувача';

$lang['adm_msg_1']='Заповніть всі поля';
$lang['adm_msg_2']='у посиланні можна використовувати тільки латинські символи a - z і цифри 0 - 9';
$lang['adm_msg_3']='у посиланні заборонені спецсимволи';
$lang['adm_msg_4']='це посилання вже існує';
$lang['adm_msg_5']='сторінка';
$lang['adm_msg_6']='створена';
$lang['adm_msg_7']='встановіть права на записування, файл cfg.php - chmod 666';
$lang['adm_msg_8']='немає прав на запис, у кореневій директорії';
$lang['adm_msg_9']='вимкніть SAFE MODE на сервері';
$lang['adm_msg_10']='зміни збережено';
$lang['adm_msg_11']='введіть заголовок';
$lang['adm_msg_12']='введіть текст';
$lang['adm_msg_13']='дані збережено';
$lang['adm_msg_14']='помилка MySql';
$lang['adm_msg_15']='розділ недоступний';
$lang['adm_msg_16']='відкрийте зміну';
$lang['adm_msg_17']='налаштування банків збережено';
$lang['adm_msg_18']='налаштування гарантії збережено';
$lang['adm_msg_19']='створіть зал';
$lang['adm_msg_20']='Банк змінено, новий баланс';
$lang['adm_msg_21']='гру додано у слайдер';
$lang['adm_msg_22']='гру видалено зі слайдеру';
$lang['adm_msg_23']='гру увімкнено';
$lang['adm_msg_24']='гру вимкнено';
$lang['adm_msg_25']='введіть заголовок и текст повідомлення';
$lang['adm_msg_26']='новину додано';
$lang['adm_msg_27']='новину видалено';
$lang['adm_msg_28']='ПІН код сгенеровано';
$lang['adm_msg_29']='помилка введення даних';
$lang['adm_msg_30']='доступ заборонено';
$lang['adm_msg_31']='мінімальна деномінація 0.01';
$lang['adm_msg_32']='групу додано';
$lang['adm_msg_33']='гравця неможливо видалити';
$lang['adm_msg_34']='не вказано логін';
$lang['adm_msg_35']='не вказано пароль';
$lang['adm_msg_36']='не вказано e-mail';
$lang['adm_msg_37']='невірний e-mail';
$lang['adm_msg_38']='гравця %username% створено, ПІН безпеки гравця %pin%';
$lang['adm_msg_39']='оберіть інший логін';
$lang['adm_msg_40']='оберіть інший e-mail';
$lang['adm_msg_41']='оберіть інший телефон';
$lang['adm_msg_42']='група недоступна';
$lang['adm_msg_43']='коментар додано';
$lang['adm_msg_44']='назначено Джек Пот гравцю';
$lang['adm_msg_45']='не вистачає параметрів';
$lang['adm_msg_46']='відмінено Джек Пот гравця';
$lang['adm_msg_47']='Користувача заблоковано';
$lang['adm_msg_48']='Користувача разблоковано';
$lang['adm_msg_49']='спін скинуто';
$lang['adm_msg_50']='лімит спінів змінено';
$lang['adm_msg_51']='гарантію змінено';
$lang['adm_msg_52']='баланс поповнено на суму';
$lang['adm_msg_53']='з балансу списано суму';
$lang['adm_msg_54']='немає даних';
$lang['adm_msg_55']='Турнір додано';
$lang['adm_msg_56']='Турнір відредаговано';

$lang['adm_popup_cancel']='Скасування';

// Копирайт

$lang['copyright']='casinofull';

$lang['gift_error']='під час реєстрації ви обрали інший бонус';

$lang['menu']='Меню';

$lang['adm_edit_game_title']='Редагування текстів гри';
$lang['adm_edit_game_tableHead_1']='Заголовок';
$lang['adm_edit_game_tableHead_2']='Опис';
$lang['adm_edit_game_tableHead_3']='Ключовики';
$lang['adm_edit_game_tableHead_4']='Поле введення тексту';

//импорт юзеров

$lang['adm_import_title']='Імпорт користувачів із текстового файлу';
$lang['adm_import_file']='Файл';
$lang['adm_import_file_format']='Формат файлу';
$lang['adm_import_browse']='Обрати';
$lang['adm_import_balance']='Баланс';
$lang['adm_import_balance_bonus']='Баланс(бонус)';
$lang['adm_import_wager']='Вейджер';
$lang['adm_import_points']='Поінти';
$lang['adm_import_mailing']='Розіслати імейли доданим юзерам';
$lang['adm_import_msg']='Додано %num% користувачів';

//adm/game_log

$lang['adm_log_title']='Логи ігор';
$lang['adm_log_username']='Логін гравця';
$lang['adm_log_game']='Гра';
$lang['adm_log_date']='Дата';
$lang['adm_log_strings']='Рядки логу';
$lang['adm_log_flash']='Flash-вікно';
$lang['adm_log_view']='Перегляд';
$lang['adm_log_loading']='Завантажуємо';
$lang['adm_log_select_string']='Треба обрати рядки';

// lobby

$lang['lobby_exit']='ВИХІД';
$lang['lobby_payment']='КАСА';
$lang['lobby_pay_label']='КАСА';
$lang['lobby_pay_collect']='ОТРИМАТИ';
$lang['lobby_pay_add']='ПОПОВНИТИ';
$lang['lobby_pay_pin_accept']='Пін-код прийнято! Ваш баланс поповнено на ';
$lang['lobby_pay_collect_wnd_lbl']='Виведення коштів';
$lang['lobby_pay_collect_wnd_text']='Для видання натисніть OK. Суму зняття вказано без деномінації !';
$lang['lobby_pay_in_wnd_lbl']='Поповнення балансу';
$lang['lobby_pay_in_wnd_text']='Введіть пін код для поповнення балансу';
$lang['lobby_pay_in_wnd_text2']='Введіть суму для поповнення балансу';
$lang['lobby_set_label']='НАЛАШТУВАННЯ';
$lang['lobby_set_main']='ОСНОВНІ';
$lang['lobby_set_save']='Налаштування збережено';
$lang['lobby_set_lang_wnd_lbl']='Мова інтерфейсу';
$lang['lobby_set_lang_wnd_text']='Оберіть мову інтерфейсу';
$lang['lobby_set_denom_wnd_lbl']='Деномінація';
$lang['lobby_set_denom_wnd_text']='Оберіть деномінацію!';

?>