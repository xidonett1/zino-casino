<?php /* Smarty version Smarty-3.1.6, created on 2020-04-29 19:47:08
         compiled from "C:\+local12\domains\site22\engine/templates/default\news.tpl" */ ?>
<?php /*%%SmartyHeaderCode:175675ea9a17c914b71-26486355%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d818e3e090f79a75e9079c31134d30c79cb2ba3' => 
    array (
      0 => 'C:\\+local12\\domains\\site22\\engine/templates/default\\news.tpl',
      1 => 1482265058,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175675ea9a17c914b71-26486355',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'sub_title' => 0,
    'news' => 0,
    'new' => 0,
    'ge' => 0,
    'lang' => 0,
    'nav' => 0,
    'cur_new' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.6',
  'unifunc' => 'content_5ea9a17d03747',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ea9a17d03747')) {function content_5ea9a17d03747($_smarty_tpl) {?>					<div class="content">
						<div class="refill news">
							<div class="heading">
								<h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
								<div class="texts">
									<p><?php echo $_smarty_tpl->tpl_vars['sub_title']->value;?>
</p>
								</div>
							</div>
<?php  $_smarty_tpl->tpl_vars['new'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['new']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['news']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['new']->key => $_smarty_tpl->tpl_vars['new']->value){
$_smarty_tpl->tpl_vars['new']->_loop = true;
?>
							<div class="news-item">
								<header class="heading-block">
									<h2><?php echo $_smarty_tpl->tpl_vars['new']->value[1];?>
</h2>
									<span class="date"><?php echo $_smarty_tpl->tpl_vars['new']->value[3];?>
</span>
								</header>
								<div class="holder">
									<div class="text-holder">
										<p><?php echo $_smarty_tpl->tpl_vars['new']->value[2];?>
</p>
										<div class="right">
											<a href="/<?php echo $_smarty_tpl->tpl_vars['ge']->value;?>
?id=<?php echo $_smarty_tpl->tpl_vars['new']->value[0];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['news_more'];?>
</a>
										</div>
									</div>
								</div>
							</div>
<?php } ?>
<?php if ($_smarty_tpl->tpl_vars['nav']->value[1]>1){?>
              <nav class="paging">
								<?php if ($_smarty_tpl->tpl_vars['nav']->value[0]>1){?><a href="" class="btn-prev" title="Предыдущая" onclick='setCookie("curpagenum", "<?php echo $_smarty_tpl->tpl_vars['nav']->value[0]-1;?>
","","/");'>«</a><?php }?>
								<ul>
									<?php if ($_smarty_tpl->tpl_vars['nav']->value[0]>2){?><li><a href="" onclick='setCookie("curpagenum", "1","","/");'>1</a></li><?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['nav']->value[0]>3){?><li>..</li><?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['nav']->value[0]>1){?><li><a href="" onclick='setCookie("curpagenum", "<?php echo $_smarty_tpl->tpl_vars['nav']->value[0]-1;?>
","","/");'><?php echo $_smarty_tpl->tpl_vars['nav']->value[0]-1;?>
</a></li> <?php }?>
									<li class="active"><a href="" onclick='return false;'><?php echo $_smarty_tpl->tpl_vars['nav']->value[0];?>
</a></li>
									<?php if (($_smarty_tpl->tpl_vars['nav']->value[0]+1<=$_smarty_tpl->tpl_vars['nav']->value[1])){?><li><a href="" onclick='setCookie("curpagenum", "<?php echo $_smarty_tpl->tpl_vars['nav']->value[0]+1;?>
","","/");'><?php echo $_smarty_tpl->tpl_vars['nav']->value[0]+1;?>
</a></li> <?php }?>  
									<?php if (($_smarty_tpl->tpl_vars['nav']->value[0]+3<=$_smarty_tpl->tpl_vars['nav']->value[1])){?><li>..</li><?php }?>
                  <?php if (($_smarty_tpl->tpl_vars['nav']->value[0]+2<=$_smarty_tpl->tpl_vars['nav']->value[1])){?><li><a href="" onclick='setCookie("curpagenum", "<?php echo $_smarty_tpl->tpl_vars['nav']->value[1];?>
","","/");'><?php echo $_smarty_tpl->tpl_vars['nav']->value[1];?>
</a></li><?php }?>
								</ul>
								<?php if (($_smarty_tpl->tpl_vars['nav']->value[0]+1<=$_smarty_tpl->tpl_vars['nav']->value[1])){?><a href="" class="btn-prev" title="Следующая" onclick='setCookie("curpagenum", "<?php echo $_smarty_tpl->tpl_vars['nav']->value[0]+1;?>
","","/");'>»</a><?php }?>
							</nav>
<?php }?>

<?php if (isset($_smarty_tpl->tpl_vars['cur_new']->value)){?>
							<div class="news-item">
								<header class="heading-block">
									<h2><?php echo $_smarty_tpl->tpl_vars['cur_new']->value[1];?>
</h2>
									<span class="date"><?php echo $_smarty_tpl->tpl_vars['cur_new']->value[0];?>
</span>
								</header>
								<div class="holder">
									<div class="text-holder">
										<p><?php echo $_smarty_tpl->tpl_vars['cur_new']->value[2];?>
</p>
									</div>
								</div>
							</div>
<?php }?>
						</div>
					</div><?php }} ?>