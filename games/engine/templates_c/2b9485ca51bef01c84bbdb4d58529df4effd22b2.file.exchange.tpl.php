<?php /* Smarty version Smarty-3.1.6, created on 2021-09-17 17:21:44
         compiled from "/home/host1835222/host1835222.hostland.pro/htdocs/www/engine/templates/default/exchange.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7413985816144a4785ec8f7-26680216%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b9485ca51bef01c84bbdb4d58529df4effd22b2' => 
    array (
      0 => '/home/host1835222/host1835222.hostland.pro/htdocs/www/engine/templates/default/exchange.tpl',
      1 => 1631825065,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7413985816144a4785ec8f7-26680216',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'sub_title' => 0,
    'lang' => 0,
    'point_pay' => 0,
    'point_cours' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.6',
  'unifunc' => 'content_6144a4786aa15',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6144a4786aa15')) {function content_6144a4786aa15($_smarty_tpl) {?><div class="content">
<div class="refill">

							<div class="heading">
								<h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
								<div class="texts">
									<p><?php echo $_smarty_tpl->tpl_vars['sub_title']->value;?>
</p>
								</div>
							</div>
</div>
	<div class="dk_body">
		
		<div class="dk_ballance">
		<div class="dk_ballance_h"><?php echo $_smarty_tpl->tpl_vars['lang']->value['exchange_TOTAL_BALANCE_POINTS'];?>
</div>
	     <div class="dk_ballance_sum"><?php echo $_smarty_tpl->tpl_vars['point_pay']->value;?>
</div>
		</div>
	 
		<div class="dk_form_ex">
			<form action="?action=exchange" method='post'>
				<div class="dk_form_elem">
				<div class="dk_inp_h"><?php echo $_smarty_tpl->tpl_vars['lang']->value['exchange_POINTS_EX'];?>
</div>
					<input type="text" name="sumpoints" class="dk_f_input" placeholder="0" value="<?php echo $_smarty_tpl->tpl_vars['point_pay']->value;?>
">
				</div>
				
				<div class="dk_form_elem2">
					<div class="dk_inp_h"><?php echo $_smarty_tpl->tpl_vars['lang']->value['exchange_EX_POINTS'];?>
</div>
					<input type="text" name="sumcredits" class="dk_f_input" placeholder="0" value="<?php echo sprintf('%01.2f',$_smarty_tpl->tpl_vars['point_pay']->value*$_smarty_tpl->tpl_vars['point_cours']->value);?>
">
				</div>
				
			<div class="dk_f_foot">
				<div class="dk_form_info">
				<span><?php echo $_smarty_tpl->tpl_vars['lang']->value['exchange_COURSE'];?>
:</span>
			     <div>1000 Cr = <?php echo $_smarty_tpl->tpl_vars['point_cours']->value*1000;?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['exchange_cur'];?>
</div>
				</div>
				<br />
				<div class="right">
					<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['button_EXCHANGE'];?>
" class="btn-green" style="width: 220px;" />
				</div>				
			</div>
			</form>
		</div>
	
	</div>

</div>
<script>
  var course=<?php echo $_smarty_tpl->tpl_vars['point_cours']->value;?>
;
  var points=<?php echo $_smarty_tpl->tpl_vars['point_pay']->value;?>
;
  $("input[name=sumpoints]").on("change", function(){
     if(this.value>points)
      {
      this.value=points;
      } 
     $("input[name=sumcredits]").val($("input[name=sumpoints]").val()*course);
  });
  
  $("input[name=sumcredits]").on("change", function(){
     if(this.value/course>points)
      {
      this.value=points*course;
      } 
     $("input[name=sumpoints]").val($("input[name=sumcredits]").val()/course);
  });
</script><?php }} ?>