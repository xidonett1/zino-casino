<?php /* Smarty version Smarty-3.1.6, created on 2018-07-26 13:36:28
         compiled from "D:\OpenServer\domains\html5.com\engine/templates/default\game\game.tpl" */ ?>
<?php /*%%SmartyHeaderCode:97785b59961c8782f0-51101682%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5f875641753b5f3be68f29bfc88cbb2bb9d6981' => 
    array (
      0 => 'D:\\OpenServer\\domains\\html5.com\\engine/templates/default\\game\\game.tpl',
      1 => 1482268653,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '97785b59961c8782f0-51101682',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'description' => 0,
    'lang' => 0,
    'keywords' => 0,
    'theme_url' => 0,
    'user_id' => 0,
    'login' => 0,
    'i' => 0,
    'game_block1' => 0,
    'game' => 0,
    'param' => 0,
    'game_block2' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.6',
  'unifunc' => 'content_5b59961cd9fe5',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b59961cd9fe5')) {function content_5b59961cd9fe5($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['description']->value;?>
 | <?php echo $_smarty_tpl->tpl_vars['lang']->value['copyright'];?>
" />
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
" />
	<link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/styles.css" />
	<link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/vendors/owl.carousel.css" />
	<link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/vendors/jquery.fancybox.css" />
    <link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/flagstrap/css/flags.css" />
  
    <!--[if IE]><link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/styles_ie.css" /><![endif]-->
  
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/vendors/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/vendors/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/jquery.main.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/common.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/cookies.js"></script>
	
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>
<body class="game-page">

<?php if (!$_smarty_tpl->tpl_vars['user_id']->value){?>
  <?php echo $_smarty_tpl->getSubTemplate ('dialogs.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }?>

	<div class="game">
		<div class="aside cell">
      <?php if ($_smarty_tpl->tpl_vars['user_id']->value){?>
			<a class="btn-orange" href="/out"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_OUT'];?>
</a><br/>
			<a class="btn-green" href="/enter"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_PAY'];?>
!</a>
      <?php }else{ ?>
      <a class="btn-orange fancybox" href="#login"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_ENTER'];?>
</a><br/>
			<a class="btn-green fancybox" href="#choose_reg_bonus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_REGISTER'];?>
</a>
      <?php }?>
			<div class="user-block">
				<img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/ava.png" width="48" height="48" alt=""/>
				<div class="texts">
					<?php echo $_smarty_tpl->tpl_vars['lang']->value['block_gamers'];?>
 - <?php if ($_smarty_tpl->tpl_vars['user_id']->value){?> <?php echo $_smarty_tpl->tpl_vars['login']->value;?>
 <?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_guest'];?>
 <?php }?>
				</div>
			</div>
			<ul class="list">
        <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->value = 0;
  if ($_smarty_tpl->tpl_vars['i']->value<3){ for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value<3; $_smarty_tpl->tpl_vars['i']->value++){
?>
        <?php $_smarty_tpl->tpl_vars['game'] = new Smarty_variable(array_shift($_smarty_tpl->tpl_vars['game_block1']->value), null, 0);?>
        <li>
					<img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/ico/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
.png" width="170" height="130" alt=""/>
					<div class="buttons">
						<a class="btn-green" href="/games/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_path'];?>
/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
/real"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_REAL'];?>
!</a>
						<a class="btn-orange small" href="/games/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_path'];?>
/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
/demo"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_DEMO'];?>
</a>
					</div>
		</li>
        <?php }} ?> 
			</ul>
		</div>
<script type="text/javascript">
	jQuery(window).load(function(){
		var block = jQuery('#game');
		var elem1 = block.find('object');
		var elem2 = block.find('embed');
		var blockHeight;

		function resizedw(){
			elem1.height(0);
			elem2.height(0);

			blockHeight = block.height();
			elem1.css({
				height: blockHeight
			})
			elem2.css({
				height: blockHeight
			})
		}

		resizedw();

		var doit;
		jQuery(window).on('resize orientationchange', function(){
			clearTimeout(doit);
			doit = setTimeout(resizedw, 100);
		})
	});
</script>
		<div class="game-holder cell" id="game">
<object  width="100%"" height="100%"" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,18,0" id="test" align="middle">
<param name="allowFullScreen" value="true" />
<param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['param']->value;?>
" />
<param name="bgcolor" value="03030F" />
<embed src="<?php echo $_smarty_tpl->tpl_vars['param']->value;?>
" bgcolor="03030F" allowFullScreen="true" name="game" align="middle" width="100%" height="100%" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
</object>

		</div>
		<div class="aside cell">
			<ul class="list">
				<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->value = 0;
  if ($_smarty_tpl->tpl_vars['i']->value<4){ for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value<4; $_smarty_tpl->tpl_vars['i']->value++){
?>
        <?php $_smarty_tpl->tpl_vars['game'] = new Smarty_variable(array_shift($_smarty_tpl->tpl_vars['game_block2']->value), null, 0);?>
        <li>
					<img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/ico/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
.png" width="170" height="130" alt=""/>
					<div class="buttons">
						<a class="btn-green" href="/games/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_path'];?>
/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
/real"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_REAL'];?>
!</a>
						<a class="btn-orange small" href="/games/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_path'];?>
/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
/demo"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_DEMO'];?>
</a>
					</div>
		</li>
        <?php }} ?>
			</ul>
		</div>
	</div>
</body>
</html><?php }} ?>