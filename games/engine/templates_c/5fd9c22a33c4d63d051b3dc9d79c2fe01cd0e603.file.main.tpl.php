<?php /* Smarty version Smarty-3.1.6, created on 2019-11-22 18:41:31
         compiled from "C:\+local11\domains\site22\engine/templates/default\main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:31055dd7f39bad4388-82366814%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5fd9c22a33c4d63d051b3dc9d79c2fe01cd0e603' => 
    array (
      0 => 'C:\\+local11\\domains\\site22\\engine/templates/default\\main.tpl',
      1 => 1483368446,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31055dd7f39bad4388-82366814',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'description' => 0,
    'lang' => 0,
    'keywords' => 0,
    'theme_url' => 0,
    'available_langs' => 0,
    'language' => 0,
    'config' => 0,
    'game_groups' => 0,
    'k' => 0,
    'val' => 0,
    'messages' => 0,
    'content_templ' => 0,
    'login' => 0,
    'user_info' => 0,
    'last_wins' => 0,
    'wins' => 0,
    'payout_block' => 0,
    'payout' => 0,
    'sub_title' => 0,
    'content' => 0,
    'game_slider' => 0,
    'game' => 0,
    'cur_lang' => 0,
    'status' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.6',
  'unifunc' => 'content_5dd7f39d663e2',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5dd7f39d663e2')) {function content_5dd7f39d663e2($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['description']->value;?>
 | <?php echo $_smarty_tpl->tpl_vars['lang']->value['copyright'];?>
" />
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
" />
	<link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/styles.css" />
	<link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/vendors/owl.carousel.css" />
	<link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/vendors/jquery.fancybox.css" />
    <link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/flagstrap/css/flags.css" />
  
    <!--[if IE]><link media="all" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/css/styles_ie.css" /><![endif]-->
  
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/vendors/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/vendors/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/jquery.main.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/common.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/cookies.js"></script>
	
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
 
</head>
<body>

	<div id="wrapper">
		<div class="w1">
    
    <div class="langs">
      <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['available_langs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
        <a href="" id="<?php echo $_smarty_tpl->tpl_vars['language']->value;?>
"><i class="flagstrap-icon flagstrap-<?php echo $_smarty_tpl->tpl_vars['language']->value;?>
" style="margin-left: 5px;"></i></a>
      <?php } ?>
    </div>
			<header id="header">
				<div class="header-holder">
					<strong class="logo"><a href="/"><?php echo $_smarty_tpl->tpl_vars['config']->value['cas_name'];?>
</a></strong>
						<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				</div>
				<nav id="main-nav">
					<ul>
            <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['game_groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value){
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['val']->key;
?>
				      <li><a href="#" onclick="load_gameList(<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
); return false;"><?php echo $_smarty_tpl->tpl_vars['val']->value;?>
</a></li>
            <?php } ?> 
					</ul>
				</nav>
			</header>
			<div id="main">
				<div class="layout">
					<div id="content">
					
			<?php if ($_smarty_tpl->tpl_vars['messages']->value){?>
              <?php echo $_smarty_tpl->getSubTemplate ('message.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

            <?php }?>
            
            <?php if (isset($_smarty_tpl->tpl_vars['content_templ']->value)&&$_smarty_tpl->tpl_vars['content_templ']->value){?>
              <?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['content_templ']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

            <?php }?>  
            
					</div>
					
					<aside id="sidebar">
        <?php if ($_smarty_tpl->tpl_vars['config']->value['use_blocks']){?>
            <?php if ($_smarty_tpl->tpl_vars['login']->value){?>
            <div class="side-block rating">
							<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_RATING'];?>
</h3>
               <div class="rate">
			   <div class="rate_icon">
			   <a href="/exchange">
               <img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/<?php echo $_smarty_tpl->tpl_vars['user_info']->value['rating_pic'];?>
">
			   </a>
               <span class="rate_level"><?php echo $_smarty_tpl->tpl_vars['user_info']->value['rating_level'];?>
</span>
               </div>
               </div>
               <div class="clear"></div>
			</div>
            <?php }?>
		<?php }?>
						<div class="side-block winners">
							<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_WIN_NOW'];?>
</h3>
							<ul>
                <?php  $_smarty_tpl->tpl_vars['wins'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['wins']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['last_wins']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['wins']->key => $_smarty_tpl->tpl_vars['wins']->value){
$_smarty_tpl->tpl_vars['wins']->_loop = true;
?>
                <li>
				<a href="/games/<?php echo $_smarty_tpl->tpl_vars['wins']->value['path'];?>
/<?php echo $_smarty_tpl->tpl_vars['wins']->value['game'];?>
/real">
                    <img class="align-left" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/ico/<?php echo $_smarty_tpl->tpl_vars['wins']->value['game'];?>
.png" width="50" height="50" alt="<?php echo $_smarty_tpl->tpl_vars['wins']->value['title'];?>
" />
				</a>
									<div class="text">
										<div class="summ">
											<b><?php echo $_smarty_tpl->tpl_vars['wins']->value['win'];?>
</b>
										</div>
										<strong class="title"><?php echo $_smarty_tpl->tpl_vars['wins']->value['title'];?>
</strong>
										<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_gamers'];?>
 <?php echo $_smarty_tpl->tpl_vars['wins']->value['login'];?>
</p>
									</div>  
				        </li>
                <?php } ?>
							</ul>
						</div>
            <?php if ($_smarty_tpl->tpl_vars['payout_block']->value){?>
						<div class="side-block payout">
							<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_WIN_BEST'];?>
</h3>
							<ul>
              <?php  $_smarty_tpl->tpl_vars['payout'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['payout']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['payout_block']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['payout']->key => $_smarty_tpl->tpl_vars['payout']->value){
$_smarty_tpl->tpl_vars['payout']->_loop = true;
?>
								<li>
                <a href="/games/<?php echo $_smarty_tpl->tpl_vars['payout']->value['g_path'];?>
/<?php echo $_smarty_tpl->tpl_vars['payout']->value['game'];?>
/real">
					<img class="align-left" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/ico/<?php echo $_smarty_tpl->tpl_vars['payout']->value['game'];?>
.png" width="80" height="63" alt="<?php echo $_smarty_tpl->tpl_vars['payout']->value['g_title'];?>
" />
				</a>
									<div class="text">
										<div class="summ">
											<b><?php echo $_smarty_tpl->tpl_vars['payout']->value['suma'];?>
</b>
										</div>
										<strong class="title"><?php echo $_smarty_tpl->tpl_vars['payout']->value['g_title'];?>
</strong>
										<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_bet'];?>
 <?php echo $_smarty_tpl->tpl_vars['payout']->value['stav'];?>
</p>
									</div>  
								</li>
               <?php } ?> 
								
							</ul>
						</div>
            <?php }?>
						<div class="side-block contacts">
							<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_CONTACT'];?>
</h3>
							<dl>
                <?php if ($_smarty_tpl->tpl_vars['config']->value['contact_phone']){?>
								<dt>
									<strong class="tel"><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_phone'];?>
:</strong>
								</dt>
								<dd><?php echo $_smarty_tpl->tpl_vars['config']->value['contact_phone'];?>
</dd>
                <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['config']->value['use_blocks']){?>
                <?php if ($_smarty_tpl->tpl_vars['config']->value['contact_mail']){?>
								<dt>                     
									<strong class="email"><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_email'];?>
:</strong>
								</dt>
								<dd><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['config']->value['contact_mail'];?>
"><?php echo $_smarty_tpl->tpl_vars['config']->value['contact_mail'];?>
</a></dd>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['config']->value['contact_icq']){?>
								<dt>
									<strong class="icq"><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_icq'];?>
:</strong>
								</dt>
								<dd><?php echo $_smarty_tpl->tpl_vars['config']->value['contact_icq'];?>
</dd>
                <?php }?>
		<?php }?>
								<dt>
									<strong class="support"><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_support'];?>
:</strong>
								</dt>
								<dd><a class="fancybox" href="<?php if ($_smarty_tpl->tpl_vars['login']->value){?>#support<?php }else{ ?>#login<?php }?>"><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_write_to_us'];?>
</a></dd>
							</dl>
						</div>
						<p></p>
					</aside>
				</div>
        <?php if ($_smarty_tpl->tpl_vars['config']->value['use_blocks']){?>				
        <?php if ($_smarty_tpl->tpl_vars['content_templ']->value=='index.tpl'){?>
  
			  <div class="text-intro" id="text-intro">
					<h2><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h2>
					<p><?php echo $_smarty_tpl->tpl_vars['sub_title']->value;?>
</p>
					<div class="slide">
                    <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

					</div>
					<div class="btn-holder">
						<a class="opener" href="" title=""></a>
					</div>
				</div>
        <?php }?>
        <?php }?>		
			</div>
        <?php if ($_smarty_tpl->tpl_vars['config']->value['use_blocks']){?>
			<footer id="footer">
				<ul class="payments">
					<li><a href="/paymethods" class="wm" title="webmoney"></a></li>
					<li><a href="/paymethods" class="yad" title="yandex dengi"></a></li>
					<li><a href="/paymethods" class="pb" title="privat24"></a></li>
					<li><a href="/paymethods" class="dm" title="Деньги@mail.ru"></a></li>
					<li><a href="/paymethods" class="lq" title="liqpay"></a></li>
					<li><a href="/paymethods" class="mr" title="moneta.ru"></a></li>
					<li><a href="/paymethods" class="ii" title="ИИ"></a></li>
					<li><a href="/paymethods" class="ak" title="АльфаКлик"></a></li>
					<li><a href="/paymethods" class="qiwi" title="qiwi"></a></li>
					<li><a href="/paymethods" class="visa" title="Visa"></a></li>
					<li><a href="/paymethods" class="mc" title="MasterCard"></a></li>
				</ul>
				<div class="columns">
					<div class="col">
						<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_ABOUT'];?>
</h3>
						<ul>							
							<li><a href="/guaranty"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_guaranty'];?>
</a></li>
							<li><a href="/rules"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_privacy'];?>
</a></li>
						</ul>
						<ul>							
							<li><a href="/bonuses"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_bonuses'];?>
</a></li>
							<li><a href="/paymethods"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_paymethods'];?>
</a></li>
						</ul>
						<ul>
						    <li><a href="/license"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_license'];?>
</a></li>
							<li><a href="/conditions"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_conditions'];?>
</a></li>						
						</ul>
					</div>
					<div class="col">
						<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_YOUR_MENU'];?>
</h3>
						<ul>
							<li><a class="fancybox" href="<?php if ($_smarty_tpl->tpl_vars['login']->value){?>/profile<?php }else{ ?>#login<?php }?>"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_profile'];?>
</a></li>
							<li><a class="fancybox" href="<?php if ($_smarty_tpl->tpl_vars['login']->value){?>/history<?php }else{ ?>#login<?php }?>"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_history'];?>
</a></li>
						</ul>
						<ul>
						    <li><a class="fancybox" href="<?php if ($_smarty_tpl->tpl_vars['login']->value){?>/out<?php }else{ ?>#login<?php }?>"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_out'];?>
</a></li>
							<li><a class="fancybox" href="<?php if ($_smarty_tpl->tpl_vars['login']->value){?>/enter<?php }else{ ?>#login<?php }?>"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_enter'];?>
</a></li>		
						</ul>
						<ul>
						    <li><a class="fancybox" href="<?php if ($_smarty_tpl->tpl_vars['login']->value){?>/partner<?php }else{ ?>#login<?php }?>"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_partner'];?>
</a></li>
							<li><a class="fancybox" href="<?php if ($_smarty_tpl->tpl_vars['login']->value){?>/enter?action=setbonus<?php }else{ ?>#login<?php }?>"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_set_bonus'];?>
</a></li>
						</ul>
					</div>
					<div class="col right">
						<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_OPTIONAL'];?>
</h3>
						<ul>
						    <li><a href="/return"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_return'];?>
</a></li>
							<li><a href="/jp"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_jp'];?>
</a></li>
						</ul>
						<ul>
							<li><a href="/faq"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_faq'];?>
</a></li>
							<li><a href="/news"><?php echo $_smarty_tpl->tpl_vars['lang']->value['menu_news'];?>
</a></li>
						</ul>
					</div>
				</div>
				<div class="bottom-block">
					<p class="copy"><?php echo $_smarty_tpl->tpl_vars['lang']->value['copyright'];?>
</p>
					<ul class="icons">
						<li><img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/ico01.png" width="44" height="21" alt=""/></li>
						<li><img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/ico02.png" width="37" height="24" alt=""/></li>
						<li><img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/ico03.png" width="43" height="22" alt=""/></li>
						<li><img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/ico04.png" width="42" height="17" alt=""/></li>
						<li><img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/ico05.png" width="43" height="21" alt=""/></li>
						<li><img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/images/ico06.png" width="43" height="20" alt=""/></li>
					</ul>
				</div>
			</footer>
        <?php }?>
		</div>
	</div>

<div class="popup-holder">
	
		<div class="lightbox" id="support">
			<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_SUPPORT'];?>
</h3>
			<div class="form">
				<form action="/contacts?action=submit" method="post">
					<fieldset>
						<label for="theme"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_subject'];?>
:*</label>
						<input type="text" name="subj" id="theme"/>
						<label for="text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_message'];?>
:*</label>
						<textarea name="textform" id="text" cols="30" rows="10"></textarea>
						<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['button_SEND'];?>
" class="btn-green" />
					</fieldset>
				</form>
			</div>
		</div>
		
</div>
  
<?php if (isset($_smarty_tpl->tpl_vars['game_slider']->value)){?>
          <div class="games-description" id="games-slider1" style="display: none">
							<ul class="slideset">
                <?php  $_smarty_tpl->tpl_vars['game'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['game']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['game_slider']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['game']->key => $_smarty_tpl->tpl_vars['game']->value){
$_smarty_tpl->tpl_vars['game']->_loop = true;
?>
                <li>
									<div class="item align-left">
										<a href=""><img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/ico/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
.png" width="165" height="130" alt=""/></a>
										<div class="buttons">
											<a class="btn-green" href="/games/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_path'];?>
/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
/real"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_REAL'];?>
!</a>
    									<a class="btn-orange small" href="/games/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_path'];?>
/<?php echo $_smarty_tpl->tpl_vars['game']->value['g_name'];?>
/demo"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_DEMO'];?>
</a>
										</div>
									</div>
									<div class="text-holder">
										<h2><?php echo $_smarty_tpl->tpl_vars['game']->value['g_title'];?>
</h2>
										<p><?php echo $_smarty_tpl->tpl_vars['game']->value['g_desc'];?>
</p>
										<br /><br />
										<div class="rating-area">
											<span class="label"><?php echo $_smarty_tpl->tpl_vars['lang']->value['block_Rating_stars'];?>
:</span>
											<div class="rating">
												<div class="rating-value" style="width: 100%;"></div>
											</div>
										</div>
									</div>
				</li>
                <?php } ?>
							</ul>
							<script type="text/javascript">
								$(document).ready(function(){
									$("#games-slider .slideset").owlCarousel({
										items:2,
										autoplay:true,
										loop:true,
										autoplayTimeout:5000,
										autoplayHoverPause:true
									});
                  
								});
							</script>
		  </div> 
<?php }?>  
</body>

<script>

function load_gameList(gr_id)
  {
  if($("#content").length)
    {
  $.ajax({
				url: "/engine/ajax/game_list.php?game_group="+gr_id,
				dataType: "json",
			 	cache: false,
				success: function(data){
        if(gr_id==-1)
          {
          $("#content").html('<div class="content"><div class="games-list">'+
							'</div></div>');
          }
        else
          {
          $("#content").html('<div class="content"><div class="games-list">'+
							'<div class="item item-games">'+
							'<span class="text">'+data.title+'</span>'+
							'</div></div></div>'+
					        '<img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/promo/<?php echo $_smarty_tpl->tpl_vars['cur_lang']->value;?>
_banner.jpg" width="900" height="300" /><br /><br />');
          }  
        
       
        
          $.each(data.games,function()
                {
                str='<div class="item">'
								str+='<img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/ico/'+this.g_name+'.png" width="165" height="130" alt=""/>'
								str+='<div class="buttons">'
								str+='	<a class="btn-green" href="/games/'+this.g_path+'/'+this.g_name+'/real"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_REAL'];?>
!</a>'
								str+='	<a class="btn-orange small" href="/games/'+this.g_path+'/'+this.g_name+'/demo"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_DEMO'];?>
</a>'
								str+='</div>'
							  str+='</div>'
                
			
                
                $("#content .content .games-list").append(str);
                });
        var item_width=165;
        var item_height=130;
               
        var width=$("#content .content").innerWidth();
        var height=$("#sidebar").height()-20-$("#content .content").height();
        var add_games_count= Math.floor(height/item_height) *  Math.floor(width/item_width) -2;
          if (add_games_count>0)
            {
                                                
          $.ajax({
				    url: "/engine/ajax/game_list.php?count="+add_games_count,
				    dataType: "json",
			 	    cache: false,
				    success: function(data){
              
              $("#content").append('<div class="content"><div id="other_games" class="games-list">'+
							'<div class="item item-other-games">'+
							'<span class="text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['corner_OTHER_GAME'];?>
</span>'+
							'</div></div></div>');
              
              $.each(data.games,function()
                {
                str='<div class="item">'
								str+='<img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/ico/'+this.g_name+'.png" width="165" height="130" alt=""/>'
								str+='<div class="buttons">'
								str+='	<a class="btn-green" href="/games/'+this.g_path+'/'+this.g_name+'/real"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_REAL'];?>
!</a>'
								str+='	<a class="btn-orange small" href="/games/'+this.g_path+'/'+this.g_name+'/demo"><?php echo $_smarty_tpl->tpl_vars['lang']->value['button_DEMO'];?>
</a>'
								str+='</div>'
							  str+='</div>'
                
			
                
                $("#other_games").append(str);
                }); 
              }
            });  
            }
         
        }
			});
    }  
        else
        {
        location.href="/";
        }  
  return false;
  }
  
$(document).ready(function()
    {
      <?php if (($_smarty_tpl->tpl_vars['status']->value==5||$_smarty_tpl->tpl_vars['status']->value==6)&&!$_smarty_tpl->tpl_vars['user_info']->value['gift']){?>
        $.fancybox(
	  '  <div id="gift" class="lightbox2 newreg" style="display: block">'+
    '   <h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_WELCOME_BONUS_CHOICE'];?>
</h3>'+
    '     <form action="/profile?action=save_gift" method="post">'+
    '       <ul class="sales">'+
    '         <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="1" name="bonus"><span class="p1"></span></label>'+
    '         <div class="saless1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_GIFT_REG'];?>
</div><br>'+
    '         <div class="saless2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_gift_reg_txt'];?>
</div></li>'+
    '         <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="2" name="bonus"><span class="p2"></span></label>'+
    '         <div class="saless1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_GIFT_DAY'];?>
</div><br>'+
    '         <div class="saless2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_gift_day_txt'];?>
</div></li>'+
    '         <li><label class="vlabel"><input type="radio" class="choose_reg_bonus" value="3" name="bonus"><span class="p3"></span></label>'+
    '         <div class="saless1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_GIFT_PAY'];?>
</div><br>'+
    '         <div class="saless2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popup_gift_pay_txt'];?>
</div></li>'+
    '       </ul>'+
    '       <div class="green-new"><input type="submit" class="btn-green" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['button_SAVE'];?>
"></div>'+
    '     </form>'+
    '  </div>',
		{
      padding	: 0,
      modal: true,
		helpers : {
			overlay: {
				css: { 'background': 'rgba(0,0,0,.8)'},
				locked: true
			}
		}
		}
	);
        
        $("#gift").trigger('click');
      <?php }?>  
    
    if($("#content .content").height()< $("#sidebar").height()-20)
      $("#content .content").height($("#sidebar").height()-20);   
    });  
</script>

</html><?php }} ?>