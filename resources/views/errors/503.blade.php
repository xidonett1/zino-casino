<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ __('Maintenance mode') }}</title>
</head>
<body>
    <h1 align="center" style="font-family: sans-serif;">{{ __('Maintenance mode') }}</h1>
    <p align="center" style="font-family: sans-serif; color: #ddd;">{{ __('Please, visit us later. We are sorry for the inconvenience.') }}</p>
</body>
</html>

