@php
use \App\Models\TournamentParticipant, \App\Models\User, \App\Models\Bet;
    $participants = TournamentParticipant::whereTournamentId($tournament->id)->get();
    $tournament_games = json_decode($tournament->games, true);
    $participating_games = [];
    foreach($tournament_games as $game) {
        $game = explode('[', $game)[1];
        $game = str_replace(']', '', $game);
        array_push($participating_games, $game);
    }
@endphp
@if($participants->isNotEmpty())
    <ul class="tournament_1__list" style="width: 100%; overflow: scroll; overflow-x: hidden; overflow-y: auto;  max-height: 40vh;">
        <li class="tournament_1__lable"><span>{{ __('Participant') }}</span> <p>{{ __('Participating since') }}</p> <p>{{ __('Result') }}</p></li>
        @php
            $participants_bets = collect();
        @endphp
        @foreach($participants as $key => $participant)
            @php
                $participation_start_date = $participant->created_at;

                $participant_bet = Bet::whereUserId($participant->user_id)
                ->where(function($query) use ($participation_start_date, $participating_games){
                    $query->where('created_at', '>=', $participation_start_date)
                          ->where('is_real', '=', 1)
                          ->whereIn('game', $participating_games);
                })
                ->sum('result');

            $participants_bets->push([
                'login' => User::find($participant->user_id)->login,
                'participation_start_date' => $participant->created_at->format('d.m.Y'),
                'result' => number_format($participant_bet, 2, '.', ' ')
            ]);
            @endphp
        @endforeach
        @foreach($participants_bets->sortByDesc('result') as $key => $participant)
            <li class="tournament_1__line_i"><span>{{ ++$key }}. {{ $participant['login'] }}</span> <span>{{ $participant['participation_start_date'] }}</span> <p>$ {{ $participant['result'] }} </p></li>
        @endforeach
    </ul>
@endif
