<div class="container">
    <div class="main-slider_wrapper">
        <button class="btn_slid btn-prev"></button>
        <button class="btn_slid btn-next"></button>
        <div class="main-slider">
            <div class="main-slider__slide"><a href="{{ route('casino', app()->getLocale()) }}" target="_blank"><img src="{{ asset('/img/banners/new_banner_1.jpg') }}" alt="{{ __('Casino') }}"></a></div>
            <div class="main-slider__slide"><a href="{{ route('casino', app()->getLocale()) }}" target="_blank"><img src="{{ asset('/img/banners/new_banner_2.jpg') }}" alt="{{ __('Casino') }}"></a></div>
            <div class="main-slider__slide"><a href="{{ route('casino', app()->getLocale()) }}" target="_blank"><img src="{{ asset('/img/banners/new_banner_3.jpg') }}" alt="{{ __('Casino') }}"></a></div>
        </div>
    </div>
</div>
