@php
  $is_current_user_participate = App\Models\TournamentParticipant::whereUserIdAndTournamentId(auth()->id(), $tournament->id)->get()->isNotEmpty();
  $tournament_games = (new App\Models\Tournament())->getTournamentGames($tournament->games);
@endphp
<div class="tournament" style="display: block !important; min-width: 840px; max-width: 840px; margin: 0 auto; margin-bottom: 20px;">
    <div class="tournament_1__wrap" style="display: flex; flex-wrap: wrap;">
        <div class="tournament_1__info"
             style="width: 100%; background: linear-gradient( rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6) ), url({{ $tournament->thumbnail ? asset('img/tournaments/'.$tournament->thumbnail) : asset('img/news/no_image.png') }});">
            <p align="center" class="tournament_1__mintitle">{{ __('Tournament') }}</p>
            <h6 class="tournament_1__name" align="center">{{ @json_decode($tournament->title, true)[app()->getLocale()] }}</h6>
            <div>
            @foreach(@json_decode(json_decode($tournament->prizes, true)[app()->getLocale()], true) as $prize)
                <p align="center" class="tournament_1__prize" style="font-size: 20px;">{{ $prize }}</p>
            @endforeach
            </div>
            <div>
                <p align="center" class="tournament_1__prize" style="font-size: 17px; overflow-wrap: break-word; color: rgba(255,255,255,0.73);">{{ strip_tags(json_decode($tournament->description, true)[app()->getLocale()]) }}</p>
            </div>
            <br>
            @include('components.tournament-games')
            @include('components.tournament-participate')
        </div>
        @include('components.tournament-list')
    </div>
</div>
