<div class="card" style="width: 269px; border: 1px solid rgba(0,255,255,0.39);">
    <div class="card__bg" style="background-image: url({{ $tournament->thumbnail ? asset('img/tournaments/'.$tournament->thumbnail) : asset('img/news/no_image.png') }});">
    </div>
    <div class="card__info">
        <h5 class="card__info-title">{{ json_decode($tournament->title, true)[app()->getLocale()] }}</h5>
        <p class="text-secondary" style="color: #fff; font-size: 0.8em;"><i class="fa-solid fa-calendar-days" style="margin-right: 5px;"></i>{{ \Carbon\Carbon::parse($tournament->created_at)->format('d/m/Y') }}</p>
        <a href="{{ route('news.single', ['lang' => app()->getLocale(), 'slug' => $tournament->slug]) }}" class="btn_02">{{ __('Participate') }}</a>
    </div>
</div>



