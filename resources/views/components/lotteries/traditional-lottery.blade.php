@php
    //$is_current_user_participate = App\Models\TournamentParticipant::whereUserIdAndTournamentId(auth()->id(), $tournament->id)->get()->isNotEmpty();
@endphp
<div class="tournament" style="display: block !important; min-width: 840px; max-width: 840px; margin: 0 auto; margin-bottom: 20px;">
    <div class="tournament_1__wrap" style="display: flex; flex-wrap: wrap;">
        <div class="tournament_1__info"
             style="width: 100%; background: linear-gradient( rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8) ), url('{{ asset('img/lotteries/banner-placeholder.webp') }}');">
            <p align="center" class="tournament_1__mintitle">{{ __('Microsoft.com | Seattle, WA') }}</p>
            <h6 class="tournament_1__name" align="center">The best lottery name ($10 per ticket)</h6>
            <div>
                <p align="center"><img src="{{ asset('img/lotteries/banner-placeholder.webp') }}" style="border-radius: 10px;" alt="banner placeholder" width="400"></p>
                <p align="center" class="tournament_1__prize" style="font-size: 14px; cursor: pointer;">Rules</p>
            </div>
            <br>
            <div style="display: flex; align-items: center; justify-content: center; padding: 20px;">
                <div class='countdown' data-date="2022-05-01"></div>
            </div>
            <form method="POST" action="{{ route('api.tournament.participate') }}">
                @csrf
                <button class="btn03 btn_toup_balance_01" style="width: 100% !important;" type="submit">{{ __('Purchase') }}</button>
            </form>
            <br>
            <p align="center" class="tournament_1__mintitle">{{ __('You have') }} <span>0</span> {{ __('tickets') }}</p>
        </div>
    </div>
</div>
