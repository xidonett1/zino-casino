@php

$support_email = \App\Models\Settings::where('key', 'support_email')->get()[0]->value;

@endphp

<div class="sideb-contact">
    <div class="support_sec_min">
        <h5 class="title_05">{{ __('Support service') }}</h5>
        <a href="mailto:{{ $support_email }}" class="mail_link_wi">{{ $support_email }}</a>
    </div>
</div>
