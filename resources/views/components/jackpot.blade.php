<div class="total-jackpots_wrapp">
    <div class="container">
        <div class="total-jackpots">
            <div class="total-jackpots__title">
                <img src="/img/total_it.svg" alt="">
            </div>
            <div class="total-jackpots__sum">
                <span>$</span> 1.548.265
            </div>
            <div class="total-jackpots__img">
                <img src="/img/2.png" alt="">
            </div>
        </div>
    </div>
</div>
