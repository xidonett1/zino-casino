<div style="display: flex; align-items: center; justify-content: center; padding: 20px; padding-top: 0px; flex-wrap: wrap;">
    @foreach($tournament_games as $game)
        <a href="@auth {{ str_replace('https', 'http', route('user.game',
                        ['lang' => app()->getLocale(), 'slug' => $game->slug, 'is_real' => 'real'])) }}
        @else #! @endauth" class="@guest btn_play @endguest" title="{{ $game->title }}"
           style="margin-right: 5px; margin-bottom: 10px;">
            <img src="{{ asset("img/games")."/{$game->image}" }}"
                 style="border-radius: 2px;"
                 width="50" alt="{{ $game->title }}">
        </a>
    @endforeach
</div>
