@php

$winner_bets = \App\Models\Bet::where('result', '>', 0)->limit(6)->latest()->get();

@endphp

<div class="tournament_1 line-winner" style="margin-bottom: 10px;">
    <div class="tournament_1__wrap">
        <div class="tournament_1__info">
            <span class="line-winner__txt1">{{ __('Life-tape') }}</span>
            <span class="line-winner__txt2">{{ __('Winners') }}</span>
        </div>
        <ul class="tournament_1__list">
            <li class="tournament_1__lable"><span>{{ __('Participant') }}</span> <p>{{ __('Sum') }}</p></li>
            @foreach($winner_bets as $bet)
                @php
                  $user = \App\Models\User::find($bet->user_id)->login;
                @endphp
                <li class="tournament_1__line_i"><span>{{ substr($user, 0, -3) }}***</span> <p>$ {{ $bet->result }}</p></li>
            @endforeach
        </ul>
    </div>
</div>
