@foreach($pages as $page)
    <li class="aside-menu1__item {{ $page->slug == request()->route('slug') ? 'aside-menu1__item_active' : '' }}"><a href="{{ route('page', ['lang' => app()->getLocale(), 'slug' => $page->slug]) }}">
            {{ @json_decode($page->title, true)[app()->getLocale()] }}
        </a></li>
@endforeach
