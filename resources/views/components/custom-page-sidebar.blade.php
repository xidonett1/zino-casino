@php
$information_pages = \App\Models\Page::where(['category' => 'information'])->get();
$safety_pages = \App\Models\Page::where(['category' => 'safety'])->get();
$support_pages = \App\Models\Page::where(['category' => 'support'])->get();
@endphp
<div class="text-page__aside-sec1 aside-menu1">
    <div class="aside-menu1__section">
        <h5 class="title_05">{{ __('INFORMATION') }}</h5>
        <ul class="aside-menu1__list">
            @php
                $pages = $information_pages;
            @endphp
            @include('components.custom-page-sidebar-link')
        </ul>
    </div>
    <div class="aside-menu1__section">
        <h5 class="title_05">{{ __('SAFETY') }}</h5>
        <ul class="aside-menu1__list">
            @php
                $pages = $safety_pages;
            @endphp
            @include('components.custom-page-sidebar-link')
        </ul>
    </div>
    <div class="aside-menu1__section">
        <h5 class="title_05">{{ __('SUPPORT') }}</h5>
        <ul class="aside-menu1__list">
            @php
                $pages = $support_pages;
            @endphp
            @include('components.custom-page-sidebar-link')
        </ul>
    </div>
</div>
