<form method="POST" action="{{ route('api.tournament.participate') }}">
    @csrf
    <input type="hidden" name="user_id" value="{{ auth()->id() }}">
    <input type="hidden" name="tournament_id" value="{{ $tournament->id }}">

    @if($tournament->is_available && !$is_current_user_participate)
        <button class="btn03 btn_toup_balance_01" style="width: 100% !important;" type="submit">{{ __('Join') }}</button>
    @elseif($is_current_user_participate)
        <button class="btn03 btn_toup_balance_01" style="width: 100% !important; filter: grayscale(0.4); cursor: not-allowed;" type="button">{{ __('Joined') }}</button>
    @else
        <button class="btn03 btn_toup_balance_01" style="width: 100% !important; filter: grayscale(1); cursor: not-allowed;" type="button">{{ __('Archived') }}</button>
    @endif
</form>
