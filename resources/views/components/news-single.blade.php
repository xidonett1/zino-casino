<div class="card" style="width: 269px;">
    <div class="card__bg" style="background-image: url({{ $new->thumbnail ? asset('img/news/'.$new->thumbnail) : asset('img/news/no_image.png') }});">
    </div>
    <div class="card__info">
        <h5 class="card__info-title">{{ json_decode($new->title, true)[$lang] }}</h5>
        <p class="text-secondary" style="color: #fff; font-size: 0.8em;"><i class="fa-solid fa-calendar-days" style="margin-right: 5px;"></i>{{ \Carbon\Carbon::parse($new->created_at)->format('d/m/Y') }}</p>
        <a href="{{ route('news.single', ['lang' => app()->getLocale(), 'slug' => $new->slug]) }}" class="btn_02">{{ __('Read') }}</a>
    </div>
</div>
