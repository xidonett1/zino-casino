@section('title', __('Casino games'))
@include('layouts.header')
<section class="content casino_page">
        @include('components.jackpot')
        <div class="content-home">
            <div class="container">
                <div class="content-home__wrapp">
                    <div class="content-side">
                        @include('components.life-winners')
{{--                        @include('components.tournament')--}}
                        <div class="sideb-propose1">
                            @php
                                $game = $games[random_int(0, count($games)-1)];
                            @endphp
                            @include('games.game')
                        </div>

                        @include('components.support')
                    </div>

                    <div class="cazinopage_w">
                        <div class="top_line-bar">
                            <div class="top_line-bar__wrapp">
                                <div class="top_line-bar__tabbtn_section">
                                    <div class="tabbtn_section__addwrp">
                                        <button class="tab-btn_01 tab-btn_01__active">{{ __('All games') }}</button>
                                        <button class="tab-btn_01">{{ __('Popular') }}</button>
                                        <button class="tab-btn_01">{{ __('New') }}</button>
                                        <button class="tab-btn_01">{{ __('Slots') }}</button>
                                        <button class="tab-btn_01">{{ __('Board') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="card_container show-content">
                                @foreach($games as $game)
                                    @include('games.game')
                                @endforeach
                            </div>
                            <div class="card_container show-content">
                                @foreach($games as $game)
                                    @php
                                        $categories = json_decode($game->categories, true);
                                    @endphp
                                    @if($categories['is_popular'])
                                        @include('games.game')
                                    @endif
                                @endforeach
                            </div>
                            <div class="card_container show-content">
                                @foreach($games as $game)
                                    @php
                                        $categories = json_decode($game->categories, true);
                                    @endphp
                                    @if($categories['is_new'])
                                        @include('games.game')
                                    @endif
                                @endforeach
                            </div>
                            <div class="card_container show-content">
                                @foreach($games as $game)
                                    @php
                                        $categories = json_decode($game->categories, true);
                                    @endphp
                                    @if($categories['is_slot'])
                                        @include('games.game')
                                    @endif
                                @endforeach
                            </div>
                            <div class="card_container hide-content">
                                @foreach($games as $game)
                                    @php
                                        $categories = json_decode($game->categories, true);
                                    @endphp
                                    @if($categories['is_board'])
                                        @include('games.game')
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@include('layouts.footer')
