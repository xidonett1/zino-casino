<div class="card">
    <div class="card__bg" style="background-image: url({{ asset('img/games/'.$game->image) }});">
    </div>
    <div class="card__info">
        <h5 class="card__info-title">{{ $game->title }}</h5>
        @if($game->is_enabled)
            <a href="@auth {{ str_replace('https', 'http', route('user.game', ['lang' => app()->getLocale(), 'slug' => $game->slug, 'is_real' => 'real'])) }} @else #! @endauth" class="btn_02 @guest btn_play @endguest">{{ __('Play') }}</a>
        @else
            <a href="#!" class="btn_02 btn_play">{{ __('Play') }}</a>
        @endif
    </div>
</div>
