@php
    $information_pages = \App\Models\Page::where(['category' => 'information'])->get();
    $safety_pages = \App\Models\Page::where(['category' => 'safety'])->get();
    $support_pages = \App\Models\Page::where(['category' => 'support'])->get();
@endphp
<footer class="footer">
    <div class="container">
        <div class="footer__container">
            <div class="footer__section1">
                <div>
                    <a href="{{ route('index') }}" class="logo">
                        <img src= "{{ asset('/img/logo-img.svg') }}" alt="{{ config('app.name') }}">
                    </a>
                    <p class="footer__copy">© 2014-{{ now()->year }} {{ config('app.name') }}</p>
                </div>

                @guest
                    <div class="footer__log-reg">
                        <button class="rigistration_btn">{{ __('Sign up in 10 sec') }}</button>
                        <button class="login_btn m_login_btn">{{ __('Sign in') }}</button>
                    </div>
                @endguest
            </div>

            <div class="footer__mobsec2">
                <div class="lang_wrapper">
                    <ul class="lang">
                        @if(app()->getLocale() === 'ir')
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'ir'], $request->route()->parameters()) : 'ir') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/ir.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">IR</span></a>
                            </li>
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'en'], $request->route()->parameters()) : 'en') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/en.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">EN</span></a>
                            </li>
                        @elseif(app()->getLocale() === 'en')
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'en'], $request->route()->parameters()) : 'en') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/en.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">EN</span></a>
                            </li>
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'ir'], $request->route()->parameters()) : 'ir') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/ir.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">IR</span></a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="footer__section2">
                <h6 class="title_05">{{ __('INFORMATION') }}</h6>
                <ul class="footer__link_list">
                    @php
                        $pages = $information_pages;
                    @endphp
                    @include('components.custom-page-sidebar-link')
                </ul>
            </div>

            <div class="footer__section3">
                <h6 class="title_05">{{ __('SAFETY') }}</h6>
                <ul class="footer__link_list">
                    @php
                        $pages = $safety_pages;
                    @endphp
                    @include('components.custom-page-sidebar-link')
                </ul>
            </div>

            <div class="footer__section4">
                <h6 class="title_05">{{ __('SUPPORT') }}</h6>
                <ul class="footer__link_list">
                    @php
                        $pages = $support_pages;
                    @endphp
                    @include('components.custom-page-sidebar-link')
                </ul>

                <div class="lang_wrapper">
                    <ul class="lang">
                        @if(app()->getLocale() === 'ir')
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'ir'], $request->route()->parameters()) : 'ir') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/ir.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">IR</span></a>
                            </li>
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'en'], $request->route()->parameters()) : 'en') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/en.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">EN</span></a>
                            </li>
                        @elseif(app()->getLocale() === 'en')
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'en'], $request->route()->parameters()) : 'en') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/en.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">EN</span></a>
                            </li>
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'ir'], $request->route()->parameters()) : 'ir') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/ir.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">IR</span></a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer__log-reg footer__log-reg-mob">
            <button class="rigistration_btn">{{ __('Sign up in 10 sec') }}</button>
            <button class="login_btn m_login_btn">{{ __('Sign in') }}</button>
        </div>
        <div class="footer__bottom-line">
            <img src="{{ asset('img/1.png') }}" alt="">
        </div>

    </div>
</footer>

</div>

<script type="text/javascript" src="{{ asset('/js/jquery-3.5.1.min.js') }}" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('/slick-1.8.1/slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/script.js') }}"></script>

</body>
</html>
