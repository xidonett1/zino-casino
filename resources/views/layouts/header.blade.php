@php
    $is_client_messages_allowed = \App\Models\Settings::where('key', 'client_messages_allowed')->first()->value;
    $has_user_messages = \App\Models\Message::where('user_id', auth()->id())->first();
@endphp

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" value="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/reset.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/slick-1.8.1/slick/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/slick-1.8.1/slick/slick-theme.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/fonts/icomoon1/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/libs/jquery-countdown/css/countdown.css') }}">

    <!-- FA icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Scripts -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('js/libs/jquery-countdown/js/countdown.js') }}" defer></script>

    <!-- Icon -->
    <link rel="icon" href="{{ asset('img/icons/icon.png') }}">

</head>
<body>
<div class="page">
    @guest()
    <!-- Popups login and registration -->
    <div class="modal-wrap modal_registration">
        <div id="modal" class="modal modal_1">
            <div class="modal_1__onesec">
                <a href="{{ route('index', app()->getLocale()) }}" class="logo">
                    <img src= "{{ asset('/img/logo-img.svg') }}" alt="{{ config('app.name') }}">
                </a>

                <p class="reg_left_p1">{{ __('Greeting bonus') }}</p>
                <p class="reg_left_p2">{{ __('Bonus') }} <span>100%</span></p>
                <p class="reg_left_p3">{{ __('From') }} <span> $250</span> {{ __('deposit') }}</p>

                <img class="reg_img_l_1" src="{{ asset('/img/reg-im1.png') }}" alt="">
                <img class="reg_img_l_2" src="{{ asset('/img/star1.svg') }}" alt="">
                <img class="reg_img_l_3" src="{{ asset('/img/star1.svg') }}" alt="">
                <img class="reg_img_l_4" src="{{ asset('/img/star1.svg') }}" alt="">
            </div>
            <div class="modal_1__twosec">
                <div class="modal_1__contentces">
                    <button class="modal_close"><img src="{{ asset('/img/icons/close1.svg') }}" alt=""></button>
                    <h5 class="title1">{{ __('Sign up') }}</h5>
                    <p class="p1">{{ __('Please, fill the fields') }}</p>
                    @if (session('status_register'))
                        <br>
                        <p style="color: red;">{{ session('status_register') }}</p>
                    @endif
                    <form action="{{ route('register.custom') }}" method="POST" class="form_log_reg">
                        @csrf
                        <input type="hidden" name="self_created" value="true">
                        <input type="hidden" name="ref" value="{{ @request()->cookie('ref') ? request()->cookie('ref') : '0' }}">
                        <div class="input_wrap1">
                            <span class="input_icon"><img src="{{ asset('/img/icons/icon-main.svg') }}" alt=""></span>
                            <input class="input1" name="email" type="email" placeholder="{{ __('Email') }}" required>
                        </div>
                        <div class="input_wrap1">
                            <span class="input_icon"><img src="{{ asset('/img/icons/icon-user.svg') }}" alt=""></span>
                            <input class="input1" name="login" type="text" maxlength="10" placeholder="{{ __('Login') }}" required>
                        </div>
                        <div class="input_wrap1">
                            <input class="input1"  pattern="^\S{6,}$"  onchange="this.setCustomValidity(this.validity.patternMismatch ? '{{ __('Password should contain 6 characters') }}' : ''); if(this.checkValidity()) form.repeat_password.pattern = this.value;" name="password" type="password" placeholder="{{ __('Придумайте пароль') }}" required>
                        </div>
                        <div class="input_wrap1">
                            <input class="input1" name="repeat_password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? '{{ __('Passwords does not match') }}' : '');"  type="password" placeholder="{{ __('Re-type password') }}" required>
                        </div>
                        <div class="form_agreeline">
                            <div class="chckbox_it">
                                <input type="checkbox" class="checkbox1 sheckshow" id="checkbox0_1" checked required>
                                <label for="checkbox0_1"></label>
                            </div>
                            <label for="checkbox0_1" class="p1">{{ __("I'm older than 21 year and agree with'") }} <a href="{{ route('page', ['lang' => app()->getLocale(), 'slug' => 'rules']) }}" class="link1">{{ __('Terms & Conditions') }}</a></label>
                        </div>

                        <button class="btn03 registr" type="submit">{{ __('Sign up') }}</button>
                    </form>
                </div>

                <div class="modal1__bottom">
                    <p class="p1">{{ __('Already have account?') }} <a href="#!" class="link1 acclogin">{{ __('Sign in') }}</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-wrap modal_login">
        <div id="modal" class="modal modal_1">
            <div class="modal_1__onesec">
                <a href="{{ route('index', app()->getLocale()) }}" class="logo">
                    <img src= "{{ asset('/img/logo-img.svg') }}" alt="{{ config('app.name') }}">
                </a>

                <p class="reg_left_p1">{{ __('Greeting bonus') }}</p>
                <p class="reg_left_p2">{{ __('Bonus') }} <span>100%</span></p>
                <p class="reg_left_p3">{{ __('From') }} <span>$250</span> {{ __('deposit') }}</p>

                <img class="reg_img_l_1" src="{{ asset('/img/reg-im1.png') }}" alt="">
                <img class="reg_img_l_2" src="{{ asset('/img/star1.svg') }}" alt="">
                <img class="reg_img_l_3" src="{{ asset('/img/star1.svg') }}" alt="">
                <img class="reg_img_l_4" src="{{ asset('/img/star1.svg') }}" alt="">
            </div>
            <div class="modal_1__twosec">
                <div class="modal_1__contentces">
                    <button class="modal_close"><img src="{{ asset('/img/icons/close1.svg') }}" alt=""></button>
                    <h5 class="title1">{{ __('Sign in') }}</h5>
                    <p class="p1">{{ __('Please, fill the fields') }}</p>
                    @if (session('status'))
                        <br>
                        <p style="color: red;">{{ session('status') }}</p>
                    @endif
                    <form action="{{ route('login.custom') }}" method="POST" class="form_log_reg">
                        @csrf
                        <div class="input_wrap1">
                            <span class="input_icon"><img src="{{ asset('/img/icons/icon-user.svg') }}" alt=""></span>
                            <input class="input1" type="text" name="login" placeholder="{{ __('Login') }}" required>
                        </div>
                        <div class="input_wrap1">
                            <input class="input1" type="password" name="password" placeholder="{{ __('Password') }}" required>
                        </div>
                        <button class="btn03 log_in_btn" type="submit">{{ __('Sign in') }}</button>
                    </form>
                </div>

                <div class="modal1__bottom">
                    <p class="p1">{{ __('No account?') }}  <a href="#!" class="link1 accregistr"> {{ __('Sign up') }}</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Popups login and registration -->
    @endguest

    <header class="header">
        <div class="container">
            <div class="header_sec_1">
                <a href="{{ route('index', app()->getLocale()) }}" class="logo">
                    <img src= "{{ asset('/img/logo-img.svg') }}" alt="{{ config('app.name') }}">
                </a>

                <nav class="main_nav">
                    <ul class="main_nav__list">
                        <li class="main_nav__item"><a href="{{ route('news', app()->getLocale()) }}" class="main_nav__link">{{ __('News') }}</a></li>
                        <li class="main_nav__item"><a href="{{ route('casino', app()->getLocale()) }}" class="main_nav__link">{{ __('Casino') }}</a></li>
                        <li class="main_nav__item"><a href="{{ route('tournaments', app()->getLocale()) }}" class="main_nav__link">{{ __('Tournaments') }}</a></li>
                        <li class="main_nav__item"><a href="{{ route('lotteries', app()->getLocale()) }}" class="main_nav__link">{{ __('Lotteries') }}</a></li>
                        @if($is_client_messages_allowed)
                            @auth
                                @if($has_user_messages)
                                    <li class="main_nav__item"><a href="{{ route('user.write-us', app()->getLocale()) }}" class="main_nav__link">{{ __('New message') }}</a></li>
                                @else
                                    <li class="main_nav__item"><a href="{{ route('user.write-us', app()->getLocale()) }}" class="main_nav__link">{{ __('Write us') }}</a></li>
                                @endif
                            @endauth
                        @endif
                    </ul>
                </nav>
            </div>
            <div class="header_sec_2">
                <div class="lang_wrapper">
                    <ul class="lang">
                        @if(app()->getLocale() === 'ir')
                        <li>
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'ir'], $request->route()->parameters()) : 'ir') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/ir.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">IR</span></a>
                        </li>
                        <li>
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'en'], $request->route()->parameters()) : 'en') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/en.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">EN</span></a>
                        </li>
                        @elseif(app()->getLocale() === 'en')
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'en'], $request->route()->parameters()) : 'en') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/en.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">EN</span></a>
                            </li>
                            <li>
                                <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), @$request ? array_merge(['lang' => 'ir'], $request->route()->parameters()) : 'ir') }}"><div class="lang__flag" style="background-image: url({{ asset('/img/icons/ir.png') }}); background-position: center; background-size: contain;"></div><span class="lang__langi">IR</span></a>
                            </li>
                        @endif
                    </ul>
                </div>


                @guest
                    <button class="rigistration_btn">{{ __('Sign up in 10 sec') }}</button>
                    <button class="login_btn m_login_btn">{{ __('Sign in') }}</button>
                @else
                    <div class="flx__002">
                        <div class="account_topbar_wrapp">
                            <div class="account_topbar">
                                <div class="flx-01">
                                    <div class="account_topbar__avatar">
                                        <img src="{{ auth()->user()->avatar != 0 ? asset('/img/avatars/'.auth()->user()->avatar) : asset('/img/user.png') }}" alt="{{ auth()->user()->login }}">
                                    </div>
                                    <div class="account_topbar__info">
                                        <span class="account_topbar__username">{{ auth()->user()->login }}</span>
                                        <span class="account_topbar__balance">$ {{ number_format(\App\Models\UserBalance::all()->where('user_id', auth()->id())->first()->real, 2, '.', ' ') }}</span>
                                    </div>
                                </div>
                                <div class="account_topbar__arrow">
                                    <img src="/img/icons/arrow-3.svg" alt="">
                                </div>
                            </div>
                            <div class="account_topbar_menu">
                                <ul>
                                    <li class="account_topbar_menu__item"><a href="{{ route('user.balance', app()->getLocale()) }}"><span class="icon1 icon-ic-1"></span> {{ __('Balance') }}</a></li>
                                    <li class="account_topbar_menu__item"><a href="{{ route('user.deposit', app()->getLocale()) }}"><span class="icon1 icon-ic-2"></span> {{ __('Deposit') }}</a></li>
                                    <li class="account_topbar_menu__item"><a href="{{ route('user.withdrawal', app()->getLocale()) }}"><span class="icon1 icon-ic-3"></span> {{__('Withdraw') }}</a></li>
                                    <li class="account_topbar_menu__item"><a href="{{ route('user.settings', app()->getLocale()) }}"><span class="icon1 icon-ic-4"></span> {{ __('Settings') }}</a></li>
                                    <li class="account_topbar_menu__item"><a href="{{ route('user.transactions', app()->getLocale()) }}"><span class="icon1 icon-ic-5"></span> {{ __('Transactions') }}</a></li>
                                    <li class="account_topbar_menu__item"><a href="{{ route('user.games-history', app()->getLocale()) }}"><span class="icon1 icon-ic-6"></span> {{ __('Games history') }}</a></li>

                                    @if($is_client_messages_allowed)
                                        <li class="account_topbar_menu__item"><a href="{{ route('user.write-us', app()->getLocale()) }}"><img src="{{ asset('/img/icons/icon-mail_min_w.svg') }}" width="20" height="20" style="opacity: 0.5; margin-right: 8px;">{{ __('Write us') }}</a></li>
                                    @endif

                                    <li class="account_topbar_menu__item"><a href="{{ route('logout', app()->getLocale()) }}"><span class="icon1 icon-ic-7"></span> {{ __('Sign out') }}</a></li>
                                </ul>
                            </div>
                        </div>

                        <a href="{{ route('user.deposit', app()->getLocale()) }}"><div class="header_account__pluse"><span></span></div></a>

                    </div>
                @endguest

                @auth

                    <div class="modal-wrap modal_not_available game-not-available-modal">
                        <div id="modal" class="modal modal_1">
                            <div class="modal_1__twosec" style="width: 100%; height: 20vh !important;">
                                <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                                    <p class="p1" style="margin-bottom: 20px;">{{ __('Game is not available in your region') }}</p>
                                    <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close">{{ __('OK') }}</button></p>
                                </div>
                            </div>
                        </div>
                    </div>

                @endauth
            </div>

            <div class="header_sec_3">
                <button class="mobile_menu_btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
        </div>
    </header>


