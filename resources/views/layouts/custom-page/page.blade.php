@section('title', __($title))
@include('layouts.header')
    <section class="content">
        <div class="text-page">
            <div class="container">
                <div class="text-page__container">
                    <aside class="text-page__sidebar">
                        @include('components.custom-page-sidebar')
                        @include('components.support')
                    </aside>

                    <div class="text-page__content">
                        <div class="text_block1" style="color: #fff;">
                            <h1 class="title_3_1">{!! $title !!}</h1>
                            <p style="margin-bottom: 20px;"><i class="fa-solid fa-calendar-days"></i> {{ Carbon\Carbon::parse(@$page->created_at)->format('d/m/Y') }}</p>
                            @if(@$page->thumbnail)
                                <img style="max-width: 800px; margin-bottom: 20px;" src="{{ asset('img/news/'.$page->thumbnail) }}" alt="{{ $title }}">
                            @endif
                            {!! $content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.footer')
