<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Zino Casino') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/admin/app.js') }}" defer></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.js" defer></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css"
          integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" integrity="sha512-Eak/29OTpb36LLo2r47IpVzPBLXnAMPAVypbSZiZ4Qkf8p/7S/XRG5xp7OKWPPYfJT6metI+IORkR5G8F900+g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Icon -->
    <link rel="icon" href="https://cdn2.iconfinder.com/data/icons/drf/PNG/advanced.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/4.1.5/css/flag-icons.min.css" integrity="sha512-UwbBNAFoECXUPeDhlKR3zzWU3j8ddKIQQsDOsKhXQGdiB5i3IHEXr9kXx82+gaHigbNKbTDp3VY/G6gZqva6ZQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
    <div id="app">
        @auth
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm sticky-top">
                <a class="navbar-brand d-flex align-items-center" href="{{ route('admin') }}">
                    <img src="{{ asset('img/icons/icon.png') }}" width="32" class="mr-2" alt="{{ __('Invalid casino site icon!') }}"><span>{{ config('app.name', 'Casino Name') }}</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto" style="font-size: 1.2em;">
                        <!-- Authentication Links -->

                        <li class="nav-item" title="{{ __('Unfortunate withdrawals') }}" data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.unfortunate-withdrawal') }}" class="nav-link"><i class="fab fa-creative-commons-nc"></i> {{ __('Unfortunate withdrawals') }}</a>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" title="{{ __('No bets') }}" data-placement="bottom">
                            <a href="{{ route('admin.no-bets') }}" class="nav-link"><i class="fas fa-gavel"></i> {{ __('No bets') }}</a>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" title="{{ __('Support') }}" data-placement="bottom">
                            <a href="{{ route('admin.support.tickets', ['slug' => 'new']) }}" class="nav-link"><i class="fas fa-headset"></i> {{ __('Support') }}</a>
                        </li>

{{--                        <li class="nav-item dropdown" title="{{ __('Поддержка') }}" data-toggle="tooltip" data-placement="right">--}}
{{--                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                <i class="fas fa-headset"></i> {{ __('Поддержка') }}--}}
{{--                            </a>--}}

{{--                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
{{--                                @if(auth()->user()->role == 2)--}}
{{--                                    <a href="{{ route('admin.support.tickets') }}" class="dropdown-item">{{ __('Все тикеты') }}</a>--}}
{{--                                    <a href="#" class="dropdown-item">{{ __('Новые тикеты') }}</a>--}}
{{--                                    <a href="#" class="dropdown-item">{{ __('Отправленные') }}</a>--}}
{{--                                @endif--}}
{{--                                @if(auth()->user()->role == 3)--}}
{{--                                    <a href="{{ route('admin.support.tickets') }}" class="dropdown-item">{{ __('Все тикеты') }}</a>--}}
{{--                                    <a href="{{ route('admin.support.ticket-categories') }}" class="dropdown-item">{{ __('Категории тикета') }}</a>--}}
{{--                                    <a href="{{ route('admin.support.ticket-templates') }}" class="dropdown-item">{{ __('Шаблоны тикета') }}</a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </li>--}}

                        <li class="nav-item" title="{{ __('Clients') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.users') }}" class="nav-link"><i class="fas fa-user"></i> {{ __('Clients') }}</a>
                        </li>


                        @if(@auth()->user()->role == 3)
                        <li class="nav-item" title="{{ __('Manage user roles') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.admins') }}" class="nav-link"><i class="fas fa-crown"></i> {{ __('Roles') }}</a>
                        </li>
                        @endif

                        <li class="nav-item" title="{{ __('To withdraw') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.withdrawal') }}" class="nav-link"><i class="fas fa-dollar-sign"></i> {{ __('To withdraw') }}</a>
                        </li>

                        <li class="nav-item" title="{{ __('Transactions') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.transactions') }}" class="nav-link"><i class="far fa-credit-card"></i> {{ __('Transactions') }}</a>
                        </li>

                        @if(@auth()->user()->role == 3)
                        <li class="nav-item" title="{{ __('Pages') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.pages') }}" class="nav-link"><i class="far fa-file"></i> {{ __('Pages') }}</a>
                        </li>
                        @endif

                        <li class="nav-item" title="{{ __('Report') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.report') }}" class="nav-link"><i class="fas fa-chart-pie"></i> {{ __('Report') }}</a>
                        </li>

                        <li class="nav-item" title="{{ __('Withdrawals') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.withdrawals') }}" class="nav-link"><i class="fas fa-wallet"></i> {{ __('Withdrawals') }}</a>
                        </li>

                        <li class="nav-item" title="{{ __('Payments') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.payments') }}" class="nav-link"><i class="fas fa-money-bill-wave"></i> {{ __('Payments') }}</a>
                        </li>

                        @if(@auth()->user()->role == 3)
                        <li class="nav-item" title="{{ __('Settings') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('admin.settings') }}" class="nav-link"><i class="fas fa-cog"></i></a>
                        </li>

                            <li class="nav-item" title="{{ __('Translations') }}"  data-toggle="tooltip" data-placement="bottom">
                                <a class="nav-link" href="{{ route('admin.translate') }}"><i class="fa-solid fa-language"></i>
                                </a>
                            </li>
                            <li class="nav-item" title="{{ __('Games') }}"  data-toggle="tooltip" data-placement="bottom">
                                <a class="nav-link" href="{{ route('admin.games') }}"><i class="fas fa-gamepad"></i>
                                </a>
                            </li>

                            <li class="nav-item" title="{{ __('News') }}"  data-toggle="tooltip" data-placement="bottom">
                                <a class="nav-link" href="{{ route('admin.news') }}"><i class="fa-solid fa-newspaper"></i>
                                </a>
                            </li>
                        @endif

                        <li class="nav-item" title="{{ __('Go to casino') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a href="{{ route('index') }}" class="nav-link"><i class="fas fa-home"></i></a>
                        </li>

                        <li class="nav-item" title="{{ __('Logout') }}"  data-toggle="tooltip" data-placement="bottom">
                            <a class="nav-link" style="color: darkred;" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i>
                            </a>
                        </li>


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </ul>
                </div>
        </nav>
        @endauth
        <main class="py-4">
            @if(@auth()->user()->role == 2)
                @php
                    $ref = \App\Models\Ref::where('user_id', auth()->id())->first();
                @endphp
                <div class="container">
                    @if(!$ref)
                        <form action="{{ route('api.generate-ref') }}" method="POST">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                            <input type="hidden" name="ref" class="ref-code">
                            <button class="btn btn-success mb-2"><i class="fas fa-plus mr-1"></i>{{ __('Create referral link') }}</button>
                        </form>
                        <script>
                            document.querySelector('.ref-code').value = (Math.random().toString(36).substr(2, 8))
                        </script>
                    @else
                        <div class="bg-secondary text-white p-3 d-flex align-items-center mb-2" style="border-radius: 3px;">
                            <span class="align-middle">
                                <b>{{ __('Your link:') }}</b> http://casino.test/?ref={{ $ref->ref }}
                            </span>
                        </div>
                    @endif
                </div>
            @endif
            @yield('content')
        </main>
    </div>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        new WOW().init();

    </script>
</body>
</html>
