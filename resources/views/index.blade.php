@section('title', __('Win with us!'))
@include('layouts.header')
    <section class="content">
        @include('components.slider')
        @include('components.jackpot')
        <div class="top_line-bar">
            <div class="container">
                <div class="top_line-bar__wrapp">
                    <div class="top_line-bar__tabbtn_section">
                        <div class="tabbtn_section__addwrp">
                            <button class="tab-btn_01 tab-btn_01__active">{{ __('All games') }}</button>
                            <button class="tab-btn_01">{{ __('Popular') }}</button>
                            <button class="tab-btn_01">{{ __('New') }}</button>
                            <button class="tab-btn_01">{{ __('Slots') }}</button>
                            <button class="tab-btn_01">{{ __('Board') }}</button>
                        </div>
                    </div>
                    @guest
                    <button class="rigistration_btn">{{ __('Sign up in 10 sec') }}</button>
                    @endguest
                </div>
            </div>
        </div>
        <div class="content-home">
            <div class="container">
                <div class="content-home__wrapp">
                    <div class="card_container show-content">
                        @foreach($games as $game)
                            @include('games.game')
                        @endforeach
                    </div>

                    <div class="card_container show-content">
                        @foreach($games as $game)
                            @php
                                $categories = json_decode($game->categories, true);
                            @endphp
                            @if($categories['is_board'])
                                @include('games.game')
                            @endif
                        @endforeach
                    </div>

                    <div class="card_container show-content">
                        @foreach($games as $game)
                            @php
                                $categories = json_decode($game->categories, true);
                            @endphp
                            @if($categories['is_popular'])
                                @include('games.game')
                            @endif
                        @endforeach
                    </div>

                    <div class="card_container show-content">
                        @foreach($games as $game)
                            @php
                                $categories = json_decode($game->categories, true);
                            @endphp
                            @if($categories['is_new'])
                                @include('games.game')
                            @endif
                        @endforeach
                    </div>

                    <div class="card_container hide-content">
                        @foreach($games as $game)
                            @php
                                $categories = json_decode($game->categories, true);
                            @endphp
                            @if($categories['is_slot'])
                                @include('games.game')
                            @endif
                        @endforeach
                    </div>

                    <div class="content-side">
                        @include('components.life-winners')
{{--                        @include('components.tournament')--}}
                    </div>

                </div>

                <h2 style="margin-top: 20px; font-size: 2em; margin-bottom: 20px;" class="title_3">{{ __('Latest') }} <span>{{ __('news') }}</span></h2>
                <div style="display: flex; flex-wrap: wrap;">
                    @foreach($news as $new)
                        @include('components.news-single')
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@include('layouts.footer')
