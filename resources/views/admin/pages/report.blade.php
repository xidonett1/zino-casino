@extends('layouts.app')

@section('title', __('Report'))

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Report') }}</span>
                        <div class="d-flex justify-content-center align-items-center">
                        <form autocomplete="off" class="datepicker-form {{ auth()->user()->role == 3 ? 'mr-2' : '' }}" method="GET" action="">
                            <input class="form-control" type="text" name="daterange" readonly />
                        </form>
                        <form class="d-flex justify-content-center align-items-center mr-2">
                            @if(auth()->user()->role == 3)
                            <select name="manager" class="form-control" onchange="this.form.submit()">
                                <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                @foreach($managers as $manager)
                                    <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                @endforeach
                            </select>
                            @endif

                            <select name="period" class="form-control ml-2" onchange="this.form.submit()">
                                <option
                                    {{ request()->get('period') == 'today' || is_null(request()->get('period'))  ? 'selected' : '' }}
                                    value="today">{{ __('Today') }}</option>
                                <option value="week" {{ (request()->get('period') == 'week') ? 'selected' : '' }}>{{ __('Week') }}</option>
                                <option value="month" {{ (request()->get('period') == 'month') ? 'selected' : '' }}>{{ __('Month') }}</option>
                                <option value="all" {{ (request()->get('period') == 'all') ? 'selected' : '' }}>{{ __('All time') }}</option>
                            </select>
                        </form>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="d-flex flex-wrap justify-content-center align-items-center">
                            <div class="mr-3 mb-3 d-flex justify-content-center align-items-center wow animate__animated animate__fadeIn" data-wow-duration="2s"  style="border: 1px solid #ddd; padding: 35px; width: 300px; height: 200px; border-radius: 20px; background: #fff;">
                                <div>
                                    <p align="center" style="font-size: 2rem;"><i class="fas fa-user"></i></p>
                                    <p align="center" style="font-size: 2rem;"><b>{{ $view_params['users'] }}</b></p>
                                    <p align="center">{{ __('new users') }}</p>
                                </div>
                            </div>
                            <div class="mr-3 mb-3 d-flex justify-content-center align-items-center wow animate__animated animate__fadeIn" data-wow-duration="2s" data-wow-delay="0.3s" style="border: 1px solid #ddd; padding: 35px; width: 300px; height: 200px; border-radius: 20px;">
                                <div>
                                    <p align="center" style="font-size: 2rem;"><i class="fas fa-money-bill-wave"></i></p>
                                    <p align="center" style="font-size: 2rem;"><b>$ {{ number_format($view_params['deposits'], 2, '.', ' ') }}</b></p>
                                    <p align="center">{{ __('deposits') }}</p>
                                </div>
                            </div>
                            <div class="mr-3 mb-3 d-flex justify-content-center align-items-center wow animate__animated animate__fadeIn" data-wow-duration="2s" data-wow-delay="0.6s" style="border: 1px solid #ddd; padding: 35px; width: 400px; height: 200px; border-radius: 20px;">
                                <div>
                                    <p align="center" style="font-size: 2rem;"><i class="far fa-credit-card"></i></p>
                                    <p align="center" style="font-size: 2rem;"><b>$ {{ number_format($view_params['swifts'], 2, '.', ' ') }}</b></p>
                                    <p align="center">{{ __('SWIFT-commissions') }}</p>
                                </div>
                            </div>
                            <div class="mr-3 mb-3 d-flex justify-content-center align-items-center wow animate__animated animate__fadeIn" data-wow-duration="2s" data-wow-delay="0.9s" style="border: 1px solid #ddd; padding: 35px; width: 35%; height: 200px; border-radius: 20px;">
                                <div>
                                    <p align="center" style="font-size: 2rem;"><i class="fas fa-hand-holding-usd"></i></p>
                                    <p align="center" style="font-size: 2rem;"><b>$ {{ number_format($view_params['taxes'], 2, '.', ' ') }}</b></p>
                                    <p align="center">{{ __('winning tax') }}</p>
                                </div>
                            </div>
                            <div class="mr-3 mb-3 d-flex justify-content-center align-items-center wow animate__animated animate__fadeIn" data-wow-duration="2.5s" data-wow-delay="1.2s" style="border: 1px solid #ddd; padding: 35px; width: 60%; height: 200px; border-radius: 20px;">
                                <div>
                                    <p align="center" style="font-size: 2rem;"><i class="fas fa-wallet"></i></p>
                                    <p align="center" style="font-size: 2rem;"><b>$ {{ number_format($view_params['all'], 2, '.', ' ') }}</b></p>
                                    <p align="center">{{ __('in sum') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(function() {
                moment.locale('en');
                $('input[name="daterange"]').daterangepicker({
                    "startDate": "{{ @$_GET['daterange'] ? explode('-', @$_GET['daterange'])[0] : date('d/m/Y') }}",
                    "endDate": "{{ @$_GET['daterange'] ? explode('-', @$_GET['daterange'])[1] : date('d/m/Y') }}",
                    opens: 'center',
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });
            });
        });
        window.onload = function(){
            setTimeout( function(){
                document.querySelector('.applyBtn').onclick = function(){
                    setTimeout(function(){
                        document.querySelector('.datepicker-form').submit();
                    }, 200);
                }
            }, 1000);
        };
    </script>
    @include('admin.components.modals.withdrawal')
    @include('admin.components.modals.transactions')
@endsection
