@extends('layouts.app')

@section('title', __('Clients'))

@section('content')

    <div class="d-flex justify-content-center p-3">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Clients') }}</span>
                        @if(auth()->user()->role === 3)
                        <form class="d-flex justify-content-center align-items-center mr-2">
                            <select name="manager" class="form-control mr-2" onchange="this.form.submit()">
                                <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                @foreach($managers as $manager)
                                    <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                @endforeach
                            </select>
                            @endif
                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#add-user-modal">
                                <i class="fas fa-user-plus"></i>
                            </button>
                        </form>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($users) != 0 ? '' : 'display: none' }}">
                                    <tr>
                                        <td align="center">{{ __('Login') }}</td>
                                        <td align="center">{{ __('Club') }}</td>
                                        <td align="center">{{ __('Real balance') }}</td>
                                        <td align="center">{{ __('All withdrawals') }}</td>
                                        <td align="center">{{ __('Bets') }}</td>
                                        <td align="center">{{ __('Register date') }}</td>
                                        <td align="center">{{ __('Last bet') }}</td>
                                        <td align="center">{{ __('Delete') }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($users as $user)
                                        @php
                                            $balance = \App\Models\UserBalance::where('user_id', $user->id)->first();
                                            $all_withdrawals_sum = \App\Http\Controllers\TransactionController::getAllWithdrawalsSum($user->id);
                                            $bets = \App\Models\Bet::where('user_id', $user->id)->count();
                                            $last_bet = \App\Models\Bet::where('user_id', $user->id)->first();
                                        @endphp
                                        <tr>
                                            <td align="center">
                                                @if($user)
                                                    @include('admin.components.user.actions')
                                                @else
                                                    {{ __('Deleted user') }}
                                                @endif
                                            </td>
                                            <td>{{ config('app.name') }}</td>
                                            <td align="center">$ {{ number_format($balance->real, 2, '.', ' ') }}</td>
                                            <td align="center">$ {{ number_format($all_withdrawals_sum, 2, '.', ' ') }}</td>
                                            <td align="center">{{ $bets }}</td>
                                            <td align="center">{{ $user->created_at }}</td>
                                            <td align="center">{{ $last_bet ? $last_bet->created_at : 'No bets' }}</td>

                                            <td align="center">
                                                <form action="{{ route('admin.post.delete-user') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <p class="mt-3 mb-3 text-center">{{ __('No players/users.') }}</p>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $users->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.components.modals.user.add')

    {{-- Withdrawal modal & JS --}}
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @include('admin.components.modals.withdrawal')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
