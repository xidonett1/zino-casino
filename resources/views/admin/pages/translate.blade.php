@extends('layouts.app')

@section('title', __('Translations'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form action="{{ route('admin.post.save-translation') }}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Translations') }}</span>
                            <button class="btn btn-success" type="submit"><i class="far fa-save"></i></button></div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                                <textarea name="translation" cols="30" rows="20" class="form-control">{{ $en }}</textarea>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
