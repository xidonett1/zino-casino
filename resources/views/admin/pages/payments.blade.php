@extends('layouts.app')

@section('title', __('Payments'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Payments') }}</span>

                        <form class="d-flex justify-content-center align-items-center mr-2">
                            @if(auth()->user()->role == 3)
                                <select name="manager" class="form-control" onchange="this.form.submit()">
                                    <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                    @foreach($managers as $manager)
                                        <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </form>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($payments) != 0 ? '' : 'display: none' }}">
                                <tr>
                                    <td align="center">{{ __('User') }}</td>
                                    <td align="center">{{ __('Payment system') }}</td>
                                    <td align="center">{{ __('Sum') }}</td>
                                    <td align="center">{{ __('Date') }}</td>
                                    <td align="center">{{ __('Payment ID') }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($payments as $payment)
                                    <tr>
                                        @php
                                            @$user = \App\Models\User::where('id', $payment->user_id)->first();
                                            @$user_login = $user->login;

                                            if($payment->status == 0){
                                                $status_name = __('Canceled');
                                            } elseif($payment->status == 1) {
                                                $status_name = __('Pending');
                                            } else {
                                                $status_name = __('Done');
                                            }
                                        @endphp
                                        <td align="center" width="20%">
                                            @if($user)
                                                @include('admin.components.user.actions')
                                            @else
                                                Пользователь удалён
                                            @endif
                                        </td>
                                        <td align="center">{{ $payment->payment_system }}</td>
                                        <td align="center" width="10%">$ {{ number_format($payment->amount, 2, '.', ' ') }}</td>
                                        <td align="center">{{ date('d.m.Y h:i:s', strtotime($payment->created_at)) }}</td>
{{--                                        <td align="center"><a onclick="editTransaction('{{ $user_login ? $user_login : 'ID #'.$payment->user_id.' ['.__('Пользователь удалён').']'  }}', '{{ $payment->id }}')" class="btn btn-warning" data-toggle="modal" data-target="#transactions-modal"><i class="fas fa-pencil-alt"></i></a></td>--}}
                                        <td align="center">{{ $payment->id }}</td>
                                    </tr>
                                @empty
                                    <p class="mt-3 mb-3 text-center">{{ __('No transactions') }}</p>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    @include('admin.components.modals.withdrawal')
    @include('admin.components.modals.transactions')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
