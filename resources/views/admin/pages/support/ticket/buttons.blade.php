<a class="btn btn-info mb-2 mt-2 text-white form-control" href="{{ route('admin.payment-history', ['id' => $user->id]) }}">
    <i class="fas fa-coins mr-1"></i>{{ __('Payment history') }}
</a>
<a class="btn btn-info text-white form-control" href="{{ route('admin.games-history', ['id' => $user->id]) }}">
    <i class="far fa-clock mr-1"></i>{{ __('Games history') }}
</a>
