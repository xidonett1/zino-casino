<ul class="nav nav-tabs mb-3">
    <li class="nav-item">
        <a class="nav-link {{ $slug == 'new' ? 'active' : '' }}" aria-current="page" href="{{ route('admin.support.tickets', ['slug' => 'new']) }}">{{ __('New') }} {{ $new_messages_count ? '('.$new_messages_count.')' : '' }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $slug == 'income' ? 'active' : '' }}" href="{{ route('admin.support.tickets', ['slug' => 'income']) }}">{{ __('Received') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $slug == 'sent' ? 'active' : '' }}" href="{{ route('admin.support.tickets', ['slug' => 'sent']) }}">{{ __('Sent') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $slug == 'all' ? 'active' : '' }}" href="{{ route('admin.support.tickets', ['slug' => 'all']) }}">{{ __('All') }}</a>
    </li>
</ul>
