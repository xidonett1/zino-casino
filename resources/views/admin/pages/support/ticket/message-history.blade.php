

<div style="border: 1px solid lightgrey; background: #f1f1f1; border-radius: 10px; padding: 15px; width: 45vw;" class="mb-5">
    @if($message_history->isNotEmpty())
        @if(isset($sender_login))
            <b>{{ __('Tickets history') }}</b>
            <a href="{{ route('api.delete-all-messages', ['user_id' => $user_id, 'user_login' => $sender_login]) }}" class="btn btn-outline-danger form-control mb-3 mt-3"><i class="fas fa-trash"></i> {{ __('Delete all messages') }}</a>

            <div class="tickets" style="overflow-y: scroll !important; height: 70vh; margin-top: 0;">
                @foreach($message_history as $message)
                    @include('admin.components.user.ticket')
                @endforeach
            </div>
        @else
            <b>{{ __('Deleted user') }}</b>
        @endif
    @else
        <b>{{ __('No messages') }}</b>
    @endif
</div>
