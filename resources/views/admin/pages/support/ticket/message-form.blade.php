<div class="tickets" style="margin-top: 0;">
    @include('admin.components.user.ticket')
</div>
<div style="border: 1px solid lightgrey; background: #f1f1f1; border-radius: 10px; padding: 15px;" class="mb-5">
    <form method="POST" action="{{ route('api.send-message') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="is_admin" value="1">
        <input type="hidden" name="for" value="{{ @$message->sender->login ? $message->sender->login : $user->login }}">
        <input type="hidden" name="is_answered" value="1">
        @if($messages->isNotEmpty())
            <input type="hidden" name="message_id" value="{{ $message->id }}">
        @endif
        <input type="hidden" name="user_id" value="{{ auth()->id() }}">

        <label for="template_category">{{ __('Template categories') }}:</label><br>
        <select name="template_category" class="form-control" id="template_category">
            <option value="all">{{ __('All') }}</option>
        </select>
        <br>
        <label for="template">{{ __('Template') }}:</label>
        <select name="template" class="form-control" id="template">
            <option value="no">{{ __('No') }}</option>
        </select>
        <br>
        <label for="msg">{{ __('Message') }}:</label>
<textarea name="msg" class="form-control" style="white-space: pre;" id="msg" cols="30" rows="10">{{ __('Dear player,') }}



{{ __('With best wishes, ') }}{{ config('app.name') }} {{ __('support') }}.</textarea>
        <br>
        <label for="file">{{ __('File') }}:</label><br>
        <input type="file" name="file" id="file">
        <p align="center" class="mt-3"><button class="btn btn-success"><i class="fas fa-paper-plane mr-1"></i>{{ __('Send') }}</button></p>
    </form>
</div>
