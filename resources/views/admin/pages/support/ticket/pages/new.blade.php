<div>
    <div class="w-100">
        @if($user)
            @include('admin.components.user.actions')
        @else
            {{ __('Deleted user') }}
        @endif
        @include('admin.pages.support.ticket.buttons')
    </div>
    <div class="d-flex justify-content-center mt-3 flex-wrap">
        <div class="mr-3" style="width: 45vw;">
            @include('admin.pages.support.ticket.message-form')
        </div>
        @include('admin.pages.support.ticket.message-history')
    </div>
</div>
