@php

    $user_id = @$messages->first()->user_id;
    $sender_login = @$messages->first()->sender->login;

    if ($user) {
        $user_id = $user->id;
        $sender_login = $user->login;
    }

    $user_messages = \App\Models\Message::whereUserId($user_id)->get();
    $admin_messages = \App\Models\Message::whereIsAdminAndFor(1, $sender_login)->get();
    $message_history = collect($user_messages)->merge($admin_messages)->sortByDesc('created_at');

@endphp

@extends('layouts.app')

@section('title', __('Support tickets'))

@section('content')
    <div class="p-5">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Support tickets') }}</span>
                        <div class="d-flex justify-content-center align-items-center">
                            <form class="d-flex justify-content-center align-items-center mr-2">
                                @if(auth()->user()->role === 3)
                                    <a href="{{ route('admin.support.ticket-categories') }}" class="btn btn-primary w-100 mr-2"><i class="fas fa-cubes"></i> {{ __('Categories') }}</a>
                                    <a href="{{ route('admin.support.ticket-templates') }}" class="btn btn-primary w-100 mr-2"><i class="fas fa-layer-group"></i> {{ __('Templates') }}</a>

                                    <select name="manager" class="form-control" onchange="this.form.submit()">
                                        <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                        @foreach($managers as $manager)
                                            <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('admin.pages.support.ticket.tabs')

                           <div class="tickets">
                            @forelse($messages as $message)
                              @php
                                  $user = $message->sender;
                              @endphp
                                  @if($slug == 'new')
                                      @include('admin.pages.support.ticket.pages.new')
                                  @elseif($slug == 'user')
                                       @include('admin.pages.support.ticket.pages.new')
                                  @else
                                       @include('admin.components.user.ticket')
                                  @endif
                            @empty
                                   @if($slug === 'user')
                                       @php
                                            @$message = $message_history->first();
                                       @endphp
                                       @include('admin.pages.support.ticket.pages.new')
                                   @else
                                        <p class="text-secondary text-center">{{ __('No new messages.') }}</p>
                                   @endif
                            @endforelse
                           </div>

                            @if($slug !== 'user')
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{ $messages->links() }}
                                    </div>
                                </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(function() {
                moment.locale('ru');
                $('input[name="daterange"]').daterangepicker({
                    "startDate": "{{ @$_GET['daterange'] ? explode('-', @$_GET['daterange'])[0] : date('d/m/Y') }}",
                    "endDate": "{{ @$_GET['daterange'] ? explode('-', @$_GET['daterange'])[1] : date('d/m/Y') }}",
                    opens: 'center',
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });
            });
        });
        window.onload = function(){
            setTimeout( function(){
                document.querySelector('.applyBtn').onclick = function(){
                    setTimeout(function(){
                        document.querySelector('.datepicker-form').submit();
                    }, 200);
                }
            }, 1000);
        };
    </script>
    @include('admin.components.modals.withdrawal')
    @include('admin.components.modals.transactions')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
