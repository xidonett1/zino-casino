@extends('layouts.app')
@section('title', __('Ticket templates'))
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Ticket templates') }}</span> <form class="d-flex justify-content-center align-items-center">@csrf
                            <a data-toggle="modal" data-target="#create-ticket-template-modal" class="btn btn-success" type="button"><i class="fas fa-plus"></i></a></form></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($templates) != 0 ? '' : 'display: none' }}">
                                <tr>
                                    <td align="center">ID</td>
                                    <td align="center">{{ __('Title') }}</td>
                                    <td align="center">{{ __('Edit') }}</td>
                                    <td align="center">{{ __('Delete') }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($templates as $template)
                                    <tr>
                                        <td align="center">{{ $template->id }}</td>
                                        <td align="center">{{ $template->name }}</td>
                                        <td align="center"><button data-toggle="modal" data-target="#edit-ticket-template-modal" class="btn btn-warning" onclick="setUpdateModalData({{ $template->id }}, 'template', '{{ $template->name }}')"><i class="fas fa-pencil-alt"></i></button></td>
                                        <td align="center"><a href="{{ route('admin.post.delete-ticket-template-or-category', ['id' => $template->id, 'type' => 'template']) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a></td>
                                    </tr>
                                @empty
                                    <p class="mt-3 mb-3 text-center">{{ __('No templates') }}</p>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $templates->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/ticket-misc.js') }}"></script>
    @include('admin.components.modals.support.ticket-template')
    @include('admin.components.modals.support.edit-ticket-template')
@endsection
