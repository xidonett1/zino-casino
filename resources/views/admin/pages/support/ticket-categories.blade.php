@extends('layouts.app')
@section('title', __('Ticket categories'))
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Ticket categories') }}</span> <form class="d-flex justify-content-center align-items-center">@csrf
                            <a data-toggle="modal" data-target="#create-ticket-category-modal" class="btn btn-success" type="button"><i class="fas fa-plus"></i></a></form></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($categories) != 0 ? '' : 'display: none' }}">
                                <tr>
                                    <td align="center">ID</td>
                                    <td align="center">{{ __('Title') }}</td>
                                    <td align="center">{{ __('Edit') }}</td>
                                    <td align="center">{{ __('Delete') }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($categories as $category)
                                    <tr>
                                        <td align="center">{{ $category->id }}</td>
                                        <td align="center">{{ $category->name }}</td>
                                        <td align="center"><button data-toggle="modal" data-target="#edit-ticket-category-modal" class="btn btn-warning" onclick="setUpdateModalData({{ $category->id }}, 'category', '{{ $category->name }}')"><i class="fas fa-pencil-alt"></i></button></td>
                                        <td align="center"><a href="{{ route('admin.post.delete-ticket-template-or-category', ['id' => $category->id, 'type' => 'category']) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a></td>
                                    </tr>
                                @empty
                                    <p class="mt-3 mb-3 text-center">{{ __('No ticket categories') }}</p>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $categories->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/ticket-misc.js') }}"></script>
    @include('admin.components.modals.support.ticket-category')
    @include('admin.components.modals.support.edit-ticket-category')
@endsection
