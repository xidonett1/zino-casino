@extends('layouts.app')

@section('title', __('Unfortunate withdrawals'))

@section('content')
    <div class="p-3">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Unfortunate withdrawals') }}</span></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ $users->isNotEmpty() ? '' : 'display: none' }}">
                                <tr>
                                    <td align="center">{{ __('Login') }}</td>
                                    <td align="center">{{ __('Credentials') }}</td>
                                    <td align="center">{{ __('Sum') }}</td>
                                    <td align="center">{{ __('Hours from registration (withdrawal try)') }}</td>
                                    <td align="center">{{ __('Request date') }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    @php
                                        $unfortunate_withdrawal = \App\Models\UnfortunateWithdrawal::orderBy('id', 'desc')->where('user_id', $user->id)->first();
                                        $transactions = \App\Models\Transaction::orderBy('id', 'desc')->where('user_id', $user->id)->first();
                                    @endphp
                                    @if($unfortunate_withdrawal)
                                    <tr>
                                        <td align="center" width="18%">
                                            @if($user)
                                                @include('admin.components.user.actions')
                                            @else
                                                {{ __('Deleted user') }}
                                            @endif
                                        </td>
                                        <td align="center">{{ $transactions->credentials }}</td>
                                        <td align="center">$ {{ number_format($transactions->amount, 2, '.', ' ') }}</td>
                                        <td align="center">{{ $unfortunate_withdrawal->time_from_registration }} ч.</td>
                                        <td align="center">{{ date('d.m.Y H:i:s', strtotime($unfortunate_withdrawal->created_at)) }}</td>
                                    </tr>
                                    @endif
                                @empty
                                    <p class="mt-3 mb-3 text-center">{{ __('No unfortunate withdrawals found.') }}</p>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    @include('admin.components.modals.transactions')

    @include('admin.components.modals.user.add')

    {{-- Withdrawal modal & JS --}}
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @include('admin.components.modals.withdrawal')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
