@extends('layouts.app')

@section('title', __('Withdrawals'))

@section('content')
    <div class="p-3">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Withdrawals') }}</span>
                        @if(auth()->user()->role === 3)
                        <form class="d-flex justify-content-center align-items-center mr-2">
                            <select name="manager" class="form-control" onchange="this.form.submit()">
                                <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                @foreach($managers as $manager)
                                    <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                @endforeach
                            </select>
                        </form>
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($users) != 0 ? '' : 'display: none' }}">
                                <tr>
                                    <td align="center">{{ __('Sum of active withdrawal') }}</td>
                                    <td align="center">{{ __('Date') }}</td>
                                    <td align="center">{{ __('Quick actions') }}</td>
                                    <td align="center">{{ __('Payment system') }}</td>
                                    <td align="center">{{ __('Status') }}</td>
                                    <td align="center">{{ __('Payment ID') }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        @php
                                            $transaction = @\App\Models\Transaction::where(['user_id' => $user->id, 'status' => 1, 'type' => 'withdraw'])->first();
                                            if ( isset($transaction->status) ) {
                                                if ($transaction->status == 0) {
                                                    $status_name = __('Canceled');
                                                } elseif ($transaction->status == 1) {
                                                    $status_name = __('Pending');
                                                } else {
                                                    $status_name = __('Done');
                                                }
                                            }
                                        @endphp
                                        <td align="center">$ {{ number_format(@$transaction->amount, 2, '.', ' ') }}</td>
                                        <td align="center">{{ @$transaction->created_at ? @$transaction->created_at : '-' }}</td>
                                        <td align="center">
                                            @if($user)
                                                @include('admin.components.user.actions')
                                            @else
                                                {{ __('Deleted user') }}
                                            @endif
                                        </td>
                                        <td align="center">{{ @$transaction->payment_system ? @$transaction->payment_system : '-'}}</td>
                                        <td align="center">{{ @$status_name ? @$status_name : '-'}}</td>
                                        <td align="center">{{ @$transaction->id ? $transaction->id : '-'}}</td>
                                    </tr>
                                    @php
                                        unset($status_name);
                                    @endphp
                                @empty
                                    <p class="mt-3 mb-3 text-center">{{ __('No users') }}</p>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $users->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    @include('admin.components.modals.transactions')

    {{-- Withdrawal modal & JS --}}
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @include('admin.components.modals.withdrawal')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
