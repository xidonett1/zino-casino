@extends('layouts.app')

@section('title', __('Transactions'))

@section('content')
    <div class="p-3">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Transactions') }}</span>
                        @if(auth()->user()->role === 3)
                        <form class="d-flex justify-content-center align-items-center mr-2">
                            <select name="manager" class="form-control" onchange="this.form.submit()">
                                <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                @foreach($managers as $manager)
                                    <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                @endforeach
                            </select>
                        </form>
                        @endif
                        </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($transactions) != 0 ? '' : 'display: none' }}">
                                <tr>
                                    <td align="center">{{ __('Payment system') }}</td>
                                    <td align="center">{{ __('Date') }}</td>
                                    <td align="center">{{ __('Operation') }}</td>
                                    <td align="center">{{ __('Status') }}</td>
                                    <td align="center">{{ __('Sum') }}</td>
{{--                                    <td align="center">{{ __('Редактировать') }}</td>--}}
                                    <td align="center">{{ __('Client') }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($transactions as $transaction)
                                    <tr>
                                        @php
                                            @$user = \App\Models\User::where('id', $transaction->user_id)->first();
                                            @$user_login = $user->login;

                                            if($transaction->status == 0){
                                                $status_name = __('Canceled');
                                            } elseif($transaction->status == 1) {
                                                $status_name = __('Pending');
                                            } else {
                                                $status_name = __('Done');
                                            }
                                        @endphp

                                        <td align="center">{{ $transaction->payment_system }}</td>
                                        <td align="center">{{ date('d.m.Y h:i:s', strtotime($transaction->created_at)) }}</td>
                                        <td align="center">{{ $transaction->type }}</td>
                                        <td align="center" width="12.5%">{{ $status_name }}</td>
                                        <td align="center" width="10%">$ {{ number_format($transaction->amount, 2, '.', ' ') }}</td>

{{--                                        <td align="center"><a onclick="editTransaction('{{ $user_login ? $user_login : 'ID #'.$transaction->user_id.' ['.__('Пользователь удалён').']'  }}', '{{ $transaction->id }}')" class="btn btn-warning" data-toggle="modal" data-target="#transactions-modal"><i class="fas fa-pencil-alt"></i></a></td>--}}
                                        <td align="center" width="20%">
                                            @if($user)
                                                @include('admin.components.user.actions')
                                            @else
                                                {{ __('Deleted user') }}
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <p class="mt-3 mb-3 text-center">{{ __('No transactions') }}</p>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $transactions->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    @include('admin.components.modals.withdrawal')
    @include('admin.components.modals.transactions')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
