<br>
<p class="text-secondary">{{__('Post name')}} ({{ $lang }})
    <input type="text" name="{{ $lang }}_title" class="form-control" value="{{ @$page ? json_decode($page->title, true)[$lang] : '' }}" required>
</p>
<p class="created-slug text-secondary">

</p>
<input type="hidden" name="{{ $lang }}_content" value="{{ @json_decode(@$page->content, true)[$lang] }}">
<div id="{{ $lang }}_summernote">{!! @$page ? json_decode($page->content, true)[$lang] : '' !!}</div>
<script>
    $('#{{ $lang }}_summernote').summernote({
        placeholder: 'Content ({{ $lang }})',
        tabsize: 2,
        height: 200,
        callbacks: {
            onChange: function(contents, $editable) {
                $('[name="{{ $lang }}_content"]').attr('value', contents);
            }
        }
    });

</script>
