<div class="mr-3 mb-3 d-flex justify-content-center align-items-center" data-wow-duration="2s"  style="border: 1px solid #ddd; padding: 35px; min-width: 300px; min-height: 300px; border-radius: 20px; background: #fff;">
    <div>
        <p class="text-center"><img src="{{ $new->thumbnail ? asset('img/news/'.$new->thumbnail) : asset('img/news/no_image.png') }}" width="200" class="rounded-lg" alt="News single"></p>
        <p class="text-center" style="width: 25ch; overflow: hidden; white-space: nowrap; text-align: center; text-overflow: ellipsis;">{{ json_decode($new->title, true)['en'] }}</p>
        <p class="text-center text-secondary">{{ $new->created_at }}</p>
        <div class="d-flex align-items-center justify-content-center">
            <p style="margin-bottom: 0 !important;" class="mr-2"><a href="{{ route('admin.news.page', $new->id) }}" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a></p>
            <p style="margin-bottom: 0 !important;" class="mr-2"><a href="{{ route('news.single', ['slug' => $new->slug, 'lang' => app()->getLocale()]) }}" class="btn btn-secondary"><i class="fa-solid fa-eye"></i></a></p>
            <form action="{{ route('admin.post.delete-page') }}" method="POST">
                @csrf
                <input type="hidden" name="page_id" value="{{ $new->id }}">
                <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
            </form>
        </div>
    </div>
</div>
