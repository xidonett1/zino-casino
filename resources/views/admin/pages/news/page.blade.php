@extends('layouts.app')

@if(@$page)
    @section('title', __('Edit news post'))
@else
    @section('title', __('Create news post'))
@endif

@section('content')
    <form action="{{ route('admin.post.page') }}" method="POST" enctype="multipart/form-data">
    @csrf
        <input type="hidden" name="slug" value="{{ @$page->slug }}"/>
        @if(@$page)
            <input type="hidden" name="page_id" value="{{ @$page->id }}">
        @endif
        <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap">
                        <span>{{ @$page ? __('Edit news post') : __('Create news post') }}</span> <div class="d-flex justify-content-center align-items-center">
                            <input type="hidden" name="category" value="news">
                            <button class="btn btn-success" type="submit"><i class="fas fa-{{ @$page ? 'save' : 'plus' }}"></i></button></div></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <p class="mb-4 text-secondary">{{ __('Post image') }}
                                <br>
                                    <img width="200" id="preview_thumbnail" src="{{ asset('img/news/') }}/{{ @$page->thumbnail ? @$page->thumbnail : 'no_image.png' }}" alt="{{ @json_decode(@$page->title, true)['en'] }}">
                                <br>
                                <br>
                                <input type="file" oninput="preview_thumbnail.src=window.URL.createObjectURL(this.files[0])" name="thumbnail" accept="image/*"></p>

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="true">
                                        <img src="{{ asset('img/icons/en.png') }}" width="22" height="16" alt="" style="outline: 1px solid #aeaeae; border-radius: 2px;"></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="ir-tab" data-toggle="tab" href="#ir" role="tab" aria-controls="ir" aria-selected="false">
                                        <img src="{{ asset('img/icons/ir.png') }}" width="22" height="16" alt="" style="outline: 1px solid #aeaeae; border-radius: 2px;"></a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="en" role="tabpanel" aria-labelledby="en-tab">
                                    @php
                                        $lang = 'en';
                                    @endphp
                                    @include('admin.pages.news.edit-form')
                                </div>
                                <div class="tab-pane fade" id="ir" role="tabpanel" aria-labelledby="ir-tab">
                                    @php
                                        $lang = 'ir';
                                    @endphp
                                    @include('admin.pages.news.edit-form')
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('[name="en_title"]').on('change', function(){
            let slug = string_to_slug($('[name="en_title"]').val());
           $('[name="slug"]').attr('value', slug);
           if(slug) {
               $('.created-slug').text('Page link: /' + slug);
           } else {
               $('.created-slug').text('');
           }
        });
    </script>

    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>

    @include('admin.components.modals.withdrawal')
@endsection
