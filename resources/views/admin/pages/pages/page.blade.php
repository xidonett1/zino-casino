@extends('layouts.app')

@if(@$page)
    @section('title', __('Edit page'))
@else
    @section('title', __('Create page'))
@endif

@section('content')
    <form action="{{ route('admin.post.page') }}" method="POST">
    @csrf
        <input type="hidden" name="slug" value="{{ @$page->slug }}"/>
        @if(@$page)
            <input type="hidden" name="page_id" value="{{ @$page->id }}">
        @endif
        <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap">
                        <span>{{ @$page ? __('Edit page') : __('Create page') }}</span> <div class="d-flex justify-content-center align-items-center">
                            <select name="category" class="form-control mr-2" required>
                                <option value="information" {{ @$page->category == 'information' ? 'selected' : '' }}
                                    {{ @!isset($page) ? 'selected' : '' }}>{{ __('INFORMATION') }}</option>
                                <option value="safety" {{ @$page->category == 'safety' ? 'selected' : '' }}>{{ __('SAFETY') }}</option>
                                <option value="support" {{ @$page->category == 'support' ? 'selected' : '' }}>{{ __('SUPPORT') }}</option>
                            </select>
                            <button class="btn btn-success" type="submit"><i class="fas fa-{{ @$page ? 'save' : 'plus' }}"></i></button></div></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="true">
                                        <img src="{{ asset('img/icons/en.png') }}" width="22" height="16" alt="" style="outline: 1px solid #aeaeae; border-radius: 2px;"></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="ir-tab" data-toggle="tab" href="#ir" role="tab" aria-controls="ir" aria-selected="false">
                                        <img src="{{ asset('img/icons/ir.png') }}" width="22" height="16" alt="" style="outline: 1px solid #aeaeae; border-radius: 2px;"></a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="en" role="tabpanel" aria-labelledby="en-tab">
                                    @php
                                        $lang = 'en';
                                    @endphp
                                    @include('admin.pages.pages.edit-form')
                                </div>
                                <div class="tab-pane fade" id="ir" role="tabpanel" aria-labelledby="ir-tab">
                                    @php
                                        $lang = 'ir';
                                    @endphp
                                    @include('admin.pages.pages.edit-form')
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('[name="en_title"]').on('change', function(){
            let slug = string_to_slug($('[name="en_title"]').val());
           $('[name="slug"]').attr('value', slug);
           if(slug) {
               $('.created-slug').text('Page link: /' + slug);
           } else {
               $('.created-slug').text('');
           }
        });
    </script>

    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>

    @include('admin.components.modals.withdrawal')
@endsection
