@extends('layouts.app')

@section('title', __('Payment history'))

@section('content')
    <div class="p-5">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Payment history') }}</span>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('admin.components.user.actions')
                        @if($transactions->isNotEmpty())
                            <div class="jumbotron mt-5">
                                <p class="text-secondary">{{ __('Filter') }}</p>
                                <form action="{{ route('admin.payment-history', $transactions[0]->user_id) }}" method="GET" class="d-flex align-items-center">
                                    <select name="filter" class="form-control w-25 mr-2" onchange="this.form.submit()">
                                        <option value="all" {{ request()->get('filter') === 'all' || !request()->has('filter') ? 'selected' : '' }}>{{ __('All') }}</option>
                                        <option value="income" {{ request()->get('filter') === 'income' ? 'selected' : '' }}>{{ __('Income') }}</option>
                                        <option value="outcome" {{ request()->get('filter') === 'outcome' ? 'selected' : '' }}>{{ __('Outcome') }}</option>
                                    </select>
                                    <select name="sort" class="form-control w-25" onchange="this.form.submit()">
                                        <option value="latest" {{ request()->get('sort') === 'latest' ? 'selected' : '' }}>{{ __('New first') }}</option>
                                        <option value="oldest" {{ request()->get('sort') === 'oldest' ? 'selected' : '' }}>{{ __('Old first') }}</option>
                                    </select>
                                </form>
                            <form class="table-responsive mt-5" method="POST" action="{{ route('api.apply-payment-history-actions') }}">
                                @csrf
                                <select name="action" class="form-control mb-3 w-50 payment-history-selected-actions" onchange="this.form.submit()" style="display: none">
                                    <option disabled selected>{{ __('With selected') }}</option>
                                    <option value="accept">{{ __('Accept') }}</option>
                                    <option value="reject">{{ __('Reject') }}</option>
                                </select>
                            </div>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td></td>
                                        <td align="center" class="text-success font-weight-bold">$ {{ number_format($transactions_sum, 2, '.', ', ') }}</td>
                                        <td align="center">{{ __('Status ID') }}</td>
                                        <td align="center">{{ __('Wallet') }}</td>
                                        <td align="center">{{ __('Date') }}</td>
                                        <td align="center">{{ __('Payment ID') }}</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transactions as $transaction)
                                        <tr>
                                            @php
                                                if($transaction->status == 0){
                                                    $status_name = __('Canceled');
                                                } elseif($transaction->status == 1) {
                                                    $status_name = __('Pending');
                                                } else {
                                                    $status_name = __('Done');
                                                }
                                            @endphp
                                            <td align="center"><input type="checkbox" name="transaction[]" class="payment-history-checkbox" value="{{ $transaction->id }}"></td>
                                            <td align="center">$ {{ number_format($transaction->amount, 2, '.', ', ') }}</td>
                                            <td align="center">{{ $status_name }}</td>
                                            <td align="center" class="transaction-credentials" data-change-allowed="{{ strtolower($transaction->credentials) === 'tax' || strtolower($transaction->credentials) === 'swift' ? 0 : 1 }}" data-transaction-id="{{ $transaction->id }}">{{ $transaction->credentials }}</td>
                                            <td align="center">{{ $transaction->created_at }}</td>
                                            <td align="center">{{ $transaction->id }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                                <p class="text-center">
                                    <a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-undo"></i> {{ __('Return back') }}</a>
                                </p>
                        @else
                            <p class="text-center text-secondary mt-3">{{ __('User')." $user->login ".__('has no real bets.') }}</p>
                            <p class="text-center">
                                <a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-undo"></i> {{ __('Return back') }}</a>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    <script src="{{ asset('js/admin/transactions.js') }}"></script>

    @include('admin.components.modals.withdrawal')
    @include('admin.components.modals.transactions')
    @include('admin.components.modals.edit-transaction-credentials')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')

    <script src="{{ asset('js/admin/payment-history.js') }}"></script>
@endsection
