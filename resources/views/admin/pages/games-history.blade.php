@extends('layouts.app')

@section('title', __('История игр'))

@section('content')
    <div class="p-5">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('История игр') }}</span>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('admin.components.user.actions')
                        @if($bets->isNotEmpty())
                            <div class="table-responsive mt-5">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td align="center">{{ __('Сумма ставки') }}</td>
                                        <td align="center">{{ __('Сумма выигрыша') }}</td>
                                        <td align="center">{{ __('Баланс') }}</td>
                                        <td align="center">{{ __('Ставка') }}</td>
                                        <td align="center">{{ __('Тип игры') }}</td>
                                        <td align="center">{{ __('Дата') }}</td>
                                        <td align="center">{{ __('ID ставки') }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($bets as $bet)
                                        <tr>
                                            <td align="center">$ {{ number_format($bet->bet, 2, '.', ', ') }}</td>
                                            <td align="center">$ {{ number_format($bet->result, 2, '.', ', ') }}</td>
                                            <td align="center">$ {{ number_format($bet->balance, 2, '.', ', ') }}</td>
                                            <td align="center">$ {{ number_format($bet->bet, 2, '.', ', ') }}</td>
                                            <td align="center">{{ $bet->game }}</td>
                                            <td align="center">{{ $bet->created_at }}</td>
                                            <td align="center">{{ $bet->round_id }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <p class="text-center text-secondary mt-3">{{ __('У игрока')." $user->login ".__('нет реальных ставок.') }}</p>
                            <p class="text-center">
                                <a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-undo"></i> Назад</a>
                            </p>
                        @endif
                        <div class="d-flex justify-content-center">
                            {{ $bets->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(function() {
                moment.locale('ru');
                $('input[name="daterange"]').daterangepicker({
                    "startDate": "{{ @$_GET['daterange'] ? explode('-', @$_GET['daterange'])[0] : date('d/m/Y') }}",
                    "endDate": "{{ @$_GET['daterange'] ? explode('-', @$_GET['daterange'])[1] : date('d/m/Y') }}",
                    opens: 'center',
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });
            });
        });
        window.onload = function(){
            setTimeout( function(){
                document.querySelector('.applyBtn').onclick = function(){
                    setTimeout(function(){
                        document.querySelector('.datepicker-form').submit();
                    }, 200);
                }
            }, 1000);
        };
    </script>
    @include('admin.components.modals.withdrawal')
    @include('admin.components.modals.transactions')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
