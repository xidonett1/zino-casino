@extends('layouts.app')

@section('title', __('To withdraw'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('To withdraw') }}</span>
                        <div class="d-flex justify-content-center align-items-center">
                            @if(auth()->user()->role === 3)
                            <form class="d-flex justify-content-center align-items-center mr-2">
                                <select name="manager" class="form-control" onchange="this.form.submit()">
                                    <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                    @foreach($managers as $manager)
                                        <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                    @endforeach
                                </select>
                            </form>
                            <form class="d-flex justify-content-center align-items-center">
                                <input name="search" type="text" placeholder="{{ __('Login') }}" class="form-control">
                                <input type="submit" class="btn btn-primary" value="{{ __('Search') }}">
                            </form>
                            @endif
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($users) != 0 ? '' : 'display: none' }}">
                                    <tr>
                                        <td align="center">{{ __('Quick actions') }}</td>
                                        <td align="center">{{ __('E-mail') }}</td>
                                        <td align="center">{{ __('Balance') }}</td>
                                        <td align="center">{{ __('Withdraw') }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $min_to_withdraw_tab_amount = \App\Models\Settings::where('key', 'min_to_withdraw_tab_amount')->first()->value;
                                    @endphp
                                    @forelse($users as $user)
                                        @php
                                            $user_balance = \App\Models\UserBalance::where('user_id', $user->id)->first();
                                        @endphp
                                        @if(@$user_balance->real > $min_to_withdraw_tab_amount)
                                            <tr>
                                                <td align="center">
                                                    @if($user)
                                                        @include('admin.components.user.actions')
                                                    @else
                                                        {{ __('Deleted user') }}
                                                    @endif
                                                </td>
                                                <td align="center"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                                <td align="center">$ {{ number_format($user_balance->real, 2, '.', ' ') }}</td>
                                                <td align="center"><a onclick="setWithdrawalModalData('{{ $user->login }}', '{{ $user->id }}')" class="btn btn-outline-danger" data-toggle="modal" data-target="#withdrawal-modal">$</a></td>
                                            </tr>
                                        @endif
                                    @empty
                                        <p class="mt-3 mb-3 text-center">{{ __('No clients.') }}</p>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-wrap modal_not_available">
        <div id="modal" class="modal modal_1">
            <div class="modal_1__twosec" style="width: 100%; height: 20vh !important;">
                <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                    <p class="p1" style="margin-bottom: 20px;">{{ __('Платёжная система недоступна в вашем регионе') }}</p>
                    <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close">{{ __('Выбрать другую') }}</button></p>
                </div>
            </div>
        </div>
    </div>
    {{-- Withdrawal modal & JS --}}
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @include('admin.components.modals.withdrawal')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
