<div class="mr-3 mb-3 d-flex justify-content-center align-items-center" data-wow-duration="2s"  style="border: 1px solid #ddd; padding: 35px; width: 300px; height: 250px; border-radius: 20px; background: #fff;">
    <div>
        <p><img src="{{ asset('img/games/'.$game->image) }}" width="200" class="rounded-lg" alt="European Roulette"></p>
        <p align="center">{{ $game->title }}</p>
        <p align="center"><a href="{{ route('admin.post.game.delete', $game->id) }}" class="btn btn-danger">{{ __('Delete') }}</a></p>
    </div>
</div>
