@extends('layouts.app')

@section('title', __('Добавить игру'))

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap">
                        <span>{{ __('Добавить игру') }}</span>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="d-flex flex-wrap justify-content-center align-items-center">
                            <form action="{{ route('admin.post.game.create') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input required type="file" name="image" class="mb-2">
                                <p class="d-flex align-items-center"><input type="checkbox" name="is_enabled"><span>{{ __('Вкл./выкл. игру') }}</span></p>
                                <input required type="text" name="title" class="form-control mb-2" placeholder="{{ __('Название') }}">
                                <input required type="text" name="slug" class="form-control mb-2" placeholder="{{ __('european-roulette') }}">
                                <input required type="text" name="integration" class="form-control mb-2" placeholder="{{ __('Ссылка интеграции') }}">
                                <p class="d-flex align-items-center"><input type="checkbox" class="mr-2" name="is_popular"><span class="text-secondary">{{ __('Популярное') }}</span></p>
                                <p class="d-flex align-items-center"><input type="checkbox" class="mr-2" name="is_new"><span class="text-secondary">{{ __('Новые') }}</span></p>
                                <p class="d-flex align-items-center"><input type="checkbox" class="mr-2" name="is_slot"><span class="text-secondary">{{ __('Слоты') }}</span></p>
                                <p class="d-flex align-items-center"><input type="checkbox" class="mr-2" name="is_board"><span class="text-secondary">{{ __('Настольные') }}</span></p>
                                <p class="text-center"><input type="submit" class="btn btn-success" value="{{ __('Добавить') }}"></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
