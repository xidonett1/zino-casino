<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="availability-tab" data-toggle="tab" href="#availability" role="tab" aria-controls="availability" aria-selected="true">
            {{ __('Availability') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="image-tab" data-toggle="tab" href="#image" role="tab" aria-controls="image" aria-selected="false">
            {{ __('Image') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="games-tab" data-toggle="tab" href="#games" role="tab" aria-controls="availability" aria-selected="false">
            {{ __('Games') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="false">
            {{ __('Description & Prizes') }}</a>
    </li>
</ul>
<div class="tab-content p-3" id="myTabContent">
    <div class="tab-pane fade show active" id="availability" role="tabpanel" aria-labelledby="availability-tab">
        @include('admin.pages.games.tournaments.components.availability')
    </div>
    <div class="tab-pane fade" id="image" role="tabpanel" aria-labelledby="image-tab">
        @include('admin.pages.games.tournaments.components.image')
    </div>
    <div class="tab-pane fade" id="games" role="tabpanel" aria-labelledby="games-tab">
        @include('admin.pages.games.tournaments.components.games')
    </div>
    <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description-tab">
        @include('admin.pages.games.tournaments.components.description')
    </div>
</div>
