<script>
    let {{ $lang }}_prizes = [];
    let prizes_{{ $lang }} = $('.tournament-prizes-{{ $lang }}');
    let prizes_count_{{ $lang }} = 0;

    $('#{{ $lang }}_summernote').summernote({
        placeholder: 'Tournament description ({{ $lang }})',
        tabsize: 2,
        height: 200,
        callbacks: {
            onChange: function(contents, $editable) {
                $('[name="{{ $lang }}_description"]').attr('value', contents);
            }
        }
    });


    $('.add-prize-btn-{{ $lang }}').on('click', function(){

        let prizeTitle = $('.prize-title-{{ $lang }}').val();

        if (!{{ $lang }}_prizes.includes(prizeTitle) && prizeTitle !== '') {
            {{ $lang }}_prizes.push(prizeTitle);
            setInputValue('{{ $lang }}_prizes', JSON.stringify({{ $lang }}_prizes));

            prizes_{{ $lang }}.append(`
            <li class="d-flex justify-content-between align-items-center tournament-prize-{{ $lang }}" prize_id="${prizes_count_{{ $lang }} }" prize_lang="{{ $lang }}">
                <span class="tournament-prizes__title">${prizeTitle}</span>
                <a class="btn btn-danger delete-prize-btn-{{ $lang }}" onclick="deletePrize(${prizes_count_{{ $lang }} }, '{{ $lang }}')"><i class="fa fa-times"></i></a>
            </li>
            `);
            prizes_count_{{ $lang }} += 1;
        } else if(prizeTitle === '') {
            alert('Prize cannot be empty!');
        }else {
            alert('This prize exists!');
        }
    });

    function loadPrizesEditData_{{ $lang }}(prizes)
    {
        try {
            JSON.parse(prizes).forEach( prize =>{

                {{ $lang }}_prizes.push(prize);
                prizes_{{ $lang }}.append(`
            <li class="d-flex justify-content-between align-items-center tournament-prize-{{ $lang }}" prize_id="${prizes_count_{{ $lang }} }" prize_lang="{{ $lang }}">
                <span class="tournament-prizes__title">${prize}</span>
                <a class="btn btn-danger delete-prize-btn-{{ $lang }}" onclick="deletePrize(${prizes_count_{{ $lang }} }, '{{ $lang }}')"><i class="fa fa-times"></i></a>
            </li>
            `);
                prizes_count_{{ $lang }}++;
            })
        } catch(e) {
            if (prizes) {
                {{ $lang }}_prizes.push(prizes);
                prizes_{{ $lang }}.append(`
                    <li class="d-flex justify-content-between align-items-center tournament-prize-{{ $lang }}" prize_id="${prizes_count_{{ $lang }} }" prize_lang="{{ $lang }}">
                        <span class="tournament-prizes__title">${prizes}</span>
                        <a class="btn btn-danger delete-prize-btn-{{ $lang }}" onclick="deletePrize(${window.prizes_count_{{ $lang }} }, '{{ $lang }}')"><i class="fa fa-times"></i></a>
                    </li>
                `);
                window.prizes_count_{{ $lang }}++;
            }
        }
        setInputValue('{{ $lang }}_prizes', JSON.stringify({{ $lang }}_prizes));
    }
</script>
@if(@json_decode($tournament->prizes, true)[$lang])
    <script>loadPrizesEditData_{{ $lang }}('{!! @json_decode($tournament->prizes, true)[$lang] !!}')</script>
@endif
