<span>{{ __('Games') }}</span>
<input type="hidden" name="games">
<div>
    <div class="d-flex align-items-center">
        <select name="games-selected" class="form-control mr-2 tournament-select-game">
            @foreach($games as $game)
                <option value="{{ $game->title }} [{{ $game->slug }}]">{{ $game->title }} [{{ $game->slug }}]</option>
            @endforeach
        </select>
        <a class="btn btn-success add-game"><i class="fa fa-plus"></i></a>
    </div>
    <ul class="tournament-games p-3"></ul>
</div>
