<span>{{ __('Disable/enable tournament') }}</span>
<div class="mb-3">
    <div class="d-flex align-items-center">
        <input type="radio" id="on"
               name="is_available" value="1" {{ @$tournament->is_available ? 'checked' : '' }}>
        <label for="on" class="mt-2 ml-1">{{ __('On') }}</label>
    </div>
    <div class="d-flex align-items-center">
        <input type="radio" id="off"
               name="is_available" value="0" {{ @!$tournament->is_available ? 'checked' : '' }}>
        <label for="off" class="mt-2 ml-1">{{ __('Off') }}</label>
    </div>
</div>
