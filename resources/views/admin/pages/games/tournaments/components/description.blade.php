<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="true">
            <img src="{{ asset('img/icons/en.png') }}" width="22" height="16" alt="" style="outline: 1px solid #aeaeae; border-radius: 2px;"></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="ir-tab" data-toggle="tab" href="#ir" role="tab" aria-controls="ir" aria-selected="false">
            <img src="{{ asset('img/icons/ir.png') }}" width="22" height="16" alt="" style="outline: 1px solid #aeaeae; border-radius: 2px;"></a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="en" role="tabpanel" aria-labelledby="en-tab">
        @php
            $lang = 'en';
        @endphp
        @include('admin.pages.games.tournaments.edit-form')
    </div>
    <div class="tab-pane fade" id="ir" role="tabpanel" aria-labelledby="ir-tab">
        @php
            $lang = 'ir';
        @endphp
        @include('admin.pages.games.tournaments.edit-form')
    </div>
</div>
