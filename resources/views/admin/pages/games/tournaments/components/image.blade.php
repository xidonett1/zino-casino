<p class="mb-4 text-secondary">{{ __('Tournament thumbnail') }}
    @if(@$tournament->thumbnail)
        <p><img width="200" id="preview_thumbnail" src="{{ asset('/img/tournaments/'.$tournament->thumbnail) }}" alt="{{ json_decode($tournament->title, true)['en'] }}"></p>
    @else
        <p><img width="200" src="" id="preview_thumbnail"></p>
    @endif
    <br><input type="file" name="thumbnail" accept="image/*" oninput="preview_thumbnail.src=window.URL.createObjectURL(this.files[0])" {{ @$tournament ? '' : 'required' }}></p>
