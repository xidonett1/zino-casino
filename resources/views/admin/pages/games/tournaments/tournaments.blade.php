@extends('layouts.app')

@section('title', __('Tournaments'))

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap">
                        <span>{{ __('Tournaments') }}</span>
                        <a href="{{ route('admin.tournament', 'new') }}" class="btn btn-success"><i class="fas fa-plus"></i></a>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="d-flex flex-wrap justify-content-center align-items-center">
                            @forelse($tournaments as $tournament)
                                @include('admin.pages.games.tournaments.single')
                            @empty
                                <p class="text-center text-secondary">{{ __('Create your first tournament!') }}</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
