<br>
<p class="text-secondary">{{__('Title')}} ({{ $lang }}, {{ __('30 symbols max.') }})
    <input type="text" name="{{ $lang }}_title" class="form-control" maxlength="30" value="{{ @$tournament ? json_decode($tournament->title, true)[$lang] : '' }}" required>
</p>

<div>
    <span class="text-secondary">{{__('Prizes')}} ({{ $lang }}, {{ __('30 symbols max.') }})</span>
    <div class="d-flex align-items-center">
        <input type="text" class="form-control mr-3 prize-title-{{ $lang }}" maxlength="30">
        <a class="btn btn-success add-prize-btn-{{ $lang }}"><i class="fa fa-plus"></i></a>
    </div>
    <ul class="tournament-prizes tournament-prizes-{{ $lang }} mt-3 p-3"></ul>
</div>

<input type="hidden" name="{{ $lang }}_description" value="{{ @json_decode(@$tournament->description, true)[$lang] }}">
<input type="hidden" name="{{ $lang }}_prizes" value="{{ @json_decode(@$tournament->prizes, true)[$lang] }}">
<div id="{{ $lang }}_summernote">{!! @$tournament ? json_decode($tournament->description, true)[$lang] : '' !!}</div>
@include('admin.pages.games.tournaments.components.edit-form-js')

