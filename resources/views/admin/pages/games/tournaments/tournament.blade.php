@extends('layouts.app')

@if(@$tournament)
    @section('title', __('Edit tournament'))
@else
    @section('title', __('Create tournament'))
@endif

@section('content')

    <form action="{{ route('admin.post.tournament') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="slug" value="{{ @$tournament->slug }}"/>
        @if(@$tournament)
            <input type="hidden" name="page_id" value="{{ @$tournament->id }}">
        @endif
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center flex-wrap">
                            <span>{{ @$tournament ? __('Edit tournament') : __('Create tournament') }}</span> <div class="d-flex justify-content-center align-items-center">
                                <input type="hidden" name="category" value="tournament">
                                <input type="hidden" name="mode" value="{{ @$tournament ? 'edit' : 'create' }}">

                                @if(@$tournament->id)
                                    <input type="hidden" name="id" value="{{ @$tournament->id }}">
                                @endif

                                <button class="btn btn-success" type="submit"><i class="fas fa-{{ @$tournament ? 'save' : 'plus' }}"></i></button></div></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @include('admin.pages.games.tournaments.components.main')
                        </div>
    </form>
    </div>
    </div>
    </div>
    </div>


    <script src="{{ asset('js/admin/tournament.js') }}"></script>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @if(@$tournament->games)
        <script>
            loadEditData('{!! @$tournament->games !!}');
        </script>
    @endif

    @include('admin.components.modals.withdrawal')
@endsection
