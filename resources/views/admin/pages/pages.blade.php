@extends('layouts.app')

@section('title', __('Pages'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Pages') }}</span> <form class="d-flex justify-content-center align-items-center">@csrf
                            <select name="category" class="form-control mr-2">
                                <option value="all">{{ __('All') }}</option>
                                <option value="information">{{ __('INFORMATION') }}</option>
                                <option value="safety">{{ __('SAFETY') }}</option>
                                <option value="support">{{ __('SUPPORT') }}</option>
                            </select>
                            <a href="{{ route('admin.pages.page', 'new') }}" class="btn btn-success" type="button"><i class="fas fa-plus"></i></a></form></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($pages) != 0 ? '' : 'display: none' }}">
                                    <tr>
                                        <td align="center">ID</td>
                                        <td align="center">{{ __('Page name') }}</td>
                                        <td align="center">{{ __('Category') }}</td>
                                        <td align="center">{{ __('Visit') }}</td>
                                        <td align="center">{{ __('Change') }}</td>
                                        <td align="center">{{ __('Delete') }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($pages as $page)
                                            <tr>
                                                <td align="center">{{ $page->id }}</td>
                                                <td align="center">{{ json_decode($page->title, true)['en'] }}</td>
                                                <td align="center">{{ $page->category }}</td>
                                                <td align="center"><a href="{{ route('page', ['lang' => 'en' ,'slug' => $page->slug ] ) }}" target="_blank">{{ $page->slug }}</a></td>
                                                <td align="center"><a class="btn btn-warning" href="{{ route('admin.pages.page', $page->id) }}"><i class="fas fa-pencil-alt"></i></a></td>
                                                <td align="center">
                                                    <form action="{{ route('admin.post.delete-page') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="page_id" value="{{ $page->id }}">
                                                        <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                    @empty
                                        <p class="mt-3 mb-3 text-center">{{ __('No pages found') }}</p>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $pages->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @include('admin.components.modals.withdrawal')
@endsection
