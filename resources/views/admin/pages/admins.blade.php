@extends('layouts.app')

@section('title', __('Roles'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Roles') }}</span> <div class="d-flex justify-content-center align-items-center filter-form">
                            <div class="d-flex align-items-center">
                                <form class="mr-3">
                                    <select name="role" class="form-control" onchange="this.form.submit()">
                                        <option value="all" {{ $request->get('role') == 'all' ? 'selected' : ''}}>{{ __('All') }}</option>
                                        <option value="1" {{ $request->get('role') == '1' ? 'selected' : ''}}>{{ __('Player') }}</option>
                                        <option value="2" {{ $request->get('role') == '2' ? 'selected' : ''}}>{{ __('Manager') }}</option>
                                        <option value="3" {{ $request->get('role') == '3' ? 'selected' : ''}}>{{ __('Admin') }}</option>
                                    </select>
                                </form>
                                <form class="d-flex align-items-center">
                                    <input name="search" type="text" placeholder="{{ __('Login') }}" class="form-control" value="{{ @$request->get('search') }}">
                                    <input type="submit" class="btn btn-primary mr-2" value="{{ __('Search') }}">
                                </form>
                            </div>
                        </div></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($users) != 0 ? '' : 'display: none' }}">
                                    <tr>
                                        <td align="center">ID</td>
                                        <td align="center">{{ __('Login') }}</td>
                                        <td align="center">{{ __('Role') }}</td>
                                        <td align="center">{{ __('Quick actions') }}</td>
                                        <td align="center">{{ __('Edit') }}</td>
                                        <td align="center">{{ __('Delete') }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($users as $user)
                                        @php
                                            $user_role_readable = __('Player');
                                            if ($user->role == 3) {
                                                $user_role_readable = __('Admin');
                                            } else if ($user->role == 2) {
                                                $user_role_readable = __('Manager');
                                            }
                                        @endphp
                                        <tr>
                                            <td align="center">{{ $user->id }}</td>
                                            <td align="center">{{ $user->login }}</td>
                                            <td align="center">{{ $user_role_readable }}</td>
                                            <td align="center">
                                                @if($user)
                                                    @include('admin.components.user.actions')
                                                @else
                                                    {{ __('Deleted user') }}
                                                @endif
                                            </td>
                                            <td align="center"><a class="btn btn-warning" data-toggle="modal" data-target="#admins-modal"
                                                onclick="editUserRole('{{ $user->login }}', {{ $user->id }})"><i class="fas fa-pencil-alt"></i></a></td>
                                            <td align="center">
                                                <form action="{{ route('admin.post.delete-user') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <p class="mt-3 mb-3 text-center">{{ __('No players/users.') }}</p>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $users->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/admins.js') }}"></script>
    @include('admin.components.modals.admins')

    {{-- Withdrawal modal & JS --}}
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @include('admin.components.modals.withdrawal')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')
@endsection
