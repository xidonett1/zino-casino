@extends('layouts.app')

@section('title', __('No bets'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('No bets') }}</span>

                        @if(auth()->user()->role === 3)
                        <form class="d-flex justify-content-center align-items-center mr-2">
                            <select name="manager" class="form-control" onchange="this.form.submit()">
                                <option {{ request()->get('manager') ? '' : 'selected' }} value="all">{{ __('All') }}</option>
                                @foreach($managers as $manager)
                                    <option value="{{ $manager->id }}" {{ $manager->id == request()->get('manager') ? 'selected' : '' }}>{{ $manager->login }}</option>
                                @endforeach
                            </select>
                        </form>
                        @endif
                        </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead style="{{ count($no_bets) != 0 ? '' : 'display: none' }}">
                                <tr>
                                    <td align="center">{{ __('Login') }}</td>
                                    <td align="center">{{ __('E-mail') }}</td>
                                    <td align="center">{{ __('Date of last bet') }}</td>
                                    <td align="center">{{ __('Registration date') }}</td>
                                    <td align="center">{{ __('Manager') }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($no_bets as $user)
                                    @php
                                        $user_bet = \App\Models\Bet::where(['user_id' => $user->id])->first();
                                    @endphp
                                    @if(!str_contains($user_bet->created_at, now()->format('Y-m-d')))
                                    <tr>
                                        <td align="center" width="20%">
                                            @if($user)
                                                @include('admin.components.user.actions')
                                            @else
                                                {{ __('Deleted user') }}
                                            @endif
                                        </td>
                                        <td align="center">{{ $user->email }}</td>
                                        <td align="center">{{ $user_bet ? date('d.m.Y h:i:s', strtotime($user_bet->created_at)) : __('No bets') }}</td>
                                        <td align="center">{{ date('d.m.Y h:i:s', strtotime($user->created_at)) }}</td>
                                        <td align="center"> {{ __('No') }} </td>
                                    </tr>
                                    @endif
                                @empty
                                    <p class="mt-3 mb-3 text-center">{{ __('Nothing to show.') }}</p>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">{{ $no_bets->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Withdrawal modal & JS --}}
    <script src="{{ asset('js/admin/withdrawal.js') }}"></script>
    @include('admin.components.modals.withdrawal')

    {{-- Quick action modals & JS --}}
    <script src="{{ asset('js/admin/actions.js') }}"></script>
    @include('admin.components.user.action-modals')

    {{-- Transaction modal & JS --}}
    <script src="{{ asset('js/admin/transactions.js') }}"></script>
    @include('admin.components.modals.transactions')
@endsection
