@extends('layouts.app')

@section('title', __('Games'))

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap">
                        <span>{{ __('Games') }}</span>
                        <div class="d-flex align-items-center">
                            <a href="{{ route('admin.tournaments') }}" class="btn btn-primary mr-2" title="{{ __('Tournaments') }}"><i class="fa-solid fa-trophy"></i></a>
                            <a href="{{ route('admin.games.create') }}" class="btn btn-success"><i class="fas fa-plus"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="d-flex flex-wrap justify-content-center align-items-center">
                            @foreach($games as $game)
                                @include('admin.pages.games.game')
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
