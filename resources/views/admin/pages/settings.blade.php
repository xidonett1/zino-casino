@extends('layouts.app')

@section('title', __('Settings'))

@section('content')
    @php

        $settings = json_decode($settings, true);

    @endphp
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form action="{{ route('admin.post.update-settings') }}" method="POST">
                    @csrf
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center flex-wrap"><span>{{ __('Settings') }}</span>
                        <button class="btn btn-success" type="submit"><i class="far fa-save"></i></button></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <p class="text-secondary">
                            {{ __('The minimum amount to get into the "To withdraw" page') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">$</span>
                                </div>
                                <input type="number" step="0.01" name="min_to_withdraw_tab_amount" class="form-control" placeholder="4000" value="{{ @$settings['min_to_withdraw_tab_amount'] }}" aria-label="Min To Withdraw Tab Amount" required>
                            </div>
                            </p>

                            <p class="text-secondary">
                            {{ __('E-mail of support') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" placeholder="example@gmail.com" name="support_email" class="form-control" value="{{ @$settings['support_email'] }}" aria-label="Support E-mail" required>
                            </div>
                            </p>

                            <p class="text-secondary">
                                {{ __('The minimum amount to withdraw funds from ').config('app.name') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">$</span>
                                </div>
                                <input type="number" step="0.01" name="min_withdrawal_amount" class="form-control" placeholder="1500" value="{{ @$settings['min_withdrawal_amount'] }}" aria-label="Withdrawal Min Amount" required>
                            </div>
                            </p>

                            <p class="text-secondary">{{ __('Cancellation of withdrawal of funds for players in "My Account"') }}
                                <select name="client_withdrawal_cancellation" class="form-control">
                                    <option value="0" {{ @$settings['client_withdrawal_cancellation'] == 0 ? 'selected' : '' }}>{{ __('Players CANNOT cancel withdrawals in the Dashboard') }}</option>
                                    <option value="1" {{ @$settings['client_withdrawal_cancellation'] == 1 ? 'selected' : '' }}>{{ __('Players MAY cancel withdrawals in their Members Area') }}</option>
                                </select>
                            </p>

                            <p class="text-secondary">{{ __('Sending messages from the client') }}
                                <select name="client_messages_allowed" class="form-control">
                                    <option value="0" {{ @$settings['client_messages_allowed'] == 0 ? 'selected' : '' }}>{{ __('Client CANNOT send messages') }}</option>
                                    <option value="1" {{ @$settings['client_messages_allowed'] == 1 ? 'selected' : '' }}>{{ __('The client MAY send messages') }}</option>
                                </select>
                            </p>

                            <p class="text-secondary">{{ __('The time that the user cannot withdraw funds after registration') }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">{{ __('Time in hours (for example: 40)') }}</span>
                                    </div>
                                    <input type="number" step="1" name="min_withdrawal_time" class="form-control" placeholder="40" aria-label="Withdrawal Min Time" required value="{{ @$settings['min_withdrawal_time'] }}">
                                </div>
                            </p>

                            <p class="text-secondary">{{ __('Ability to create referral links for managers') }}
                                <select name="managers_ref_creation_permit" class="form-control">
                                    <option value="0" {{ @$settings['managers_ref_creation_permit'] == 0 ? 'selected' : '' }}>{{ __('Managers CANNOT create referral links') }}</option>
                                    <option value="1" {{ @$settings['managers_ref_creation_permit'] == 1 ? 'selected' : '' }}>{{ __('Managers MAY create referral links') }}</option>
                                </select>
                            </p>

                            <p class="text-secondary">{{ __('Maintenance mode') }}
                                <select name="under_construction_mode" class="form-control">
                                    <option value="1" {{ @$settings['under_construction_mode'] == 1 ? 'selected' : '' }}>{{ __('Enabled') }}</option>
                                    <option value="0" {{ @$settings['under_construction_mode'] == 0 ? 'selected' : '' }}>{{ __('Disabled') }}</option>
                                </select>
                            </p>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
