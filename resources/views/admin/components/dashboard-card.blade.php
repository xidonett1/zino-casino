<a href="{{ route($route_name) }}"
   class="d-flex justify-content-center align-items-center p-5 btn btn-outline-secondary text-decoration-none rounded-lg w-25 m-3"
   style="font-size: 1.25rem; min-width: 300px; min-height: 100px; max-height: 100px !important;">
    {{ __($title) }}
</a>
