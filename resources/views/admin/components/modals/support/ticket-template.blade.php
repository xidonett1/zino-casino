<div class="modal fade" id="create-ticket-template-modal" tabindex="-1" role="dialog" aria-labelledby="ticket template" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Create ticket template') }}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.post.create-ticket-template') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <p class="text-secondary">
                        {{ __('Ticket template name') }}
                        <input type="text" class="form-control" name="name" placeholder="{{ __('Ticket template name') }}">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Create') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
