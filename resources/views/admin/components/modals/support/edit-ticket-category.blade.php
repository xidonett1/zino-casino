<div class="modal fade" id="edit-ticket-category-modal" tabindex="-1" role="dialog" aria-labelledby="edit ticket category" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Edit ticket category') }}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('api.update-ticket-misc') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="" class="ticket-misc-id">
                <input type="hidden" name="type" value="" class="ticket-misc-type">
                <div class="modal-body">
                    <p class="text-secondary">
                        {{ __('Category name') }}
                        <input type="text" class="form-control ticket-misc-name" name="name" placeholder="{{ __('Category name') }}">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
