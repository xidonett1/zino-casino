<div class="modal fade" id="transactions-modal" tabindex="-1" role="dialog" aria-labelledby="transactions" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Edit transaction') }}:</span> <span class="modal-user-login text-secondary"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('api.update-transaction') }}" method="POST">
                @csrf
                <input type="hidden" name="transaction_id" class="modal-transaction-id">
                <div class="modal-body">
                    <p><span class="text-secondary">Платёжная система:</span><br>
                        <select name="payment_system" class="form-control" required>
                            <option value="Perfect Money">Perfect Money</option>
                            <option value="Qiwi">Qiwi</option>
                            <option value="PayPal">PayPal</option>
                        </select>
                    </p>
                    <p><span class="text-secondary">Статус:</span>
                        <select name="status" class="form-control">
                            <option value="0">Отклонён</option>
                            <option value="1">В ожидании</option>
                            <option value="2">Завершён</option>
                        </select>
                    </p>
                    <p>
                        <span class="text-secondary">Сумма:</span>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">$</span>
                            </div>
                            <input type="number" step="0.01" name="amount" class="form-control" placeholder="1500" aria-label="Withdrawal" required>
                        </div>
                    </p>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Подтвердить</button>
            </div>
            </form>
        </div>
    </div>
</div>
