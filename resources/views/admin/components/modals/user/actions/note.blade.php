<div class="modal fade" id="actions-user-note-modal" tabindex="-1" role="dialog" aria-labelledby="actions-user-note-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Note about player ') }}</span> <span class="modal-user-add text-secondary"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('api.user-note-update') }}" class="note" method="POST">
                @csrf
                <input type="hidden" name="user_id" class="modal-user-id">
                <div class="modal-body">
                    <textarea name="note" class="form-control modal-user-note" placeholder="{{ __('Content of note about player (will be visible only for managers and admins).') }}"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
