<div class="modal fade" id="deposit-modal" tabindex="-1" role="dialog" aria-labelledby="deposit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Add funds to player ') }}</span> <span class="deposit-user-login text-secondary"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.post.make-deposit') }}" class="withdrawal" method="POST">
                @csrf
                <input type="hidden" name="type" value="deposit">
                <input type="hidden" name="user_id" class="deposit-user-id">
                <div class="modal-body">
                    <p><span class="text-secondary">{{ __('Payment system') }}</span><br>
                        <select name="payment_system" class="form-control" required>
                            <option value="Perfect Money">Perfect Money</option>
                            <option value="Qiwi">Qiwi</option>
                            <option value="PayPal">PayPal</option>
                        </select>
                    </p>
                    <p>
                        <span class="text-secondary">{{ __('Amount') }}</span>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">$</span>
                        </div>
                        <input type="number" step="0.01" name="amount" class="form-control" placeholder="1500" aria-label="Withdrawal" required>
                    </div>
                    </p>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="checkbox-transaction" name="transaction">
                        <label class="form-check-label" for="checkbox-transaction">
                            {{ __('Create transaction') }}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="checkbox-message" name="checkbox-message">
                        <label class="form-check-label" for="checkbox-message">
                            {{ __('Send message') }}
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Deposit') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
