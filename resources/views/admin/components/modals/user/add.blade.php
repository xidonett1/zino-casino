<div class="modal fade" id="add-user-modal" tabindex="-1" role="dialog" aria-labelledby="add-user" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Add client') }}</span> <span class="modal-user-add text-secondary"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('api.add-user-player') }}" method="POST">
                @csrf
                <input type="hidden" name="user_id" class="modal-user-id">
                <div class="modal-body">
                    <p>
                        <span class="text-secondary">{{ __('Login') }}</span>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="login" required class="form-control" placeholder="{{ __('Login') }}">
                    </div>
                    </p>

                    <input type="hidden" name="email" required class="form-control" value="n/a_{{ uniqid()}}">
                    <input type="hidden" name="password" required class="form-control" value="{{ uniqid()}}">

                    <p>
                        <span class="text-secondary">{{ __('Balance') }}</span>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">$</span>
                        </div>
                        <input type="number" step="0.01" name="balance" class="form-control" placeholder="1500" aria-label="amount" required>
                    </div>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Add') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
