<div class="modal fade" id="admins-modal" tabindex="-1" role="dialog" aria-labelledby="admins" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Change role') }}:</span> <span class="modal-user-login text-secondary"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('api.update-role') }}" method="POST">
                @csrf
                <input type="hidden" name="user_id" class="modal-user-id">
                <div class="modal-body">
                    <p><span class="text-secondary">{{ __('Role') }}:</span><br>
                        <select name="role" class="form-control mr-3">
                            <option value="1">{{ __('Player') }}</option>
                            <option value="2">{{ __('Manager') }}</option>
                            <option value="3">{{ __('Admin') }}</option>
                        </select>
                    </p>
                    <p class="text-secondary d-flex align-items-center">
                        <input type="text" class="form-control disabled w-75" name="ref" readonly placeholder="{{ __('Referral link') }}">
                        <input type="button" class="form-control btn btn-primary w-50 generate-ref" onclick="
                            let ref = (Math.random().toString(36).substr(2, 8))
                            document.querySelector('[name=\'ref\']').value = ref
                            document.querySelector('.ref-code').textContent = ref
                        " value="{{ __('Generate') }}">
                    </p>
                    <p class="text-secondary"><b>{{ __('Full link') }}:</b>
                        {{ route('index', 'ru') }}?ref=<span class="ref-code"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Confirm') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
