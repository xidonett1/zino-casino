<div class="modal fade" id="edit-transaction-credentials-modal" tabindex="-1" role="dialog" aria-labelledby="edit transaction credentials" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>{{ __('Edit credentials') }}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('api.update-transaction') }}" method="POST">
                @csrf
                <input type="hidden" name="transaction_id" class="modal-credentials-id">
                <div class="modal-body">
                    <p class="text-secondary">
                        {{ __('Credentials') }}
                        <input type="text" class="form-control modal-credentials-input" name="credentials" placeholder="{{ __('Client credentials') }}">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
