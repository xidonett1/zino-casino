@if($message_history->isNotEmpty())
@php
    $user_blocked = \App\Models\Blocked::whereUserId($message->user_id)->first('new_password');
@endphp
<div class="ticket">
    <div class="ticket-heading d-flex justify-content-between align-items-center" style="{{ $message->is_admin ? 'background: rgba(255,107,107,0.26)' : '' }}">
        <span class="date">
            {{ __('Sent:') }} {{ $message->created_at }}
        </span>
        @if($user_blocked)
            <span>{{ __('Password:').' '.$user_blocked->new_password }}</span>
        @endif
        @if($slug !== 'new' && $slug !== 'user')
            @include('admin.components.user.actions')
        @endif
        <a href="{{ route('api.delete-message', ['id' => $message->id]) }}" title="{{ __('Delete message') }}" style="font-size: 1.5rem;"><i class="fas fa-times"></i></a>
    </div>
    <div class="ticket-body">
        <p class="ticket-text" style="color: #000; white-space: pre-line;">
            @if($message->is_admin)
                {!! $message->msg !!}
            @else
                {{ $message->msg }}
            @endif
        </p><br>
        @if($message->file)
            <hr>
            <a href="{{ asset('img/tickets/'.$message->file) }}" style="display: block;" target="_blank">{{ __('Open image') }}</a>
        @endif
    </div>
</div>
@endif
