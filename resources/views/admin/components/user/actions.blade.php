@php
    $user_id = $user->id;
    $reg_pass = $user->registration_password;
    $manager = @\App\Models\ManagerClients::where('user_id', $user_id)->first()->manager_id;
    $manager_login = '[No manager]';
    if ($manager) {
        $manager_login = \App\Models\User::find($manager)->login;
    }

    $user_note = \App\Models\UserNote::where('user_id', $user_id)->first();
    $user_blocked = \App\Models\Blocked::where('user_id', $user_id)->first();

    $user_balance = \App\Models\UserBalance::where('user_id', $user_id)->first()->real;
    $user_deposit = \App\Http\Controllers\TransactionController::getAllUserDepositTransactionsSum($user_id);

    $user_location = \App\Models\UserLocation::where('user_id', $user_id)->first();

@endphp
<div class="d-flex justify-content-between align-items-center" style="max-width: 450px; min-width: 450px;">
    <div class="mr-2 d-flex align-items-center justify-content-center">
        @if(isset($user_location) && $user_location->city !== 'N/A' && $user_location->country !== 'N/A' && $user_location->id !== '127.0.0.1')
            <div class="mr-1" title="{{ $user_location->city }}, {{ $user_location->country }} [{{ $user_location->ip }}]" style="border: 1px solid lightgrey; height: 21px; width: 30px; background: url('https://flagcdn.com/h20/{{ strtolower($user_location->country) }}.png')">
            </div>
        @elseif(!isset($user_location) || $user_location->city === 'N/A' && $user_location->country === 'N/A' && $user_location->ip === '127.0.0.1')
            <div class="mr-1" title="{{ __('City, country and IP are unknown.') }}" style="border: 1px solid lightgrey; height: 21px; width: 30px; background: url('https://cdn-icons-png.flaticon.com/16/41/41943.png') center no-repeat;">
            </div>
        @endif
        <span>{{ $reg_pass ? "[$reg_pass]" : ''}} {{ $user->login }} - {{ $manager_login }} | +{{ $user_deposit }} | {{ $user_balance }}</span>
    </div>
    <div class="d-flex justify-content-center align-items-center flex-wrap" style="font-size: 0.9rem;">
        <a href="#" onclick="setUserNoteModalData({{ $user_id }}, '{{ @$user_note->note ? $user_note->note : '' }}')" title="{{ __('Note') }}" data-toggle="modal" data-target="#actions-user-note-modal" class="mr-2">
            <i class="fas fa-edit"></i></a>
        <a href="{{ route('admin.payment-history', ['id' => $user_id]) }}" class="mr-2 text-success" title="{{ __('Payment history') }}"><i class="fas fa-dollar"></i></a>

        <a href="{{ route('admin.games-history', ['id' => $user_id]) }}" class="mr-2" title="{{ __('Bets history') }}"><i class="far fa-calendar"></i></a>
        <a href="{{ route('admin.support.tickets', ['slug' => 'user', 'id' => $user_id]) }}" class="mr-2" title="{{ __('Support') }}"><i class="far fa-message"></i></a>

        <a href="#"  class="mr-2 text-success" data-toggle="modal" data-target="#deposit-modal" title="{{ __('Add balance') }}" onclick="setDepositModalData('{{ $user->login }}', {{ $user->id }})">
            <i class="fas fa-money-bill"></i></a>

        <a href="#" class="mr-2 text-danger" data-toggle="modal" data-target="#withdrawal-modal" onclick="setWithdrawalModalData('{{ $user->login }}', {{ $user->id }})" title="{{ __('Withdraw') }}">
            <i class="fas fa-money-bill"></i></a>

        <a href="{{ route('api.block-user', $user_id) }}" class="mr-2 {{ $user_blocked ? 'text-danger' : '' }}" title="{{ $user_blocked ? __('Unblock') : __('Block') }}">
            <i class="fas {{ $user_blocked ? 'fa-lock' : 'fa-unlock' }}"></i></a>
    </div>
</div>
