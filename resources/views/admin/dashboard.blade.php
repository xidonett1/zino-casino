@extends('layouts.app')

@section('title', __('Админ-панель'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between flex-wrap align-items-center">
                    <span>{{ __('Dashboard') }} | {{ config('app.name') }} &copy;</span>
                    <span class="text-danger">{{ auth()->user()->role == 2 ? 'Manager' : 'Admin' }}</span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="d-flex p-2 flex-wrap align-items-center justify-content-center">
                        @foreach($cards as $title => $route_name)
                            @include('admin.components.dashboard-card')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
