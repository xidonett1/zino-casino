@section('title', __('Withdrawal'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">

            @include('user.components.sidebar')
            <div class="account_main tax-pg">
                <div class="account_main__wrapper">
                    @include('components.winner-tax')
                    <p class="txt-title1">{{ __('Select payment system:') }}</p>
                    <div class="pay_system">
                        <div class="pay_system__wrapper">
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/visa.png" alt="">
                                <span>Visa</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/advcache.png" alt="">
                                <span>AdvCash</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/master.svg" alt="">
                                <span>MasterCard</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/maestro.svg" alt="">
                                <span>Maestro</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/webmoney.svg" alt="">
                                <span>Webmoney</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/umoney.svg" alt="">
                                <span>YandexMoney</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/qiwi.svg" alt="">
                                <span>Qiwi</span>
                            </div>
{{--                            <a href="{{ route('user.withdrawal-request', app()->getLocale()) }}" style="color: #888888 !important;" class="pay_system__item">--}}
{{--                                <img src="/img/paysystem/perfect.png" alt="">--}}
{{--                                <span>PerfectMoney</span>--}}
{{--                            </a>--}}
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/walletone.png" alt="">
                                <span>WalletOne</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/unistream.png" alt="">
                                <span>Юнистрим</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/alfaclick.png" alt="">
                                <span>Альфа-Клик</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/payeer.png" alt="">
                                <span>Payeer</span>
                            </div>
                            <div class="pay_system__item btn_deposit">
                                <img src="/img/paysystem/okpay.png" alt="">
                                <span>OkPay</span>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>


    </section>

<div class="modal-wrap modal_not_available">
    <div id="modal" class="modal modal_1">
        <div class="modal_1__twosec" style="width: 100%; height: 20vh !important;">
            <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                <p class="p1" style="margin-bottom: 20px;">{{ __('Payment system is not available in your region') }}</p>
                <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close">{{ __('Select another') }}</button></p>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
