@section('title', __('Write us'))
@include('layouts.header')
    <section class="account_content">
        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main">
                <form class="account_main__wrapper" method="POST" action="{{ route('api.send-message') }}" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="for" value="{{ $user_manager ? $user_manager : 'xidonett' }}">
                    <input type="hidden" name="user_id" value="{{ auth()->id() }}">

                    <h1 align="center" class="title_05">{{ __('Support') }}</h1><br>
                    <p align="center"><textarea name="msg" cols="60" rows="10" placeholder="{{ __('Text message') }}" required></textarea></p>
                    <br>
                    <p align="center" style="margin-left: 200px;"><input style="display: none" type="file" id="getFile" name="file"></p><br>
                    <div style="display: flex; justify-content: center; margin-bottom: 30px;">
                        <span style="display:inline-block; padding: 15px 30px; border-radius: 5px; color: #000; cursor: pointer; background: #fff;" onclick="document.getElementById('getFile').click()">{{ __('Upload image') }}</span>
                    </div>
                    <p align="center"><button class="rigistration_btn" type="submit">{{ __('Send') }}</button></p>
                    <br>
                    <div class="tickets">
                        @foreach($messages as $message)
                            @include('user.components.ticket')
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
    </section>
@include('layouts.footer')
