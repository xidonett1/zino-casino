@section('title', $game->title)
@include('layouts.header')
<section class="account_content">
    <h1 align="center" class="title_05">{{ __('Game') }} &laquo;@yield('title')&raquo;</h1>
    <br>

    <div style="display: flex; justify-content: center; align-items: center;">
        <iframe class="game-frame" src="{{ $game->integration }}/?id={{ auth()->id() }}&domain={{ config('app.url') }}&is_real={{ $is_real }}&game={{ $slug }}&round_id={{ uniqid() }}" style="width: 90vw; height: 90vh;"></iframe>
    </div>

    <br>
    <br>
    <p class="txt-title1" align="center">{{ $is_real === 'virtual' ? __('You are playing on virtual balance') : __('You are playing on real balance') }}</p>
    <br>

    <div style="display: flex; justify-content: center; align-items: center;">
        <button class="rigistration_btn" style="margin-right: 10px;" onclick="document.location = '{{ route('user.game', ['lang' => app()->getLocale(), 'slug' => $slug, 'is_real' => 'real']) }}'">{{ __('Real') }}</button>
        <button class="rigistration_btn" onclick="document.location = '{{ route('user.game', ['lang' => app()->getLocale(), 'slug' => $slug, 'is_real' => 'virtual']) }}'">{{ __('Virtual') }}</button>
    </div>
    <br><br><br>
</section>
@include('layouts.footer')
