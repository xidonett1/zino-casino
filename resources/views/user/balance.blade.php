@section('title', __('Balance'))
@include('layouts.header')
    <section class="account_content">
        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main">
                <div class="account_main__wrapper">
                    @include('components.winner-tax')

                    <p class="txt-title1">{{ __('Your balance') }}</p>

                    <div class="balance_01">
                        <ul class="balance_01__head">
                            <li><span>{{ __('Real') }}</span></li>
                            <li><span>{{ __('Virtual') }}</span></li>
                        </ul>
                        <ul class="balance_01__body">
                            <li><span>$ {{ number_format($balance->real, 2, '.', ' ') }}</span></li>
                            <li><span>$ {{ number_format($balance->virtual, 2, '.', ' ') }}</span></li>
                        </ul>
                    </div>

{{--                    <p class="txt-title1">{{ __('Ваши бонусы') }}</p>--}}

{{--                    <div class="table_01_wrapp">--}}
{{--                        <div class="table_01">--}}
{{--                            <ul class="table_01__head">--}}
{{--                                <li><span>{{ __('Бонус') }}</span></li>--}}
{{--                                <li><span>{{ __('Сумма бонуса') }}</span></li>--}}
{{--                                <li><span>{{ __('Ставки/Всего') }}</span></li>--}}
{{--                                <li><span>{{ __('Дата') }}</span></li>--}}
{{--                                <li><span>{{ __('Статус') }}</span></li>--}}
{{--                            </ul>--}}
{{--                            <ul class="table_01__row">--}}
{{--                                <li><span>#214322</span></li>--}}
{{--                                <li><span>$2 500.00</span></li>--}}
{{--                                <li><span>21/50</span></li>--}}
{{--                                <li><span>30.09.2021</span></li>--}}
{{--                                <li><span class="status_active">Активен</span></li>--}}
{{--                            </ul>--}}
{{--                            <ul class="table_01__row">--}}
{{--                                <li><span>#214322</span></li>--}}
{{--                                <li><span>$2 500.00</span></li>--}}
{{--                                <li><span>50/50</span></li>--}}
{{--                                <li><span>30.09.2021</span></li>--}}
{{--                                <li><span class="status_used">Использован</span></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
@include('layouts.footer')
