@section('title', __('Оплата налога'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main tax-pg">
                <div class="bank-transfer_pg">
                    <h5 class="bank-transfer_pg__title">Уважаемый игрок! </h5>
                    <p class="bank-transfer_pg__txt">
                        Для зачисления средств на Ваш счет Вам необходимо оплатить налог на выигрыш в размере 13%, в Комиссию по контролю азартных игр. Данная плата регламентируется правительством Мальты в соответствии с правовыми нормами Указа об азартных играх Мальты, распространяющимся на управление и предложение услуг по азартным играм в Интернете, включая, среди прочего, такие услуги, как казино, покер, ставки на результаты спортивных состязаний и бинго.
                    </p>

                    <button class="ppay_tax">Опалатить налог на выигрыш</button>

                </div>



                <div class="account_main__wrapper">
                    <p class="txt-title1">Выберите систему для вывода:</p>

                    <div class="pay_system">
                        <div class="pay_system__wrapper">
                            <div class="pay_system__item">
                                <img src="/img/paysystem/visa.png" alt="">
                                <span>Visa</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/advcache.png" alt="">
                                <span>AdvCash</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/master.svg" alt="">
                                <span>MasterCard</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/maestro.svg" alt="">
                                <span>Maestro</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/webmoney.svg" alt="">
                                <span>Webmoney</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/umoney.svg" alt="">
                                <span>YandexMoney</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/qiwi.svg" alt="">
                                <span>Qiwi</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/perfect.png" alt="">
                                <span>PerfectMoney</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/walletone.png" alt="">
                                <span>WalletOne</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/unistream.png" alt="">
                                <span>Юнистрим</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/alfaclick.png" alt="">
                                <span>Альфа-Клик</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/payeer.png" alt="">
                                <span>Payeer</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/okpay.png" alt="">
                                <span>OkPay</span>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>


    </section>
@include('layouts.footer')
