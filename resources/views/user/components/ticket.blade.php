 <div class="ticket">
        <div class="ticket-heading d-flex justify-content-between" style="{{ $message->is_admin ? 'background: rgba(255,107,107,0.26)' : '' }}">
            <span class="date">
                {{ $message->is_admin ? __('Support service').' '.config('app.name') : __('Your message') }} {{ $message->created_at }}
            </span>
        </div>
        <div class="ticket-body">
            <p class="ticket-text" style="color: #000; white-space: pre-line;">
                @if($message->is_admin)
                    {!! $message->msg !!}
                @else
                    {{ $message->msg }}
                @endif
            </p><br>
            @if($message->file)
                <hr>
                <a href="{{ asset('img/tickets/'.$message->file) }}" style="display: block;" target="_blank">{{ __('Open image') }}</a>
            @endif
        </div>
    </div>
