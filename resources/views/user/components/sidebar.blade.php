@php
    $is_client_messages_allowed = \App\Models\Settings::where('key', 'client_messages_allowed')->first()->value;
@endphp

<aside class="account_sidebar">
    <div class="account_sidebar__wrapper">
        <div class="account_sidebar_menu">

            <div class="account_sidebar_top">
                <div class="account_sidebar_top__avatar-wrapp">
                    <div class="account_sidebar_top__avatar">
                        <img src="{{ auth()->user()->avatar != 0 ? asset('/img/avatars/'.auth()->user()->avatar) : asset('/img/user.png') }}" alt="{{ auth()->user()->login }}">
                    </div>
                    <form method="POST" action="{{ route('api.update-avatar') }}" id="edit-img-form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                        <input type="file" name="img" id="edit-img-form__img" style="display: none;">
                        <span class="btn_edit_img"></span>
                    </form>
                </div>
                <div class="account_sidebar_top__username">
                    <span>{{ auth()->user()->login }}</span>
                </div>
            </div>

            <ul class="account_sidebar_menu__list">
                <div class="account_sidebar_menu__mnuwrapp">
                    <li class="account_sidebar_menu__item"><a href="{{ route('user.balance', app()->getLocale()) }}"><span class="icon1 icon-ic-1"></span> {{ __('Balance') }}</a></li>
                    <li class="account_sidebar_menu__item"><a href="{{ route('user.deposit', app()->getLocale()) }}"><span class="icon1 icon-ic-2"></span> {{ __('Deposit') }}</a></li>
                    <li class="account_sidebar_menu__item"><a href="{{ route('user.withdrawal', app()->getLocale()) }}"><span class="icon1 icon-ic-3"></span> {{ __('Withdraw') }}</a></li>
                    <li class="account_sidebar_menu__item"><a href="{{ route('user.settings', app()->getLocale()) }}"><span class="icon1 icon-ic-4"></span> {{ __('Settings') }}</a></li>
                    <li class="account_sidebar_menu__item"><a href="{{ route('user.transactions', app()->getLocale()) }}"><span class="icon1 icon-ic-5"></span> {{ __('Transactions') }}</a></li>
                    <li class="account_sidebar_menu__item"><a href="{{ route('user.games-history', app()->getLocale()) }}"><span class="icon1 icon-ic-6"></span> {{ __('Games history') }}</a></li>
                    @if($is_client_messages_allowed)
                        <li class="account_sidebar_menu__item"><a href="{{ route('user.write-us', app()->getLocale()) }}"><img src="{{ asset('/img/icons/icon-mail_min_w.svg') }}" width="20" height="20" style="opacity: 0.5;">
                            &nbsp;&nbsp;{{ __('Write us') }}</a></li>
                    @endif
                </div>

                <div class="account_sidebar_menu__logout">
                    <li class="account_sidebar_menu__item"><a href="{{ route('logout') }}"><span class="icon1 icon-ic-7"></span> {{ __('Sign out') }}</a></li>
                </div>
            </ul>
        </div>
    </div>
</aside>

<script>
    document.querySelector('.btn_edit_img').addEventListener('click', editImg);
    function editImg(){
        let formImage = document.querySelector('#edit-img-form__img');
        formImage.click();
        formImage.addEventListener('change', function(){
           document.querySelector('#edit-img-form').submit();
        });
    }
</script>
