@section('title', __('Balance'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main deposit-pg">
                <div class="bank-transfer_pg">
                    <p class="bank-transfer_pg__txt"><strong>Уважаемый игрок!</strong> Средства успешно списаны с Вашего счета и переведены на транзитный счет в банке корреспонденте, для отправки средств на Ваш счет, необходимо оплатить Банковский Перевод.</p>

                    <div class="bank-transfer_pg__wrapper">
                        <div class="bank-transfer_pg__item">
                            <div class="bt-pg-flx">
                                <span class="bank-transfer_pg__prc">$ 110</span>
                                <button class="bank-transfer_pg__pay">Опалатить</button>
                            </div>
                            <p class="bank-transfer_pg__p_3">(зачисление в течении 24 часов)</p>
                        </div>
                        <div class="bank-transfer_pg__item">
                            <div class="bt-pg-flx">
                                <span class="bank-transfer_pg__prc">$ 160</span>
                                <button class="bank-transfer_pg__pay">Опалатить</button>
                            </div>
                            <p class="bank-transfer_pg__p_3">(зачисление в течении 10-15 минут)</p>
                        </div>
                        <div class="bank-transfer_pg__item">
                            <div class="bt-pg-flx">
                                <span class="bank-transfer_pg__prc">или</span>
                                <button class="bank-transfer_pg__cancel">Отменить вывод</button>
                            </div>
                            <p class="bank-transfer_pg__p_3">(средвства вернутся обратно на игровой счет)</p>
                        </div>
                    </div>

                </div>



                <div class="account_main__wrapper">
                    <p class="txt-title1">Выберете платежную систему с помощью которой хотите внести депозит на счет:</p>

                    <div class="pay_system">
                        <div class="pay_system__wrapper">
                            <div class="pay_system__item">
                                <img src="/img/paysystem/visa.png" alt="">
                                <span>Visa</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/advcache.png" alt="">
                                <span>AdvCash</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/master.svg" alt="">
                                <span>MasterCard</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/maestro.svg" alt="">
                                <span>Maestro</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/sber.png" alt="">
                                <span>Сбер Онлайн</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/umoney.svg" alt="">
                                <span>YandexMoney</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/qiwi.svg" alt="">
                                <span>Qiwi</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/perfect.png" alt="">
                                <span>PerfectMoney</span>
                            </div>
                            <div class="pay_system__item">
                                <img src="/img/paysystem/payeer.png" alt="">
                                <span>Payeer</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="bonuses_page__list">

                    <div class="bonuses_page__baner">
                        <img src="/img/banners/bous-banner1.jpg" alt="">
                    </div>

                    <div class="bonuses_page__baner">
                        <img src="/img/banners/bous-banner2.jpg" alt="">
                    </div>

                </div>
            </div>
        </div>


    </section>
@include('layouts.footer')
