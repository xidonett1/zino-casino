@section('title', __('Transactions'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main transaction">
                <div class="account_main__wrapper">
                    @include('components.winner-tax')
                    <div class="table_01_wrapp">
                        <div class="table_01 transaction__table table_01_02">
                            <ul class="table_01__head" style="{{ count($transactions) > 0 ? '' : 'display: none' }}">
                                <li><span>{{ __('Date') }}</span></li>
                                <li><span>{{ __('Sum') }}</span></li>
                                <li><span>{{ __('Payment method') }}</span></li>
                                <li><span>{{ __('Status') }}</span></li>
                                <li><span>ID</span></li>
                            </ul>
                            @forelse($transactions as $transaction)
                            <ul class="table_01__row">
                                <li><span>{{ date('d.m.Y', strtotime($transaction->created_at)) }}</span></li>
                                <li><span>$ {{ number_format($transaction->amount, 2, '.', ' ') }}</span></li>
                                <li><span>{{ $transaction->payment_system == 'TAX' ? __('Winning tax').' (13%)' : $transaction->payment_system }}</span></li>
                                @php
                                if($transaction->status == 0){
                                    $status = 'cancel';
                                    $status_name = __('Rejected');
                                } elseif($transaction->status == 1) {
                                    $status = 'pending';
                                    $status_name = __('Pending');
                                } else {
                                    $status = 'done';
                                    $status_name = __('Done');
                                }
                                @endphp
                                <li><span class="status_{{ $status }}">{{ $status_name }}</span></li>
                                <li><span>{{ $transaction->id }}</span></li>
                            </ul>
                            @empty
                                <p align="center">{{ __('No transactions') }}</p>
                                <br>
                            @endforelse
                        </div>
                    </div>


                </div>

            </div>
        </div>

    </section>

@if($has_pending_transactions)
<div class="modal-wrap modal_not_available modal-active-flex" >
    <div id="modal" class="modal modal_1" style="min-height: 33vh; min-width: 40vw;">
        <div class="modal_1__twosec" style="width: 100%; height: 100vh !important;">
            <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                <p align="center" style="color: #BCBCBC; margin-bottom: 20px;">{{ __('Оплата Международного Банковского перевода') }}</p>
                <p align="center" style="color: #BCBCBC; margin-bottom: 20px;">({{ __('SWIFT payment') }})</p>
                <form action="https://perfectmoney.com/api/step1.asp" method="POST" style="display: flex; justify-content: center; flex-wrap: wrap;">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="payment_system" value="SWIFT">
                    <input type="hidden" name="credentials" value="SWIFT">
                    <div class="select_1_wrapp" bis_skin_checked="1" style="margin-bottom: 20px;">
                        <select class="select_1" name="PAYMENT_AMOUNT" required>
                            <option disabled>{{ __('Amount') }}</option>
                            <option value="160">$ 160.00</option>
                            <option value="110">$ 110.00</option>
                        </select>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="payment_system" value="PerfectMoney">
                    <input type="hidden" name="status" value="1">
                    <input type="hidden" name="type" value="swift">
                    <input type="hidden" name="is_user_request" value="1">
                    <input type="hidden" name="transaction" value="1">
                    <input type="hidden" name="PAYEE_ACCOUNT" value="U31487946">
                    <input type="hidden" name="PAYEE_NAME" value="{{ config('app.name') }}">
                    <input type="hidden" name="PAYMENT_ID" value="ID{{ $transaction_id }}">
                    <input type="hidden" name="PAYMENT_UNITS" value="USD">
                    <input type="hidden" name="STATUS_URL" value="{{ route('admin.post.make-swift') }}">
                    <input type="hidden" name="PAYMENT_URL" value="{{ route('admin.post.make-swift') }}">
                    <input type="hidden" name="PAYMENT_URL_METHOD" value="POST">
                    <input type="hidden" name="NOPAYMENT_URL" value="{{ route('admin.post.make-swift') }}">
                    <input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST">
                    <input type="hidden" name="SUGGESTED_MEMO" value="{{ auth()->id() }}">

                    <div style="display: flex; align-items: center; justify-content: center; {{ !$client_withdrawal_cancellation ? 'flex-basis: 100%;' : '' }}">
                        @if($client_withdrawal_cancellation)
                            <p style="margin-left: 18px;"><button name="cancel" value="true" class="rigistration_btn modal_not_available__close"> {{ __('Cancel') }}</button></p>
                        @endif
                        <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close">{{ __('Pay') }}</button></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@include('layouts.footer')
