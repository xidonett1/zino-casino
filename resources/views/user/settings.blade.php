@section('title', __('Settings'))
@include('layouts.header')
<div class="modal-wrap modal_not_available">
    <div id="modal" class="modal modal_1">
        <div class="modal_1__twosec" style="width: 100%; height: 20vh !important;">
            <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                <p class="p1" align="center" style="margin-bottom: 20px;">{{ __('E-mail message has been sent.') }}</p>
                <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close" onclick="document.location = document.location">{{ __('ОК') }}</button></p>
            </div>
        </div>
    </div>
</div>
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')

            <div class="account_main account-settings">
                <div class="account_main__wrapper">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert" style="margin-bottom: 20px;">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h5 class="account-settings__title">{{ __('Account information') }}</h5>

                    <div class="min-sec3_1 min-login">
                        <span class="min-sec3_1__title">{{ __('Login') }}:</span>
                        <form class="f_flx_0_5 change-login-form" style="display: none; margin-bottom: 10px; margin-top: 10px;">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                            <input class="i_input_0_4" type="text" name="login" placeholder="{{ __('Login') }}" required value="{{ auth()->user()->login }}">
                            <button class="s_submit_1 btn_play" type="button" onclick="function(e) {e.preventDefault()}" required>{{ __('Save') }}</button>
                        </form>
                        <div class="min-sec3_1__it"><p>{{ auth()->user()->login }}</p></div>
                    </div>

                    <div class="min-sec3_1 min-wmail">
                        <span class="min-sec3_1__title">Email:</span>
                        <form class="f_flx_0_5 change-email-form" style="display: none; margin-bottom: 10px; margin-top: 10px;">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                            <input class="i_input_0_4" type="email" name="email" placeholder="Email" required value="{{ auth()->user()->email }}">
                            <button class="s_submit_1 btn_play" type="button" onclick="function(e) {e.preventDefault()}" required>{{ __('Save') }}</button>
                        </form>
                        <div class="min-sec3_1__it"><p>{{ auth()->user()->email }}</p></div>
                    </div>


                    <form class="account-settings__password" method="POST" action="{{ route('auth.change-password') }}">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                        <h5 class="account-settings__title">{{ __('Change password') }}</h5>

                        <div class="f_flx_0_5">
                            <input class="i_input_0_4" type="password" name="old_pass" placeholder="{{ __('Old password') }}" required>
                            <input class="i_input_0_4" id="password" name="new_pass" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? '{{ __('Password should contain min 6 characters') }}' : ''); if(this.checkValidity()) form.new_pass_conf.pattern = this.value;" placeholder="{{ __('New password') }}" required>
                        </div>

                        <div class="f_flx_0_5">
                            <input class="i_input_0_4" type="password" name="new_pass_conf" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? '{{ __('Passwords do not match') }}' : '');" placeholder="{{ __('Confirm password') }}" required>
                            <button class="s_submit_1" id="change-pass-submit" type="submit" required>{{ __('Change password') }}</button>
                        </div>
                    </form>

                    <form class="account-settings__info" action="{{ route('api.update-personal-data') }}" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                        <h5 class="account-settings__title">{{ __('Personal information') }}</h5>

                        <div class="f_flx_0_5">
                            <input class="i_input_0_4" type="text" name="name" maxlength="30" value="{{ @$personal_data->name }}" placeholder="{{ __('Name') }}" required>
                            <input class="i_input_0_4" type="text" name="surname" maxlength="30" value="{{ @$personal_data->surname }}" placeholder="{{ __('Surname') }}" required>
                        </div>

                        <div class="f_flx_0_4">
                            <div class="select_1_wrapp">
                                <select class="select_1" name="birth_day" required>
                                    <option value="0" disabled selected>{{ __('Birth day') }}</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>

                            <div class="select_1_wrapp">
                                <select class="select_1" name="birth_month" required>
                                    <option value="value0" selected disabled>{{ __('Birth month') }}</option>
                                    <option value="1">{{ __('January') }}</option>
                                    <option value="2">{{ __('February') }}</option>
                                    <option value="3">{{ __('March') }}</option>
                                    <option value="4">{{ __('April') }}</option>
                                    <option value="5">{{ __('May') }}</option>
                                    <option value="6">{{ __('June') }}</option>
                                    <option value="7">{{ __('July') }}</option>
                                    <option value="8">{{ __('August') }}</option>
                                    <option value="9">{{ __('September') }}</option>
                                    <option value="10">{{ __('October') }}</option>
                                    <option value="11">{{ __('November') }}</option>
                                    <option value="12">{{ __('December') }}</option>
                                </select>
                            </div>
                            <div class="select_1_wrapp">
                                <select class="select_1" name="birth_year" required>
                                    <option value="0" selected disabled>{{ __('Birth year') }}</option>
                                    @for($i = 1945; $i <= 2001; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>

                        <button class="s_submit_1" type="submit">{{ __('Change personal information') }}</button>
                    </form>

                    <form action="{{ route('api.update-personal-data') }}" method="POST" class="account-settings__contact">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                        <h5 class="account-settings__title">{{ __('Contact information') }}</h5>
                        <div class="f_flx_0_5">
                            <input class="i_input_0_4" required value="{{ @$personal_data->phone }}" name="phone" pattern="(\+|(\+[1-9])?[0-9]*)" onchange="this.setCustomValidity(this.validity.patternMismatch ? '{{ __('Invalid phone number. Format:') }} +777777777' : '');" maxlength="16" placeholder="{{ __('Phone number') }}">
                            <button class="s_submit_1" type="submit">{{ __('Change information') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<script>
    let birthDay = {{ @$personal_data->birth_day ? $personal_data->birth_day : 0 }};
    let birthMonth = {{ @$personal_data->birth_month ? $personal_data->birth_month : 0 }};
    let birthYear = {{ @$personal_data->birth_year ? $personal_data->birth_year : 0}};

    if(birthDay){
        document.querySelector(`select[name="birth_day"] option[value="${birthDay}"]`).selected = true;
        document.querySelector(`select[name="birth_month"] option[value="${birthMonth}"]`).selected = true;
        document.querySelector(`select[name="birth_year"] option[value="${birthYear}"]`).selected = true;
    }

    document.querySelector('.change-login-btn').addEventListener('click', function () {
        let loginForm = document.querySelector('.change-login-form')
        if (loginForm.style.display === 'none') {
            loginForm.style.display = 'flex'
        } else {
            loginForm.style.display = 'none'
        }
    })

    document.querySelector('.change-email-btn').addEventListener('click', function () {
        let emailForm = document.querySelector('.change-email-form')
        if (emailForm.style.display === 'none') {
            emailForm.style.display = 'flex'
        } else {
            emailForm.style.display = 'none'
        }
    })
</script>
@include('layouts.footer')
