@section('title', __('Games history'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main games_history">


                <div class="games_history__top">
                    <button class="games_history__tab_btn games_history__tab_btn_active real-history">{{ __('Real') }}</button>
                    <button class="games_history__tab_btn virtual-history">{{ __('Virtual') }}</button>
                </div>

                <div class="account_main__wrapper">
                    <div class="table_01_wrapp">
                        <!-- Real History Table -->
                        <div class="table_01 table_01_02 games_history__tab_contnent real-history-table content-show">
                            <ul class="table_01__head">
                                <li><span>{{ __('Round ID') }}</span></li>
                                <li><span>{{ __('Game') }}</span></li>
                                <li><span>{{ __('Bet') }}</span></li>
                                <li><span>{{ __('Win') }}</span></li>
                                <li><span>{{ __('Date') }}</span></li>
                                <li><span>{{ __('Balance') }}</span></li>
                            </ul>
                            @forelse($real as $bet)
                            <ul class="table_01__row">
                                <li><span>{{ $bet->round_id }}</span></li>
                                <li><span class="str_min">{{ $bet->game }}</span></li>
                                <li><span>$ {{ number_format($bet->bet, 2, '.', ', ') }}</span></li>
                                <li><span class="status_{{ $bet->result >= 0 ? 'increased' : 'decrase' }}">$ {{ number_format($bet->result, 2, '.', ', ')}}</span></li>
                                <li><span>{{ $bet->created_at->format('m/d/Y') }}</span></li>
                                <li><span>$ {{ number_format($bet->balance, 2, '.', ', ') }}</span></li>
                            </ul>
                            @empty
                                <br>
                                <p align="center">{{ __('No bets on real balance.') }}</p>
                                <br>
                            @endforelse
                        </div>

                        <!-- Virtual History Table -->
                        <div class="table_01 table_01_02 games_history__tab_contnent virtual-history-table content-hide">
                            <ul class="table_01__head">
                                <li><span>{{ __('Round ID') }}</span></li>
                                <li><span>{{ __('Game') }}</span></li>
                                <li><span>{{ __('Bet') }}</span></li>
                                <li><span>{{ __('Win') }}</span></li>
                                <li><span>{{ __('Date') }}</span></li>
                                <li><span>{{ __('Balance') }}</span></li>
                            </ul>
                            @forelse($virtual as $bet)
                                <ul class="table_01__row">
                                    <li><span>{{ $bet->round_id }}</span></li>
                                    <li><span class="str_min">{{ $bet->game }}</span></li>
                                    <li><span>$ {{ number_format($bet->bet, 2, '.', ', ') }}</span></li>
                                    <li><span class="status_{{ $bet->result >= 0 ? 'increased' : 'decrase' }}">$ {{ number_format($bet->result, 2, '.', ', ')}}</span></li>
                                    <li><span>{{ $bet->created_at->format('m/d/Y') }}</span></li>
                                    <li><span>$ {{ number_format($bet->balance, 2, '.', ', ') }}</span></li>
                                </ul>
                            @empty
                                <br>
                                <p align="center">{{ __('No bets on virtual balance.') }}</p>
                                <br>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script src="{{ asset('js/user/games-history.js') }}"></script>
@include('layouts.footer')
