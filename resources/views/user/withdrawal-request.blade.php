@section('title', __('Запрос на вывод'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main">
                <div class="account_main__wrapper">
                    <form class="form_req_withdraw" method="POST" action="{{ route('api.withdraw') }}">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <input type="hidden" name="payment_system" value="PerfectMoney">
                        <input type="hidden" name="status" value="1">
                        <input type="hidden" name="type" value="withdraw">

                        <p class="txt-title1">{{ __('Система для вывода:') }}</p>
                        <div class="form_req_withdraw__paymeth perfect">
                            <img src="{{ asset('img/paysystem/perfect.png') }}" alt="">
                            <span>PerfectMoney</span>
                        </div>

                        <p class="input_0_1__wrapp"><input class="input_0_1" name="credentials" type="text" placeholder="{{ __('Номер кошелька') }}" pattern="[a-zA-Z0-9-]+" title="{{ __('Разрешены только латинские символы и цифры.') }}" required></p>
                        <p class="input_0_1__wrapp"><input class="input_0_1" name="amount" type="number" step="0.01" placeholder="{{ __('Сумма') }}" min="{{ $min_withdrawal_amount }}" max="20000" required></p>

                        <p class="txt_8_9">{{ __('Минимальная сумма') }} $ {{ number_format($min_withdrawal_amount, 2, '.', ' ') }} </p>
                        <p class="txt_8_9">{{ __('Максимальная сумма') }} $ 20000.00</p>

                        <button class="form_req_withdraw__submit" type="submit" required>{{ __('Вывести') }}</button>
                    </form>


                </div>

            </div>
        </div>


    </section>
<div class="modal-wrap modal_not_available {{ session('status') == 'success' ? 'modal-active-flex' : '' }}" >
    <div id="modal" class="modal modal_1" style="min-height: 35vh;">
        <div class="modal_1__twosec" style="width: 100%; height: 100vh !important;">
            <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                <p align="center" style="color: #BCBCBC; margin-bottom: 20px;">{{ __('Уважаемый игрок!') }}</p>
                <p align="center" style="color: #BCBCBC; margin-bottom: 20px;">{{ __('Средства успешно списаны с Вашего счета и переведены на транзитный счет в банке корреспонденте. Для отправки средств на Ваш счет, необходимо оплатить Международный Банковский перевод 110 или 160 долларов.') }}</p>
                <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close" onclick="window.location = '{{route('user.transactions', app()->getLocale())}}'">{{ __('Транзакции') }}</button></p>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
