@section('title', __('Deposit'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main deposit-pg">
                <div class="account_main__wrapper">
                    @include('components.winner-tax')
                    @if($self_created)
                        <p class="txt-title1">{{ __('Dear player! You can deposit sum from') }} $1</p><br>
                    @endif
                    <p class="txt-title1">{{ __('Please, select amount:') }}</p>

                    <div class="pay_system">
                        <div class="pay_system__wrapper">
                            <a href="https://widget.okipays.com/checkout/155d1cf60-9cedded2-fbc27a46-46de87ce-7f7de291-3b1e5a1b-bf148a6d-f483e19" target="_blank" class="pay_system__item deposit-amount">
                                <span class="deposit-amount__text">$0.01</span>
                            </a>
                            <a href="https://widget.okipays.com/checkout/0ef962215-cc055786-d5163552-38a80dac-c204ecf9-b160d0a2-52190bf5-c0cc370" target="_blank" class="pay_system__item deposit-amount">
                                <span class="deposit-amount__text">$1</span>
                            </a>
                            <a href="https://widget.okipays.com/checkout/18d37c950-a3e810d9-b9a84c72-c230ca16-b7cec19f-7fb55c62-5e544179-0d448ef" target="_blank" class="pay_system__item deposit-amount">
                                <span class="deposit-amount__text">$5</span>
                            </a>
                            <a href="https://widget.okipays.com/checkout/050a010ce-24d08960-56e9a36a-1940738d-38f469d6-44b3682c-fcc47569-739c525" target="_blank" class="pay_system__item deposit-amount">
                                <span class="deposit-amount__text">$10</span>
                            </a>
                            <a href="https://widget.okipays.com/checkout/e3f695978-1c353c20-1d378e02-d9da5326-01673e08-a1706fa1-5a5ebbd9-ea1bd36" target="_blank" class="pay_system__item deposit-amount">
                                <span class="deposit-amount__text">$25</span>
                            </a>
                            <a href="https://widget.okipays.com/checkout/2dfe70c43-208f52b9-ef4ea7e1-34705283-94711649-1e81fbac-05f0aedc-25c5956" target="_blank" class="pay_system__item deposit-amount">
                                <span class="deposit-amount__text">$50</span>
                            </a>


{{--                            <a href="@if(@$_GET['ref'] == 'vip') {{ route('user.deposit-request', ['lang' => app()->getLocale(), 'ref' => 'vip']) }}  @else {{ route('user.deposit-request', app()->getLocale()) }} @endif" style="color: #888888 !important;" class="pay_system__item">--}}
{{--                                <img src="/img/paysystem/perfect.png" alt="">--}}
{{--                                <span>PerfectMoney</span>--}}
{{--                            </a>--}}
                        </div>
                    </div>


                </div>

                <div class="bonuses_page__list">

                    <div class="bonuses_page__baner">
                        <img src="/img/banners/bous-banner1.jpg" alt="">
                    </div>

                    <div class="bonuses_page__baner">
                        <img src="/img/banners/bous-banner2.jpg" alt="">
                    </div>

                </div>
            </div>
        </div>

    </section>
<div class="modal-wrap modal_not_available payment-not-available-modal">
    <div id="modal" class="modal modal_1">
        <div class="modal_1__twosec" style="width: 100%; height: 20vh !important;">
            <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                <p class="p1" style="margin-bottom: 20px;">{{ __('Payment system is not available in your region') }}</p>
                <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close">{{ __('Select another') }}</button></p>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
