@section('title', __('Deposit request'))
@include('layouts.header')
    <section class="account_content">

        <div class="container account_content__wrapper">
            @include('user.components.sidebar')
            <div class="account_main">
                <div class="account_main__wrapper">
                    <form class="form_req_withdraw" action="https://perfectmoney.com/api/step1.asp" method="POST">
                        @csrf
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <input type="hidden" name="payment_system" value="PerfectMoney">
                        <input type="hidden" name="status" value="1">
                        <input type="hidden" name="type" value="deposit">
                        <input type="hidden" name="is_user_request" value="1">
                        <input type="hidden" name="transaction" value="1">
                        <input type="hidden" name="PAYEE_ACCOUNT" value="U31487946">
                        <input type="hidden" name="PAYEE_NAME" value="{{ config('app.name') }}">
                        <input type="hidden" name="PAYMENT_ID" value="ID{{ $transaction_id }}">
                        <input type="hidden" name="PAYMENT_UNITS" value="USD">
                        <input type="hidden" name="STATUS_URL" value="{{ route('admin.post.make-deposit') }}">
                        <input type="hidden" name="PAYMENT_URL" value="{{ route('admin.post.make-deposit') }}">
                        <input type="hidden" name="PAYMENT_URL_METHOD" value="POST">
                        <input type="hidden" name="NOPAYMENT_URL" value="{{ route('admin.post.make-deposit') }}">
                        <input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST">
                        <input type="hidden" name="SUGGESTED_MEMO" value="{{ auth()->id() }}">

                        <p class="txt-title1">{{ __('Payment system:') }}</p>

                        <div class="form_req_withdraw__paymeth perfect">
                             <img src="{{ asset('img/paysystem/perfect.png') }}" alt="">
                             <span>OkiPays</span>
                        </div>

                        <p class="input_0_1__wrapp"><input class="input_0_1" name="PAYER_ACCOUNT" type="text" placeholder="{{ __('Credentials') }}" pattern="[a-zA-Z0-9-]+" title="{{ __('Only characters and numbers allowed.') }}" required></p>
                        <p class="input_0_1__wrapp"><input class="input_0_1" name="PAYMENT_AMOUNT" type="number" step="0.01" placeholder="{{ __('Sum') }}" min="250" max="20000" @if(@$_GET['ref'] == 'vip') value="1000" disabled @endif required></p>
                        <button type="submit" class="form_req_withdraw__submit" name="PAYMENT_METHOD">{{ __('Deposit') }}</button>
                    </form>

                </div>

            </div>
        </div>


    </section>
<div class="modal-wrap modal_not_available {{ session('status') == 'success' ? 'modal-active-flex' : '' }}" >
    <div id="modal" class="modal modal_1" style="min-height: 35vh;">
        <div class="modal_1__twosec" style="width: 100%; height: 100vh !important;">
            <div style="display: flex; align-content: center; justify-content: center; flex-direction: column; align-items: center;">
                <p align="center" style="color: #BCBCBC; margin-bottom: 20px;">{{ __('Dear player!') }}</p>
                <p style="margin-left: 18px;"><button class="rigistration_btn modal_not_available__close" onclick="window.location = '{{route('user.transactions', app()->getLocale())}}'">{{ __('Транзакции') }}</button></p>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
