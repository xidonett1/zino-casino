@section('title', __('Lotteries'))
@include('layouts.header')
<section class="content casino_page">
    <div class="content-home">
        <div class="container">
            @include('components.slider')
            <div class="content-home__wrapp">
                <div class="content-side">
                    @include('components.life-winners')
                    @include('components.support')
                </div>
                <div class="cazinopage_w">
                    <div class="top_line-bar">
                        <div class="top_line-bar__wrapp">
                            <div class="top_line-bar__tabbtn_section">
                                <div class="tabbtn_section__addwrp">
                                    <button class="tab-btn_01 tab-btn_01__active">{{ __('Traditional') }}</button>
                                    <button class="tab-btn_01">{{ __('Instant') }}</button>
                                    <button class="tab-btn_01">{{ __('My lotteries') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card_container show-content">
                            @include('components.lotteries.traditional-lottery')
{{--                            <p class="text-white text-center font-1em mt-2em">{{ __('There are no traditional lotteries') }}</p>--}}
                        </div>
                        <div class="card_container show-content">
                            @include('components.lotteries.traditional-lottery')
{{--                            <p class="text-white text-center font-1em mt-2em">{{ __('There are no instant lotteries') }}</p>--}}
                        </div>
                        <div class="card_container show-content">
                            @include('components.lotteries.traditional-lottery')
{{--                            <p class="text-white text-center font-1em mt-2em">{{ __("You don't participate in any lottery") }}</p>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('layouts.footer')
