@section('title', __('Tournaments'))
@include('layouts.header')
<section class="content casino_page">
    @include('components.jackpot')
    <div class="content-home">
        <div class="container">
            <div class="content-home__wrapp">
                <div class="content-side">
                    @include('components.life-winners')
                    <div class="sideb-propose1">
{{--                        @php--}}
{{--                            $game = $games[random_int(0, count($games)-1)];--}}
{{--                        @endphp--}}
{{--                        @include('games.game')--}}
                    </div>

                    @include('components.support')
                </div>

                <div class="cazinopage_w">
                    <div class="top_line-bar">
                        <div class="top_line-bar__wrapp">
                            <div class="top_line-bar__tabbtn_section">
                                <div class="tabbtn_section__addwrp">
                                    <button class="tab-btn_01 tab-btn_01__active">{{ __('Current') }}</button>
                                    <button class="tab-btn_01">{{ __('Participating') }}</button>
                                    <button class="tab-btn_01">{{ __('Past') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card_container show-content">
                            @forelse($active_tournaments as $tournament)
                                @include('components.featured-tournament')
                            @empty
                                <p class="text-white text-center font-1em mt-2em">{{ __('There are no current tournaments') }}</p>
                            @endforelse
                        </div>
                        <div class="card_container show-content">
                            @forelse($participating_tournaments as $tournament)
                                @include('components.featured-tournament')
                            @empty
                                <p class="text-white text-center font-1em mt-2em">{{ __('You are not participating in any tournament') }}</p>
                            @endforelse
                        </div>
                        <div class="card_container show-content">
                            @forelse($inactive_tournaments as $tournament)
                                @include('components.featured-tournament')
                            @empty
                                <p class="text-white text-center font-1em mt-2em">{{ __('There are no past tournaments') }}</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('layouts.footer')
