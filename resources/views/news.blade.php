@section('title', __('News'))
@include('layouts.header')
<section class="content casino_page">
    @include('components.jackpot')
    <div class="content-home">
        <div class="container">
            <div class="content-home__wrapp">
                <div class="content-side">
                    @include('components.life-winners')
{{--                    @include('components.tournament')--}}
                    @include('components.support')
                </div>

                <div class="cazinopage_w">
                    <div>
                        <div class="card_container show-content">
                            @foreach($news as $new)
                                @include('components.news-single')
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('layouts.footer')
