$(document).ready(function () {
    // deposit page
    $('.pay_system__item').click(function () {
        $('.pay_system__item').removeClass('pay_system__item__active');
        $(this).toggleClass('pay_system__item__active');
    });




    // Tabs games_history
    let btnBox = document.querySelector('.games_history__top'),
        btn = document.querySelectorAll('.games_history__tab_btn'),
        tabContent = document.querySelectorAll('.games_history__tab_contnent');

    function hideTabContent(a) {
        for (let i = a; i < tabContent.length; i++) {
            tabContent[i].classList.remove('content-show');
            tabContent[i].classList.add('content-hide');
        }
    }
    hideTabContent(1);

    function showTabContent(b) {
        if (tabContent[b].classList.contains('content-hide')) {
            tabContent[b].classList.remove('content-hide');
            tabContent[b].classList.add('content-show');
        }
    }
    if (btnBox) {
        btnBox.addEventListener('click', function (event) {
            let target = event.target;

            if (target && target.classList.contains('games_history__tab_btn')) {
                for (let i = 0; i < tabContent.length; i++) {
                    if (target == btn[i]) {
                        hideTabContent(0);
                        showTabContent(i);
                        break;
                    }
                }
            }
        });
    }
    $('.games_history__tab_btn').click(function () {
        $('.games_history__tab_btn').removeClass('games_history__tab_btn_active');
        $(this).toggleClass('games_history__tab_btn_active');
    });
    // Tabs END


    
    window.onresize = function (e) {
        if( window.screen.width <= 768 ){
            Array.prototype.forEach.call(document.querySelectorAll('.str_min'), function(a){
                var text = a.innerHTML;
                if(text.length > 6){
                    a.innerHTML = text.slice(0, 6) + '...'
                }
            });
        } else {
            
        } 
    }




});