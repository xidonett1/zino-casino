$(document).ready(function () {

    // Slider
    $('.main-slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $('.btn-prev'),
        nextArrow: $('.btn-next'),
    });
    // Slider END


    // Tabs
    let btnBox = document.querySelector('.tabbtn_section__addwrp'),
        btn = document.querySelectorAll('.tab-btn_01'),
        tabContent = document.querySelectorAll('.card_container');

    function hideTabContent(a) {
        for (let i = a; i < tabContent.length; i++) {
            tabContent[i].classList.remove('show-content');
            tabContent[i].classList.add('hide-content');
        }
    }
    hideTabContent(1);

    function showTabContent(b) {
        if (tabContent[b].classList.contains('hide-content')) {
            tabContent[b].classList.remove('hide-content');
            tabContent[b].classList.add('show-content');
        }
    }
    if (btnBox) {
        btnBox.addEventListener('click', function (event) {
            let target = event.target;

            if (target && target.classList.contains('tab-btn_01')) {
                for (let i = 0; i < tabContent.length; i++) {
                    if (target == btn[i]) {
                        hideTabContent(0);
                        showTabContent(i);
                        break;
                    }
                }
            }
        });
    }
    // Tabs END

    $('.tab-btn_01').click(function () {
        $('.tab-btn_01').removeClass('tab-btn_01__active');
        $(this).toggleClass('tab-btn_01__active');
    });


    $('.rigistration_btn').click(function () {
        $('.modal_registration').toggleClass('modal-active');
    });
    $('.m_login_btn').click(function () {
        $('.modal_login').toggleClass('modal-active');
    });
    $('.modal_close').click(function () {
        $('.modal_registration').removeClass('modal-active');
        $('.modal_login').removeClass('modal-active');
    });
    $('.modal-wrap').click(function () {
        $('.modal_registration').removeClass('modal-active');
        $('.modal_login').removeClass('modal-active');
    });

    $('.modal').click(function () {
        event.stopPropagation();
    });

    $('.modal_not_available__close').click(function () {
        $('.modal_not_available').removeClass('modal-active-flex');
    });

    $('.acclogin').click(function () {
        $('.modal_registration').removeClass('modal-active');
        $('.modal_login').toggleClass('modal-active');
    });

    $('.accregistr').click(function () {
        $('.modal_login').removeClass('modal-active');
        $('.modal_registration').toggleClass('modal-active');
    });

    $('.btn_play').each(function(){
        $(this).click(function(){
            if ($('.modal_not_available').length) {
                $('.modal_not_available').toggleClass('modal-active-flex');
            } else {
                $('.modal_login').toggleClass('modal-active');
            }
        });
    });

    $('.btn_deposit').each(function(){
        if($(this).attr('system-allowed') !== 'true') {
            $(this).click(function () {
                $('.modal_not_available').toggleClass('modal-active-flex');
            });
        }
    });

    $('.pay_tax').each(function(){
        $(this).click(function () {
            $('.modal_not_available').toggleClass('modal-active-flex');
        });
    });
});
