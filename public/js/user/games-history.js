let virtualHistoryBtn = $('.virtual-history');
let virtualHistoryTable = $('.virtual-history-table');

let realHistoryBtn = $('.real-history');
let realHistoryTable = $('.real-history-table');

function showVirtualHistoryTable()
{
    virtualHistoryTable.addClass('content-show');
    virtualHistoryTable.removeClass('content-hide');
}

function hideVirtualHistoryTable()
{
    virtualHistoryTable.removeClass('content-show');
    virtualHistoryTable.addClass('content-hide');
}

function showRealHistoryTable()
{
    realHistoryTable.addClass('content-show');
    realHistoryTable.removeClass('content-hide');
}

function hideRealHistoryTable()
{
    realHistoryTable.removeClass('content-show');
    realHistoryTable.addClass('content-hide');
}

function makeRealHistoryBtnActive()
{
    virtualHistoryBtn.removeClass('games_history__tab_btn_active');
    realHistoryBtn.addClass('games_history__tab_btn_active');
}

function makeVirtualHistoryBtnActive()
{
    realHistoryBtn.removeClass('games_history__tab_btn_active');
    virtualHistoryBtn.addClass('games_history__tab_btn_active');
}

realHistoryBtn.on('click', function(){
    makeRealHistoryBtnActive();
    showRealHistoryTable();
    hideVirtualHistoryTable();
});
virtualHistoryBtn.on('click', function(){
    makeVirtualHistoryBtnActive();
    showVirtualHistoryTable();
    hideRealHistoryTable();
});
