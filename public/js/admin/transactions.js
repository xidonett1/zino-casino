function editTransaction(userLogin, transactionID)
{
    document.querySelector('#transactions-modal .modal-user-login').textContent = userLogin
    document.querySelector('#transactions-modal .modal-transaction-id').value = transactionID

    axios.post('/api/get-transaction', {
        id: transactionID
    })
    .then(function (response) {
        let data = response.data;
        document.querySelector(`#transactions-modal select[name="payment_system"] option[value="${data.payment_system}"]`).selected = true
        document.querySelector(`#transactions-modal select[name="status"] option[value="${data.status}"]`).selected = true
        document.querySelector(`#transactions-modal input[name="amount"]`).value = data.amount
    })
    .catch(function (error) {
        console.log(error);
    });
}
