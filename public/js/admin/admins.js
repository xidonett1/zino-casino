function editUserRole(userLogin, userID)
{
    document.querySelector('#admins-modal .modal-user-login').textContent = userLogin
    document.querySelector('#admins-modal .modal-user-id').value = userID

    axios.post('/api/get-user', {
        id: userID
    })
    .then(function (response) {
        let data = response.data;
        console.table(data)
        document.querySelector(`#admins-modal select[name="role"] option[value="${data.user.role}"]`).selected = true
        if (data.ref) {
            document.querySelector('[name="ref"]').value = data.ref.ref;
            document.querySelector('.ref-code').textContent = data.ref.ref;
            document.querySelector('.generate-ref').disabled = true;
            document.querySelector('.generate-ref').title = 'Реферальная ссылка уже сгенерирована';
        } else {
            document.querySelector('[name="ref"]').value = '';
            document.querySelector('.ref-code').textContent = '';
            document.querySelector('.generate-ref').disabled = false;
            document.querySelector('.generate-ref').title = '';
        }
    })
    .catch(function (error) {
        console.log(error);
    });
}
