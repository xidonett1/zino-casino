$('.transaction-credentials').each( function(index) {
    $(this).on('click', function() {
        let isChangeAllowed = parseInt($(this).attr('data-change-allowed'));
        if (isChangeAllowed) {
            let credentials = $(this).text();
            let transactionId = $(this).attr('data-transaction-id');

            $('.modal-credentials-id').val(transactionId);
            $('.modal-credentials-input').val(credentials);

            $('#edit-transaction-credentials-modal').modal('toggle');
        }
    });
});

$('.payment-history-checkbox').on('click', function () {
    let checkedItems = '';

    $('.payment-history-checkbox').each( function () {
        if ( $(this).is(':checked') ) {
           checkedItems += '+';
        } else {
            checkedItems += '-';
        }
    });

    if ( checkedItems.includes('+') ) {
        $('.payment-history-selected-actions').css('display', 'flex');
    } else {
        $('.payment-history-selected-actions').css('display', 'none');
    }
});
