games = $('.tournament-games');
selectedGames = [];
gamesCount = 0;

$('[name="en_title"]').on('change', function(){
    let slug = string_to_slug($('[name="en_title"]').val());
    $('[name="slug"]').attr('value', slug);
    if(slug) {
        $('.created-slug').text('Page link: /' + slug);
    } else {
        $('.created-slug').text('');
    }
});

function setInputValue(inputNameAttr, value)
{
    $(`input[name="${inputNameAttr}"]`).attr('value', value);
}

function deletePrize(id, lang)
{
    let prizeTitle = $(`.tournament-prize-${lang}[prize_id=${id}] span`).html();
    $(`.tournament-prize-${lang}[prize_id=${id}]`).remove();

    let prizesArrayUpdated = [];
    if (lang === 'en') {
        prizesArrayUpdated = en_prizes = en_prizes.filter(e => e !== prizeTitle);
    }
    else if(lang === 'ir') {
        prizesArrayUpdated = ir_prizes = ir_prizes.filter(e => e !== prizeTitle);
    }

    $(`input[name="${lang}_prizes"]`).attr('value', JSON.stringify(prizesArrayUpdated));
}

function deleteGame(id, slug)
{
    $(`.game[game_id=${id}]`).remove();
    selectedGames.splice(selectedGames.indexOf(slug), 1);
    setInputValue('games', JSON.stringify(selectedGames));
}

function loadGames(games)
{
    try {
        JSON.parse(games).forEach( game => {
            window.selectedGames.push(game);
            $('.tournament-games').append(`
            <li class="d-flex justify-content-between align-items-center game" game_id="${window.gamesCount}">
                <span class="tournament-prizes__title">${game}</span>
                <a class="btn btn-danger delete-game-btn" onclick="deleteGame(${window.gamesCount}, '${game}')"><i class="fa fa-times"></i></a>
            </li>
            `);
            window.gamesCount++;
        })
    } catch(e) {
        if (games) {
            window.selectedGames.push(games);
            $('.tournament-games').append(`
            <li class="d-flex justify-content-between align-items-center game" game_id="0">
                <span class="tournament-prizes__title">${games}</span>
                <a class="btn btn-danger delete-game-btn" onclick="deleteGame(${window.gamesCount}, '${games}')"><i class="fa fa-times"></i></a>
            </li>
            `);
            window.gamesCount++;
        }
        console.error(e.message);
    }
    setInputValue('games', JSON.stringify(window.selectedGames));
}

function loadEditData(games)
{
    loadGames(games);
}

$('.add-game').on('click', function(){
    let selectedGameValue = $('.tournament-select-game').val();
    let selectedGame = $(`option[value="${selectedGameValue}"]`).html();
    if (!window.selectedGames.includes(selectedGame)) {
        selectedGames.push(selectedGameValue);
        setInputValue('games', JSON.stringify(selectedGames));

        games.append(`
            <li class="d-flex justify-content-between align-items-center game" game_id="${gamesCount}">
                <span class="tournament-prizes__title">${selectedGame}</span>
                <a class="btn btn-danger delete-game-btn" onclick="deleteGame(${gamesCount}, '${selectedGame}')"><i class="fa fa-times"></i></a>
            </li>
            `);
        window.gamesCount += 1;
    } else {
        alert('Game is already in the list!');
    }
});
