function setUserNoteModalData(userID, note)
{
    document.querySelector('.note .modal-user-id').value = userID;
    document.querySelector('.note .modal-user-note').innerHTML = note;
}

function setDepositModalData(userLogin, userID)
{
    document.querySelector('.deposit-user-login').textContent = userLogin
    document.querySelector('.deposit-user-id').value = userID
}
